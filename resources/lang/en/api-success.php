<?php

return [

    101 => 'successfully loggedin!!',

    102 => 'Mail sent successfully',

    103 => 'Your account deleted.',

    104 => 'Your password changed.',

    106 => 'Logged out',

    105 => 'The card added to your account.',

    108 => 'The card changed to default.',

    109 => 'The card deleted',

    110 => 'The card marked as default.',

    111 => 'The profile updated',

    112 => 'The billing account added',

    113 => 'The billing account deleted',

    114 => 'The document uploaded and waiting for approval',

    115 => 'The uploaded document deleted',

    116 => 'The uploaded documents are deleted',

    117 => 'The amount added to wallet',

    118 => 'Your payment done successfully.',

    119 => 'The auto renewal is enabled.',

    120 => 'The auto renewal is paused.',

    121 => 'The product created.',

    122 => 'The product updated.',

    123 => "The product deleted.",

    124 => "The product approved.",
    
    125 => "The product declined.",

    126 => "The product is available now..!!",

    127 => "The product is marked as out of stock..!!",

    128 => "You are now following :other_key now.",
    
    129 => "Unfollowed.",

    132 => "The product image deleted.",

    133 => "The product image updated.",

    137 => 'The default account changed',

    138 => 'Sent money successfully.',

    139 => 'The request is cancelled.',

    140 => "Payment done.",

    141 => 'Added comment',

    142 => 'The comment removed',

    143 => 'Added to bookmarks',

    147 => 'The verification sent to your email address',

    148 => 'Thanks for the email verification.',

    151 => 'File uploaded',

    152 => 'File deleted',

    153 => 'Password Changed Successfully',

    154 => 'Removed from bookmarks',

    155 => 'Added to blocklist',

    156 => 'Removed from the blocklist',

    157 => 'Your report submitted.',

    158 => 'Removed from the reported list',

    159 => 'Badge Verify Enabled',

    160 => 'Badge Verify Disabled',

    161 => 'Valid Username',

    162 => 'Redirection Success',

    163 => 'Comment Updated',

    164 => 'Added comment reply',

    165 => 'The comment reply removed',

    166 => 'Edited comment reply',

    200 => 'Your payment done successfully.',

    201 => 'The broadcast stopped',

    202 => 'The snapshot updated.',

    203 => 'The viewer updated.',

    204 => 'The broadcast started successfully!',

    205 => 'The broadcasts erased successfully!',

    217 => "Cart Created Successfully",

    218 => "Cart Updated Successfully",

    219 => 'Referral Code Is Valid',

    220 => "Cart Removed Successfully",

    3000 => 'The chat deleted',

    232 => 'PromoCode Updated Successfully...!',

    233 => 'PromoCode Created Successfully...!',

    234 => 'PromoCode Deleted Successfully...!',

    235 => 'You are now following the content creator',

    236 => 'Login Session Created',

    237 => 'Login Session Updated',

    238 => 'Two Step Authentication Successfully Updated!!',

    239 => 'User Category Updated',

    240 => 'Thanks for the rating.',

    241 => 'Review Updated',

    242 => 'Live Streaming Created Successfully',

    243 => 'Order Placed Successfully',

    244 => 'Order Details Updated Successfully',

    245 => "Demo Control Updated Successfully",

    246 => "Is Demo Enabled",

    247 => "Added to Cart",

    248 => "Cart Updated Successfully",

    //Demo login success messages

    801 => 'Admin demo login credentials are already present',

    802 => 'Admin demo login credentials created successfully',

    803 => 'Admin Password has been Updated Successfully',

    804 => 'User Password has been Updated Successfully',

    805 => 'User demo login credentials are already present',

    806 => 'User demo login credentials created successfully',

    807 => 'Admin demo login credentials updated successfully',

    808 => 'Setting details are updated successfully',

    // whatnotclone 

    300 => "Delivery address updated successfully",
    
    301 => "Delivery address deleted successfully",

    302 => "Delivery address changed to default.",

    303 => "Order status updated successfully.",

    304 => "Order cancelled successfully.",

    305 => "You are now following :other_key.",

    306 => "Category Unfollowed",

    307 => "SubCategory Unfollowed",

    308 => "Forget password request verified successfully.",

    309 => "Agreed to rules successfully.",

    310 => "Document uploaded successfully.",

    311 => "Document is waiting for approval.",

    312 => "Document is approved.",

    313 => "Document is declined.",

    314 => "Billing account saved successfully. You are a seller now!",

    315 => "You are a seller now!",

    316 => "Amount :other_key added to your wallet successfully.",
];
