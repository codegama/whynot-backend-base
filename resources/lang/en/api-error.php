<?php

return [

    // Authentication
    1000 => 'Your account is waiting for admin approval',

    1001 => 'Please verify your email address!!',

    1002 => 'You are not a registered user!!',

    1003 => 'Token Expired',

    1004 => 'Invalid Token ',

    1005 => 'Authentication parameters are invalid.!',

    1006 => 'The record not exists.',

    1007 => 'Your account is waiting for admin approval.',

    101 => 'Invalid Input',

    102 => 'Sorry, the username or password you entered do not match. Please try again',

    103 => 'Oops! something went wrong. We couldn’t save your changes. Please try again.',

    104 => 'Invalid Email Address',

    105 => 'The mail send process is failed!!!',

    106 => 'The mail configuration failed!!!',

    107 => 'The payment configuration is failed',

    108 => 'Sorry, the password is not matched.',

    109 => 'Update the payment mode in account and try again!!!',

    111 => 'Add card and try again.',

    113 => 'Payment Failed!!',

    114 => 'Failed to Add Card! Try after sometime!',

    115 => 'The forgot password only available for manual login.',

    116 => 'The email verification not yet done Please check you inbox.',

    117 => 'The requested email is disabled by admin.',

    118 => 'The change password only available for manual login.',

    119 => 'Account delete failed',

    120 => 'The card record is not found.',

    121 => 'The payment configuration Failed',

    122 => 'The card update failed.',

    123 => 'Registeration failed!!',

    124 => 'You do not have an account, please register and continue',

    127 => 'You have already logged into this account using another device, please logout and try again',

    128 => 'Details update failed!!',

    131 => 'You registered as normal user.',

    132 => 'The product details updating failed!!',

    133 => 'The selected product details not found.',

    134 => "The product deletion failed!!",

    135 => "The user is not found",

    136 => "You can\'t follow yourself.",

    137 => "Already you following this user",

    138 => "The follow restricted for the viewer",

    140 => 'The withdraw request is not found',

    141 => 'The withdraw cancel is not allowed',

    142 => 'Your wallet balance is low.',

    143 => 'The requested amount is less than the min balance(:other_key)',

    145 => 'Already paid',

    147 => 'Your wallet balance is low.',

    148 => 'The withdraw request is not found',

    149 => 'The withdraw cancel is not allowed',

    150 => 'The wallet payment record not exits',

    151 => 'The comment is not found',

    152 => 'The bookmark is not found',

    156 => 'Your account is not yet verified.',

    157 => 'Please verify your email and continue',

    158 => 'Your documents are waiting for admin approval',

    159 => 'Your document verification declined. Please check and reupload',

    160 => 'Upload the documents for verification',

    161 => 'Add bank account details',

    162 => 'Already chat has been saved between the users',

    163 =>  'Invalid Token',

    165 =>  'You have already blocked the User',

    166 =>  'Badge Verification Failed',

    167 =>  'Incorrect password',

    168 => 'You have already blocked the user.please unblock and add to favorites',

    170 => 'Requested amount should be less than wallet balance',

    181 => 'That username already taken. Please try another',

    3000 =>  'The Chat info not found',

    182 =>  'You can\'t block the User',

    183 => 'Referral Code is Invalid',

    200 => 'You already have an ongoing live broadcast.Please try again.',

    201 => 'The live video record not found.',

    202 => 'The live streaming is stopped already',

    203 => 'The live streaming is stopped',

    204 => 'The configuration Failed',

    206 => 'Request should be greater than current time',

    207 => "You can\'t request yourself.",

    215 => "Payment not done by the user",

    216 => "Already Ended the Call",

    217 => 'Not allowed for this call',

    218 => 'Not a premium user, upgrade your account to premium .',

    221 => "Cart Details Not Found",

    222 => "Something went wrong !!",

    223 => 'Referral Code is Invalid',

    224 => 'Your wallet balance is low. You cant withdraw Referral Balance',

    227 => 'Empty File, Please Select the files',

    233 => 'No Transaction Found...!',

    234 => 'Content Creator Not Found',

    235 => 'Payment ID Not Found...!',

    236 => 'PromoCode Already Exist...!',

    237 => 'Invalid PromoCode ID...!',

    238 => 'PromoCode Not Exist...!',

    239 => "Live Call payment already done!!",

    240 => "Please Check your Registered email For two step Authentication Code",

    241 => 'Sorry, the email or verification code you entered do not match. Please try again',

    242 => "Two Step Authentication is not Enabled...!!",

    243 => "Out of Stock...!",

    244 => 'The user is not eligible for review now.',

    245 => 'The review already done.',

    246 => 'Order Details Not Found',

    247 => 'Address Deatils Not Found',

    248 => 'Invalid verification code. Please check and try again',

    249 => 'Insufficient Amount. Please check your wallet balance and try again',

    250 => 'Schedule time should be greater than current time',

    251 => "Cannnot add products from diffrent models",

    252 => "You cannot buy your own product",

    253 => "Product quantity is less",



    //demo api messages

    601 => "Demo credetials are incorrect, please run 'demo update' API if it doesn't work configure Settings table with correct details...",

    602 => "Setting key not found",

    603 => "Setting failed to save",

    605 => "Demo login password is incorrect, please run 'admin demo update' API!...",

    300 => "Order is already shipped. You cant cancel now",

    301 => "Failed to update status, Try again after sometime!!",

    302 =>  'The category not found',

    303 =>  'The subcategory not found',

    304 => "Please add delivery address.",

    305 => 'Verification code expired',

    306 => "Billing account details not found.",

    307 => 'The selected billing account is already your default.',

    308 => 'Sorry, We couldn’t save your changes. Please check your biling account details and try again.',

    309 => "You are already a seller.",

    310 => "User details not found.",

    311 => "The selected step already completed.",

    312 => "The selected step is not valid.",

    313 => "Document upload failed. Please try again.",

    314 => "Billing account save failed. Please try again.",

    315 => "Please complete the previous steps to continue.",

    316 => "Your documents already verified.",

    317 => "Stripe keys not configured.",

    318 => 'Payment already completed. (Payment Id - :other_key)',

    319 => 'Payment is not yet completed.',

    320 => 'Payment save failed. Please try again.',
];
