  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
		 <div class="ulogo">
			 <a href="">
			  <!-- logo for regular state and mobile devices -->
			  <span><b> </b>{{Auth::guard('admin')->user()->name}}</span>
			</a>
		</div>
        <div class="image">
          <img src="{{Auth::guard('admin')->user()->picture}}" class="rounded-circle" alt="User Image">
        </div>
        <div class="info">
			<a href="{{route('admin.settings')}}" class="link" data-toggle="tooltip" title="" data-original-title="Settings"><i class="ion ion-gear-b"></i></a>
            <a href="{{route('admin.profile')}}" class="link" data-toggle="tooltip" title="" data-original-title="Profile"><i class="ion ion-person"></i></a>
            <a onclick="return confirm(&quot;{{tr('confirm_logout')}}&quot;);" href="{{route('admin.logout')}}" class="link" data-toggle="tooltip" title="" data-original-title="Logout"><i class="ion ion-power"></i></a>
        </div>
      </div>
      <!-- sidebar menu -->
      <ul class="sidebar-menu" data-widget="tree">
		<li class="nav-devider"></li>
        <li >
          <a href="{{route('admin.dashboard')}}">
            <i class="fa fa-tachometer-alt"></i> <span>{{tr('dashboard')}}</span>
          
          </a>
        </li>
        <li class="nav-devider"></li>
        <li class="header nav-small-cap">{{tr('account_management')}}</li>
        <li class="treeview" id="users">
          <a href="{{route('admin.users.index')}}">
            <i class="fa fa-users"></i>
            <span>{{tr('users')}}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="users-create"><a href="{{route('admin.users.create')}}">{{tr('add_user')}}</a></li>
            <li id="users-view"><a href="{{route('admin.users.index')}}">{{tr('view_users')}}</a></li>
            @if(Setting::get('is_referral_enabled') == YES)
                <li id="user_referrals"><a href="{{route('admin.referrals.index')}}">{{tr('user_referrals')}}</a></li>
            @endif
          </ul>
        </li>
       
        <li id="users-documents">
          <a href="{{route('admin.user_documents.index')}}">
            <i class="fa fa-shield-alt"></i> <span>{{tr('verification_documents')}}</span>
          </a>
        </li>

        <li id="sellers">
          <a href="{{route('admin.users.index', ['is_content_creator' => CONTENT_CREATOR])}}">
            <i class="fa fa-users"></i> <span>{{tr('premium_users')}}</span>
          </a>
        </li>

       
        <li class="header nav-small-cap">{{tr('products_management')}}</li>

        <li class="treeview" id="categories">
          <a href="#">
            <i class="fa fa-list"></i>
            <span>{{tr('categories')}}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="categories-create"><a href="{{route('admin.categories.create')}}">{{tr('add_category')}}</a></li>
            <li id="categories-view"><a href="{{route('admin.categories.index')}}"> {{tr('view_categories')}}</a></li>
          </ul>
        </li>

        <li class="treeview" id="sub_categories">
          <a href="#">
            <i class="fa fa-sitemap"></i>
            <span>{{tr('sub_categories')}}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="sub_categories-create"><a href="{{route('admin.sub_categories.create')}}">{{tr('add_sub_category')}}</a></li>
            <li id="sub_categories-view"><a href="{{route('admin.sub_categories.index')}}"> {{tr('view_sub_categories')}}</a></li>
          </ul>
        </li>

        <li class="treeview" id="user_products">
          <a href="#">
            <i class="fa fa-shopping-cart"></i>
            <span>{{tr('user_products')}}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="user_products-create"><a href="{{route('admin.user_products.create')}}">{{tr('add_user_product')}}</a></li>
            <li id="user_products-view"><a href="{{route('admin.user_products.index')}}"> {{tr('view_user_products')}}</a></li>
          </ul>
        </li>

        <li id="product_reviews">
          <a href="{{route('admin.product_reviews.index')}}">
            <i class="fa fa-star"></i> <span>{{tr('reviews')}}</span>
          </a>
        </li>

        <li id="orders">
          <a href="{{route('admin.orders.index')}}">
            <i class="fa fa-cart-plus"></i> <span>{{tr('orders')}}</span>
          </a>        
        </li>

        

        <!-- <li class="treeview" id="promo_codes">

            <a href='#'>
                <i class="menu-icon fa fa-globe"></i>
                <span>{{ tr('promo_codes') }}</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-right pull-right"></i>
                </span>
            </a>
              <ul class="treeview-menu">
                    <li id="promo_codes-create"> 
                        <a class="nav-link" href="{{route('admin.promo_codes.create')}} "> {{ tr('add_promo_code') }} </a>
                    </li>

                    <li id="promo_codes-view"> 
                        <a class="nav-link" href=" {{route('admin.promo_codes.index')}} "> {{ tr('view_promo_codes') }}  </a>
                    </li>
              </ul>

        </li> -->

        <li class="header nav-small-cap">{{tr('video_management')}}</li>

            <li class="treeview" id="live-videos">
              <a href="#">
                <i class="fa fa-video"></i>
                <span>{{tr('live_videos')}}</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-right pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li id="live-videos-live"><a href="{{route('admin.live_videos.onlive')}}">{{tr('on_live')}}</a></li>
                <li id="live-videos-scheduled"><a href="{{route('admin.live_videos.scheduled')}}">{{tr('scheduled')}}</a></li>
                <li id="live-videos-history"><a href="{{route('admin.live_videos.index')}}"> {{tr('history')}}</a></li>
              </ul>
            </li>

        <li class="header nav-small-cap">{{tr('revenue_management')}}</li>
        <li id="revenue-dashboard">
          <a href="{{route('admin.revenues.dashboard')}}">
            <i class="fa fa-tachometer-alt"></i> <span>{{tr('revenue_dashboard')}}</span>
           
          </a>
        </li>
        <li class="treeview" id="payments">
          <a href="#">
            <i class="fa fa-money"></i> <span>{{tr('payments')}}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li id="order-payments"><a href="{{route('admin.order.payments')}}">{{tr('order_payments')}}</a></li>
          </ul>
        </li>
            
        <li id="user_wallets">
          <a href="{{route('admin.user_wallets.index')}}">
            <i class="fa fa-wallet"></i> <span>{{tr('user_wallets')}}</span>
           
          </a>
        </li>

        <li id="content_creator-withdrawals">
          <a href="{{route('admin.user_withdrawals')}}">
            <i class="fa fa-location-arrow"></i> <span>{{tr('user_withdrawals')}}</span>
          </a>
        </li>


        <li class="header nav-small-cap">{{tr('lookups_management')}}</li>

        <li class="treeview" id="documents">
          <a href="#">
            <i class="fa fa-file-text"></i> <span>{{tr('documents')}}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="documents-create"><a href="{{route('admin.documents.create')}}">{{tr('add_document')}}</a></li>
            <li id="documents-view"><a href="{{route('admin.documents.index')}}">{{tr('view_documents')}}</a></li>
          </ul>
        </li>

        <li class="treeview" id="static_pages">
          <a href="#">
            <i class="fa fa-file"></i> <span>{{tr('static_pages')}}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="static_pages-create"><a href="{{route('admin.static_pages.create')}}">{{tr('add_static_page')}}</a></li>
            <li id="static_pages-view"><a href="{{route('admin.static_pages.index')}}">{{tr('view_static_pages')}}</a></li>
          </ul>
        </li>


        <li class="header nav-small-cap">{{tr('setting_management')}}</li>

        <li id="settings">
          <a href="{{route('admin.settings')}}">
            <i class="fa fa-cog"></i> <span>{{tr('settings')}}</span>
           
          </a>
        </li>
        <li id="profile">
          <a href="{{route('admin.profile')}}">
            <i class="fa fa-user"></i> <span>{{tr('account')}}</span>
           
          </a>
        </li>

        <li>
          <a onclick="return confirm(&quot;{{tr('confirm_logout')}}&quot;);" href="{{route('admin.logout')}}">
            <i class="fa fa-power-off"></i> <span>{{tr('logout')}}</span>
           
          </a>
        </li>
        
      </ul>
    </section>
  </aside>