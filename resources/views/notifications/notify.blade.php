@if(Session::has('flash_error'))

    <script type="text/javascript">
        toastr.error("{{Session::get('flash_error')}}", "{{ Setting::get('site_name') }}", {
            progressBar: true,
            closeButton: true,
            timeOut: 5000,
            extendedTimeOut: 5000
        });
    </script> 

@endif 

@if(Session::has('flash_success'))

    <script type="text/javascript">
        toastr.success("{{Session::get('flash_success')}}", "{{ Setting::get('site_name') }}", {
            progressBar: true,
            closeButton: true,
            timeOut: 5000,
            extendedTimeOut: 5000
        });
    </script> 

@endif