@extends('layouts.admin') 

@section('title', tr('reviews')) 

@section('content-header', tr('reviews')) 

@section('breadcrumb')

<li class="breadcrumb-item">
    <a href="{{route('admin.product_reviews.index')}}">{{ tr('reviews') }}</a>
</li>

<li class="breadcrumb-item active">{{ tr('view_reviews') }}</li>

@endsection 

@section('content')

<section id="configuration">

    <div class="row">

        <div class="col-12">

            <div class="card">

                <div class="card-header border-bottom border-gray">

                    <h4 class="card-title">{{ tr('view_reviews') }}</h4>
                    
                </div>

                <div class="card-body box box-outline-info">

                    <div class="row">

                        <div class="col-lg-6">

                            <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">
                
                                <tbody>

                                    <tr>
                                        <td>{{ tr('unique_id') }}</td>
                                        <td>{{$product_review->unique_id ?: tr('n_a')}}</td>
                                    </tr>

                                    <tr>
                                        <td>{{ tr('from_username') }}</td>
                                        <td><a href="{{route('admin.users.view',['user_id'=>$product_review->user_id])}}">{{$product_review->fromUser->name ?? tr('n_a')}}</a></td>
                                    </tr>

                                    <tr>
                                        <td>{{ tr('to_username') }}</td>
                                        <td><a href="{{route('admin.users.view',['user_id'=>$product_review->seller_id])}}">{{$product_review->toUser->name ?? tr('n_a')}}</a></td>
                                    </tr>

                                    <tr>
                                        <td>{{ tr('order_id') }}</td>
                                        <td><a href="{{  route('admin.orders.view' , ['order_product_id' => $product_review->order_id] )  }}">
                                            {{ $product_review->order->unique_id ?? tr('n_a') }}
                                            </a></td>
                                    </tr>

                                    <tr>
                                        <td>{{ tr('ratings') }}</td>
                                        <td>{{$product_review->ratings ?: 0}}</td>
                                    </tr>
                                    
                                    <tr>
                                        <td>{{tr('review')}}</td>
                                        <td>{{$product_review->review ?: tr('n_a')}}</td>
                                    </tr>

                                    <tr>
                                        <td>{{tr('price')}}</td>
                                        <td>{{$product_review->order->per_quantity_price_formatted ?? 0}}</td>
                                    </tr>

                                </tbody>

                            </table>
                        </div>

                        @if($product_review->userProductDetails)

                        <div class="col-lg-6">

                            <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">
                        
                                <tbody>
                                    <tr>
                                        <td>{{tr('user_product')}}</td>
                                        <td>{{$product_review->userProductDetails->name ?? "-"}}</td>
                                    </tr>

                                    <tr>
                                        <td>{{ tr('is_outofstock') }}</td>

                                        <td>
                                            @if($product_review->userProductDetails->is_outofstock == YES)

                                                <span class="btn btn-success btn-sm">{{ tr('yes') }}</span> 
                                            @else

                                                <span class="btn btn-warning btn-sm">{{ tr('no') }}</span> 
                                            @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>{{ tr('updated_at') }}</td>
                                        <td>{{common_date($product_review->updated_at , Auth::guard('admin')->user()->timezone)}}</td>
                                    </tr>

                                    <tr>
                                        <td>{{ tr('created_at') }}</td>
                                        <td>{{common_date($product_review->created_at , Auth::guard('admin')->user()->timezone)}}</td>
                                    </tr>

                                    <tr>
                                        <td>{{tr('description')}}</td>
                                        <td>{!!$product_review->userProductDetails->description ?? tr('n_a')!!}</td>
                                    </tr>

                                </tbody>

                            </table>
                            
                        </div>

                        @endif

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>

@endsection

