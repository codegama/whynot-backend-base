@extends('layouts.admin') 

@section('title', tr('reviews')) 

@section('content-header', tr('reviews')) 

@section('breadcrumb')


    
<li class="breadcrumb-item active">
    <a href="">{{ tr('reviews') }}</a>
</li>

<li class="breadcrumb-item">
    {{tr('view_reviews')}}
</li>

@endsection 

@section('content')

<section id="configuration">

    <div class="row">

        <div class="col-12">

            <div class="card">

                <div class="card-header border-bottom border-gray">

                    <h4 class="card-title">{{ tr('view_reviews') }}</h4>
                    
                </div>

                <div class="box box-outline-info">

                    <div class="box-body">

                        <div class="table-responsive">

                            @include('admin.product_reviews._search')
                            
                            <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">
                                
                                <thead>
                                    <tr>
                                        <th>{{ tr('s_no') }}</th>
                                        <th>{{tr('unique_id')}}</th>
                                        <th>{{tr('order_id')}}</th>
                                        <th>{{ tr('username') }}</th>
                                        <th>{{ tr('sellername')}}</th>
                                        <th>{{ tr('user_product') }}</th>
                                        <th>{{ tr('review')}}</th>
                                        <th>{{ tr('ratings')}}</th>
                                        <th>{{ tr('action') }}</th>
                                    </tr>
                                </thead>
                               
                                <tbody>

                                    @foreach($product_reviews as $i => $product_review)

                                    <tr>
                                        <td>{{ $i + $product_reviews->firstItem() }}</td>

                                        <td>
                                           <a href="{{  route('admin.product_reviews.view' , ['product_review_id' => $product_review->id] )  }}">
                                            {{$product_review->unique_id ?: tr('n_a')}}
                                           </a>
                                        </td>

                                        <td>
                                            <a href="{{  route('admin.orders.view' , ['order_product_id' => $product_review->order_id] )  }}">
                                            {{ $product_review->orders->unique_id ?? tr('n_a') }}
                                            </a>
                                        </td>

                                        <td>
                                            <a href="{{  route('admin.users.view' , ['user_id' => $product_review->user_id] )  }}">
                                            {{ $product_review->fromUser->name ?? tr('n_a') }}
                                            </a>
                                        </td>

                                        <td>
                                            <a href="{{  route('admin.users.view' , ['user_id' => $product_review->seller_id] )  }}">
                                            {{ $product_review->toUser->name ?? tr('n_a') }}
                                            </a>
                                        </td>

                                        <td>
                                            <a href="{{  route('admin.user_products.view' , ['user_product_id' => $product_review->user_product_id] )  }}">
                                            {{ $product_review->userProductDetails->name ?? tr('n_a') }}
                                            </a>
                                        </td>

                                        <td>
                                            <a href="{{  route('admin.user_products.view' , ['user_product_id' => $product_review->user_product_id] )  }}">
                                            {{ $product_review->review ? substr($product_review->review , 0,25) : tr('n_a') }}...
                                            </a>
                                        </td>

                                        <td>
                                
                                            {{ $product_review->ratings ?: tr('n_a') }}
                                            
                                        </td>

                                        <td>
                                        
                                            <a class="btn btn-success" href="{{route('admin.product_reviews.view',['product_review_id' => $product_review->id])}}">{{tr('view')}}</a>

                                        </td>

                                    </tr>

                                    @endforeach

                                </tbody>
                            
                            </table>

                            <div class="pull-right" id="paglink">{{ $product_reviews->appends(request()->input())->links() }}</div>

                        </div>

                    </div>

            </div>

            </div>

        </div>

    </div>

</section>

@endsection