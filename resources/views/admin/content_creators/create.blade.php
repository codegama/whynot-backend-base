@extends('layouts.admin')

@section('title', tr('sellers'))

@section('content-header', tr('sellers'))

@section('breadcrumb')

    

    <li class="breadcrumb-item"><a href="{{route('admin.content_creators.index')}}">{{tr('sellers')}}</a></li>
    
    <li class="breadcrumb-item active">{{tr('add_seller')}}</a></li>

@endsection

@section('content')

    @include('admin.content_creators._form')

@endsection
