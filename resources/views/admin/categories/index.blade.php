@extends('layouts.admin') 

@section('content-header', tr('categories')) 

@section('breadcrumb')

<li class="breadcrumb-item">
    <a href="{{route('admin.categories.index')}}">{{tr('categories')}}</a>
</li>

<li class="breadcrumb-item active">{{ tr('view_categories') }}</li>

@endsection 

@section('content')

<section class="content">

    <div class="row">

        <div class="col-12">

            <div class="card">

                <div class="card-header border-bottom border-gray">

                    <h4 class="card-title">{{ tr('view_categories') }}</h4>

                    <div class="heading-elements">
                        <a href="{{ route('admin.categories.create') }}" class="btn btn-primary"><i class="ft-plus icon-left"></i>{{ tr('add_category') }}</a>
                    </div>
                    
                </div>
                <div class="box-body">
                    <div class="callout bg-pale-secondary">
                        <h4>{{tr('notes')}}</h4>
                        <p>
                            </p><ul>
                                <li>
                                    {{tr('product_category_notes')}}
                                </li>
                            </ul>
                        <p></p>
                    </div>
                </div>

                <div class="box box-outline-info">

                    <div class="box-body">

                        @include('admin.categories._search')
                        
                        <div class="table-responsive">

                            <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">
                            
                                <thead>
                                    <tr>
                                        <th>{{ tr('s_no') }}</th>
                                        <th>{{ tr('category_name') }}</th>
                                        <th>{{tr('sub_category_count')}}</th>
                                        <th>{{tr('total_products_count')}}</th>
                                        <th>{{ tr('picture') }}</th>
                                        <th>{{ tr('status') }}</th>
                                        <th>{{ tr('action') }}</th>
                                    </tr>
                                </thead>
                               
                                <tbody>

                                    @foreach($categories as $i => $category)
                                    <tr>
                                        <td>{{ $i+$categories->firstItem() }}</td>

                                        <td>
                                            <a href="{{  route('admin.categories.view' , ['category_id' => $category->id] )  }}">
                                            {{ $category->name ?: tr('n_a')}}
                                            </a>
                                        </td>

                                        <td>
                                        <a href="{{route('admin.sub_categories.index',['category_id' => $category->id])}}">{{$category->sub_categories_count ? : 0}}
                                       </a>
                                    </td>

                                        <td>
                                            <a href="{{route('admin.user_products.index',['category_id' => $category->id])}}">
                                            {{ $category->user_products_count ? : 0}}
                                            </a>
                                        </td>

                                        <td><img src="{{$category->picture ?: asset('placeholder.jpg')}}" class="category-image"></td>
                                        
                                        <td>
                                            @if($category->status == APPROVED)

                                                <span class="btn btn-success btn-sm">{{ tr('approved') }}</span> 
                                            @else

                                                <span class="btn btn-warning btn-sm">{{ tr('declined') }}</span> 
                                            @endif
                                        </td>

                                        <td>
                                        
                                            <div class="btn-group" role="group">

                                                <button class="btn btn-outline-primary dropdown-toggle dropdown-menu-right" id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ft-settings icon-left"></i> {{ tr('action') }}</button>

                                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">

                                                    <a class="dropdown-item" href="{{ route('admin.categories.view', ['category_id' => $category->id] ) }}">&nbsp;{{ tr('view') }}</a> 

                                                    @if(Setting::get('is_demo_control_enabled') == YES)

                                                        <a class="dropdown-item" href="javascript:void(0)">&nbsp;{{ tr('edit') }}</a>

                                                        <a class="dropdown-item" href="javascript:void(0)">&nbsp;{{ tr('delete') }}</a> 

                                                    @else

                                                        <a class="dropdown-item" href="{{ route('admin.categories.edit', ['category_id' => $category->id] ) }}">&nbsp;{{ tr('edit') }}</a>

                                                        <a class="dropdown-item" onclick="return confirm(&quot;{{ tr('category_delete_confirmation' , $category->name) }}&quot;);" href="{{ route('admin.categories.delete', ['category_id' => $category->id] ) }}">&nbsp;{{ tr('delete') }}</a>

                                                    @endif

                                                    @if($category->status == APPROVED)

                                                        <a class="dropdown-item" href="{{  route('admin.categories.status' , ['category_id' => $category->id] )  }}" onclick="return confirm(&quot;{{ $category->name }} - {{ tr('category_decline_confirmation') }}&quot;);">&nbsp;{{ tr('decline') }}
                                                    </a> 

                                                    @else

                                                        <a class="dropdown-item" href="{{ route('admin.categories.status' , ['category_id' => $category->id] ) }}">&nbsp;{{ tr('approve') }}</a> 

                                                    @endif
                                                    <a class="dropdown-item" href="{{route('admin.user_products.index',['category_id' => $category->id])}}">&nbsp;{{ tr('total_products') }}</a> 

                                                    <a class="dropdown-item" href="{{route('admin.sub_categories.index',['category_id' => $category->id])}}">&nbsp;{{ tr('sub_categories') }}</a> 

                                                    <a class="dropdown-item" href="{{ route('admin.sub_categories.create') }}">&nbsp;{{ tr('add_sub_categories') }}</a> 
                                                </div>

                                            </div>

                                        </td>

                                    </tr>

                                    @endforeach

                                </tbody>
                        
                            </table>

                            <div class="pull-right" id="paglink">{{ $categories->appends(request()->input())->links() }}</div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>

@endsection
