<form method="GET" action="{{route('admin.live_video_categories.index')}}">

    <div class="row search-form">

        <div class="col-xs-12 col-sm-12 col-lg-3 col-md-6 resp-mrg-btm-md">

        </div>

        <div class="col-xs-12 col-sm-12 col-lg-3 col-md-6 md-full-width resp-mrg-btm-md">

        </div>

        <div class="col-xs-12 col-sm-12 col-lg-6 col-md-12">

            <div class="input-group">
               
                <input type="text" class="form-control" name="search_key"
                placeholder="{{tr('order_products_search_placeholder')}}" value="{{Request::get('search_key')??''}}"> <span class="input-group-btn">
                &nbsp
         
                <input type="hidden" name="category_id" value="{{Request::get('category_id') ?? ''}}">

                <button type="submit" class="btn btn-default">
                   <a href=""><i class="fa fa-search" aria-hidden="true"></i></a>
                </button>

                <a class="btn btn-default reset-btn" href="{{route('admin.live_video_categories.index',['category_id'=>Request::get('category_id') ?? ''])}}"><i class="fa fa-eraser" aria-hidden="true"></i>
                </a>
                   
                </span>

            </div>
            
        </div>

    </div>

</form></br>