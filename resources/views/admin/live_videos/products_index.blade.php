@extends('layouts.admin') 

@section('title', tr('live_video_products')) 

@section('content-header', tr('live_video_products')) 

@section('breadcrumb')


    
<li class="breadcrumb-item active">
    <a href="">{{ tr('live_videos') }}</a>
</li>

<li class="breadcrumb-item">
    {{tr('live_video_products')}}
</li>

@endsection 

@section('content')

<section id="configuration">

    <div class="row">

        <div class="col-12">

            <div class="card">

                <div class="card-header border-bottom border-gray">

                        <h4 class="card-title">{{ tr('live_video_products') }}</h4>
                    
                </div>

                <div class="box box-outline-info">

                    <div class="box-body">

                        <div class="table-responsive">

                            @include('admin.live_videos._product_search')
                            
                            <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">
                                
                                <thead>
                                    <tr>
                                        <th>{{ tr('s_no') }}</th>
                                        <th>{{ tr('user') }}</th>
                                        <th>{{ tr('order_product_id')}}</th>
                                        <th>{{ tr('total_products') }}</th>
                                        <th>{{ tr('total')}}</th>
                                        <th>{{ tr('status') }}</th>
                                        <th>{{ tr('action') }}</th>
                                    </tr>
                                </thead>
                               
                                <tbody>

                                    @foreach($live_video_products as $i => $live_video_product)

                                    <tr>
                                        <td>{{ $i + $live_video_products->firstItem() }}</td>

                                        <td>
                                            <a href="{{  route('admin.users.view' , ['user_id' => $live_video_product->user_id] )  }}">
                                            {{ $live_video_product->user->name ?? tr('n_a') }}
                                            </a>
                                        </td>

                                        <td>
                                           <a href="{{  route('admin.orders.view' , ['order_id' => $live_video_product->id] )  }}">
                                            {{$live_video_product->unique_id ?: tr('n_a')}}
                                           </a>
                                        </td>

                                        <td>
                                            {{ $live_video_product->quantity ?: 0}}
                                        </td>

                                        <td>{{$live_video_product->total_formatted ?: 0}}</td>

                                        <td>
                                            @switch($live_video_product->status)

                                                @case(SORT_BY_ORDER_CANCELLED)
                                                    <span class="badge bg-danger">{{tr('cancelled')}}</span>

                                                @case(SORT_BY_ORDER_SHIPPED)
                                                    <span class="badge bg-secondary">{{tr('shipped')}}</span>

                                                @case(SORT_BY_ORDER_DELIVERD) 
                                                    <span class="badge bg-success">
                                                        {{tr('deliverd')}}
                                                    </span>
                                                @default
                                                    <span class="badge bg-primary">{{tr('placed')}}</span>

                                            @endswitch
                                        </td>

                                        <td>
                                        
                                            <a class="btn btn-success" href="{{route('admin.live_video_products.view',['live_video_product_id' => $live_video_product->id])}}">{{tr('view')}}</a>

                                        </td>

                                    </tr>

                                    @endforeach

                                </tbody>
                            
                            </table>

                            <div class="pull-right" id="paglink">{{ $live_video_products->appends(request()->input())->links() }}</div>

                        </div>

                    </div>

            </div>

            </div>

        </div>

    </div>

</section>

@endsection