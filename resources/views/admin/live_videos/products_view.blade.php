@extends('layouts.admin') 

@section('title', tr('live_video_products')) 

@section('content-header', tr('live_video_products')) 

@section('breadcrumb')

<li class="breadcrumb-item">
    <a href="{{route('admin.live_video_products.index',['live_video_id'=>$live_video_product->live_video_id])}}">{{ tr('live_video_products') }}</a>
</li>

<li class="breadcrumb-item active">{{ tr('view_live_video_products') }}</li>

@endsection 

@section('content')

<section id="configuration">

    <div class="row">

        <div class="col-12">

            <div class="card">

                <div class="card-header border-bottom border-gray">

                    <h4 class="card-title">{{ tr('view_live_video_products') }}</h4>
                    
                </div>

                <div class="card-body box box-outline-info">

                    <div class="row">

                        <div class="col-lg-12">

                            <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">
                
                                <tbody>

                                    <tr>
                                        <td>{{ tr('unique_id') }}</td>
                                        <td>{{$live_video_product->unique_id ?: tr('n_a')}}</td>
                                    </tr>

                                    <tr>
                                        <td>{{ tr('user') }}</td>
                                        <td><a href="{{route('admin.users.view',['user_id'=>$live_video_product->user_id])}}">{{$live_video_product->user->name ?? tr('n_a')}}</a></td>
                                    </tr>

                                    @if($live_video_product->userProduct)

                                    <tr>
                                        <td>{{tr('user_product')}}</td>
                                        <td>{{$live_video_product->userProduct->name ?? tr('n_a')}}</td>
                                    </tr>

                                    <tr>
                                        <td>{{ tr('is_outofstock') }}</td>

                                        <td>
                                            @if($live_video_product->userProduct->is_outofstock == YES)

                                                <span class="btn btn-success btn-sm">{{ tr('yes') }}</span> 
                                            @else

                                                <span class="btn btn-warning btn-sm">{{ tr('no') }}</span> 
                                            @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>{{ tr('updated_at') }}</td>
                                        <td>{{common_date($live_video_product->userProduct->updated_at , Auth::guard('admin')->user()->timezone)}}</td>
                                    </tr>

                                    <tr>
                                        <td>{{ tr('created_at') }}</td>
                                        <td>{{common_date($live_video_product->userProduct->created_at , Auth::guard('admin')->user()->timezone)}}</td>
                                    </tr>

                                    <tr>
                                        <td>{{tr('description')}}</td>
                                        <td>{!!$live_video_product->userProduct->description ?? tr('n_a')!!}</td>
                                    </tr>

                                    @endif

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>

@endsection

