<form method="GET" action="{{route('admin.live_video_products.index')}}" class="form-bottom">

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-lg-6 col-md-12">

        </div>

        <div class="col-xs-12 col-sm-12 col-lg-6 col-md-12">

            <div class="input-group">

                <input type="hidden" class="form-control" name="live_video_id" value="{{Request::get('live_video_id')}}">
               
                <input type="text" class="form-control" name="search_key" value="{{Request::get('search_key')}}"
                placeholder="{{tr('live__payment_search_placeholder')}}"> <span class="input-group-btn">
                &nbsp

                <button type="submit" class="btn btn-default reset-btn">
                   <i class="fa fa-search" aria-hidden="true"></i>
                </button>
                
                <a  href="{{route('admin.live_video_products.index',['live_video_id'=>Request::get('live_video_id')])}}" class="btn btn-default reset-btn"><i class="fa fa-eraser" aria-hidden="true"></i>
                </a>
                   
                </span>

            </div>
            
        </div>

    </div>

</form>
