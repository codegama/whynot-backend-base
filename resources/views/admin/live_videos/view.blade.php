@extends('layouts.admin')

@section('title', tr('live_videos'))

@section('content-header', tr('live_videos'))

@section('breadcrumb')



<li class="breadcrumb-item">
    <a href="{{route('admin.live_videos.index')}}">{{ tr('live_videos') }}</a>
</li>

<li class="breadcrumb-item active">{{ tr('view_live_videos') }}</li>

@endsection

@section('content')

<section class="content">

    <div class="row">

        <div class="col-12">

            <div class="card">

                <div class="card-header border-bottom border-gray">

                    <h4 class="card-title">{{ tr('live_video') }} - <a
                    href="{{route('admin.users.view',['user_id' => $live_video->user_id])}}">{{ $live_video->title ?: tr('n_a')}}</a> </h4>

                </div>

                <div class="box box-outline-info">

                    <div class="box-body">

                        <div class="row">

                            <div class="col-md-6">

                                <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">

                                    <tbody>

                                        <tr>
                                            <td>{{ tr('stream_id')}} </td>
                                            <td class="text-uppercase">{{ $live_video->unique_id}}</td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('stream_title')}} </td>
                                            <td>{{ $live_video->title ?: tr('n_a')}}</td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('username')}} </td>
                                            <td><a href="{{ route('admin.users.view', ['user_id' => $live_video->user_id]) }}">{{ $live_video->user->name ?? tr('n_a')}}</a></td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('category')}} </td>
                                            <td><a href="{{ route('admin.categories.view', ['category_id' => $live_video->category_id]) }}">{{ $live_video->category->name ?? tr('n_a')}}</a></td>
                                        </tr> 

                                        <tr>
                                            <td>{{ tr('sub_category')}} </td>
                                            <td><a href="{{ route('admin.sub_categories.view', ['sub_category_id' => $live_video->sub_category_id]) }}">{{ $live_video->subCategory->name ?? tr('n_a')}}</td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('scheduled_video')}} </td>
                                            <td>{{ $live_video->live_schedule_type ? tr('no') : tr('yes') }}</td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('status')}} </td>
                                            <td>
                                                {{$live_video->status_formatted}}
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('stream_type')}} </td>
                                            <td>
                                                {{$live_video->type ?: tr('n_a')}}
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('payment_type') }}</td>
                                            <td>
                                                @if($live_video->payment_status)
                                                <label class="label label-warning">{{tr('paid')}}</label>
                                                @else
                                                <label class="label label-success">{{tr('free')}}</label>
                                                @endif
                                            </td>
                                        </tr>
                                    </tbody>

                                </table>

                            </div>

                            <div class="col-md-6">

                                <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">

                                    <tbody>

                                        <tr>
                                            <td>{{ tr('viewer_count') }}</td>
                                            <td>{{$live_video->viewer_cnt}}</td>
                                        </tr>

                                      <!--   <tr>
                                            <td>{{ tr('start_time') }}</td>
                                            <td>{{common_date($live_video->start_time,'','g:i A')}}</td>

                                        </tr> -->

                                        <tr>
                                            <td>{{ tr('end_time') }}</td>
                                            <td>{{common_date($live_video->end_time, '','g:i A')}}</td>

                                        </tr>

                                    <!--     <tr>
                                            <td nowrap="">{{ tr('scheduled_time') }}</td>
                                            <td>{{$live_video->live_schedule_type ? tr('n_a') : common_date($live_video->schedule_time, Auth::guard('admin')->user()->timezone)}}</td>

                                        </tr> -->

                                        <tr>
                                            <td>{{ tr('no_of_minutes') }}</td>
                                            <td>{{ $live_video->no_of_minutes}}</td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('streamed_at') }}</td>
                                            <td>{{common_date($live_video->created_at, Auth::guard('admin')->user()->timezone)}}</td>

                                        </tr>

                                        <tr>
                                            <td>{{ tr('created_at') }}</td>
                                            <td>{{common_date($live_video->created_at , Auth::guard('admin')->user()->timezone)}}</td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('updated_at') }}</td>
                                            <td>{{common_date($live_video->updated_at , Auth::guard('admin')->user()->timezone)}}</td>
                                        </tr>


                                        <tr>
                                            <td>{{ tr('description') }}</td>
                                            <td>{!! $live_video->description ?: tr('n_a') !!}</td>
                                        </tr>

                                    </tbody>

                                </table>

                            </div>

                            @if($live_video->amount > 0)
                            <div class="col-md-6">
                                <h3>Revenue Details</h3>
                                <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">

                                    <tbody>

                                        <tr>
                                            <td>{{ tr('video_amount')}} </td>
                                            <td class="text-uppercase">{{ formatted_amount($live_video->amount)}}</td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('total_amount')}} </td>
                                            <td>{{ $live_video->live_video_amount}}</td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('user_commission')}} </td>
                                            <td>{{ $live_video->user_amount}}</td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('admin_commission') }}</td>
                                            <td>{{ $live_video->admin_amount}}</td>

                                        </tr>
                                    </tbody>

                                </table>

                            </div>
                            @endif

                            <div class="col-md-12">
                                <h3>Preview File</h3>
                                <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">

                                    <tbody>

                                        <tr>
                                            <td class="text-capitalize">{{ $live_video->preview_file_type }}</td>
                                            <td> <img src="{{ $live_video->preview_file}}" /></td>

                                        </tr>
                                    </tbody>

                                </table>

                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>

@endsection