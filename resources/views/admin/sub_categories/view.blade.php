@extends('layouts.admin')

@section('title', tr('view_sub_sub_categories'))

@section('content-header', tr('product_sub_categories'))

@section('breadcrumb')

    
    <li class="breadcrumb-item"><a href="{{route('admin.sub_categories.index')}}">{{tr('product_sub_categories')}}</a>
    </li>
    <li class="breadcrumb-item active">{{tr('view_sub_categories')}}</a>
    </li>

@endsection

@section('content')

<section class="content">

    <div class="row">

            <div class="col-xl-12 col-lg-12">

                <div class="card user-profile-view-sec">

                     <div class="card-header border-bottom border-gray">

                        <h4 class="card-title">{{tr('view_sub_categories')}} - {{$sub_category->name ?: tr('n_a')}}</h4>

                    </div>
                    
                    <div class="card-content">

                        <div class="col-md-12">

                        <div class="card profile-with-cover">

                            <div class="media profil-cover-details w-100">

                                <div class="media-left pl-2 pt-2">

                                    <a class="profile-image">
                                       <img src="{{ $sub_category->picture ?: asset('placeholder.jpg')}}" alt="{{ $sub_category->name}}" class="img-thumbnail img-fluid img-border height-100"
                                        alt="Card image">
                                    </a>

                                </div>

                                
                            </div>

                            
                        </div>
                        
                    </div>

                     <hr>

                      <div class="user-view-padding">
                        <div class="row"> 

                            <div class=" col-xl-6 col-lg-6 col-md-12">
                                <div class="table-responsive">

                                    <table class="table table-xl mb-0">
                                         <tr>
                                    <th>{{tr('sub_category_name')}}</th>
                                    <td>{{$sub_category->name ?: tr('n_a')}}</td>
                                </tr>

                                <tr>
                                    <th>{{tr('category_name')}}</th>
                                    <td><a href="{{route('admin.categories.view',['category_id' => $sub_category->category_id])}}">{{$sub_category->category->name}}</a></td>
                                </tr>

                                <tr>
                                                
                                    <th>{{tr('user_products')}}</th>
                                    <td><a href="{{route('admin.user_products.index',['sub_category_id' => $sub_category->id])}}">{{$sub_category->UserProducts->count() ?? tr('n_a')}}</a></td>

                                </tr>

                                <tr>
                                    <th>{{tr('status')}}</th>
                                    <td>
                                        @if($sub_category->status == APPROVED) 

                                            <span class="badge badge-success">{{tr('approved')}}</span>

                                        @else
                                            <span class="badge badge-danger">{{tr('declined')}}</span>

                                        @endif
                                    </td>
                                </tr>

                                <tr>
                                  <th>{{tr('created_at')}} </th>
                                  <td>{{common_date($sub_category->created_at , Auth::guard('admin')->user()->timezone)}}</td>
                                </tr>

                                <tr>
                                  <th>{{tr('updated_at')}} </th>
                                  <td>{{common_date($sub_category->updated_at , Auth::guard('admin')->user()->timezone)}}</td>
                                </tr> 

                                <tr>
                                    <th>{{tr('description')}}</th>
                                    <td>{{$sub_category->description ?: tr('n_a')}}</td>
                                </tr>
                                    </table>

                                </div>
                            </div>

                            <div class="col-xl-6 col-lg-6 col-md-12">

                                <div class="px-2 resp-marg-top-xs">

                                    <div class="card-title">{{tr('action')}}</div>

                                    <div class="row">

                                        @if(Setting::get('is_demo_control_enabled') == YES)

                                        <div class="col-xl-6 col-lg-6 col-md-12">

                                            <a class="btn btn-secondary btn-block btn-min-width mr-1 mb-1 " href="javascript:void(0)"> &nbsp;{{tr('edit')}}</a>

                                        </div>

                                        <div class="col-xl-6 col-lg-6 col-md-12">

                                            <a class="btn btn-danger btn-block btn-min-width mr-1 mb-1" href="javascript:void(0)">&nbsp;{{tr('delete')}}</a>

                                        </div>


                                        @else

                                        <div class="col-xl-6 col-lg-6 col-md-12">

                                           <a class="btn btn-secondary btn-block btn-min-width mr-1 mb-1 " href="{{route('admin.sub_categories.edit', ['sub_category_id'=>$sub_category->id] )}}"> &nbsp;{{tr('edit')}}</a>

                                        </div>

                                        <div class="col-xl-6 col-lg-6 col-md-12">

                                            <a class="btn btn-danger btn-block btn-min-width mr-1 mb-1" onclick="return confirm(&quot;{{tr('sub_category_delete_confirmation' , $sub_category->name)}}&quot;);" href="{{route('admin.sub_categories.delete', ['sub_category_id'=> $sub_category->id] )}}">&nbsp;{{tr('delete')}}</a>

                                        </div>

                                        <div class="col-xl-6 col-lg-6 col-md-12">

                                            <a class="btn btn-danger btn-block btn-min-width mr-1 mb-1"  href="{{route('admin.user_products.index',['sub_category_id' => $sub_category->id])}}">&nbsp;{{tr('total_products')}}</a>

                                        </div>

                                        @endif

                                        <div class="col-xl-6 col-lg-6 col-md-12">

                                            @if($sub_category->status == APPROVED)
                                                 <a class="btn btn-warning btn-block btn-min-width mr-1 mb-1" href="{{route('admin.sub_categories.status' ,['sub_category_id'=> $sub_category->id] )}}" onclick="return confirm(&quot;{{$sub_category->name}} - {{tr('sub_category_decline_confirmation')}}&quot;);">&nbsp;{{tr('decline')}} </a> 
                                            @else

                                                <a  class="btn btn-success btn-block btn-min-width mr-1 mb-1" href="{{route('admin.sub_categories.status' , ['sub_category_id'=> $sub_category->id] )}}">&nbsp;{{tr('approve')}}</a> 
                                            @endif

                                       </div>

                                   </div>

                               </div>

                           </div>
                           
                       </div>

                   </div> 
                
                </div>

            </div>    

        </div>

    </div>
 
</section> 
    
@endsection

@section('scripts')

@endsection
