@extends('layouts.admin') 

@section('title', tr('view_sub_categories')) 

@section('content-header', tr('product_sub_categories')) 

@section('breadcrumb')



<li class="breadcrumb-item">
    <a href="{{route('admin.sub_categories.index')}}">{{tr('product_sub_categories')}}</a>
</li>

<li class="breadcrumb-item active">{{tr('view_sub_categories')}}</li>

@endsection 

@section('content')

<section class="content">

    <div class="row">

        <div class="col-12">

            <div class="card">

                <div class="card-header border-bottom border-gray">

                    @if(request()->category_id)

                    <h4 class="card-title">{{ tr('view_sub_categories') }}-<a href="{{  route('admin.categories.view' , ['category_id' => $category->id] )  }}">{{ $category->name ?? tr('n_a')}}</a></h4>

                    @else

                        <h4 class="card-title">{{ tr('view_sub_categories') }}</h4>

                    @endif

                    <div class="heading-elements">
                        <a href="{{ route('admin.sub_categories.create') }}" class="btn btn-primary"><i class="ft-plus icon-left"></i>{{ tr('add_sub_category') }}</a>
                    </div>
                    
                </div>
                <div class="box-body">
                    <div class="callout bg-pale-secondary">
                        <h4>{{tr('notes')}}</h4>
                        <p>
                            </p><ul>
                                <li>
                                    {{tr('product_sub_category_notes')}}
                                </li>
                            </ul>
                        <p></p>
                    </div>
                </div>

                <div class="box box-outline-info">

                    <div class="box-body">

                        @include('admin.sub_categories._search')

                        <div class="table-responsive">

                            <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">

                                <thead>
                                    <tr>
                                        <th>{{ tr('s_no') }}</th>
                                        <th>{{ tr('sub_category_name') }}</th>
                                        @if(!request()->category_id)
                                        <th>{{ tr('category_name') }}</th>
                                        @endif
                                        <th>{{ tr('picture') }}</th>
                                        <th>{{ tr('status') }}</th>
                                        <th>{{ tr('action') }}</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    @foreach($sub_categories as $i => $sub_category)
                                    <tr>
                                        <td>{{ $i+$sub_categories->firstItem() }}</td>

                                        <td>
                                            <a href="{{  route('admin.sub_categories.view' , ['sub_category_id' => $sub_category->id] )  }}">
                                                {{ $sub_category->name ?: tr('n_a')}}
                                            </a>
                                        </td>
                                        
                                        @if(!request()->category_id)
                                        <td>
                                            <a href="{{  route('admin.categories.view' , ['category_id' => $sub_category->category_id] )  }}">
                                                {{ $sub_category->category->name ?? tr('n_a')}}
                                            </a>
                                        </td>
                                        @endif

                                        <td><img src="{{$sub_category->picture ?: asset('placeholder.jpg')}}" class="category-image"></td>

                                        <td>
                                            @if($sub_category->status == APPROVED)

                                            <span class="btn btn-success btn-sm">{{ tr('approved') }}</span> 
                                            @else

                                            <span class="btn btn-warning btn-sm">{{ tr('declined') }}</span> 
                                            @endif
                                        </td>

                                        <td>

                                            <div class="btn-group" role="group">

                                                <button class="btn btn-outline-primary dropdown-toggle dropdown-menu-right" id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ft-settings icon-left"></i> {{ tr('action') }}</button>

                                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">

                                                    <a class="dropdown-item" href="{{ route('admin.sub_categories.view', ['sub_category_id' => $sub_category->id] ) }}">&nbsp;{{ tr('view') }}</a> 

                                                    @if(Setting::get('is_demo_control_enabled') == YES)

                                                    <a class="dropdown-item" href="javascript:void(0)">&nbsp;{{ tr('edit') }}</a>

                                                    <a class="dropdown-item" href="javascript:void(0)">&nbsp;{{ tr('delete') }}</a> 

                                                    @else

                                                    <a class="dropdown-item" href="{{ route('admin.sub_categories.edit', ['sub_category_id' => $sub_category->id] ) }}">&nbsp;{{ tr('edit') }}</a>

                                                    <a class="dropdown-item" onclick="return confirm(&quot;{{ tr('sub_category_delete_confirmation' , $sub_category->name) }}&quot;);" href="{{ route('admin.sub_categories.delete', ['sub_category_id' => $sub_category->id] ) }}">&nbsp;{{ tr('delete') }}</a>

                                                    @endif

                                                    @if($sub_category->status == APPROVED)

                                                    <a class="dropdown-item" href="{{  route('admin.sub_categories.status' , ['sub_category_id' => $sub_category->id] )  }}" onclick="return confirm(&quot;{{ $sub_category->name }} - {{ tr('sub_category_decline_confirmation') }}&quot;);">&nbsp;{{ tr('decline') }}
                                                    </a> 

                                                    @else

                                                    <a class="dropdown-item" href="{{ route('admin.sub_categories.status' , ['sub_category_id' => $sub_category->id] ) }}">&nbsp;{{ tr('approve') }}</a> 

                                                    @endif

                                                    <a class="dropdown-item" href="{{route('admin.user_products.index',['sub_category_id' => $sub_category->id])}}">&nbsp;{{ tr('total_products') }}</a> 
                                                </div>

                                            </div>

                                        </td>

                                    </tr>

                                    @endforeach

                                </tbody>

                            </table>

                            <div class="pull-right" id="paglink">{{ $sub_categories->appends(request()->input())->links() }}</div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>

@endsection