@extends('layouts.admin')

@section('title', tr('view_documents'))

@section('content-header', tr('view_documents'))

@section('breadcrumb')

<li class="breadcrumb-item">
    <a href="{{route('admin.documents.index')}}">{{tr('documents')}}</a>
</li>

<li class="breadcrumb-item active">{{tr('view_documents')}}</li>

@endsection

@section('content')

<!-- <section class="content">

    <div class="card">

        <div class="card-header">

            <h4 id="basic-forms" class="card-title">{{tr('document')}}-{{$document->name ?: tr('n_a')}} </h4>

        </div>

        <div class="card-content collapse show" aria-expanded="true">

            <div class="card-body">

                <div class="row">

                    <div class="col-md-3">
                        <div class="card-title text-center"><h4>{{tr('document_image')}}</h4></div><br>

                        <img src="{{$document->picture ?: asset('placeholder.png')}}" class="document-image">
                    </div>

                    <div class="col-lg-7">

                        <div class="card-title text-center"><h4>{{tr('document')}}</h4></div><br>

                        <p><strong>{{tr('document_name')}}</strong>

                            <span class="pull-right">{{$document->name ?: tr('n_a')}}
                            </span>
                            
                        </p>
                        <hr>

                       <p><strong>{{tr('status')}}</strong>

                            @if($document->status == APPROVED)
                                <span class="badge bg-success pull-right">{{tr('approved')}}</span>
                            @else
                                <span class="badge bg-danger pull-right">{{tr('declined')}}</span>
                            @endif

                        </p>
                        <hr>

                        <p><strong>{{tr('created_at')}} </strong>
                            <span class="pull-right">{{common_date($document->created_at , Auth::guard('admin')->user()->timezone)}}</span>
                        </p>
                        <hr>

                        <p><strong>{{tr('updated_at')}} </strong>
                            <span class="pull-right">{{common_date($document->updated_at , Auth::guard('admin')->user()->timezone)}}
                            </span>
                        </p>
                        <hr>

                        <p><strong>{{tr('description')}}</strong>
                            <span class="pull-right">{{$document->description ?: tr('n_a')}}</span>
                        </p>

                    </div>

                    <div class="col-md-2 text-center">

                        <div class="card-title text-center"><h4>{{tr('action')}}</h4></div><br>

                         @if(Setting::get('admin_delete_control') == YES )

                            <a href="javascript:;" class="btn btn-warning mb-2" title="{{tr('edit')}}"><b>{{tr('edit')}}</b></a><br>

                            <a onclick="return confirm(&quot;{{ tr('document_delete_confirmation', $document->title ) }}&quot;);" href="javascript:;" class="btn btn-danger" title="{{tr('delete')}}"><b>{{tr('delete')}}</b>
                                </a><br>

                        @else
                            <a href="{{ route('admin.documents.edit' , ['document_id' => $document->id] ) }}" class="btn btn-warning btn-min-width ml-2 mb-1" title="{{tr('edit')}}"><b>{{tr('edit')}}</b></a> <br> 
                                                        
                            <a onclick="return confirm(&quot;{{ tr('document_delete_confirmation', $document->name ) }}&quot;);" href="{{ route('admin.documents.delete', ['document_id' => $document->id] ) }}" class="btn btn-danger btn-min-width mb-1 ml-2" title="{{tr('delete')}}"><b>{{tr('delete')}}</i></b>
                                </a><br>
                        @endif

                        @if($document->status == APPROVED)

                            <a class="btn btn-info btn-min-width mb-1 ml-2" title="{{tr('decline')}}" href="{{ route('admin.documents.status', ['document_id' => $document->id]) }}" onclick="return confirm(&quot;{{$document->name}} - {{tr('document_decline_confirmation')}}&quot;);" >
                                <b>{{tr('decline')}}</b>
                            </a><br>

                        @else
                            
                            <a class="btn btn-success btn-min-width mr-1 mb-1" title="{{tr('approve')}}" href="{{ route('admin.documents.status', ['document_id' => $document->id]) }}">
                                <b>{{tr('approve')}}</b> 
                            </a><br>
                               
                        @endif
                           
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
</section> -->

<section class="content">


        <div class="row">

            <div class="col-xl-12 col-lg-12">

                <div class="card user-profile-view-sec">

                    <div class="card-header border-bottom border-gray">

                        <h4 class="card-title">{{tr('document')}}-{{$document->name ?: tr('n_a')}} </h4>

                    </div>

                    <div class="card-content">

                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="card profile-with-cover">

                                        

                                        <div class="media profil-cover-details w-200">

                                            <div class="media-left pl-2 pt-2">

                                                <a class="profile-image">
                                                    <img src="{{$document->picture ?: asset('placeholder.png')}}" class="document-image">
                                                </a>

                                                <span class="prof-img-name-pos">{{tr('document_image')}}</span>

                                            </div>
                                           
                                        </div>
                                        
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <br>
                                    <div class="row">
                                        @if(Setting::get('is_demo_control_enabled') == YES)
                                        <div class="col-md-6">
                                            <a class="btn btn-secondary btn-block btn-min-width mr-1 mb-1" href="javascript:void(0)" title="{{tr('edit')}}">&nbsp;{{tr('edit')}}</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a class="btn btn-danger btn-block btn-min-width mr-1 mb-1" onclick="return confirm(&quot;{{ tr('document_delete_confirmation', $document->title ) }}&quot;);" href="javascript:;" href="javascript:void(0)">&nbsp;{{tr('delete')}}</a>
                                        </div>
                                        @else
                                        <div class="col-md-6">
                                            <a class="btn btn-secondary btn-block btn-min-width mr-1 mb-1 "href="{{ route('admin.documents.edit' , ['document_id' => $document->id] ) }}"> &nbsp;{{tr('edit')}}</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a class="btn btn-danger btn-block btn-min-width mr-1 mb-1" onclick="return confirm(&quot;{{ tr('document_delete_confirmation', $document->name ) }}&quot;);" href="{{ route('admin.documents.delete', ['document_id' => $document->id] ) }}">&nbsp;{{tr('delete')}}</a>
                                        </div>
                                        @endif

                                    </div>  
                                    
                                    <div class="row">
                                         
                                         <div class="col-md-6">
                                              @if($document->status == APPROVED)
                                                <a class="btn btn-warning btn-block btn-min-width mr-1 mb-1" title="{{tr('decline')}}" href="{{ route('admin.documents.status', ['document_id' => $document->id]) }}" onclick="return confirm(&quot;{{$document->name}} - {{tr('document_decline_confirmation')}}&quot;);" >{{tr('decline')}} </a>
                                            @else

                                                <a class="btn btn-success btn-block btn-min-width mr-1 mb-1" title="{{tr('approve')}}" href="{{ route('admin.documents.status', ['document_id' => $document->id]) }}">&nbsp;{{tr('approve')}}</a>
                                            @endif
                                        
                                        </div>
                                    </div>   
                                </div>
                                  
                            </div>
                           
                        </div>

                        <hr>
                        <div class="user-view-padding">
                            <div class="row"> 

                                <div class=" col-xl-12 col-lg-12 col-md-12">
                                    <div class="table-responsive">

                                        <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">

                                            <tr >
                                                <th style="border-top: 0">{{tr('document_name')}}</th>
                                                <td style="border-top: 0">{{$document->name ?: tr('n_a')}}</td>
                                            </tr>

                                            <tr>
                                            <th>{{tr('status')}}</th>
                                            <td>
                                                 @if($document->status == APPROVED)
                                                    <span class="badge bg-success pull-left">{{tr('approved')}}</span>
                                                 @else
                                                    <span class="badge bg-danger pull-left">{{tr('declined')}}</span>
                                                 @endif
                                            </td>    
                                            </tr>

                                            <tr>
                                                <th>{{tr('created_at')}}</th>
                                                <td>{{common_date($document->created_at , Auth::guard('admin')->user()->timezone)}}</td>
                                            </tr>   
                                            
                                            <tr>
                                                <th>{{tr('updated_at')}}</th>
                                                <td>
                                                    {{common_date($document->updated_at , Auth::guard('admin')->user()->timezone)}}
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>{{tr('description')}}</th>
                                                <td>
                                                    
                                                  {{$document->description ?: tr('n_a')}}
                                                    
                                                </td>
                                            </tr>                            
                                    </div>
                              </div>
                         </div>
                     </div>
                </table>
            </div>
        </div>

 </section>

  
@endsection

