@extends('layouts.admin') 

@section('title', tr('products')) 

@section('content-header', tr('orders')) 

@section('breadcrumb')

<li class="breadcrumb-item active"><a href="">{{ tr('orders') }}</a></li>

<li class="breadcrumb-item">{{tr('view_orders')}}</li>

@endsection 

@section('content')

<section class="content">

    <div class="row">

        <div class="col-12">

            <div class="card">

                <div class="card-header border-bottom border-gray">

                        <h4 class="card-title">{{ tr('view_orders') }} - <a href="{{route('admin.user_products.view',['user_product_id' => $product->id])}}">{{$product->name ?: tr('n_a')}}</a>

                        </h4>
                    
                </div>

                <div class="box box-outline-info">

                    <div class="box-body">

                        <div class="table-responsive">

                            <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">
                                
                            <thead>
                                <tr>
                                    <th>{{ tr('product_name') }}</th>
                                    <th>{{ tr('quantity')}}</th>
                                    <th>{{ tr('order_id')}}</th>
                                    <th>{{ tr('per_quantity_price') }}</th>
                                    <th>{{ tr('sub_total') }}</th>
                                    <th>{{ tr('tax_price')}}</th>
                                    <th>{{ tr('delivery_price') }}</th>
                                    <th>{{ tr('total') }}</th>
                                     <th>{{ tr('action') }}</th>
                                </tr>
                            </thead>
                           
                            <tbody>

                                @foreach($order_products as $i => $order_product_details)

                                <tr>

                                    <td>
                                        <a href="{{route('admin.user_products.view',['user_product_id' => $order_product_details->user_product_id])}}">{{ $order_product_details->userProductDetails->name ?? tr('n_a')}}</a>
                                    </td>

                                    <td>{{ $order_product_details->quantity ? : tr('n_a')}}</td>


                                    <td>
                                        <a href="{{route('admin.orders.view',['order_product_id' => $order_product_details->id])}}">{{ $order_product_details->unique_id ?: tr('n_a') }}</a>

                                    </td>


                                    <td>{{ $order_product_details->per_quantity_price_formatted ?:formatted_amount(0)}}</td>

                                    <td>{{ $order_product_details->sub_total_formatted ?: formatted_amount(0)}}</td>

                                    <td>{{ $order_product_details->tax_price_formatted ?: formatted_amount(0)}}</td>

                                    <td>{{ $order_product_details->delivery_price_formatted ?: formatted_amount(0)}}</td>

                                    <td>{{$order_product_details->total_formatted ?: formatted_amount(0)}}</td>
                                    <td>
                                         <a class="btn btn-primary" href="{{route('admin.orders.view',['order_product_id' => $order_product_details->id])}}">&nbsp;{{ tr('view') }}</a> 

                                    </td>
                                </tr>

                                @endforeach

                            </tbody>
                        
                            </table>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>

@endsection