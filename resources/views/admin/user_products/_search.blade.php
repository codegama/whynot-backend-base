
<form method="GET" action="{{route('admin.user_products.index')}}">

    <div class="row product-search-bar">

        <input type="hidden" id="user_id" name="user_id" value="{{Request::get('user_id') ?? ''}}">
        
        <input type="hidden" id="category_id" name="category_id" value="{{Request::get('category_id') ?? ''}}">

        <div class="col-xs-12 col-sm-12 col-lg-4 col-md-12">

            <div class="input-group">
               
                <input type="text" class="form-control" name="search_key" value="{{Request::get('search_key')??''}}"
                placeholder="{{tr('product_search_placeholder')}}">

            </div>
            
        </div>

        <div class="col-xs-12 col-sm-12 col-lg-3 col-md-5 md-full-width resp-mrg-btm-md">

            <select class="form-control select2" name="status">

                <option class="select-color" value="">{{tr('select_status')}}</option>

                <option class="select-color" value="{{SORT_BY_APPROVED}}" @if(Request::get('status') == SORT_BY_APPROVED && Request::get('status')!='' ) selected @endif>{{tr('approved')}}</option>

                <option class="select-color" value="{{SORT_BY_DECLINED}}" @if(Request::get('status') == SORT_BY_DECLINED && Request::get('status')!='' ) selected @endif>{{tr('declined')}}</option>

            </select>
        
        </div>

        <div class="col-xs-12 col-sm-12 col-lg-3 col-md-6 md-full-width resp-mrg-btm-md">

            <select class="form-control select2" name="product_status">

                <option class="select-color" value="">{{tr('select_availability_status')}}</option>

                <option class="select-color" value="{{NO}}" @if(Request::get('product_status') == NO && Request::get('product_status')!='' ) selected @endif>{{tr('out_of_stock')}}</option>

                <option class="select-color" value="{{YES}}" @if(Request::get('product_status') == YES && Request::get('product_status')!='' ) selected @endif>{{tr('in_stock')}}</option>

            </select>
        
        </div>

        <div class="col-xs-12 col-sm-12 col-lg-2 col-md-6 md-full-width resp-mrg-btm-md">

            <span class="input-group-btn">
                &nbsp

                <button type="submit" class="btn btn-default">
                   <a href=""><i class="fa fa-search" aria-hidden="true"></i></a>
                </button>
                
                <a  href="{{route('admin.user_products.index',['user_id'=>Request::get('user_id')??'','category_id'=>Request::get('category_id')??''])}}" class="btn btn-default"><i class="fa fa-eraser" aria-hidden="true"></i></a>
                
            </span>
        
        </div>

    </div>

</form>
<br>