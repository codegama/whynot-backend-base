<option value="">{{tr('select_sub_category')}}</option>

@if(count($sub_categories) > 0)

	@foreach($sub_categories as $sub_category)

		<option value="{{$sub_category->id}}" >{{$sub_category->name}}</option>

	@endforeach

@endif