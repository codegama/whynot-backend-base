@extends('layouts.admin')

@section('title', tr('view_user_products'))

@section('content-header', tr('user_products'))

@section('breadcrumb')

    
    <li class="breadcrumb-item"><a href="{{route('admin.user_products.index')}}">{{tr('user_products')}}</a>
    </li>
    <li class="breadcrumb-item active">{{tr('view_user_products')}}</a>
    </li>

@endsection

@section('content')
    <section class="content">

        <div class="row">

            <div class="col-xl-12 col-lg-12">

                <div class="card user-profile-view-sec">

                    <div class="card-header border-bottom border-gray">

                        <h4 class="card-title">{{tr('view_products')}}-{{Str::limit($user_product->name ,15 ?: tr('n_a'))}}</h4>

                    </div>

                    <div class="card-content">

                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="card profile-with-cover">

                                        <div class="media profil-cover-details">

                                            <div class="media-left pl-2 pt-2">

                                                <a class="profile-image">
                                                    <img src="{{ $user_product->picture ?: asset('product-placeholder.jpeg')}}" alt="{{Str::limit($user_product->name ,15)}}" class="img-thumbnail img-fluid img-border height-100 prod-img-size" alt="Card image">
                                                </a>

                                            </div>
                                           
                                        </div>
                                        
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-6">

                                             @if($user_product->status == APPROVED)
                                             <a class="btn btn-warning btn-block btn-min-width mr-1 mb-1" href="{{route('admin.user_products.status' ,['user_product_id'=> $user_product->id] )}}" onclick="return confirm(&quot;{{$user_product->name}} - {{tr('user_product_decline_confirmation')}}&quot;);">&nbsp;{{tr('decline')}} </a> 
                                            @else

                                            <a  class="btn btn-success btn-block btn-min-width mr-1 mb-1" href="{{route('admin.user_products.status' , ['user_product_id'=> $user_product->id] )}}">&nbsp;{{tr('approve')}}</a> 
                                            @endif

                                        </div>
                                        <div class="col-md-6">
                                            <a class="btn btn-secondary btn-block btn-min-width mr-1 mb-1 " href="{{route('admin.user_products.edit', ['user_product_id'=>$user_product->id] )}}"> &nbsp;{{tr('edit')}}</a>

                                        </div>
                                        <div class="col-md-6">
                                            <a class="btn btn-danger btn-block btn-min-width mr-1 mb-1" onclick="return confirm(&quot;{{tr('user_product_delete_confirmation' , $user_product->name)}}&quot;);" href="{{route('admin.user_products.delete', ['user_product_id'=> $user_product->id] )}}">&nbsp;{{tr('delete')}}</a>
                                        </div>
                                         
                                        <div class="col-md-6">
                                             <a class="btn btn-primary btn-block btn-min-width mr-1 mb-1" href="{{route('admin.order_products',['user_product_id' => $user_product->id])}}">&nbsp;{{tr('orders')}}</a>
                                        
                                        </div>
                                        <div class="col-md-6">
                                            <a class="btn btn-info btn-block btn-min-width mr-1 mb-1" href="{{route('admin.user_products.dashboard',['user_product_id' => $user_product->id])}}">&nbsp;{{tr('dashboard')}}</a>
                                        </div>

                                        @if($user_product->is_outofstock == YES)

                                            <div class="col-md-6">

                                                <a class="btn btn-dark btn-block btn-min-width mr-1 mb-1" href="{{ route('admin.user_products.availability_update' , ['user_product_id' => $user_product->id] ) }}" onclick="return confirm(&quot;{{ $user_product->name }} - {{ tr('user_product_out_of_stock_confirmation') }}&quot;);">&nbsp;{{tr('out_of_stock')}}</a>

                                            </div>

                                        @else

                                            <div class="col-md-6">

                                                <a class="btn btn-info btn-block btn-min-width mr-1 mb-1" href="{{  route('admin.user_products.availability_update' , ['user_product_id' => $user_product->id] )  }}">&nbsp;{{tr('in_stock')}}</a>

                                            </div>
                                        @endif 
                                    </div>   
                                </div>
                            </div>
                           
                        </div>

                        <hr>
                    </div>
  
                    <div class="col-xl-12 col-lg-12">

                        <div class="card">

                            <div class="card-header border-bottom border-gray">

                                  <h4 class="card-title">{{tr('user_product')}}</h4>

                            </div>

                            <div class="card-content">

                                <div class="table-responsive">

                                    <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">
                                        <tr>
                                            <th>{{tr('product_name')}}</th>
                                            <td>{{Str::limit($user_product->name , 15 ?: tr('n_a'))}}</td>
                                        </tr>

                                        <tr>
                                            <th>{{tr('seller_name')}}</th>

                                            <td><a href="{{route('admin.users.view',['user_id' => $user_product->user_id])}}">{{$user_product->user->name ?? tr('n_a')}}</a></td>
                                        </tr>

                                        <tr>
                                            <th>{{tr('quantity')}}</th>
                                            <td>{{$user_product->quantity ?: 0}}</td>
                                        </tr> 

                                        <tr>
                                            <th>{{tr('price')}}</th>
                                            <td>{{$user_product->price ?: 0}}</td>
                                        </tr>

                                        <tr>
                                            <th>{{tr('category')}}</th>
                                            <td><a href="{{route('admin.categories.view',['category_id' => $user_product->category_id])}}">
                                                {{$user_product->productCategories->name ?? tr('n_a')}}</a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>{{tr('sub_category')}}</th>
                                            <td><a href="{{route('admin.sub_categories.view',['sub_category_id' => $user_product->sub_category_id])}}">
                                                {{$user_product->productSubCategories->name ?? tr('n_a')}}</a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>{{tr('status')}}</th>
                                            <td>
                                                @if($user_product->status == APPROVED) 

                                                    <span class="badge badge-success">{{tr('approved')}}</span>

                                                @else
                                                    <span class="badge badge-danger">{{tr('declined')}}</span>

                                                @endif
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>{{tr('is_outofstock')}}</th>
                                            <td>
                                                @if($user_product->is_outofstock == YES) 

                                                    <span class="badge badge-success">{{tr('yes')}}</span>

                                                @else
                                                    <span class="badge badge-danger">{{tr('no')}}</span>

                                                @endif
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>{{tr('description')}}</th>
                                            <td>{!!$user_product->description ?: tr('n_a')!!}</td>
                                        </tr>

                                        <tr>
                                          <th>{{tr('created_at')}} </th>
                                          <td>{{common_date($user_product->created_at , Auth::guard('admin')->user()->timezone)}}</td>
                                        </tr>

                                        <tr>
                                          <th>{{tr('updated_at')}} </th>
                                          <td>{{common_date($user_product->updated_at , Auth::guard('admin')->user()->timezone)}}</td>
                                        </tr>   
                                        
                                    </table>

                                </div>

                            </div>

                        </div>

                    </div>

        </div>

    </div>

</div>

</section> 
    
@endsection

@section('scripts')

@endsection
