@extends('layouts.admin') 

@section('title', tr('view_user_products')) 

@section('content-header', tr('user_products')) 

@section('breadcrumb')

<li class="breadcrumb-item">
    <a href="{{route('admin.user_products.index')}}">{{tr('user_products')}}</a>
</li>

<li class="breadcrumb-item active">{{ tr('view_user_products') }}
</li>

@endsection 

@section('content')

<section class="content">

    <div class="row">

        <div class="col-12">

            <div class="card">

                <div class="card-header border-bottom border-gray">

                    <h4 class="card-title">{{ tr('view_user_products') }}

                        @if($category)
                        - 
                        <a href="{{route('admin.categories.view',['category_id'=>$category->id ?? ''])}}">{{$category->name ?? tr('n_a')}}</a>


                        @elseif($subcategory)
                        - 
                        <a href="{{route('admin.user_products.view',['user_product_id'=>$subcategory->id ?? ''])}}">{{$subcategory->name ?? tr('n_a')}}</a>

                        @elseif($user)

                        -<a href="{{route('admin.users.view',['user_id'=>$user->id ?? ''])}}">{{$user->name ?? tr('n_a')}}</a>
                        
                        @endif  

                    </h4>
                    <a class="heading-elements-toggle"></a>

                    <div class="heading-elements">
                        <a href="{{ route('admin.user_products.create') }}" class="btn btn-primary"><i class="ft-plus icon-left"></i>{{ tr('add_user_product') }}</a>
                    </div>
                    
                </div>

                <div class="box box-outline-info">

                    <div class="box-body">

                        @include('admin.user_products._search')

                        @if(Request::has('search_key'))
                             <div class="col-xs-12 col-sm-12 col-lg-6 col-md-12">
                                    <p class="text-muted">{{tr('search_results_for')}}<b>{{Request::get('search_key')}}</b></p>
                            </div>
                        @endif

                        <div class="table-responsive">

                            <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">
                                
                                <thead>
                                    <tr>
                                        <th>{{ tr('s_no') }}</th>
                                        <th>{{ tr('product_name') }}</th>
                                        <th>{{ tr('seller_name') }}</th>
                                        <th>{{ tr('quantity') }}</th>
                                        <th>{{ tr('price') }}</th>
                                        <th>{{ tr('status') }}</th>
                                        <th>{{ tr('is_outofstock') }}</th>
                                        <th>{{ tr('action') }}</th>
                                    </tr>
                                </thead>
                               
                                <tbody>

                                    @foreach($user_products as $i => $user_product)
                                    <tr>
                                        <td>{{  $i+$user_products->firstItem() }}</td>

                                        <td>
                                            <a href="{{  route('admin.user_products.view' , ['user_product_id' => $user_product->id] )  }}">
                                            {{ Str::limit($user_product->name ,15 ?: tr('n_a'))}}
                                            </a>
                                        </td>

                                        <td> 
                                            <a href="{{  route('admin.users.view' , ['user_id' => $user_product->user_id] )  }}">{{ $user_product->user->name ?? tr('n_a')}}
                                            </a>
                                        </td>

                                        <td>{{ $user_product->quantity ?: 0 }}</td>

                                        <td>{{ $user_product->user_product_price_formatted}}</td>

                                        <td>
                                            @if($user_product->status == APPROVED)

                                                <span class="badge badge-success">{{ tr('approved') }}</span> 
                                            @else

                                                <span class="badge badge-warning">{{ tr('declined') }}</span> 
                                            @endif
                                        </td>

                                        <td>
                                            @if($user_product->is_outofstock == PRODUCT_NOT_AVAILABLE)

                                                <span class="badge badge-warning">{{ tr('no') }}</span> 
                                            @else

                                                <span class="badge badge-success">{{ tr('yes') }}</span> 
                                            @endif
                                        </td>

                                        <td>
                                        
                                            <div class="btn-group" role="group">

                                                <button class="btn btn-outline-primary dropdown-toggle dropdown-menu-right" id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ft-settings icon-left"></i> {{ tr('action') }}</button>

                                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">

                                                    <a class="dropdown-item" href="{{ route('admin.user_products.view', ['user_product_id' => $user_product->id] ) }}">&nbsp;{{ tr('view') }}</a> 

                                                    @if(Setting::get('is_demo_control_enabled') == YES)

                                                        <a class="dropdown-item" href="javascript:void(0)">&nbsp;{{ tr('edit') }}</a>

                                                        <a class="dropdown-item" href="javascript:void(0)">&nbsp;{{ tr('delete') }}</a> 

                                                    @else

                                                        <a class="dropdown-item" href="{{ route('admin.user_products.edit', ['user_product_id' => $user_product->id] ) }}">&nbsp;{{ tr('edit') }}</a>

                                                        <a class="dropdown-item" onclick="return confirm(&quot;{{ tr('user_product_delete_confirmation' , $user_product->name) }}&quot;);" href="{{ route('admin.user_products.delete', ['user_product_id' => $user_product->id] ) }}">&nbsp;{{ tr('delete') }}</a>

                                                    @endif

                                                    @if($user_product->status == APPROVED)

                                                        <a class="dropdown-item" href="{{  route('admin.user_products.status' , ['user_product_id' => $user_product->id] )  }}" onclick="return confirm(&quot;{{ $user_product->name }} - {{ tr('user_product_decline_confirmation') }}&quot;);">&nbsp;{{ tr('decline') }}
                                                    </a> 

                                                    @else

                                                        <a class="dropdown-item" href="{{ route('admin.user_products.status' , ['user_product_id' => $user_product->id] ) }}">&nbsp;{{ tr('approve') }}</a> 

                                                    @endif

                                                    @if($user_product->is_outofstock == PRODUCT_NOT_AVAILABLE)

                                                        <a class="dropdown-item" href="{{  route('admin.user_products.availability_update' , ['user_product_id' => $user_product->id] )  }}">&nbsp;{{ tr('in_stock') }}
                                                    </a> 

                                                    @else

                                                        <a class="dropdown-item" href="{{ route('admin.user_products.availability_update' , ['user_product_id' => $user_product->id] ) }}" onclick="return confirm(&quot;{{ $user_product->name }} - {{ tr('user_product_out_of_stock_confirmation') }}&quot;);">&nbsp;{{ tr('out_of_stock') }}</a> 

                                                    @endif

                                                    <div class="dropdown-divider"></div>

                                                    <!-- <a  class="dropdown-item" href="{{route('admin.product_inventories.index',['user_product_id' => $user_product->id])}}">{{tr('inventory')}}</a> -->

                                                    <a  class="dropdown-item" href="{{route('admin.order_products',['user_product_id' => $user_product->id])}}">{{tr('orders')}}</a>

                                                    <a href="{{route('admin.user_products.dashboard',['user_product_id' => $user_product->id])}}" class="dropdown-item">{{tr('dashboard')}}</a>

                                                </div>

                                            </div>

                                        </td>

                                    </tr>

                                    @endforeach

                                </tbody>
                            
                            </table>

                            <div class="pull-right" id="paglink">{{ $user_products->appends(request()->input())->links() }}</div>


                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>

@endsection