@extends('layouts.admin')

@section('title', tr('users'))

@section('content-header', tr('carts'))

@section('breadcrumb')


<li class="breadcrumb-item active">
    <a href="{{route('admin.users.index')}}">{{ tr('users') }}</a>
</li>

<li class="breadcrumb-item">{{tr('carts')}}</li>

@endsection

@section('content')

<section class="content">

    <div class="row">
        <div class="col-12">
        <div class="card">

            <div class="card-header border-bottom border-gray">

                 
                  <h4 class="card-title">{{tr('carts')}}- <a href="{{ route('admin.users.view', ['user_id' => $user->id] ) }}">{{$user->name ?: tr('not_available')}}</h4></a>

            </div>

            <div class="box box-outline-info">

                <div class="box-body">

                    <div class="table-responsive">

                        <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">

                            <thead>
                                <tr>
                                    <th>{{ tr('s_no') }}</th>
                                    <th>{{ tr('unique_id') }}</th>
                                    <th>{{ tr('user_product_name') }}</th>
                                    <th>{{ tr('quantity') }}</th>
                                    <th>{{ tr('price') }}</th>
                                    <th>{{ tr('added_at')}}</th>
                                    <th>{{ tr('action') }}</th>
                                </tr>
                            </thead>

                            <tbody>

                                @foreach($carts as $i => $cart)

                                <tr>

                                    <td>{{ $i+$carts->firstItem() }}</td>

                                    <td>{{$cart->order->unique_id ?? tr('n_a')}}</td>
                                    

                                    <td>
                                        <a href="{{ route('admin.user_products.view', ['user_product_id' => $cart->user_product_id] ) }}"> 
                                        {{$cart->userProduct->name ?? tr('n_a')}}
                                        </a>
                                    </td>

                                    <td>{{$cart->quantity ?: 0}}</td>

                                    <td>{{$cart->per_quantity_price ?: 0}}</td>

                                    <td>{{common_date($cart->created_at,Auth::guard('admin')->user()->timezone) }}</td>

                                    <td>

                                        <div class="btn-group" role="group">

                                            <button class="btn btn-outline-primary dropdown-toggle dropdown-menu-right" id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ft-settings icon-left"></i> {{ tr('action') }}</button>

                                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">

                                                <a class="dropdown-item" href="{{ route('admin.user_products.view', ['user_product_id' => $cart->user_product_id] ) }}">&nbsp;{{ tr('view') }}</a>

                                                <a class="dropdown-item" href="{{ route('admin.users.carts.remove', ['cart_id' => $cart->id] ) }}">&nbsp;{{ tr('remove') }}</a>
                                                

                                            </div>

                                        </div>

                                    </td>

                                </tr>

                                @endforeach

                            </tbody>

                        </table>

                        <div class="pull-right" id="paglink">{{ $carts->appends(request()->input())->links() }}</div>

                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /card -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

@endsection
