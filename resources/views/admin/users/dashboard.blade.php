@extends('layouts.admin')

@section('content-header', tr('dashboard'))

@section('breadcrumb')

<li class="breadcrumb-item active">{{tr('dashboard')}}</li>

@endsection

@section('content')

<section class="content">


    <div class="row">
        <div class="col-12 ">
            <div class="box">
              <div class="row no-gutters py-2">

              



              </div>
            </div>
        </div>
        <!-- /.col -->
          
      </div>



</section>

@endsection

@section('scripts')

<script src="{{asset('admin-assets/vendors/js/charts/raphael-min.js')}}" type="text/javascript"></script>

<script src="{{asset('admin-assets/vendors/js/charts/morris.min.js')}}" type="text/javascript"></script>

<script type="text/javascript">
    $(window).on("load", function() {

        var e = [<?php foreach ($data->analytics->last_x_days_revenues as $key => $value) {
                        echo '"' . $value->formatted_month . '"' . ',';
                    }
                    ?>
                    ];

        Morris.Area({

            element: "recent_lives",
            data: [],
            xkey: "month",
            ykeys: ["no_of_lives"],
            labels: ["No of Live videos"],
            behaveLikeLine: !0,
            ymax: 300,
            resize: !0,
            pointSize: 0,
            pointStrokeColors: ["#00B5B8", "#FA8E57", "#F25E75"],
            smooth: !0,
            gridLineColor: "#E4E7ED",
            numLines: 6,
            gridtextSize: 14,
            lineWidth: 0,
            fillOpacity: .9,
            hideHover: "auto",
            lineColors: ["#00B5B8", "#FA8E57", "#F25E75"]
        })
    });
</script>



@endsection