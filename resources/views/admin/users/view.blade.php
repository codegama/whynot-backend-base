@extends('layouts.admin')

@section('title', tr('users'))

@section('content-header', tr('users'))

@section('breadcrumb')


<li class="breadcrumb-item"><a href="{{route('admin.users.index')}}">{{tr('users')}}</a>
</li>
<li class="breadcrumb-item active">{{tr('view_user')}}</a>
</li>

@endsection

@section('content')

<section class="content">


        <div class="row">

            <div class="col-xl-12 col-lg-12">

                <div class="card user-profile-view-sec">

                    <div class="card-header border-bottom border-gray">

                        <h4 class="card-title">{{tr('view_users')}}-{{$user->name ?: tr('not_available')}}</h4>

                    </div>

                    <div class="card-content">

                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3">

                                    <div class="card profile-with-cover">

                                        

                                        <div class="media profil-cover-details w-200">

                                            <div class="media-left pl-2 pt-2">

                                                <a class="profile-image">
                                                    <img src="{{ $user->picture}}" alt="{{ $user->name}}" class="img-thumbnail img-fluid img-border height-100 prof-img-size" alt="Card image">
                                                </a>

                                                <span class="prof-img-name-pos">{{tr('profile')}}</span>

                                            </div>
                                           
                                        </div>
                                        
                                    </div>
                                </div>

                                <div class="col-md-3">

                                    <div class="card profile-with-cover">

                                        <div class="media profil-cover-details w-200">

                                            <div class="media-left pl-2 pt-2">

                                                <a class="profile-image">
                                                    <img src="{{ $user->cover}}" alt="{{ $user->name}}" class="img-thumbnail img-fluid img-border height-100 prof-img-size" alt="Card image">
                                                </a>

                                                <span class="prof-img-name-pos">{{tr('cover')}}</span>

                                            </div>
                                           
                                        </div>
                                        
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <br>
                                    <div class="row">
                                        @if(Setting::get('is_demo_control_enabled') == YES)
                                        <div class="col-md-6">
                                            <a class="btn btn-secondary btn-block btn-min-width mr-1 mb-1" href="javascript:void(0)">&nbsp;{{tr('edit')}}</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a class="btn btn-danger btn-block btn-min-width mr-1 mb-1" href="javascript:void(0)">&nbsp;{{tr('delete')}}</a>
                                        </div>
                                        @else
                                        <div class="col-md-6">
                                            <a class="btn btn-secondary btn-block btn-min-width mr-1 mb-1 " href="{{route('admin.users.edit', ['user_id'=>$user->id] )}}"> &nbsp;{{tr('edit')}}</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a class="btn btn-danger btn-block btn-min-width mr-1 mb-1" onclick="return confirm(&quot;{{tr('user_delete_confirmation' , $user->name)}}&quot;);" href="{{route('admin.users.delete', ['user_id'=> $user->id] )}}">&nbsp;{{tr('delete')}}</a>
                                        </div>
                                        @endif

                                    </div>  
                                    
                                    <div class="row">
                                         
                                         <div class="col-md-6">
                                             @if($user->status == APPROVED)
                                                <a class="btn btn-warning btn-block btn-min-width mr-1 mb-1" href="{{route('admin.users.status' ,['user_id'=> $user->id] )}}" onclick="return confirm(&quot;{{$user->name}} - {{tr('user_decline_confirmation')}}&quot;);">&nbsp;{{tr('decline')}} </a>
                                            @else

                                                <a class="btn btn-success btn-block btn-min-width mr-1 mb-1" href="{{route('admin.users.status' , ['user_id'=> $user->id] )}}">&nbsp;{{tr('approve')}}</a>
                                            @endif
                                        
                                        </div>
                                        <div class="col-md-6">
                                            <a href="{{route('admin.user_documents.view', array('user_id'=>$user->id))}}" class="btn btn-primary btn-block btn-min-width mr-1 mb-1">{{tr('documents')}}</a>
                                        </div>
                                    </div>   
                                </div>
                            </div>
                           
                        </div>

                        <hr>
                        <div class="user-view-padding">
                            <div class="row"> 

                                <div class=" col-xl-6 col-lg-6 col-md-12">
                                    <div class="table-responsive">

                                        <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">

                                            <tr >
                                                <th style="border-top: 0">{{tr('name')}}</th>
                                                <td style="border-top: 0">{{$user->name}} 
                                                    @if($user->is_verified_badge == YES)&nbsp;<i class="fa fa-check-circle verify_badge"></i> @endif</td>
                                            </tr>

                                            <tr >
                                                <th style="border-top: 0">{{tr('username')}}</th>
                                                <td style="border-top: 0">{{$user->username}}</td>
                                            </tr>

                                            <tr>
                                                <th>{{tr('email')}}</th>
                                                <td>{{$user->email}}</td>
                                            </tr>
                                            <tr>
                                                <th>{{tr('login_type')}}</th>
                                                <td class="text-capitalize">{{$user->login_by ?: tr('not_available')}}</td>
                                            </tr>

                                            <tr>
                                                <th>{{tr('device_type')}}</th>
                                                <td class="text-capitalize">{{$user->device_type ?: tr('not_available')}}</td>
                                            </tr>

                                            <tr>
                                            
                                                <th>{{tr('is_badge_verified')}}</th>
                                                <td>
                                                    @if($user->is_verified_badge == YES)

                                                    <span class="badge badge-success">{{tr('yes')}}</span>

                                                    @else
                                                    <span class="badge badge-primary">{{tr('no')}}</span>

                                                    @endif
                                                </td>
                                                
                                            </tr>

                                            <!-- @if(Setting::get('is_verified_badge_enabled'))
                                            <tr>
                                            
                                                <th>{{tr('is_badge_verified')}}</th>
                                                <td>
                                                    @if($user->is_verified_badge == YES)

                                                    <span class="badge badge-success">{{tr('yes')}}</span>

                                                    @else
                                                    <span class="badge badge-danger">{{tr('no')}}</span>

                                                    @endif
                                                </td>
                                                
                                            </tr>
                                            @endif -->
                                            
                                            <tr>
                                                <th>{{tr('status')}}</th>
                                                <td>
                                                    @if($user->status == USER_APPROVED)

                                                    <span class="badge badge-success">{{tr('approved')}}</span>

                                                    @else
                                                    <span class="badge badge-danger">{{tr('declined')}}</span>

                                                    @endif
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <th>{{tr('website')}}</th>
                                                <td><a href="{{$user->website}}" target="_blank">{{$user->website ?: tr('not_available')}}</a></td>
                                            </tr>

                                            <tr>
                                                <th>{{tr('account_type')}}</th>
                                                <td>
                                                    @if($user->user_account_type == USER_PREMIUM_ACCOUNT)

                                                    <span class="badge badge-success">{{tr('premium_users')}}</span>

                                                    @else
                                                    <span class="badge badge-danger">{{tr('free_users')}}</span>

                                                    @endif
                                                </td>
                                            </tr>

                                          

                                            <tr>
                                                <th>{{tr('mobile')}}</th>
                                                <td>{{$user->mobile ?: tr('not_available')}}</td>
                                            </tr>

                                            
                                            
                                            <tr>
                                                <th>{{tr('user_wallet_balance')}}</th>
                                                <td>
                                                    {{$user->userWallets->remaining_formatted ?? formatted_amount(0.00)}}
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>{{tr('total_followers')}}</th>
                                                <td>
                                                    
                                                    {{$user->total_followers ?? 0}}
                                                    
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>{{tr('total_following')}}</th>
                                                <td>
                                                    {{$user->total_followings ?? 0}}
                                                
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>{{tr('total_blocked_users')}}</th>
                                                <td>
                                                    
                                                    {{$user->total_blocked_users ?? 0}}
                                                    
                                                </td>
                                            </tr>

                                         
                                            <tr>
                                                <th>{{tr('total_products')}}</th>
                                                <td>
                                                    {{$user->total_user_products ?? 0}}
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>{{tr('email_notification')}}</th>
                                                <td>
                                                    @if($user->is_email_notification == YES)

                                                    <span class="badge badge-success">{{tr('on')}}</span>

                                                    @else
                                                    <span class="badge badge-danger">{{tr('off')}}</span>

                                                    @endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>{{tr('push_notification')}}</th>
                                                <td>
                                                    @if($user->is_push_notification == YES && $user->mobile !='')

                                                    <span class="badge badge-success">{{tr('on')}}</span>

                                                    @else
                                                    <span class="badge badge-danger">{{tr('off')}}</span>

                                                    @endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>{{tr('two_step_auth')}}</th>

                                                <td>
                                                    @if($user->is_two_step_auth_enabled == TWO_STEP_AUTH_DISABLE)

                                                    <span class="badge badge-success">{{ tr('on') }}</span>

                                                    @else

                                                    <span class="badge badge-danger">{{ tr('off') }}</span>

                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>{{tr('created_at')}} </th>
                                                <td>{{common_date($user->created_at , Auth::guard('admin')->user()->timezone)}}</td>
                                            </tr>

                                            <tr>
                                                <th>{{tr('updated_at')}} </th>
                                                <td>{{common_date($user->updated_at , Auth::guard('admin')->user()->timezone)}}</td>
                                            </tr>

                                        </table>
                                        @if(Setting::get('is_referral_enabled') == YES)
                                        <br><br>
                                        <hr>
                                        <div class="card-title"><h4><b>{{tr('referrals')}}</b></h4></div>
                                        
                                        <table class="table table-xl mb-0">
                                           
                                            <tr>
                                                <th>{{tr('referral')}}</th>
                                                <td>{{$referral_code->referral_code }}</td>
                                            </tr>

                                            <tr>
                                                <th>{{tr('total_referrals')}}</th>

                                                <td>
                                                    <a href="{{ route('admin.users.index',['referral_code_id' => $referral_code->referral_code_id])}}">
                                                        {{$referral_code->total_referrals ?? '-'}}
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>{{tr('referral_earning')}}</th>
                                                <td>{{$referral_code->referral_earnings_formatted}}</td>
                                            </tr>
                                            <tr>
                                                <th>{{tr('referee_earnings')}}</th>
                                                <td>{{$referral_code->referee_earnings_formatted}}</td>
                                            </tr>
                                            <tr>
                                                <th>{{tr('total_earnings')}}</th>
                                                <td>{{$referral_code->total_formatted}}</td>
                                            </tr>
                                            <tr>
                                                <th>{{tr('used')}}</th>
                                                <td>{{$user->userWallets ? (formatted_amount($referral_code->total_earnings - $user->userWallets->referral_amount)) : formatted_amount(0.00)}}</td>
                                            </tr>
                                            <tr>
                                                <th>{{tr('remaining')}}</th>
                                                <td>{{$referral_code->remaining_formatted ?? formatted_amount(0.00)}}</td>
                                            </tr>
                                          
                                        </table>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-xl-6 col-lg-6 col-md-12">

                                    <div class="px-2 resp-marg-top-xs">

                                        <div class="card-title"><h4>{{tr('action')}}</h4></div>

                                        <div class="row">

                                           

                                            <div class="col-xl-6 col-lg-6 col-md-12" style="display: none;">

                                                <a href="{{route('admin.delivery_address.index',['user_id' => $user->id])}}" class="btn btn-outline-warning btn-block btn-min-width mr-1 mb-1">{{tr('delivery_address')}}</a>

                                            </div>

                                        
                                            @if($user->is_content_creator == CONTENT_CREATOR)


                                              @else

                                                <div class="col-xl-6 col-lg-6 col-md-12">

                                                    <a href="{{route('admin.users.content_creator_upgrade',['user_id' => $user->id])}}" class="btn btn-warning btn-block btn-min-width mr-1 mb-1" onclick="return confirm(&quot;{{ tr('user_upgrade_to_seller_confirmation' , $user->name) }}&quot;);">{{tr('upgrade_to_seller')}}</a>

                                                </div>

                                            @endif

                                        

                                            <div class="col-xl-6 col-lg-6 col-md-12">

                                                <a class="btn btn-danger btn-block btn-min-width mr-1 mb-1" href="{{ route('admin.user_wallets.view', ['user_id' => $user->id] ) }}">&nbsp;{{ tr('wallet') }}</a>

                                            </div>

                                            <div class="col-xl-6 col-lg-6 col-md-12">

                                                <a class="btn btn-danger btn-block btn-min-width mr-1 mb-1" href="{{ route('admin.user_withdrawals', ['user_id' => $user->id] ) }}">&nbsp;{{ tr('withdrawal') }}</a>

                                            </div>

                                            <div class="col-xl-6 col-lg-6 col-md-12">

                                                <a class="btn btn-info btn-block btn-min-width mr-1 mb-1" href="{{ route('admin.users.view', ['user_id' => $user->id] ) }}" data-toggle="modal" data-target="#{{$user->id}}">&nbsp;{{ ($user->user_account_type  == USER_FREE_ACCOUNT) ? tr('upgrade_to_premium') : tr('update_premium') }}</a>

                                            </div>

                                            <div class="col-xl-6 col-lg-6 col-md-12">

                                                <a class="btn btn-secondary btn-block btn-min-width mr-1 mb-1 " href="{{route('admin.users.billing_accounts', ['user_id'=>$user->id] )}}"> &nbsp;{{tr('billing_accounts')}}</a>

                                            </div>

                                             <div class="col-xl-6 col-lg-6 col-md-12">

                                                <a class="btn btn-secondary btn-block btn-min-width mr-1 mb-1 " href="{{route('admin.delivery_address.index', ['user_id' => $user->id] )}}"> &nbsp;{{tr('delivery_address')}}</a>

                                            </div>

                                             <div class="col-xl-6 col-lg-6 col-md-12">

                                                <a class="btn btn-secondary btn-block btn-min-width mr-1 mb-1 " href="{{ route('admin.orders.index', ['user_id' => $user->id] ) }}"> &nbsp;{{tr('my_orders')}}</a>

                                            </div>

                                            <div class="col-xl-6 col-lg-6 col-md-12">

                                                <a href="{{route('admin.user_followings',['following_id' => $user->id])}}" class="btn btn-success btn-block btn-min-width mr-1 mb-1">{{tr('followings')}}</a>

                                            </div>

                                            <div class="col-xl-6 col-lg-6 col-md-12">

                                                <a href="{{route('admin.user_followers', ['user_id' => $user->id])}}" class="btn btn-warning btn-block btn-min-width mr-1 mb-1">{{tr('followers')}}</a>

                                            </div>

                                             @if($user->is_featured_seller == NO)

                                            <div class="col-xl-6 col-lg-6 col-md-12">

                                                <a class="btn btn-warning btn-block btn-min-width mr-1 mb-1" href="{{route('admin.users.featured_seller_upgrade', ['user_id' => $user->id] )}}" onclick="return confirm(&quot;{{ tr('upgrade_to_feature_seller_confirmation' , $user->name) }}&quot;);">&nbsp;{{tr('is_featured_seller')}}</a>

                                            </div>
                                            @endif

                                            <div class="col-xl-6 col-lg-6 col-md-12">

                                                <a class="btn btn-secondary btn-block btn-min-width mr-1 mb-1 " href="{{route('admin.users.carts', ['user_id'=>$user->id] )}}"> &nbsp;{{tr('cart')}}</a>

                                            </div>

                                            @if($user->is_content_creator == CONTENT_CREATOR)

                                            <div class="col-xl-6 col-lg-6 col-md-12">

                                                <a class="btn btn-primary btn-block btn-min-width mr-1 mb-1" href="{{ route('admin.user_products.index', ['user_id' => $user->id] ) }}">&nbsp;{{ tr('products') }}</a>

                                            </div>

                                            @endif

                                            <div class="col-xl-6 col-lg-6 col-md-12">

                                                <a class="btn btn-info btn-block btn-min-width mr-1 mb-1" href="{{ route('admin.block_users.view', ['user_id' => $user->id, 'type' => BLOCK_BY] ) }}">&nbsp;{{ tr('blocked_users') }}</a>

                                            </div>

                                             @if($user->is_content_creator == CONTENT_CREATOR)

                                                    <div class="col-xl-6 col-lg-6 col-md-12">

                                                         <a class="btn btn-warning btn-block btn-min-width mr-1 mb-1" href="{{ route('admin.orders.index', ['seller_id' => $user->id] ) }}">&nbsp;{{ tr('sold_products') }}</a>

                                                     </div>

                                                    <div class="col-xl-6 col-lg-6 col-md-12">

                                                        <a class="btn btn-danger btn-block btn-min-width mr-1 mb-1" href="{{ route('admin.live_videos.index', ['seller_id' => $user->id] ) }}">&nbsp;{{ tr('live_streaming') }}</a>

                                                    </div>

                                             @else

                                                <div class="col-xl-6 col-lg-6 col-md-12">

                                                     <a class="btn btn-info btn-block btn-min-width mr-1 mb-1" href="{{ route('admin.orders.index', ['user_id' => $user->id] ) }}">&nbsp;{{ tr('my_orders') }}</a>

                                                </div>

                                             @endif

                                                <div class="col-xl-6 col-lg-6 col-md-12">

                                                    @if($user->is_verified_badge == YES)

                                                         <a class="btn btn-danger btn-block btn-min-width mr-1 mb-1" href="{{ route('admin.users.verify_badge', ['user_id' => $user->id] ) }}" onclick="return confirm(&quot;{{ tr('user_un_verify_badge_confirmation' , $user->name) }}&quot;);">&nbsp;{{ tr('un_verify_badge')  }}</a>

                                                    @else

                                                         <a class="btn btn-success btn-block btn-min-width mr-1 mb-1" href="{{ route('admin.users.verify_badge', ['user_id' => $user->id] ) }}" onclick="return confirm(&quot;{{ tr('user_verify_badge_confirmation' , $user->name) }}&quot;);">&nbsp;{{ tr('verify_badge') }}</a>

                                                    @endif

                                                    

                                                </div>

                                    </div>

                                    <div class="table-responsive">

                                        <hr>

                                        <h4>{{tr('social_settings')}}</h4>
                                        <hr>

                                        <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">
                                            <tr >
                                                <th style="border-top: 0">{{tr('amazon_wishlist')}}</th>
                                                <td style="border-top: 0">{{$user->amazon_wishlist ?: tr('n_a')}}</td>
                                            </tr>

                                            <tr>
                                                <th>{{tr('website')}}</th>
                                                <td>{{$user->website ?: tr('n_a')}}</td>
                                            </tr>

                                            <tr>
                                                <th>{{tr('instagram_link')}}</th>
                                                <td>{{$user->instagram_link ?: tr('n_a')}}</td>
                                            </tr>
                                            <tr>
                                                <th>{{tr('facebook_link')}}</th>
                                                <td>{{$user->facebook_link ?: tr('n_a')}}</td>
                                            </tr>
                                            <tr>
                                                <th>{{tr('twitter_link')}}</th>
                                                <td>{{$user->twitter_link ?: tr('n_a')}}</td>
                                            </tr>
                                            <tr>
                                                <th>{{tr('linkedin_link')}}</th>
                                                <td>{{$user->linkedin_link ?: tr('n_a')}}</td>
                                            </tr>
                                            <tr>
                                                <th>{{tr('pinterest_link')}}</th>
                                                <td>{{$user->pinterest_link ?: tr('n_a')}}</td>
                                            </tr>
                                            <tr>
                                                <th>{{tr('youtube_link')}}</th>
                                                <td>{{$user->youtube_link ?: tr('n_a')}}</td>
                                            </tr>
                                            <tr>
                                                <th>{{tr('twitch_link')}}</th>
                                                <td>{{$user->twitch_link ?: tr('n_a')}}</td>
                                            </tr>

                                            <tr>
                                                <th>{{tr('snapchat_link')}}</th>
                                                <td>{{$user->snapchat_link ?: tr('n_a')}}</td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>

                                @include('admin.users._premium_account_form')
                        
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

</section>


@endsection

@section('scripts')

@endsection