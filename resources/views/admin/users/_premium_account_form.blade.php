<div id="{{$user->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog  modal-lg">

        <form action="{{route('admin.users.upgrade_account')}}">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">{{ ($user->user_account_type  == USER_FREE_ACCOUNT) ? tr('upgrade_to_premium') : tr('update_premium') }}</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                @if($user->user_billing_accounts_count <= 0)

                <div class="box-body">
                    <div class="callout bg-pale-secondary">
                        <h4>{{tr('notes')}}</h4>
                        <p>
                            </p><ul>
                                <li>{{tr('upgrade_to_premium_note')}}</li>
                            </ul>
                        <p></p>
                    </div>
                </div>

                @endif
                
                <div class="modal-body">

                    <div class="row">

                        <input type="hidden" name="user_id" value="{{$user->id}}">
                       

                        <div class="col-md-6 premium_account">
                            <h6 class="">
                                <label for="user_name">{{ tr('user_name') }}</label>&nbsp;: &nbsp;
                                <a href="{{route('admin.users.view' , ['user_id' => $user->id])}}">
                                    {{$user->name}}
                                </a>
                            </h6>
                        </div>

                    </div>

                   

                    @if($user->user_billing_accounts_count <= 0)

                        <div class="row">

                            <div class="col-md-12">
                                <hr>
                                <h5><b>{{tr('add_billing_account')}}</b></h5>
                                <hr>
                            </div>

                            <input type="hidden" name="is_billing_account" value="1">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="route_number">{{ tr('route_no') }} *</label><br>
                                    <input type="text" id="route_number" name="route_number" class="form-control" placeholder="{{ tr('route_no') }}" value="" required>

                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="account_number">{{ tr('account_number') }} *</label><br>
                                    <input type="number" id="account_number" name="account_number" class="form-control" placeholder="{{ tr('account_number') }}" value="" required>

                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="type_of_bank">{{ tr('type_of_bank') }} *</label><br>
                                    <select id="type_of_bank" name="bank_type" class="form-control" required>
                                        <option value="{{ tr('savings')}}">{{ tr('savings')}}</option>
                                        <option value="{{ tr('checking')}}">{{ tr('checking')}}</option>
                                    </select>

                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="first_name">{{ tr('first_name') }} *</label><br>
                                    <input type="text" id="first_name" name="first_name" class="form-control" placeholder="{{ tr('first_name') }}" value="" required>

                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="last_name">{{ tr('last_name') }} *</label><br>
                                    <input type="text" id="last_name" name="last_name" class="form-control" placeholder="{{tr('last_name')}}" value="" required>

                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="business_name">{{ tr('business_name') }}</label><br>
                                    <input type="text" id="business_name" name="business_name" class="form-control" placeholder="{{ tr('business_name') }}" value="">

                                </div>
                            </div>
                        </div>

                    @endif
                        
                    <br>

                </div>
                <div class="modal-footer">
                    <div class="pull-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{tr('cancel')}}</button>
                        <button type="submit" class="btn btn-primary">{{tr('submit')}}</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </form>

    </div>

</div>