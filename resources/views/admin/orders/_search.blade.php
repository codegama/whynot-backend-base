<form method="GET" action="{{route('admin.orders.index')}}">

    <div class="row search-form">

        <div class="col-xs-12 col-sm-12 col-lg-3 col-md-6 resp-mrg-btm-md">

        </div>

        <div class="col-xs-12 col-sm-12 col-lg-3 col-md-6 md-full-width resp-mrg-btm-md">

            <select class="form-control select2" name="status">

                <option  class="select-color" value="">{{tr('select_status')}}</option>

                <option  class="select-color" value="{{SORT_BY_ORDER_PLACED}}">{{tr('order_placed')}}</option>

                <option  class="select-color" value="{{SORT_BY_ORDER_SHIPPED}}">{{tr('order_shipped')}}</option>

                <option  class="select-color" value="{{SORT_BY_ORDER_DELIVERD}}">{{tr('order_deliverd')}}</option>

                <option  class="select-color" value="{{SORT_BY_ORDER_CANCELLED}}">{{tr('order_cancelled')}}</option>

            </select>

        </div>

        <div class="col-xs-12 col-sm-12 col-lg-6 col-md-12">

            <div class="input-group">

                @if($seller)
               
                <input type="text" class="form-control" name="search_key" value="{{Request::get('search_key')??''}}"
                placeholder="{{tr('sold_products_search_placeholder')}}"> <span class="input-group-btn">
                &nbsp

                @else

                  <input type="text" class="form-control" name="search_key" value="{{Request::get('search_key')??''}}"
                placeholder="{{tr('order_products_search_placeholder')}}"> <span class="input-group-btn">
                &nbsp

                @endif
         
                <input type="hidden" name="user_id" value="{{Request::get('user_id') ?? ''}}">
                <input type="hidden" name="seller_id" value="{{Request::get('seller_id') ?? ''}}">

                <button type="submit" class="btn btn-default">
                   <a href=""><i class="fa fa-search" aria-hidden="true"></i></a>
                </button>
                
                <a href="{{route('admin.orders.index',['seller_id'=>Request::get('seller_id')??''])}}" class="btn btn-default"><i class="fa fa-eraser" aria-hidden="true"></i>
                </a>
                   
                </span>

            </div>
            
        </div>

    </div>

</form></br>