@extends('layouts.admin') 

@section('title', tr('payments')) 

@section('content-header', tr('order_payment_view')) 

@section('breadcrumb')

<li class="breadcrumb-item">
    <a href="{{route('admin.order.payments')}}">{{ tr('payments') }}</a>
</li>

<li class="breadcrumb-item active">
    <a href="{{route('admin.order.payments')}}">
        {{ tr('order_payments') }}
    </a>
</li>

<li class="breadcrumb-item active">{{ tr('order_payment_view') }}</li>

@endsection 

@section('content')

<section class="content">

    <div class="row">

        <div class="col-12">

            <div class="card">

                <div class="card-header border-bottom border-gray">

                    <h4 class="card-title">{{ tr('order_payment') }}-{{ $order_payment->payment_id}}</h4>
                    
                </div>

                <div class="card-content collapse show">

                    <div class="card-body">

                        <div class="row">
                            
                            <div class="col-md-6">

                                <table class="table table-bordered table-striped tab-content">
                       
                                    <tbody>

                                        <tr>
                                            <td>{{ tr('user')}} </td>
                                            <td>
                                                <a href="{{ route('admin.users.view', ['user_id' => $order_payment->user_id])}}">
                                                {{ $order_payment->user->name ?? "-"}}
                                                </a>
                                            </td>
                                        </tr> 

                                        <tr>
                                            <td>{{ tr('unique_id')}} </td>
                                            <td class="text-uppercase">{{ $order_payment->unique_id}}</td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('payment_id')}} </td>
                                            <td>{{ $order_payment->payment_id}}</td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('payment_mode')}} </td>
                                            <td>{{ $order_payment->payment_mode}}</td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('total')}} </td>
                                            <td>{{ $order_payment->total_formatted}}</td>
                                        </tr> 

                                        <tr>
                                            <td>{{ tr('sub_total') }}</td>
                                            <td>{{ $order_payment->sub_total_formatted}}</td>
                                        </tr>

                                         <tr>
                                            <td>{{tr('delivery_price')}}</td>
                                            <td>{{$order_payment->delivery_price_formatted}}</td>
                                        </tr>
                                    </tbody>

                                </table>

                            </div>

                            <div class="col-md-6">
                                
                                 <table class="table table-bordered table-striped tab-content">
                       
                                    <tbody>

                                        <tr>
                                            <td>{{tr('tax_price')}}</td>
                                            <td>{{$order_payment->tax_price_formatted}}</td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('paid_date') }}</td>
                                            <td>{{common_date($order_payment->paid_date , Auth::guard('admin')->user()->timezone)}}</td>
                                        </tr>

                                          <tr>
                                            <td>{{tr('payment_status')}}</td>
                                            <td>

                                                    @if($order_payment->status == PAID)

                                                    <span class="btn btn-success btn-sm">{{ tr('paid') }}</span>
                                                    @else

                                                    <span class="btn btn-warning btn-sm">{{ tr('not_paid') }}</span>
                                                    @endif
                                                </td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('status') }}</td>
                                            <td>
                                                @if($order_payment->status ==YES)

                                                    <span class="badge bg-success">{{tr('yes')}}</span>

                                                @else 
                                                    <span class="badge bg-danger">{{tr('no')}}</span>

                                                @endif
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('is_failed') }}</td>
                                            <td>
                                                @if($order_payment->is_failed ==YES)

                                                    <span class="badge bg-success">{{tr('yes')}}</span>

                                                @else 
                                                    <span class="badge bg-danger">{{tr('no')}}</span>

                                                @endif
                                            </td>
                                        </tr>

                                        <tr>
                                             @if($order_payment->is_failed ==YES)

                                            <td>{{ tr('failed_reason') }}</td>
                                            <td>{{ $order_payment->failed_reason}}</td>
                                             
                                             @endif

                                        </tr>

                                        <tr>
                                            <td>{{ tr('created_at') }}</td>
                                            <td>{{common_date($order_payment->created_at , Auth::guard('admin')->user()->timezone)}}</td>
                                        </tr>

                                        <tr>
                                            <td>{{ tr('updated_at') }}</td>
                                            <td>{{common_date($order_payment->updated_at , Auth::guard('admin')->user()->timezone)}}</td>
                                        </tr>

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>

@endsection