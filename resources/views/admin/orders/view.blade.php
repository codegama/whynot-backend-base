@extends('layouts.admin') 

@section('title', tr('orders')) 

@section('content-header', tr('orders')) 

@section('breadcrumb')

<li class="breadcrumb-item">
    <a href="{{route('admin.orders.index')}}">{{ tr('orders') }}</a>
</li>

<li class="breadcrumb-item active">{{ tr('view_orders') }}</li>

@endsection 

@section('content')

<section id="configuration">

    <div class="content">

    <div class="row">

        <div class="col-12">

            <div class="card">

                <div class="card-header border-bottom border-gray">

                    <h4 class="card-title">{{ tr('view_order') }}-{{$order_product->unique_id ?: tr('n_a')}}</h4>
                    
                </div>

                <div class="card-body">

                    <div class="row">

                        <div class="col-lg-6">

                            <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">
                
                                <tbody>

                                    <tr>
                                        <td>{{ tr('order_id') }}</td>
                                        <td>{{$order_product->unique_id ?: tr('n_a')}}</td>
                                    </tr>

                                    <tr>
                                        <td>{{ tr('user') }}</td>
                                        <td><a href="{{route('admin.users.view',['user_id'=>$order_product->user_id])}}">{{$order_product->user->name ?? tr('n_a')}}</a></td>
                                    </tr>

                                    <tr>
                                        <td>{{ tr('quantity') }}</td>
                                        <td>{{$order_product->quantity ?: 0}}</td>
                                    </tr>

                                    <tr>
                                        <td>{{ tr('price') }}</td>
                                        <td>{{$order_product->per_quantity_price_formatted ?: 0}}</td>
                                    </tr>
                                    
                                    <tr>
                                        <td>{{tr('sub_total')}}</td>
                                        <td>{{$order_product->sub_total_formatted ?: 0}}</td>
                                    </tr>

                                    <tr>
                                        <td>{{tr('tax_price')}}</td>
                                        <td>{{$order_product->tax_price_formatted ?: 0}}</td>
                                    </tr>

                                    <tr>
                                        <td>{{tr('delivery_price')}}</td>
                                        <td>{{$order_product->delivery_price_formatted ?: 0}}</td>
                                    </tr>

                                    <tr>
                                        <td>{{tr('delivery_address')}}</td>
                                        <td>
                                              <a href="{{  route('admin.delivery_address.view' , ['delivery_address_id' => $order_product->user_id] )  }}">
                                            {{$order_product->order->deliveryAddress->name ?? tr('n_a')}}
                                            </a>    
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>{{tr('total')}}</td>
                                        <td>{{$order_product->total_formatted ?: 0}}</td>
                                    </tr>

                                </tbody>

                            </table>
                        </div>

                        @if($order_product->userProductDetails)

                        <div class="col-lg-6">

                            <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">
                        
                                <tbody>
                                    <tr>
                                        <td>{{tr('user_product')}}</td>
                                        <td>{{$order_product->userProductDetails->name ?? tr('n_a')}}</td>
                                    </tr>

                                    <tr>
                                        <td>{{ tr('is_outofstock') }}</td>

                                        <td>
                                            @if($order_product->userProductDetails->is_outofstock == YES)

                                                <span class="btn btn-success btn-sm">{{ tr('yes') }}</span> 
                                            @else

                                                <span class="btn btn-warning btn-sm">{{ tr('no') }}</span> 
                                            @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>{{ tr('updated_at') }}</td>
                                        <td>{{common_date($order_product->userProductDetails->updated_at , Auth::guard('admin')->user()->timezone)}}</td>
                                    </tr>

                                    <tr>
                                        <td>{{ tr('created_at') }}</td>
                                        <td>{{common_date($order_product->userProductDetails->created_at , Auth::guard('admin')->user()->timezone)}}</td>
                                    </tr>

                                   <!--  <tr>
                                        <td>{{tr('description')}}</td>
                                        <td>{!!$order_product->userProductDetails->description ?? tr('n_a')!!}</td>
                                    </tr>
 -->
                                </tbody>

                            </table>
                            
                        </div>

                        @endif

                    </div>

                </div>


            </div>

        </div>

    </div>

</section>

@endsection

