@extends('layouts.admin') 

@section('title', tr('orders')) 

@section('content-header', tr('orders')) 

@section('breadcrumb')


    
<li class="breadcrumb-item active">
    <a href="">{{ tr('orders') }}</a>
</li>

<li class="breadcrumb-item">
    {{Request::get('new_orders') ? tr('new_orders') : tr('view_orders')}}
</li>

@endsection 

@section('content')

<section id="configuration">

    <div class="row">

        <div class="col-12">

            <div class="card">

                <div class="card-header border-bottom border-gray">

                     @if($user)

                        <h4 class="card-title">{{ tr('new_orders') }} - <a href="{{ route('admin.users.view' , ['user_id' => $user->id] )  }}">{{$user->name ?: tr('n_a')}} </a></h4>

                     @elseif($seller)
                    
                        <h4 class="card-title">{{ tr('sold_products') }} - <a href="{{ route('admin.users.view' , ['user_id' => $seller->id] )  }}">{{($seller->name ?: tr('n_a'))}}</a></h4>

                     @else

                        <h4 class="card-title">{{ tr('view_orders') }}

                    @endif
                    
                </div>

                <div class="box box-outline-info">

                    <div class="box-body">

                        <div class="table-responsive">

                            @include('admin.orders._search')


                            
                            <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">
                                
                                <thead>
                                    <tr>
                                        <th>{{ tr('s_no') }}</th>
                                        <th>{{ tr('user_name') }}</th>
                                        <th>{{ tr('order_id')}}</th>
                                        <th>{{ tr('total_amount')}}</th>    
                                        <th>{{ tr('total_products')}}</th>    
                                        <th>{{ tr('quantity') }}</th>
                                        <th>{{ tr('delivery_address')}}</th>
                                        <th>{{ tr('status') }}</th>
                                        <th>{{ tr('action') }}</th>
                                    </tr>
                                </thead>
                               
                                <tbody>

                                    @foreach($order_products as $i => $order_product)

                                    <tr>
                                        <td>{{ $i + $order_products->firstItem() }}</td>

                                        <td>
                                            <a href="{{  route('admin.users.view' , ['user_id' => $order_product->user_id] )  }}">
                                            {{ $order_product->user->name ?? tr('n_a') }}
                                            </a>
                                        </td>


                                         <td>
                                           <a href="{{route('admin.orders.view',['order_product_id' => $order_product->id])}}">
                                            {{$order_product->unique_id ?: tr('n_a')}}
                                           </a>
                                        </td>

                                        <td>{{$order_product->total_formatted ?: 0}}</td>

                                        <td>{{$order_product->user_product_details_count ?: 0}}</td>
                                          
                                        <td>
                                            {{ $order_product->quantity ?: 0}}
                                        </td>

                                        <td>
                                            @if($order_product->order)
                                            <a href="{{  route('admin.delivery_address.view' , ['delivery_address_id' => $order_product->order->delivery_address_id] )}}">
                                            {{$order_product->order->deliveryAddress->name ?? tr('n_a')}}
                                            </a>
                                            @endif
                                        </td>

                                        <td>
                                            @if($order_product->order)

                                                @if($order_product->order->status == ORDER_CACELLED)
                                                    <span class="badge bg-danger">{{tr('cancelled')}}</span>

                                                @elseif($order_product->order->status == ORDER_SHIPPED)
                                                    <span class="badge bg-secondary">{{tr('shipped')}}</span>

                                                @elseif($order_product->order->status == ORDER_DELIVERD) 
                                                    <span class="badge bg-success">
                                                        {{tr('deliverd')}}
                                                    </span>
                                                @else
                                                    <span class="badge bg-primary">{{tr('placed')}}</span>
                                                @endif
                                            @endif
                                        </td>

                                        <td>
                                        
                                            <a class="btn btn-success" href="{{route('admin.orders.view',['order_product_id' => $order_product->id])}}">{{tr('view')}}</a>

                                        </td>

                                    </tr>

                                    @endforeach

                                </tbody>
                            
                            </table>

                            <div class="pull-right" id="paglink">{{ $order_products->appends(request()->input())->links() }}</div>

                        </div>

                    </div>

            </div>

            </div>

        </div>

    </div>

</section>

@endsection