@extends('layouts.admin') 

@section('title', tr('payments')) 

@section('content-header', tr('payments')) 

@section('breadcrumb')

<li class="breadcrumb-item active">
    <a href="">{{ tr('payments') }}</a>
</li>
<li class="breadcrumb-item">{{tr('order_payments')}}</li>

@endsection 

@section('content')

<section class="content">

    <div class="row">

        <div class="col-12">

            <div class="card">

                <div class="card-header border-bottom border-gray">

                    <h4 class="card-title">{{ tr('order_payments') }}</h4>
                    
                </div>

                <div class="box box-outline-info">
                
                    <div class="box-body">

                        @include('admin.orders._payment_search')

                        <div class="table-responsive">
                        
                        <table id="dataTable" class="table table-bordered table-striped display nowrap margin-top-10">
                                    
                            <thead>
                                <tr>
                                    <th>{{ tr('s_no') }}</th>
                                     <th>{{ tr('order_id')}}</th>
                                    <th>{{ tr('user_name') }}</th>
                                    <th>{{ tr('payment_id') }}</th>
                                    <th>{{ tr('delivery_price') }}</th>
                                    <th>{{ tr('sub_total') }}</th>
                                    <th>{{ tr('total') }}</th>
                                    <th>{{ tr('action') }}</th>
                                </tr>
                            </thead>
                           
                            <tbody>

                                @foreach($order_payments as $i => $order_payment)

                                    <tr>
                                        <td>{{ $i+$order_payments->firstItem() }}</td>

                                          <td><a href="{{route('admin.order.payments.view',['order_payment_id' => $order_payment->id])}}">{{$order_payment->unique_id ?: tr('n_a')}}</a></td>
                                          
                                        <td>
                                            <a href="{{route('admin.users.view',['user_id' => $order_payment->user_id])}}">{{$order_payment->user->name ?? tr('n_a')}}</a>
                                        </td>
                                        <td>
                                            {{ $order_payment->payment_id }}
                                        </td>

                                        <td>
                                            {{ $order_payment->delivery_price_formatted}}
                                        </td>

                                        <td>{{$order_payment->sub_total_formatted}}</td>

                                        <td>{{$order_payment->total_formatted}}</td>

                                        <td><a class="btn btn-info" href="{{route('admin.order.payments.view',['order_payment_id' => $order_payment->id])}}">{{tr('view')}}</a></td>

                                    </tr>

                                @endforeach

                            </tbody>
                        
                        </table>

                        <div class="pull-right" id="paglink">{{ $order_payments->appends(request()->input())->links() }}</div>

                      </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>

@endsection