<?php

Route::group(['middleware' => 'web'], function() {

    Route::group(['as' => 'admin.', 'prefix' => 'admin'], function(){

        Route::get('clear-cache', function() {

            $exitCode = Artisan::call('config:cache');

            return back();

        })->name('clear-cache');

        Route::get('login', 'Auth\AdminLoginController@showLoginForm')->name('login');

        Route::post('login', 'Auth\AdminLoginController@login')->name('login.post');

        Route::get('logout', 'Auth\AdminLoginController@logout')->name('logout');

        /***
         *
         * Admin Account releated routes
         *
         */

        Route::get('profile', 'Admin\AdminAccountController@profile')->name('profile');

        Route::post('profile/save', 'Admin\AdminAccountController@profile_save')->name('profile.save');

        Route::post('change/password', 'Admin\AdminAccountController@change_password')->name('change.password');

        Route::get('/', 'Admin\AdminRevenueController@main_dashboard')->name('dashboard');
        
        // Users CRUD Operations

        Route::get('users', 'Admin\AdminUserController@users_index')->name('users.index');

        Route::get('users/create', 'Admin\AdminUserController@users_create')->name('users.create');

        Route::get('users/edit', 'Admin\AdminUserController@users_edit')->name('users.edit');

        Route::post('users/save', 'Admin\AdminUserController@users_save')->name('users.save');

        Route::get('users/view', 'Admin\AdminUserController@users_view')->name('users.view');

        Route::get('users/delete', 'Admin\AdminUserController@users_delete')->name('users.delete');

        Route::get('users/status', 'Admin\AdminUserController@users_status')->name('users.status');

        Route::get('users/verify', 'Admin\AdminUserController@users_verify_status')->name('users.verify');

        Route::get('users/verify_badge', 'Admin\AdminUserController@users_verify_badge_status')->name('users.verify_badge');

        Route::get('users/excel','Admin\AdminUserController@users_excel')->name('users.excel');

        Route::post('/users/bulk_action', 'Admin\AdminUserController@users_bulk_action')->name('users.bulk_action');

        Route::get('/users/billing_accounts','Admin\AdminUserController@billing_accounts_index')->name('users.billing_accounts');
        
        Route::get('/users/content_creator_upgrade','Admin\AdminUserController@content_creator_upgrade')->name('users.content_creator_upgrade');

        Route::get('/users/featured_seller_upgrade','Admin\AdminUserController@featured_seller_upgrade')->name('users.featured_seller_upgrade');

        Route::get('users/carts', 'Admin\AdminUserController@carts_list')->name('users.carts');

        Route::get('users/remove', 'Admin\AdminUserController@carts_remove')->name('users.carts.remove');

        //user documents 

        Route::get('user-documents', 'Admin\AdminUserController@user_documents_index')->name('user_documents.index');

        Route::get('user-document', 'Admin\AdminUserController@user_documents_view')->name('user_documents.view');
        
        Route::get('user-documents/verify', 'Admin\AdminUserController@user_documents_verify')->name('user_documents.verify');

        Route::get('users/upgrade_account', 'Admin\AdminUserController@user_upgrade_account')->name('users.upgrade_account');

        //followers
         Route::get('followers' , 'Admin\AdminUserController@user_followers')->name('user_followers');

        Route::get('followings', 'Admin\AdminUserController@user_followings')->name('user_followings');

        //user products CRUD Operations

        Route::get('products', 'Admin\AdminProductController@user_products_index')->name('user_products.index');

        Route::get('products/create', 'Admin\AdminProductController@user_products_create')->name('user_products.create');

        Route::get('products/edit', 'Admin\AdminProductController@user_products_edit')->name('user_products.edit');

        Route::post('products/save', 'Admin\AdminProductController@user_products_save')->name('user_products.save');

        Route::get('products/view', 'Admin\AdminProductController@user_products_view')->name('user_products.view');

        Route::get('products/delete', 'Admin\AdminProductController@user_products_delete')->name('user_products.delete');

        Route::get('products/status', 'Admin\AdminProductController@user_products_status')->name('user_products.status');

        Route::get('products/dashboard', 'Admin\AdminProductController@user_products_dashboard')->name('user_products.dashboard');

        Route::get('order_products','Admin\AdminProductController@order_products')->name('order_products');

        // Document CRUD Operations

        Route::get('documents', 'Admin\AdminLookupController@documents_index')->name('documents.index');

        Route::get('documents/create', 'Admin\AdminLookupController@documents_create')->name('documents.create');

        Route::get('documents/edit', 'Admin\AdminLookupController@documents_edit')->name('documents.edit');

        Route::post('documents/save', 'Admin\AdminLookupController@documents_save')->name('documents.save');

        Route::get('documents/view', 'Admin\AdminLookupController@documents_view')->name('documents.view');

        Route::get('documents/delete', 'Admin\AdminLookupController@documents_delete')->name('documents.delete');

        Route::get('documents/status', 'Admin\AdminLookupController@documents_status')->name('documents.status');

        // Documents end

        //orders start

        Route::get('orders' , 'Admin\AdminOrderController@orders_index')->name('orders.index');

        Route::get('orders/view', 'Admin\AdminOrderController@orders_view')->name('orders.view');

        Route::get('order/payments','Admin\AdminRevenueController@order_payments')->name('order.payments');

        Route::get('order/payments/view','Admin\AdminRevenueController@order_payments_view')->name('order.payments.view');

        //orders end

        //delivery address routes start

        Route::get('delivery_address' , 'Admin\AdminOrderController@delivery_address_index')->name('delivery_address.index');

        Route::get('delivery_address/delete', 'Admin\AdminOrderController@delivery_address_delete')->name('delivery_address.delete');

        Route::get('delivery_address/view', 'Admin\AdminOrderController@delivery_address_view')->name('delivery_address.view');

        //delivery address routes end
        
        //user wallet route start

        Route::get('user_wallets' , 'Admin\AdminRevenueController@user_wallets_index')->name('user_wallets.index');

        Route::get('user_wallets/view', 'Admin\AdminRevenueController@user_wallets_view')->name('user_wallets.view');

        Route::get('user_wallet_payments/view', 'Admin\AdminRevenueController@user_wallet_payments_view')->name('user_wallet_payments.view');

        //user wallet route end

        //revenue dashboard start

        Route::get('revenues/dashboard','Admin\AdminRevenueController@revenues_dashboard')->name('revenues.dashboard');

        //revenue dashboard end

        Route::post('categories/get_sub_categories', 'Admin\AdminLookupController@get_sub_categories')->name('get_sub_categories');

        //categories end

        //sub_categories start
        Route::get('sub_categories', 'Admin\AdminLookupController@sub_categories_index')->name('sub_categories.index');

        Route::get('sub_categories/create', 'Admin\AdminLookupController@sub_categories_create')->name('sub_categories.create');

        Route::get('sub_categories/edit', 'Admin\AdminLookupController@sub_categories_edit')->name('sub_categories.edit');

        Route::post('sub_categories/save', 'Admin\AdminLookupController@sub_categories_save')->name('sub_categories.save');

        Route::get('sub_categories/view', 'Admin\AdminLookupController@sub_categories_view')->name('sub_categories.view');

        Route::get('sub_categories/delete', 'Admin\AdminLookupController@sub_categories_delete')->name('sub_categories.delete');

        Route::get('sub_categories/status', 'Admin\AdminLookupController@sub_categories_status')->name('sub_categories.status');



        //sub_categories end

        // CC withdrawals start

        Route::get('user_withdrawals','Admin\AdminRevenueController@user_withdrawals')->name('user_withdrawals');

        Route::get('user_withdrawals/paynow','Admin\AdminRevenueController@user_withdrawals_paynow')->name('user_withdrawals.paynow');

        Route::get('user_withdrawals/reject','Admin\AdminRevenueController@user_withdrawals_reject')->name('user_withdrawals.reject');

         Route::get('user_withdrawals/view','Admin\AdminRevenueController@user_withdrawals_view')->name('user_withdrawals.view');

        // CC withdrawals end

        //inventory route start

        Route::get('product_inventories/index' , 'Admin\AdminRevenueController@product_inventories_index')->name('product_inventories.index');

        Route::get('product_inventories/view', 'Admin\AdminRevenueController@product_inventories_view')->name('product_inventories.view');

        //inventory route end

        //faq CRUD
        Route::get('faqs', 'Admin\AdminLookupController@faqs_index')->name('faqs.index');

        Route::get('faqs/create', 'Admin\AdminLookupController@faqs_create')->name('faqs.create');

        Route::get('faqs/edit', 'Admin\AdminLookupController@faqs_edit')->name('faqs.edit');

        Route::post('faqs/save', 'Admin\AdminLookupController@faqs_save')->name('faqs.save');

        Route::get('faqs/view', 'Admin\AdminLookupController@faqs_view')->name('faqs.view');

        Route::get('faqs/delete', 'Admin\AdminLookupController@faqs_delete')->name('faqs.delete');

        Route::get('faqs/status', 'Admin\AdminLookupController@faqs_status')->name('faqs.status');
        //faq end


        // Static pages start

        Route::get('static_pages' , 'Admin\AdminLookupController@static_pages_index')->name('static_pages.index');

        Route::get('static_pages/create', 'Admin\AdminLookupController@static_pages_create')->name('static_pages.create');

        Route::get('static_pages/edit', 'Admin\AdminLookupController@static_pages_edit')->name('static_pages.edit');

        Route::post('static_pages/save', 'Admin\AdminLookupController@static_pages_save')->name('static_pages.save');

        Route::get('static_pages/delete', 'Admin\AdminLookupController@static_pages_delete')->name('static_pages.delete');

        Route::get('static_pages/view', 'Admin\AdminLookupController@static_pages_view')->name('static_pages.view');

        Route::get('static_pages/status', 'Admin\AdminLookupController@static_pages_status_change')->name('static_pages.status');

        // Static pages end

        // settings

        Route::get('settings-control', 'Admin\AdminSettingController@admin_control')->name('control');

        Route::get('features-control', 'Admin\AdminSettingController@features_control')->name('features_control');

        Route::get('settings', 'Admin\AdminSettingController@settings')->name('settings'); 

        Route::post('settings/save', 'Admin\AdminSettingController@settings_save')->name('settings.save'); 

        Route::post('settings_placeholder_img/save', 'Admin\AdminSettingController@settings_placeholder_img_save')->name('settings_placeholder_img.save'); 

        Route::post('env_settings','Admin\AdminSettingController@env_settings_save')->name('env-settings.save');

        Route::get('block_users', 'Admin\AdminUserController@block_users_index')->name('block_users.index');

        Route::get('block_users/view', 'Admin\AdminUserController@block_users_view')->name('block_users.view');

        Route::get('block_users/delete', 'Admin\AdminUserController@block_users_delete')->name('block_users.delete');

        Route::get('categories', 'Admin\AdminLookupController@categories_index')->name('categories.index');

        Route::get('categories/create', 'Admin\AdminLookupController@categories_create')->name('categories.create');

        Route::get('categories/edit', 'Admin\AdminLookupController@categories_edit')->name('categories.edit');

        Route::post('categories/save', 'Admin\AdminLookupController@categories_save')->name('categories.save');

        Route::get('categories/view', 'Admin\AdminLookupController@categories_view')->name('categories.view');

        Route::get('categories/status', 'Admin\AdminLookupController@categories_status')->name('categories.status');

        Route::get('categories/delete', 'Admin\AdminLookupController@categories_delete')->name('categories.delete');
    
        Route::post('/categories/bulk_action', 'Admin\AdminLookupController@categories_bulk_action')->name('categories.bulk_action');

        Route::get('chat_asset_payments/index','Admin\AdminUserController@chat_asset_payments')->name('chat_asset_payments.index');

        Route::get('chat_asset_payments/view','Admin\AdminUserController@chat_asset_payment_view')->name('chat_asset_payments.view');

        Route::get('live_videos/index','Admin\AdminLiveVideoController@live_videos_index')->name('live_videos.index');

        Route::get('live_videos','Admin\AdminLiveVideoController@live_videos_onlive')->name('live_videos.onlive');

        Route::get('live_videos/view','Admin\AdminLiveVideoController@live_videos_view')->name('live_videos.view');

        Route::get('live_videos/delete','Admin\AdminLiveVideoController@live_videos_delete')->name('live_videos.delete');

        Route::get('live_videos/payments','Admin\AdminLiveVideoController@live_video_payments')->name('live_videos.payments');

        Route::get('live_videos/payments/view','Admin\AdminLiveVideoController@live_video_payments_view')->name('live_videos.payments.view');

        Route::get('users/dashboard', 'Admin\AdminUserController@user_dashboard')->name('users.dashboard');
      
        //referral routes
        Route::get('referrals/index','Admin\AdminUserController@referral_codes')->name('referrals.index');

        Route::get('referrals/view','Admin\AdminUserController@referrals_view')->name('referrals.view');

        Route::get('users/send_week_report', 'Admin\AdminUserController@send_week_report')->name('users.send_week_report');

        Route::get('users/send_monthly_report', 'Admin\AdminUserController@send_monthly_report')->name('users.send_monthly_report');

        Route::get('users/send_custom_report', 'Admin\AdminUserController@send_custom_report')->name('users.send_custom_report');


        // promo_codes CRUD operations

        Route::get('promo_codes/index', 'Admin\AdminPromoCodeController@promo_codes_index')->name('promo_codes.index');

        Route::get('promo_codes/create', 'Admin\AdminPromoCodeController@promo_codes_create')->name('promo_codes.create');

        Route::get('promo_codes/edit', 'Admin\AdminPromoCodeController@promo_codes_edit')->name('promo_codes.edit');

        Route::post('promo_codes/save', 'Admin\AdminPromoCodeController@promo_codes_save')->name('promo_codes.save');

        Route::get('promo_codes/view', 'Admin\AdminPromoCodeController@promo_codes_view')->name('promo_codes.view');

        Route::get('promo_codes/delete', 'Admin\AdminPromoCodeController@promo_codes_delete')->name('promo_codes.delete');

        Route::get('promo_codes/status', 'Admin\AdminPromoCodeController@promo_codes_status')->name('promo_codes.status');

        Route::get('users/report_dashboard', 'Admin\AdminUserController@report_dashboard')->name('users.report_dashboard');

        Route::get('users/weekly_report','Admin\AdminUserController@weekly_report')->name('users.weekly_report');

        Route::get('users/monthly_report','Admin\AdminUserController@monthly_report')->name('users.monthly_report');

        Route::get('users/custom_report','Admin\AdminUserController@custom_report')->name('users.custom_report');

        
        Route::get('products/availability_update', 'Admin\AdminProductController@user_products_availability_update')->name('user_products.availability_update');

        //review start

        Route::get('product_reviews' , 'Admin\AdminProductController@product_reviews_index')->name('product_reviews.index');

        Route::get('product_reviews/view', 'Admin\AdminProductController@product_reviews_view')->name('product_reviews.view');

        //review end

        Route::get('live_video_products','Admin\AdminLiveVideoController@live_video_products_index')->name('live_video_products.index');

        Route::get('live_video_products/view','Admin\AdminLiveVideoController@live_video_products_view')->name('live_video_products.view');

        // Live Videos 

        Route::get('scheduled_videos','Admin\AdminLiveVideoController@live_videos_scheduled')->name('live_videos.scheduled'); // Schedules

    });


});