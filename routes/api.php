<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'user' ,'as' => 'user.', 'middleware' => 'cors'], function() {

    Route::get('get_settings_json', function () {

        $settings_folder = storage_path('public/'.SETTINGS_JSON);

        if(\File::isDirectory($settings_folder)){

        } else {

            \File::makeDirectory($settings_folder, 0777, true, true);

            \App\Helpers\Helper::settings_generate_json();
        }

        $jsonString = file_get_contents(storage_path('app/public/'.SETTINGS_JSON));

        $data = json_decode($jsonString, true);

        return $data;
    
    });

	/***
	 *
	 * User Account releated routs
	 *
	 */
    Route::get('configurations' , 'ApplicationController@configuration_site');

    Route::post('username_validation','Api\UserAccountApiController@username_validation');
    
    Route::any('chat_messages_save', 'ApplicationController@chat_messages_save');

    Route::any('lv_chat_messages_save', 'ApplicationController@lv_chat_messages_save');

    Route::any('get_notifications_count', 'ApplicationController@get_notifications_count');

    Route::post('register','Api\UserAccountApiController@register');
    
    Route::post('login','Api\UserAccountApiController@login');

    Route::post('forgot_password', 'Api\UserAccountApiController@forgot_password');

    Route::post('verify_forgot_password_code', 'Api\UserAccountApiController@verify_forgot_password_code');

    Route::post('reset_password', 'Api\UserAccountApiController@reset_password');


    Route::post('regenerate_email_verification_code', 'Api\UserAccountApiController@regenerate_email_verification_code');

    Route::post('verify_email', 'Api\UserAccountApiController@verify_email');

    Route::any('static_pages_web', 'ApplicationController@static_pages_web');

    Route::any('static_pages', 'ApplicationController@static_pages_api');

    Route::post('chat_users_save', 'Api\UserAccountApiController@chat_users_save');


    Route::group(['middleware' => 'UserApiVal'] , function() {

        Route::post('profile','Api\UserAccountApiController@profile');

        Route::post('update_profile', 'Api\UserAccountApiController@update_profile')->middleware(['CheckEmailVerify']);
        
        Route::post('user_premium_account_check', 'Api\UserAccountApiController@user_premium_account_check');

        Route::post('change_password', 'Api\UserAccountApiController@change_password');

        Route::post('delete_account', 'Api\UserAccountApiController@delete_account');

        Route::post('logout', 'Api\UserAccountApiController@logout');

        Route::post('push_notification_update', 'Api\UserAccountApiController@is_push_notification_change');

        Route::post('email_notification_update', 'Api\UserAccountApiController@is_email_notification_change');

        Route::post('notifications_status_update','Api\UserAccountApiController@notifications_status_update');

        Route::post('lists_index','Api\UserAccountApiController@lists_index');
        
        Route::post('payments_index','Api\UserAccountApiController@payments_index');
        
        Route::post('bell_notifications_index','Api\UserAccountApiController@bell_notifications_index');

        Route::post('verified_badge_status', 'Api\UserAccountApiController@verified_badge_status');


        // Cards management start

        Route::post('cards_add', 'Api\UserAccountApiController@cards_add');

        Route::post('cards_list', 'Api\UserAccountApiController@cards_list');

        Route::post('cards_delete', 'Api\UserAccountApiController@cards_delete');

        Route::post('cards_default', 'Api\UserAccountApiController@cards_default');

        Route::post('payment_mode_default', 'Api\UserAccountApiController@payment_mode_default');

        Route::post('documents_list', 'Api\VerificationApiController@documents_list');

        Route::post('documents_save','Api\VerificationApiController@documents_save');

        Route::post('documents_delete','Api\VerificationApiController@documents_delete');

        Route::post('documents_delete_all','Api\VerificationApiController@documents_delete_all');

        Route::post('user_documents_status','Api\VerificationApiController@user_documents_status');

        Route::post('user_billing_accounts_list','Api\UserAccountApiController@user_billing_accounts_list');

        Route::post('user_billing_accounts_view','Api\UserAccountApiController@user_billing_accounts_view');

        Route::post('billing_accounts_save','Api\UserAccountApiController@user_billing_accounts_save')->middleware(['CheckEmailVerify']);

        Route::post('billing_accounts_delete','Api\UserAccountApiController@user_billing_accounts_delete');
        
        Route::post('user_billing_accounts_default','Api\UserAccountApiController@user_billing_accounts_default');


        //content creater list

        Route::post('content_creators_dashboard','Api\UserAccountApiController@content_creators_dashboard');

        Route::post('delivery_addresses_list','Api\UserAccountApiController@delivery_addresses_list');

        Route::post('delivery_addresses_default','Api\UserAccountApiController@delivery_addresses_default');

        Route::post('delivery_addresses_delete','Api\UserAccountApiController@delivery_addresses_delete');

        Route::post('delivery_addresses_save','Api\UserAccountApiController@delivery_addresses_save');

        Route::post('delivery_addresses_view','Api\UserAccountApiController@delivery_addresses_view');

        Route::post('carts_list','Api\UserProductApiController@carts_list');

        Route::post('carts_save','Api\UserProductApiController@carts_save');

        Route::post('carts_remove','Api\UserProductApiController@carts_remove');

        Route::post('orders_payment_by_stripe', 'Api\UserProductApiController@orders_payment_by_stripe');

        Route::post('orders_payment_by_paypal', 'Api\UserProductApiController@orders_payment_by_paypal');

    });

    Route::post('admin_account_details','Api\WalletApiController@admin_account_details');

    Route::group(['middleware' => ['UserApiVal']], function() {

        Route::post('wallets_index','Api\WalletApiController@user_wallets_index');

        Route::post('wallets_add_money_by_stripe', 'Api\WalletApiController@user_wallets_add_money_by_stripe');

        Route::post('wallets_add_money_by_paypal', 'Api\WalletApiController@user_wallets_add_money_by_paypal');

        Route::post('wallets_add_money_by_bank_account','Api\WalletApiController@user_wallets_add_money_by_bank_account');
       
        Route::post('wallets_history','Api\WalletApiController@user_wallets_history');

        Route::post('wallets_history_for_add','Api\WalletApiController@user_wallets_history_for_add');

        Route::post('wallets_history_for_sent','Api\WalletApiController@user_wallets_history_for_sent');

        Route::post('wallets_history_for_received','Api\WalletApiController@user_wallets_history_for_received');

        Route::post('wallets_payment_view','Api\WalletApiController@user_wallets_payment_view');

        Route::post('wallets_send_money','Api\WalletApiController@user_wallets_send_money');
        // Withdrawls start

        Route::post('withdrawals_index','Api\WalletApiController@user_withdrawals_index');
        
        Route::post('withdrawals_view','Api\WalletApiController@user_withdrawals_view');

        Route::post('withdrawals_search','Api\WalletApiController@user_withdrawals_search');

        Route::post('withdrawals_send_request','Api\WalletApiController@user_withdrawals_send_request');

        Route::post('withdrawals_cancel_request','Api\WalletApiController@user_withdrawals_cancel_request');

        Route::post('withdrawals_check','Api\WalletApiController@user_withdrawals_check');

        Route::post('generate_stripe_payment_intent','Api\WalletApiController@generate_stripe_payment_intent');

        Route::post('user_wallets_stripe_payments_save','Api\WalletApiController@user_wallets_stripe_payments_save');

    });

    Route::group(['middleware' => ['UserApiVal','CheckDocumentVerify']] , function() {

        Route::post('user_products','Api\UserProductApiController@user_products_index');

        Route::post('user_products_for_owner','Api\UserProductApiController@user_products_for_owner');

        Route::post('user_products_orders_list','Api\UserProductApiController@user_products_orders_list');


        Route::post('user_products_save','Api\UserProductApiController@user_products_save');

        Route::post('user_products_view','Api\UserProductApiController@user_products_view');

        Route::post('user_products_delete','Api\UserProductApiController@user_products_delete');

        Route::post('user_products_set_visibility','Api\UserProductApiController@user_products_set_visibility');

        Route::post('user_products_update_availability','Api\UserProductApiController@user_products_update_availability');

        Route::post('product_categories','Api\UserProductApiController@product_categories');

        Route::post('product_sub_categories','Api\UserProductApiController@product_sub_categories');

        Route::post('user_products_search','Api\UserProductApiController@user_products_search');

        Route::post('user_product_pictures' , 'Api\UserProductApiController@user_product_pictures');

        Route::post('user_product_pictures_save','Api\UserProductApiController@user_product_pictures_save');

        Route::post('user_product_pictures_delete','Api\UserProductApiController@user_product_pictures_delete');

        Route::post('sold_product_list','Api\UserProductApiController@sold_product_list');

        Route::post('product_reviews_save', 'Api\UserProductApiController@product_reviews_save');

        Route::post('ecommerce_home','Api\UserProductApiController@ecommerce_home');

        Route::post('user_products_view_for_others','Api\UserProductApiController@user_products_view_for_others');

        Route::post('orders_list','Api\UserProductApiController@orders_list');

        Route::post('orders_view','Api\UserProductApiController@orders_view');

        Route::post('orders_list_for_seller','Api\UserProductApiController@orders_list_for_seller');

        Route::post('orders_status_update','Api\UserProductApiController@orders_status_update');

        Route::post('orders_cancel','Api\UserProductApiController@orders_cancel');

        Route::post('order_payments_list','Api\UserProductApiController@order_payments_list');

        Route::post('orders_create_by_paypal','Api\UserProductApiController@orders_create_by_paypal');

        Route::post('orders_create_by_stripe','Api\UserProductApiController@orders_create_by_stripe');

        Route::post('orders_create_by_wallet','Api\UserProductApiController@orders_create_by_wallet');

        Route::post('orders_list_for_others','Api\UserProductApiController@orders_list_for_others');

        Route::post('orders_view_for_others','Api\UserProductApiController@orders_view_for_others');

    });

    Route::group(['middleware' => 'UserApiVal'] , function() {

        // Followers and Followings list for content creators
        Route::post('followers', 'Api\FollowersApiController@followers');

        Route::post('followings', 'Api\FollowersApiController@followings');

        Route::post('active_followers', 'Api\FollowersApiController@active_followers');

        Route::post('active_followings', 'Api\FollowersApiController@active_followings');

        Route::post('follow_users','Api\FollowersApiController@follow_users');

        Route::post('unfollow_users','Api\FollowersApiController@unfollow_users');

        Route::post('follow_category_sub_categories','Api\FollowersApiController@follow_category_sub_categories');

        Route::post('chat_assets', 'Api\ChatApiController@chat_assets_index');

        Route::post('chat_assets_save', 'Api\ChatApiController@chat_assets_save');

        Route::post('chat_assets_delete', 'Api\ChatApiController@chat_assets_delete');
        

        Route::post('chat_assets_payment_by_stripe', 'Api\ChatApiController@chat_assets_payment_by_stripe');

        Route::post('chat_assets_payment_by_wallet', 'Api\ChatApiController@chat_assets_payment_by_wallet');

        Route::post('chat_assets_payment_by_paypal', 'Api\ChatApiController@chat_assets_payment_by_paypal');

        Route::post('chat_asset_payments', 'Api\ChatApiController@chat_assets_payments_list');

        Route::post('chat_asset_payments_view', 'Api\ChatApiController@chat_assets_payments_view');

        Route::post('chat_users_search','Api\ChatApiController@chat_users_search');

        Route::post('chat_messages_search','Api\ChatApiController@chat_messages_search');

        Route::post('home_search','Api\HomeApiController@home_search');

    });

    Route::post('other_profile','Api\UserAccountApiController@other_profile');

    Route::post('home','Api\PostsApiController@home');

    Route::post('users_search', 'Api\FollowersApiController@users_search');

    Route::post('user_suggestions', 'Api\FollowersApiController@user_suggestions');

    Route::post('chat_users','Api\FollowersApiController@chat_users');

    Route::post('chat_messages','Api\FollowersApiController@chat_messages')->middleware(['CheckEmailVerify']);

    Route::post('block_users_save','Api\UserAccountApiController@block_users_save');

    Route::post('block_users','Api\UserAccountApiController@block_users');
  

    Route::post('other_model_product_list','Api\UserProductApiController@other_model_product_list');


    Route::group(['middleware' => 'UserApiVal'] , function() {

        // Live videos

        Route::post('live_videos_create', 'Api\LiveVideoApiController@live_videos_create');

        Route::post('live_videos_broadcast_create', 'Api\LiveVideoApiController@live_videos_broadcast_create');

        Route::post('live_videos_check_streaming', 'Api\LiveVideoApiController@live_videos_check_streaming');

        Route::post('live_videos_viewer_update', 'Api\LiveVideoApiController@live_videos_viewer_update');

        Route::post('live_videos_snapshot_save', 'Api\LiveVideoApiController@live_videos_snapshot_save');

        Route::post('live_videos_broadcast_stop', 'Api\LiveVideoApiController@live_videos_broadcast_stop');

        Route::post('live_videos_broadcast_start', 'Api\LiveVideoApiController@live_videos_broadcast_start');

        Route::post('live_videos_erase_old_streamings', 'Api\LiveVideoApiController@live_videos_erase_old_streamings');

        Route::post('live_videos_owner_list', 'Api\LiveVideoApiController@live_videos_owner_list');

        Route::post('live_videos_owner_view', 'Api\LiveVideoApiController@live_videos_owner_view');

        Route::post('live_videos_scheduled_owner', 'Api\LiveVideoApiController@live_videos_scheduled_owner');

        Route::post('live_videos_search', 'Api\LiveVideoApiController@live_videos_search');

        Route::post('live_videos', 'Api\LiveVideoApiController@live_videos');

        Route::post('live_videos_view', 'Api\LiveVideoApiController@live_videos_view');

        Route::post('live_videos_payment_by_card', 'Api\LiveVideoApiController@live_videos_payment_by_card');

        Route::post('live_videos_payment_by_paypal', 'Api\LiveVideoApiController@live_videos_payment_by_paypal');

        Route::post('live_videos_payment_by_wallet', 'Api\LiveVideoApiController@live_videos_payment_by_wallet');

        Route::post('live_videos_payment_history', 'Api\LiveVideoApiController@live_videos_payment_history');

        Route::post('live_video_chat_message_save_api','Api\LiveVideoApiController@live_video_chat_message_save_api');
        
        Route::post('live_video_chat_messages','Api\LiveVideoApiController@live_video_chat_messages');

        Route::post('live_video_bookmarks_save','Api\LiveVideoApiController@live_video_bookmarks_save');

        Route::post('live_video_bookmarks','Api\LiveVideoApiController@live_video_bookmarks');

        Route::post('referral_code', 'Api\ReferralApiController@referral_code');

        Route::post('live_video_products_list', 'Api\LiveVideoApiController@live_video_products_list');
        
        Route::post('live_videos_orders_list', 'Api\LiveVideoApiController@live_videos_orders_list');

        // Home Page Api's Start

        Route::post('following_categories_list', 'Api\HomeApiController@following_categories_list');

        Route::post('following_category_videos', 'Api\HomeApiController@following_category_videos');

        Route::post('following_sub_category_videos', 'Api\HomeApiController@following_sub_category_videos');

        // Home Page Api's End

        Route::post('become_a_seller', 'Api\HomeApiController@become_a_seller');
    });

    Route::post('ongoing_live_videos', 'Api\HomeApiController@ongoing_live_videos');

    Route::post('category_videos', 'Api\HomeApiController@category_videos');

    Route::post('sub_category_videos', 'Api\HomeApiController@sub_category_videos');

    Route::post('explore_categories', 'Api\HomeApiController@explore_categories');

    Route::post('recent_categories', 'Api\HomeApiController@recent_categories');


    Route::post('upcoming_live_streamings', 'Api\LiveVideoApiController@upcoming_live_streamings');

    Route::post('live_streamings_ongoing', 'Api\LiveVideoApiController@live_streamings_ongoing');
    
    Route::post('featured_sellers_home', 'Api\UserAccountApiController@featured_sellers_home');

    Route::post('categories_home', 'Api\LookupApiController@categories_home');

    Route::post('categories', 'Api\LookupApiController@categories');

    Route::post('sub_categories', 'Api\LookupApiController@sub_categories');

    Route::post('categories_list', 'Api\LookupApiController@categories_list');

    Route::post('referral_code_validate', 'Api\ReferralApiController@referral_code_validate');

    Route::post('promo_code_index', 'Api\PromocodeApiController@promo_code_index');

    Route::post('promo_code_save', 'Api\PromocodeApiController@promo_code_save');

    Route::post('promo_code_delete', 'Api\PromocodeApiController@promo_code_delete');

    Route::post('login_session_index', 'Api\UserAccountApiController@login_session_index');

    Route::post('two_step_auth_update', 'Api\UserAccountApiController@two_step_auth_update');

    Route::post('two_step_auth_login', 'Api\UserAccountApiController@two_step_auth_login');

    Route::post('two_step_auth_resend_code', 'Api\UserAccountApiController@two_step_auth_resend_code');

    Route::get('is_demo_enable_api', 'Api\DemoController@is_demo_enable_api');

    Route::get('demo_control_status_update_api', 'Api\DemoController@demo_control_status_update_api');

    Route::get('user_demo_update', 'Api\DemoController@user_demo_update');

    Route::get('user_demo_login_check', 'Api\DemoController@user_demo_login_check');

    Route::get('admin_demo_update', 'Api\DemoController@admin_demo_update');

    Route::get('admin_demo_login_check', 'Api\DemoController@admin_demo_login_check');

});