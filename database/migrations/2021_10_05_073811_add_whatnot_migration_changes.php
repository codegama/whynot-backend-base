<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWhatnotMigrationChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if(!Schema::hasTable('product_reviews')) {

            Schema::create('product_reviews', function (Blueprint $table) {
                $table->id();
                $table->string('unique_id')->default(rand());
                $table->integer('order_id')->default(0);
                $table->integer('user_product_id')->default(0);
                $table->integer('user_id')->default(0);
                $table->integer('seller_id')->default(0);
                $table->integer('ratings')->default(0);
                $table->string('review')->default('');
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
        }

        if(!Schema::hasTable('live_video_wishlists')) {

            Schema::create('live_video_wishlists', function (Blueprint $table) {
                $table->id();
                $table->string('unique_id')->default(rand());
                $table->integer('user_id');
                $table->integer('live_video_id');
                $table->tinyInteger('status')->default(APPROVED);
                $table->timestamps();
            });
        }

        if(!Schema::hasTable('category_followers')) {

            Schema::create('category_followers', function (Blueprint $table) {
                $table->id();
                $table->integer('user_id')->comment("login user id - content_creators,users");
                $table->integer('category_id')->default(0);
                $table->integer('sub_category_id')->default(0);
                $table->integer('status');
                $table->timestamps();
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_reviews');
        Schema::dropIfExists('live_video_wishlists');
        Schema::dropIfExists('category_followers');

    }
}
