<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLookupRelatedMigrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if(!Schema::hasTable('static_pages')) {

            Schema::create('static_pages', function (Blueprint $table) {
                $table->id();
                $table->string('unique_id')->default(uniqid());
                $table->string('title')->unique();
                $table->text('description');
                $table->enum('type',['about','privacy','terms','refund','cancellation','faq','help','contact','others'])->default('others');
                $table->string('section_type')->nullable();
                $table->tinyInteger('status')->default(APPROVED);
                $table->timestamps();
            });

        }

        if(!Schema::hasTable('settings')) {

            Schema::create('settings', function (Blueprint $table) {
                $table->id();
                $table->string('key');
                $table->text('value');
                $table->tinyInteger('status')->default(YES);
                $table->timestamps();
            });  
        }

        if(!Schema::hasTable('faqs')) {

            Schema::create('faqs', function (Blueprint $table) {
                $table->id();
                $table->string('unique_id')->default(rand());
                $table->string('question');
                $table->text('answer');
                $table->tinyInteger('status')->default(YES);            
                $table->timestamps();
            });
        }

        if(!Schema::hasTable('bell_notifications')) {

            Schema::create('bell_notifications', function (Blueprint $table) {
                $table->id();
                $table->string('unique_id')->default(rand());
                $table->integer('from_user_id');
                $table->integer('to_user_id');
                $table->string('image')->default("");
                $table->string('subject')->default("");
                $table->text('message');
                $table->integer('live_video_id')->default(0);
                $table->string('action_url')->default("/home");
                $table->string('notification_type')->default(BELL_NOTIFICATION_TYPE_FOLLOW);
                $table->tinyInteger('is_read')->default(0);
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });

        }

        if(!Schema::hasTable('page_counters')) {

            Schema::create('page_counters', function (Blueprint $table) {
                $table->id();
                $table->string('page');
                $table->integer('count');
                $table->timestamps();
            });

        }


        if(!Schema::hasTable('categories')) {

            Schema::create('categories', function (Blueprint $table) {
                $table->id();
                $table->string('unique_id')->default(rand());
                $table->string('name');
                $table->text('description')->nullable();
                $table->string('picture')->default(asset('placeholder.jpeg'));
                $table->tinyInteger('status')->default(YES);
                $table->timestamps();
            });

        }

        if(!Schema::hasTable('promo_codes')) {

            Schema::create('promo_codes', function (Blueprint $table) {
                $table->id();
                $table->string('unique_id')->default(uniqid());
                $table->string('title')->default("");
                $table->string('promo_code')->unique();
                $table->string('amount_type')->default(0);
                $table->float('amount')->default(0);
                $table->dateTime('start_date')->nullable();
                $table->dateTime('expiry_date')->nullable();
                $table->integer('user_id')->default(0);
                $table->string('description')->default("");
                $table->smallInteger('no_of_users_limit')->nullable();
                $table->tinyInteger('per_users_limit')->nullable();
                $table->tinyInteger('status')->default(APPROVED);
                $table->timestamps();
            });

        }

        if(!Schema::hasTable('documents')) {

            Schema::create('documents', function (Blueprint $table) {
                $table->id();
                $table->string('unique_id')->default(rand());
                $table->string('name');
                $table->string('image_type')->default('jpg');
                $table->string('picture')->default(asset('document.jpg'));
                $table->text('description')->nullable();
                $table->tinyInteger('is_required')->default(1);
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });

        }

         if(!Schema::hasTable('sub_categories')) {

            Schema::create('sub_categories', function (Blueprint $table) {
                $table->id();
                $table->string('unique_id')->default(rand());
                $table->string('name');
                $table->integer('category_id');
                $table->text('description')->nullable();
                $table->string('picture')->default(asset('cat-placeholder.jpeg'));
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('static_pages');
        Schema::dropIfExists('settings');
        Schema::dropIfExists('faqs');
        Schema::dropIfExists('bell_notifications');
        Schema::dropIfExists('page_counters');
        Schema::dropIfExists('documents');
        Schema::dropIfExists('promo_codes');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('sub_categories');
    }
}
