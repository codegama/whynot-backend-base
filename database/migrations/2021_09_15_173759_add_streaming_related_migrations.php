<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStreamingRelatedMigrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(!Schema::hasTable('live_videos')) {

            Schema::create('live_videos', function (Blueprint $table) {
                $table->id();
                $table->string('unique_id')->default(rand());
                $table->integer('user_id');
                $table->integer('category_id')->default(0);
                $table->integer('sub_category_id')->default(0);
                $table->string('agora_token')->default(uniqid());
                $table->string('virtual_id')->default(uniqid());
                $table->string('type')->default(TYPE_PUBLIC)->comment('Public, Private');
                $table->string('broadcast_type')->default(BROADCAST_TYPE_BROADCAST);
                $table->integer('payment_status')->default(0)->comment('0 - No, 1 - Yes');
                $table->string('title')->default('');
                $table->text('description')->nullabe();
                $table->string('browser_name')->default('')->comment("Store Streamer Browser Name");
                $table->float('amount')->default(0.00);
                $table->integer('is_streaming')->default(0);
                $table->string('preview_file')->default(asset('images/live-streaming.jpeg'));
                $table->tinyInteger('live_schedule_type')->default(LIVE_SCHEDULE_TYPE_NOW);
                $table->string('preview_file_type')->default('image');
                $table->datetime('schedule_time')->nullable();
                $table->string('video_url')->nullabe();
                $table->integer('viewer_cnt')->default(0);
                $table->time('start_time')->nullable();
                $table->time('end_time')->nullable();
                $table->integer('no_of_minutes')->default(0);
                $table->tinyInteger('status')->default(1);
                $table->softDeletes();
                $table->timestamps();
            });
            
        }

        if(!Schema::hasTable('live_video_products')) {

            Schema::create('live_video_products', function (Blueprint $table) {
                $table->id();
                $table->string('unique_id')->default(rand());
                $table->integer('live_video_id')->default(0);
                $table->integer('user_id')->default(0);
                $table->integer('user_product_id')->default(0);
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
        }

        if(!Schema::hasTable('viewers')) {

            Schema::create('viewers', function (Blueprint $table) {
                $table->id();
                $table->string('unique_id')->default(rand());
                $table->integer('live_video_id');
                $table->integer('user_id');
                $table->integer('count')->default(0);
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
            
        }


        if(!Schema::hasTable('live_video_chat_messages')) {

            Schema::create('live_video_chat_messages', function (Blueprint $table) {
                $table->id();
                $table->string('unique_id')->default(rand());
                $table->integer('from_user_id');
                $table->integer('live_video_id')->default(0);
                $table->text('message')->nullable();
                $table->tinyInteger('status')->default(0);
                $table->timestamps();
            });

        }

        if(!Schema::hasTable('live_video_bookmarks')) {

            Schema::create('live_video_bookmarks', function (Blueprint $table) {
                $table->id();
                $table->string('unique_id')->default(rand());
                $table->integer('user_id');
                $table->integer('live_video_id');
                $table->tinyInteger('status')->default(APPROVED);
                $table->timestamps();
            });

        }


        if(!Schema::hasTable('live_video_payments')) {

            Schema::create('live_video_payments', function (Blueprint $table) {
                $table->increments('id');
                $table->string('unique_id')->default(rand());
                $table->integer('live_video_id');
                $table->integer('user_id');
                $table->integer('live_video_viewer_id');
                $table->string('payment_id');
                $table->string('payment_mode')->default(CARD);
                $table->float('live_video_amount')->default(0.00);
                $table->float('amount')->default(0.00);
                $table->float('admin_amount')->default(0.00);
                $table->float('user_amount')->default(0.00);
                $table->string('currency')->default('$');
                $table->tinyInteger('status')->default(1);
                $table->softDeletes();
                $table->timestamps();
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('live_videos');
        Schema::dropIfExists('viewers');
        Schema::dropIfExists('live_video_products');
        Schema::dropIfExists('live_video_payments');
        Schema::dropIfExists('live_video_chat_messages');
        Schema::dropIfExists('live_video_bookmarks');
    }
}
