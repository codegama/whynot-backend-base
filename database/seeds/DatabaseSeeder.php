<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SettingSeeder::class);
        $this->call(DemoSeeder::class);
        $this->call(PageDemoSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(CartSeeder::class);
        $this->call(OfflineProductSeeder::class);

    }
}
