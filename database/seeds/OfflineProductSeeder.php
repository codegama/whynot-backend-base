<?php

use Illuminate\Database\Seeder;

class OfflineProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('settings')->insert([
            [
                'key' => 'is_offline_products_available',
                'value' => NO,
            ]
        ]);
    }
}
