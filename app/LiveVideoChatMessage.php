<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveVideoChatMessage extends Model
{
    protected $hidden = ['id','unique_id'];

	protected $appends = ['lv_chat_message_id', 'lv_chat_message_unique_id', 'from_username', 'from_displayname', 'from_userpicture', 'from_user_unique_id'];

	public function getLvChatMessageIdAttribute() {

		return $this->id;
	}

	public function getLvChatMessageUniqueIdAttribute() {

		return $this->unique_id;
	}

	public function getFromUsernameAttribute() {

		return $this->fromUser->username ?? tr('n_a');

		unset($this->fromUser);
	}

	public function getFromUserPictureAttribute() {

		return $this->fromUser->picture ?? asset('placeholder.jpeg');

		unset($this->fromUser);
	}

	public function getFromDisplaynameAttribute() {

		return $this->fromUser->name ?? tr('n_a');

		unset($this->fromUser);
	}

	public function getFromUserUniqueIdAttribute() {

		return $this->fromUser->unique_id ?? '';

		unset($this->fromUser);
	}

	public function fromUser() {

	   return $this->belongsTo(User::class, 'from_user_id');
	}

	public function liveVideo() {

	   return $this->belongsTo(LiveVideo::class, 'live_video_id');
	}
}
