<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryFollower extends Model
{
    /**
     * Load follower using relation model
     */
    public function getCategory()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommonResponse($query) {

        return $query->leftJoin('categories' , 'categories.id' ,'=' , 'category_followers.category_id')
            ->leftJoin('sub_categories' , 'sub_categories.id' ,'=' , 'category_followers.sub_category_id')
			->select(
                \DB::raw('IFNULL(categories.unique_id,"") as category_unique_id'),
                \DB::raw('IFNULL(sub_categories.unique_id,"") as sub_category_unique_id'),
                \DB::raw('IFNULL(categories.name,"") as category_name'),
                \DB::raw('IFNULL(sub_categories.name,"") as sub_category_name'),
                'category_followers.user_id',
                'category_followers.category_id',
                'category_followers.sub_category_id',
                'category_followers.created_at',
                'category_followers.updated_at'
            );
    
    }

    public function categoryFollower() {

        return $this->belongsTo(CategoryFollower::class,'category_id');
    }

    public function user() {

        return $this->belongsTo(User::class,'user_id');
    }

    public function category() {

        return $this->belongsTo(Category::class,'category_id');
    }

    public function subCategory() {

        return $this->belongsTo(SubCategory::class,'sub_category_id');
    }

}
