<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $appends = ['total_formatted','tax_price_formatted','sub_total_formatted','order_btn_status','order_status','order_status_formatted'];

    public function getTotalFormattedAttribute() {

    	return formatted_amount($this->total);
    }

    public function getTaxPriceFormattedAttribute() {

    	return formatted_amount($this->tax_price);
    }

    public function getSubTotalFormattedAttribute() {

    	return formatted_amount($this->sub_total);
    }

    public function user() {

    	return $this->belongsTo(User::class,'user_id');
    } 

    public function deliveryAddress() {

    	return $this->belongsTo(DeliveryAddress::class,'delivery_address_id');
    }

    public function orderProducts() {

        return $this->hasMany(OrderProduct::class, 'order_id');
    }

    public function orderPayments() {

        return $this->hasMany(OrderPayment::class, 'order_id');
    }   

    public function getOrderBtnStatusAttribute() {

        return order_btn_status($this->status);
    }

    public function getOrderStatusFormattedAttribute() {

        return order_status_formatted($this->status);
    }

    public function getOrderStatusAttribute() {

        return order_status($this->status);
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {
            $model->attributes['unique_id'] = "O"."-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "O"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

        static::deleting(function ($model) {

            foreach ($model->orderProducts as $key => $orderProduct) {

                $orderProduct->delete();

            }
            
            $model->orderPayments()->delete();

        });

    }
}
