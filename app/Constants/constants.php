<?php

/*
|--------------------------------------------------------------------------
| Application Constants
|--------------------------------------------------------------------------
|
| 
|
*/

if(!defined('TAKE_COUNT')) define('TAKE_COUNT', 6);

if(!defined('NO')) define('NO', 0);
if(!defined('YES')) define('YES', 1);

if(!defined('PAID')) define('PAID',1);
if(!defined('UNPAID')) define('UNPAID', 0);

if(!defined('DEVICE_ANDROID')) define('DEVICE_ANDROID', 'android');
if(!defined('DEVICE_IOS')) define('DEVICE_IOS', 'ios');
if(!defined('DEVICE_WEB')) define('DEVICE_WEB', 'web');

if(!defined('MALE')) define('MALE', 'male');
if(!defined('FEMALE')) define('FEMALE', 'female');
if(!defined('OTHERS')) define('OTHERS', 'others');
if(!defined('RATHER_NOT_SELECT')) define('RATHER_NOT_SELECT', 'rather-not-select');

if(!defined('APPROVED')) define('APPROVED', 1);
if(!defined('DECLINED')) define('DECLINED', 0);

if(!defined('DEFAULT_TRUE')) define('DEFAULT_TRUE', true);
if(!defined('DEFAULT_FALSE')) define('DEFAULT_FALSE', false);

if(!defined('ADMIN')) define('ADMIN', 'admin');
if(!defined('USER')) define('USER', 'user');
if(!defined('ContentCreator')) define('ContentCreator', 'creator');

if(!defined('COD')) define('COD',   'COD');
if(!defined('PAYPAL')) define('PAYPAL', 'PAYPAL');
if(!defined('CARD')) define('CARD',  'CARD');
if(!defined('BANK_TRANSFER')) define('BANK_TRANSFER',  'BANK_TRANSFER');
if(!defined('PAYMENT_OFFLINE')) define('PAYMENT_OFFLINE',  'OFFLINE');
if(!defined('PAYMENT_MODE_WALLET')) define('PAYMENT_MODE_WALLET',  'WALLET');
if(!defined('CCBILL')) define('CCBILL', 'CCBILL');

if(!defined('STRIPE_MODE_LIVE')) define('STRIPE_MODE_LIVE',  'live');
if(!defined('STRIPE_MODE_SANDBOX')) define('STRIPE_MODE_SANDBOX',  'sandbox');

//////// USERS

if(!defined('USER_PENDING')) define('USER_PENDING', 2);

if(!defined('USER_APPROVED')) define('USER_APPROVED', 1);

if(!defined('USER_DECLINED')) define('USER_DECLINED', 0);

if(!defined('USER_EMAIL_NOT_VERIFIED')) define('USER_EMAIL_NOT_VERIFIED', 0);

if(!defined('USER_EMAIL_VERIFIED')) define('USER_EMAIL_VERIFIED', 1);


if(!defined('CONTENT_CREATOR_EMAIL_NOT_VERIFIED')) define('CONTENT_CREATOR_EMAIL_NOT_VERIFIED', 0);

if(!defined('CONTENT_CREATOR_EMAIL_VERIFIED')) define('CONTENT_CREATOR_EMAIL_VERIFIED', 1);

//////// USERS END

/***** ADMIN CONTROLS KEYS ********/

if(!defined('ADMIN_CONTROL_ENABLED')) define('ADMIN_CONTROL_ENABLED', 1);
if(!defined('ADMIN_CONTROL_DISABLED')) define('ADMIN_CONTROL_DISABLED', 0);

if(!defined('NO_DEVICE_TOKEN')) define("NO_DEVICE_TOKEN", "NO_DEVICE_TOKEN");

if(!defined('PLAN_TYPE_MONTH')) define('PLAN_TYPE_MONTH', 'months');

if(!defined('PLAN_TYPE_YEAR')) define('PLAN_TYPE_YEAR', 'years');

if(!defined('PLAN_TYPE_WEEK')) define('PLAN_TYPE_WEEK', 'weeks');

if(!defined('PLAN_TYPE_DAY')) define('PLAN_TYPE_DAY', 'days');

if(!defined('TODAY')) define('TODAY', 'today');

if(!defined('COMPLETED')) define('COMPLETED',3);

if(!defined('SORT_BY_APPROVED')) define('SORT_BY_APPROVED',1);

if(!defined('SORT_BY_DECLINED')) define('SORT_BY_DECLINED',0);

if(!defined('SORT_BY_EMAIL_VERIFIED')) define('SORT_BY_EMAIL_VERIFIED',3);

if(!defined('SORT_BY_EMAIL_NOT_VERIFIED')) define('SORT_BY_EMAIL_NOT_VERIFIED',4);

if(!defined('SORT_BY_DOCUMENT_VERIFIED')) define('SORT_BY_DOCUMENT_VERIFIED',5);

if(!defined('SORT_BY_DOCUMENT_APPROVED')) define('SORT_BY_DOCUMENT_APPROVED',6);

if(!defined('SORT_BY_DOCUMENT_PENDING')) define('SORT_BY_DOCUMENT_PENDING',7);


if(!defined('STATIC_PAGE_SECTION_1')) define('STATIC_PAGE_SECTION_1', 1);

if(!defined('STATIC_PAGE_SECTION_2')) define('STATIC_PAGE_SECTION_2', 2);

if(!defined('STATIC_PAGE_SECTION_3')) define('STATIC_PAGE_SECTION_3', 3);

if(!defined('STATIC_PAGE_SECTION_4')) define('STATIC_PAGE_SECTION_4', 4);

if(!defined('USER_DOCUMENT_VERIFIED')) define('USER_DOCUMENT_VERIFIED', 1);


if(!defined('STARDOM')) define('STARDOM', 'stardom');

if(!defined('USER'))  define('USER', 'user');

if(!defined('FREE')) define('FREE', 3);


if(!defined('SORT_BY_ORDER_PLACED')) define('SORT_BY_ORDER_PLACED',1);

if(!defined('SORT_BY_ORDER_SHIPPED')) define('SORT_BY_ORDER_SHIPPED',2);

if(!defined('SORT_BY_ORDER_DELIVERD')) define('SORT_BY_ORDER_DELIVERD',3);

if(!defined('SORT_BY_ORDER_CANCELLED')) define('SORT_BY_ORDER_CANCELLED',4);

if(!defined('ORDER_PLACED')) define('ORDER_PLACED',1);

if(!defined('ORDER_SHIPPED')) define('ORDER_SHIPPED',2);

if(!defined('ORDER_DELIVERD')) define('ORDER_DELIVERD',3);

if(!defined('ORDER_CACELLED')) define('ORDER_CACELLED',4);

if(!defined('PAYMENT_OFFLINE')) define('PAYMENT_OFFLINE','offline_payment');

if(!defined('WITHDRAW_INITIATED')) define('WITHDRAW_INITIATED', 0);

if(!defined('WITHDRAW_PAID')) define('WITHDRAW_PAID', 1);

if(!defined('WITHDRAW_ONHOLD')) define('WITHDRAW_ONHOLD', 2);

if(!defined('WITHDRAW_DECLINED')) define('WITHDRAW_DECLINED', 3);

if(!defined('WITHDRAW_CANCELLED')) define('WITHDRAW_CANCELLED', 4);



if(!defined('USER_WALLET_PAYMENT_INITIALIZE')) define('USER_WALLET_PAYMENT_INITIALIZE', 0);
if(!defined('USER_WALLET_PAYMENT_PAID')) define('USER_WALLET_PAYMENT_PAID', 1);
if(!defined('USER_WALLET_PAYMENT_UNPAID')) define('USER_WALLET_PAYMENT_UNPAID', 2);
if(!defined('USER_WALLET_PAYMENT_CANCELLED')) define('USER_WALLET_PAYMENT_CANCELLED', 3);
if(!defined('USER_WALLET_PAYMENT_DISPUTED')) define('USER_WALLET_PAYMENT_DISPUTED', 4);
if(!defined('USER_WALLET_PAYMENT_WAITING')) define('USER_WALLET_PAYMENT_WAITING', 5);


// amount_type - add and debitedd
if(!defined('WALLET_AMOUNT_TYPE_ADD')) define('WALLET_AMOUNT_TYPE_ADD', 'add');
if(!defined('WALLET_AMOUNT_TYPE_MINUS')) define('WALLET_AMOUNT_TYPE_MINUS', 'minus');

// payment type - specifies the transaction usage
if(!defined('WALLET_PAYMENT_TYPE_ADD')) define('WALLET_PAYMENT_TYPE_ADD', 'add');
if(!defined('WALLET_PAYMENT_TYPE_PAID')) define('WALLET_PAYMENT_TYPE_PAID', 'paid');
if(!defined('WALLET_PAYMENT_TYPE_CREDIT')) define('WALLET_PAYMENT_TYPE_CREDIT', 'credit');
if(!defined('WALLET_PAYMENT_TYPE_WITHDRAWAL')) define('WALLET_PAYMENT_TYPE_WITHDRAWAL', 'withdrawal');

if (!defined('PAID_STATUS')) define('PAID_STATUS', 1);


// Subscribed user status

if(!defined('SUBSCRIBED_USER')) define('SUBSCRIBED_USER', 1);

if(!defined('NON_SUBSCRIBED_USER')) define('NON_SUBSCRIBED_USER', 0);

if(!defined('TAKE_COUNT')) define('TAKE_COUNT', 12);

if(!defined('SHOW')) define('SHOW', 1);

if(!defined('HIDE')) define('HIDE', 0);

if(!defined('READ')) define('READ', 1);

if(!defined('UNREAD')) define('UNREAD', 0);

// AUTORENEWAL STATUS

if(!defined('AUTORENEWAL_ENABLED')) define('AUTORENEWAL_ENABLED',0);

if(!defined('AUTORENEWAL_CANCELLED')) define('AUTORENEWAL_CANCELLED',1);

if(!defined('PRODUCT_AVAILABLE')) define('PRODUCT_AVAILABLE',1);

if(!defined('PRODUCT_NOT_AVAILABLE')) define('PRODUCT_NOT_AVAILABLE',0);

if(!defined('PRODUCT_SOLD')) define('PRODUCT_SOLD', 2);

if(!defined('PUBLISHED')) define('PUBLISHED',1);

if(!defined('UNPUBLISHED')) define('UNPUBLISHED', 0);


if(!defined('USER_DOCUMENT_NONE')) define('USER_DOCUMENT_NONE', 0);
if(!defined('USER_DOCUMENT_PENDING')) define('USER_DOCUMENT_PENDING', 1);
if(!defined('USER_DOCUMENT_APPROVED')) define('USER_DOCUMENT_APPROVED', 2);
if(!defined('USER_DOCUMENT_DECLINED')) define('USER_DOCUMENT_DECLINED', 3);

if(!defined('USER_FREE_ACCOUNT')) define('USER_FREE_ACCOUNT', 0);
if(!defined('USER_PREMIUM_ACCOUNT')) define('USER_PREMIUM_ACCOUNT', 1);

if(!defined('BOOKMARK_TYPE_ALL')) define('BOOKMARK_TYPE_ALL', 'all');
if(!defined('BOOKMARK_TYPE_VIDEOS')) define('BOOKMARK_TYPE_VIDEOS', 'videos');
if(!defined('BOOKMARK_TYPE_LOCKED')) define('BOOKMARK_TYPE_LOCKED', 'locked');
if(!defined('BOOKMARK_TYPE_OTHERS')) define('BOOKMARK_TYPE_OTHERS', 'others');

// Bell notification status

if(!defined('BELL_NOTIFICATION_STATUS_UNREAD')) define('BELL_NOTIFICATION_STATUS_UNREAD', 1);

if(!defined('BELL_NOTIFICATION_STATUS_READ')) define('BELL_NOTIFICATION_STATUS_READ', 2);


if(!defined('FOLLOWER_ACTIVE')) define('FOLLOWER_ACTIVE', 1);

if(!defined('FOLLOWER_EXPIRED')) define('FOLLOWER_EXPIRED', 0);

if(!defined('BELL_NOTIFICATION_TYPE_FOLLOW')) define('BELL_NOTIFICATION_TYPE_FOLLOW', 'follow');

if(!defined('PRODUCTION')) define('PRODUCTION', 'production');
if(!defined('SANDBOX')) define('SANDBOX', 'sandbox');

if(!defined('FILE_TYPE_IMAGE')) define('FILE_TYPE_IMAGE', 'image');
if(!defined('FILE_TYPE_VIDEO')) define('FILE_TYPE_VIDEO', 'video');
if(!defined('FILE_TYPE_AUDIO')) define('FILE_TYPE_AUDIO', 'audio');
if(!defined('FILE_TYPE_TEXT')) define('FILE_TYPE_TEXT', 'text');

if(!defined('STORAGE_TYPE_S3')) define('STORAGE_TYPE_S3', 1);
if(!defined('STORAGE_TYPE_LOCAL')) define('STORAGE_TYPE_LOCAL', 0);


if(!defined('USAGE_TYPE_WITHDRAW')) define('USAGE_TYPE_WITHDRAW', 'withdraw');

if(!defined('USAGE_TYPE_REFERRAL')) define('USAGE_TYPE_REFERRAL', 'referral');

if(!defined('USAGE_TYPE_CHAT')) define('USAGE_TYPE_CHAT', 'chat');

if(!defined('USAGE_TYPE_PRODUCT')) define('USAGE_TYPE_PRODUCT', 'product');


if(!defined('SORT_BY_HIGH')) define('SORT_BY_HIGH',1);

if(!defined('SORT_BY_LOW')) define('SORT_BY_LOW',2);

if(!defined('SORT_BY_FREE')) define('SORT_BY_FREE',3);

if(!defined('SORT_BY_PAID')) define('SORT_BY_PAID',4);


if(!defined('TYPE_PUBLIC')) define('TYPE_PUBLIC', 'public');
if(!defined('TYPE_PRIVATE')) define('TYPE_PRIVATE', 'private');

if(!defined('BROADCAST_TYPE_BROADCAST')) define('BROADCAST_TYPE_BROADCAST', 'broadcast');
if(!defined('BROADCAST_TYPE_CONFERENCE')) define('BROADCAST_TYPE_CONFERENCE', 'conference');
if(!defined('BROADCAST_TYPE_SCREENSHARE')) define('BROADCAST_TYPE_SCREENSHARE', 'screenshare');

if(!defined('PAID_VIDEO')) define('PAID_VIDEO', 1);

if(!defined('FREE_VIDEO')) define('FREE_VIDEO', 0);


// VIDEO STATUS

if (!defined('VIDEO_STREAMING_STOPPED')) define('VIDEO_STREAMING_STOPPED' , 1);

if (!defined('VIDEO_STREAMING_ONGOING')) define('VIDEO_STREAMING_ONGOING' , 0);

if (!defined('VIDEO_STREAMING_CREATED')) define('VIDEO_STREAMING_CREATED' , 2);

// VIDEO STATUS

if (!defined('IS_STREAMING_YES')) define('IS_STREAMING_YES' , 1);

if (!defined('IS_STREAMING_NO')) define('IS_STREAMING_NO' , 0);


if (!defined('LIVE_VIDEO')) define('LIVE_VIDEO' , 'live_video');


if(!defined('WATERMARK_TOP_LEFT')) define('WATERMARK_TOP_LEFT','top-left');
if(!defined('WATERMARK_TOP_RIGHT')) define('WATERMARK_TOP_RIGHT','top-right');
if(!defined('WATERMARK_BOTTOM_LEFT')) define('WATERMARK_BOTTOM_LEFT','bottom-left');
if(!defined('WATERMARK_BOTTOM_RIGHT')) define('WATERMARK_BOTTOM_RIGHT','bottom-right');
if(!defined('WATERMARK_CENTER')) define('WATERMARK_CENTER','center');

if(!defined('BANK_TYPE_SAVINGS')) define('BANK_TYPE_SAVINGS', 'savings');
if(!defined('BANK_TYPE_CHECKING')) define('BANK_TYPE_CHECKING', 'checking');

if(!defined('DEFAULT_USER')) define('DEFAULT_USER', 1);
if(!defined('CONTENT_CREATOR')) define('CONTENT_CREATOR', 2);

if(!defined('WEELKY_REPORT')) define('WEELKY_REPORT', 1);

if(!defined('MONTHLY_REPORT')) define('MONTHLY_REPORT', 2);

if(!defined('CUSTOM_REPORT')) define('CUSTOM_REPORT', 3);

if(!defined('PERCENTAGE')) define('PERCENTAGE',0);

if(!defined('ABSOULTE')) define('ABSOULTE',1);

if(!defined('PROMO_CODE_APPLIED')) define('PROMO_CODE_APPLIED',1);
if(!defined('PROMO_CODE_NOT_APPLIED')) define('PROMO_CODE_NOT_APPLIED', 0);

if (!defined('CONTENT_CREATOR_INITIAL')) define('CONTENT_CREATOR_INITIAL' , 0);
if (!defined('CONTENT_CREATOR_DOC_UPLOADED')) define('CONTENT_CREATOR_DOC_UPLOADED' , 1);
if (!defined('CONTENT_CREATOR_DOC_VERIFIED')) define('CONTENT_CREATOR_DOC_VERIFIED' , 2);
if (!defined('CONTENT_CREATOR_BILLING_UPDATED')) define('CONTENT_CREATOR_BILLING_UPDATED' , 3);
if (!defined('CONTENT_CREATOR_APPROVED')) define('CONTENT_CREATOR_APPROVED' , 5);

if (!defined('IS_CURRENT_SESSION')) define('IS_CURRENT_SESSION' , 1);

if (!defined('IS_CURRENT_SESSION_NO')) define('IS_CURRENT_SESSION_NO' , 0);

if(!defined('TWO_STEP_AUTH_DISABLE')) define('TWO_STEP_AUTH_DISABLE', 0);

if(!defined('TWO_STEP_AUTH_ENABLE')) define('TWO_STEP_AUTH_ENABLE', 1);

if(!defined('BLOCK_BY')) define('BLOCK_BY', 1);

if(!defined('BLOCKED_TO')) define('BLOCKED_TO', 2);


if(!defined('LIVE_SCHEDULE_TYPE_NOW')) define('LIVE_SCHEDULE_TYPE_NOW', 1);
if(!defined('LIVE_SCHEDULE_TYPE_LATER')) define('LIVE_SCHEDULE_TYPE_LATER', 2);

if(!defined('IS_FEATURED_USER')) define('IS_FEATURED_USER', 0);
if(!defined('IS_FEATURED_SELLER')) define('IS_FEATURED_SELLER', 1);

if(!defined('DOCUMENT_PENDING')) define('DOCUMENT_PENDING', 0);
if(!defined('DOCUMENT_APPROVED')) define('DOCUMENT_APPROVED', 1);
if(!defined('DOCUMENT_DECLINED')) define('DOCUMENT_DECLINED', 2);

if (!defined('BECOME_A_SELLER_STEP_ONE')) define('BECOME_A_SELLER_STEP_ONE', 1); // Agreed Rules
if (!defined('BECOME_A_SELLER_STEP_TWO')) define('BECOME_A_SELLER_STEP_TWO', 2); // Document Uploaded
if (!defined('BECOME_A_SELLER_STEP_THREE')) define('BECOME_A_SELLER_STEP_THREE', 3); // Document Verified
if (!defined('BECOME_A_SELLER_STEP_FOUR')) define('BECOME_A_SELLER_STEP_FOUR', 4); // Billing Updated
if (!defined('BECOME_A_SELLER_STEP_FIVE')) define('BECOME_A_SELLER_STEP_FIVE', 5); // All Steps Completed

if(!defined('SELLER')) define('SELLER', 2);

if(!defined('DEFAULT_CURRENCY_CODE')) define('DEFAULT_CURRENCY_CODE', "USD");