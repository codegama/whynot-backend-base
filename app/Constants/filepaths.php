<?php

/*
|--------------------------------------------------------------------------
| Application Constants
|--------------------------------------------------------------------------
|
| 
|
*/

if(!defined('COMMON_FILE_PATH')) define('COMMON_FILE_PATH', 'uploads/images/');

if(!defined('PROFILE_PATH_USER')) define('PROFILE_PATH_USER', 'uploads/users/');

if(!defined('PROFILE_PATH_ADMIN')) define('PROFILE_PATH_ADMIN', 'uploads/admins/');

if(!defined('FILE_PATH_SITE')) define('FILE_PATH_SITE', 'uploads/sites/');

if(!defined('SETTINGS_JSON')) define('SETTINGS_JSON', 'default-json/settings.json');

if(!defined('DOCUMENTS_PATH')) define('DOCUMENTS_PATH', 'uploads/documents/');

if(!defined('PRODUCT_FILE_PATH')) define('PRODUCT_FILE_PATH', 'uploads/products/');

if(!defined('CATEGORY_FILE_PATH')) define('CATEGORY_FILE_PATH', 'uploads/categories/');

if(!defined('CHAT_ASSETS_PATH')) define('CHAT_ASSETS_PATH', 'uploads/chat-assets/');

if(!defined('PUBLIC_HOME')) define('PUBLIC_HOME', '/');

if(!defined('PUBLIC_COMMON_FILE_PATH')) define('PUBLIC_COMMON_FILE_PATH', 'images/');

if(!defined('USER_DOCUMENTS_PATH')) define('USER_DOCUMENTS_PATH', 'uploads/user_documents/');

