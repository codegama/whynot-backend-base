<?php

namespace App\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Http\Request;

use Illuminate\Http\Exceptions\HttpResponseException;

use Illuminate\Validation\Rule;

use App\Models\{Document, UserDocument};

class BecomeASellerRequest extends FormRequest
{   
    public function __construct(Request $request) {

        $request->request->add(['user_id' => $request->id]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {   
        switch ($request->step) {

            case 1:

                return [ 'user_id' => ['required', 'exists:users,id'] ];

                break;

            case 2:

                return [
                    'user_id' => ['required', 'exists:users,id'],
                    'document_id' => ['required', 'exists:documents,id'],
                    'document_file' => ['required', 'mimes:jpeg,jpg,png', 'exclude_unless:document_file,null'],
                ];

                break;

            case 3:

                return [
                    'user_id' => ['required', 'exists:users,id'],
                    'first_name' => ['required', 'max:50'],
                    'last_name' => ['required', 'max:50'],
                    'bank_name' => ['nullable', 'exclude_if:bank_name,null'],
                    'bank_type' => ['required', Rule::in([BANK_TYPE_SAVINGS, BANK_TYPE_CHECKING])],
                    'route_number' => ['required'],
                    'account_number' => ['required', 'unique:user_billing_accounts,account_number'],
                    'nickname' => ['nullable'],
                    'business_name' => ['nullable', 'max:50'],
                    'account_holder_name' => ['nullable', 'exclude_if:account_holder_name,null'],
                    'ifsc_code' => ['nullable', 'exclude_if:ifsc_code,null'],
                    'swift_code' => ['nullable', 'exclude_if:swift_code,null'],
                    'iban_number' => ['nullable', 'exclude_if:iban_number,null'],
                ];

                break;
            
            default:
            
                return [ 'step' => ['required', 'numeric', Rule::in([1,2,3])] ];
        }
    }

    /**
    * Custom Validation Errors.
    *
    * @return array
    */
    public function messages()
    {
        return [];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @return void
     */
    public function failedValidation(Validator $validator)
    {

        throw new HttpResponseException(response()->json([
            'success'   => false,
            'error'   => $validator->errors()->first(),
            'error_code' => 422
        ]));
    }
}
