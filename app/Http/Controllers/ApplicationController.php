<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Log, Validator, Exception, DB, Setting;

use App\Helpers\Helper;

use App\{StaticPage,User};

class ApplicationController extends Controller
{

    protected $loginUser;

    public function __construct(Request $request) {
        
        $this->loginUser = User::find($request->id);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

    }

    /**
     * @method static_pages_api()
     *
     * @uses to get the pages
     *
     * @created Vidhya R 
     *
     * @edited Vidhya R
     *
     * @param - 
     *
     * @return JSON Response
     */

    public function static_pages_api(Request $request) {

        $base_query = \App\StaticPage::where('status', APPROVED)->orderBy('title', 'asc');
                                
        if($request->page_type) {

            $static_pages = $base_query->where('type' , $request->page_type)->first();

        } elseif($request->page_id) {

            $static_pages = $base_query->where('id' , $request->page_id)->first();

        } elseif($request->unique_id) {

            $static_pages = $base_query->where('unique_id' , $request->unique_id)->first();

        } else {

            $static_pages = $base_query->get();

        }

        $response_array = ['success' => true , 'data' => $static_pages ? $static_pages->toArray(): []];

        return response()->json($response_array , 200);

    }

    /**
     * @method static_pages_api()
     *
     * @uses to get the pages
     *
     * @created Bhawya
     *
     * @updated Bhawya
     *
     * @param - 
     *
     * @return JSON Response
     */

    public function static_pages_web(Request $request) {

        $static_page = StaticPage::where('unique_id' , $request->unique_id)
                            ->Approved()
                            ->first();

        $response_array = ['success' => true , 'data' => $static_page];

        return response()->json($response_array , 200);

    }

    /**
     * @method chat_messages_save()
     * 
     * @uses - To save the chat message.
     *
     * @created vidhya R
     *
     * @updated vidhya R
     * 
     * @param 
     *
     * @return No return response.
     *
     */

    public function chat_messages_save(Request $request) {

        try {

            Log::info("message_save".print_r($request->all() , true));

            DB::beginTransaction();
            
            $rules = [
                'from_user_id' => 'required|exists:users,id',
                'to_user_id' => 'required|exists:users,id',
                'message' => 'required',
            ];

            Helper::custom_validator($request->all(),$rules);
            
            $message = $request->message;

            // $from_chat_user_inputs = ['from_user_id' => $request->from_user_id, 'to_user_id' => $request->to_user_id];

            \App\Repositories\CommonRepository::chat_user_update($request->from_user_id,$request->to_user_id);

            \App\Repositories\CommonRepository::chat_user_update($request->to_user_id,$request->from_user_id);

            // $from_chat_user = \App\ChatUser::updateOrCreate($from_chat_user_inputs);

            // $to_chat_user_inputs = ['from_user_id' => $request->to_user_id, 'to_user_id' => $request->from_user_id];

            // $to_chat_user = \App\ChatUser::updateOrCreate($to_chat_user_inputs);

            // $from_chat_user = \App\ChatUser::where('from_user_id', $request->from_user_id)->where('to_user_id', $request->to_user_id)->first();

            // if(!$from_chat_user) {

            //     $chat_user = \App\ChatUser::createor(['from_user_id' => $request->from_user_id, 'to_user_id' => $request->to_user_id]);

            // }

            // $to_chat_user = \App\ChatUser::where('from_user_id', $request->to_user_id)->where('to_user_id', $request->from_user_id)->first();

            // if(!$to_chat_user) {

            //     $chat_user = \App\ChatUser::create(['from_user_id' => $request->from_user_id, 'to_user_id' => $request->to_user_id]);

            // }

            if($request->chat_message_id) {

                $chat_message = \App\ChatMessage::find($request->chat_message_id) ?? new \App\ChatMessage;

            } else {

               $chat_message = new \App\ChatMessage;
 
            }
            
            $chat_message->from_user_id = $request->from_user_id;

            $chat_message->to_user_id = $request->to_user_id;

            $chat_message->message = $request->message;

            $chat_message->save();

            DB::commit();

            return $this->sendResponse("", "", $chat_message);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }
    
    }

    /**
     * @method settings_generate_json()
     * 
     * @uses
     *
     * @created vidhya R
     *
     * @updated vidhya R
     * 
     * @param 
     *
     * @return No return response.
     *
     */

    public function settings_generate_json(Request $request) {

        try {

            Helper::settings_generate_json();

            return $this->sendResponse("", "", $data = []);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }
    
    }


    /**
     * @method chat_messages_save()
     * 
     * @uses - To save the chat message.
     *
     * @created vidhya R
     *
     * @updated vidhya R
     * 
     * @param 
     *
     * @return No return response.
     *
     */

    public function get_notifications_count(Request $request) {

        try {

            Log::info("Notification".print_r($request->all(),true));

            $rules = [
                'user_id' => 'required|exists:users,id',
            ];

            Helper::custom_validator($request->all(),$rules);

            $chat_message = \App\ChatMessage::where('to_user_id', $request->user_id)->where('status',NO);

            $bell_notification = \App\BellNotification::where('to_user_id', $request->user_id)->where('is_read',BELL_NOTIFICATION_STATUS_UNREAD);

            $data['chat_notification'] = $chat_message->count() ?: 0;

            $data['bell_notification'] = $bell_notification->count() ?: 0;

            return $this->sendResponse("", "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }
    
    }

  
    /**
     * @method lv_chat_messages_save()
     * 
     * @uses - To save the chat message.
     *
     * @created vidhya R
     *
     * @updated vidhya R
     * 
     * @param 
     *
     * @return No return response.
     *
     */

    public function lv_chat_messages_save(Request $request) {

        try {

            Log::info("message_save".print_r($request->all() , true));

            $rules = [
                'user_id' => 'required|exists:users,id',
                'live_video_id' => 'required|exists:live_videos,id',
                'message' => 'required',
            ];

            Helper::custom_validator($request->all(),$rules);
            
            $message = $request->message;

            $chat_message = new \App\LiveVideoChatMessage;

            $chat_message->from_user_id = $request->user_id;

            $chat_message->live_video_id = $request->live_video_id;

            $chat_message->message = $request->message;

            $chat_message->save();

            DB::commit();

            return $this->sendResponse("", "", $chat_message);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }
    
    }
    
    /**
     * @method configuration_mobile()
     *
     * @uses used to get the configurations for base products
     *
     * @created Vidhya R 
     *
     * @edited Vidhya R
     *
     * @param - 
     *
     * @return JSON Response
     */

    public function configuration_site(Request $request) {

        try {

            $data['is_chat_asset_enabled'] = Setting::get('is_chat_asset_enabled', 0);

            $data['is_referral_wallet_enabled'] = Setting::get('is_referral_enabled', 0);

            $response_array = ['success' => true , 'data' => $data];

            return response()->json($response_array , 200);

        } catch(Exception $e) {

            $error_message = $e->getMessage();

            $response_array = ['success' => false,'error' => $error_message,'error_code' => 101];

            return response()->json($response_array , 200);

        }
   
    }

}
