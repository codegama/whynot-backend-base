<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Helpers\Helper;

use DB, Log, Hash, Validator, Exception, Setting;

use App\{User, Category, SubCategory, CategoryFollower, LiveVideo, UserDocument, UserBillingAccount};

use App\Repositories\LiveVideoRepository as LiveVideoRepo;

use App\Http\Requests\Api\{BecomeASellerRequest};

class HomeApiController extends Controller
{
    protected $loginUser;

    protected $skip, $take;

    public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = User::find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

    }

    /**
     * @method following_categories_list()
     *
     * @uses to get all the categories follower by an user based on user id
     *
     * @created Karthick
     *
     * @updated 
     *
     * @param user_id
     *
     * @return JSON Response
     */
    public function following_categories_list(Request $request) {

        try {

            $base_query = $total_query = CategoryFollower::where('user_id',$request->id)->CommonResponse()->orderBy('created_at', 'desc');

            $data['categories'] = $base_query->skip($this->skip)->take($this->take)->get();

            $data['total'] = $base_query->count() ?? 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    }

    /**
     * @method home_search()
     *
     * @uses to get all the categories follower by an user based on user id
     *
     * @created Karthick
     *
     * @updated 
     *
     * @param user_id
     *
     * @return JSON Response
     */
    public function home_search(Request $request) {

        try {

            $live_videos = $sub_categories = $categories = $users = [];

            $display_search = NO;

            if($request->search_key) {

                $display_search = YES;
            
                $live_videos = LiveVideo::LiveVideos();

                if($request->search_key) {

                    $search_live_video_ids = \App\LiveVideo::where('live_videos.title','LIKE','%'.$request->search_key.'%')
                            ->pluck('id');

                    $live_videos = $live_videos->whereIn('live_videos.id',$search_live_video_ids);
                }

                $live_videos = $live_videos->get();

                $categories = Category::where('status',APPROVED)
                    ->whereHas('subCategories');

                if($request->search_key) {

                    $search_category_ids = \App\Category::where('name','LIKE','%'.$request->search_key.'%')->pluck('id');

                    $categories = $categories->whereIn('id',$search_category_ids);
                }

                $categories = $categories->orderBy('categories.name', 'asc')->get();

                $sub_categories = SubCategory::where('status',APPROVED);

                if($request->search_key) {

                    $search_sub_category_ids = \App\SubCategory::where('name','LIKE','%'.$request->search_key.'%')->pluck('id');

                    $sub_categories = $sub_categories->whereIn('id',$search_sub_category_ids);
                }

                $sub_categories = $sub_categories->orderBy('sub_categories.name', 'asc')->get();

                $users = \App\User::where('is_content_creator',CONTENT_CREATOR)
                    ->Approved();

                if($request->search_key) {

                    $search_user_ids = \App\User::where('users.name','LIKE','%'.$request->search_key.'%')
                            ->orWhere('users.email','LIKE','%'.$request->search_key.'%')
                            ->orWhere('users.mobile','LIKE','%'.$request->search_key.'%')
                            ->pluck('id');

                    $users = $users->whereIn('users.id',$search_user_ids);
                }

                $users = $users->orderBy('name', 'asc')->get();

            }

            $data['sub_categories'] = $sub_categories;

            $data['categories'] = $categories;

            $data['live_videos'] = $live_videos;

            $data['users'] = $users;

            $data['display_search'] = $display_search;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    }

    /**
     * @method following_category_videos()
     *
     * @uses to get all the categories with videos follower by an user based on user id
     *
     * @created Karthick
     *
     * @updated 
     *
     * @param user_id
     *
     * @return JSON Response
     */
    public function following_category_videos(Request $request) {

        try {

            $following_category_ids  = CategoryFollower::where('user_id',$request->id)->pluck('category_id')->toArray();

            $live_videos = LiveVideo::whereIn('category_id', $following_category_ids)->where('is_streaming', YES)->where('live_schedule_type', LIVE_SCHEDULE_TYPE_NOW)->orderBy('schedule_time', 'asc');

            $data['total_category_live_videos'] = $live_videos->count() ?? 0;

            $live_videos = $live_videos->skip($this->skip)->take($this->take)->get();

            $data['category_live_videos']= LiveVideoRepo::live_videos_list_response($live_videos, $request);

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    }

    /**
     * @method following_sub_category_videos()
     *
     * @uses to get all the sub categories with videos follower by an user based on user id
     *
     * @created Karthick
     *
     * @updated 
     *
     * @param user_id
     *
     * @return JSON Response
     */
    public function following_sub_category_videos(Request $request) {

        try {

            $following_category_ids  = array_diff(CategoryFollower::where('user_id',$request->id)->distinct('category_id')->pluck('category_id')->toArray(), [0]);

            $following_sub_category_ids  = array_diff(CategoryFollower::where('user_id',$request->id)->whereNotIn('category_id', $following_category_ids)->distinct('sub_category_id')->pluck('sub_category_id')->toArray(), [0]);

            $following_sub_categories = SubCategory::whereHas('liveVideos')->whereIn('id', $following_sub_category_ids)->get();

            $data['following_sub_category_videos'] = LiveVideoRepo::live_video_sub_category_response($following_sub_categories, $this->skip, $this->take,$request);

            $data['total_videos'] = LiveVideo::whereIn('sub_category_id', $following_sub_category_ids)->count() ?? 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    }

    /**
     * @method category_videos()
     *
     * @uses to get all the videos of a category based on category id
     *
     * @created Karthick
     *
     * @updated 
     *
     * @param category_id
     *
     * @return JSON Response
     */
    public function category_videos(Request $request) {

        try {

            $rules = [ 'category_id' => 'required|exists:categories,unique_id'];

            $custom_errors = [
                'category_id.required' => 'The category field is required',
                'category_id.exists' => 'The category not found',
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            $category = Category::firstWhere('unique_id', $request->category_id);

            $featured_videos = $total_query = LiveVideo::where('category_id', $category->id)->FeaturedVideos()->orderBy('id', 'desc');

            $is_following = CategoryFollower::where(['user_id' => $request->id, 'category_id' => $category->id])->count();

            $data['name'] = $category->name;

            $data['is_following'] = $is_following ? 1 : 0;

            $data['total_featured_videos'] = $total_query->count();

            $featured_videos = $featured_videos->skip($this->skip)->take($this->take)->get();

            $featured_videos = LiveVideoRepo::live_videos_list_response($featured_videos, $request);

            $data['featured_videos'] = $featured_videos;

            $live_videos = $base_query = LiveVideo::where('category_id', $category->id)->LiveVideos()->orderBy('id', 'desc');

            $data['total_live_videos'] = $base_query->count();

            $live_videos = $live_videos->skip($this->skip)->take($this->take)->get();

            $live_videos = LiveVideoRepo::live_videos_list_response($live_videos, $request);
            
            $data['live_videos'] = $live_videos;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    }

    /**
     * @method sub_category_videos()
     *
     * @uses to get all the videos of a sub category based on sub category id
     *
     * @created Karthick
     *
     * @updated 
     *
     * @param sub_category_id
     *
     * @return JSON Response
     */
    public function sub_category_videos(Request $request) {

        try {

            $rules = [ 'sub_category_id' => 'required|exists:sub_categories,unique_id'];

            $custom_errors = [
                'sub_category_id.required' => 'The sub category field is required',
                'sub_category_id.exists' => 'The sub category not found',
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            $sub_category = SubCategory::firstWhere('unique_id', $request->sub_category_id);

            $featured_videos = LiveVideo::where('sub_category_id', $sub_category->id)->FeaturedVideos()->orderBy('id', 'desc');

            $is_following = CategoryFollower::where(['user_id' => $request->id, 'sub_category_id' => $sub_category->id])->count();

            $data['name'] = $sub_category->name;

            $data['is_following'] = $is_following ? 1 : 0;

            $data['total_featured_videos'] = $featured_videos->count();

            $featured_videos_formatted = $featured_videos->skip($this->skip)->take($this->take)->get();

            $featured_videos_formatted = LiveVideoRepo::live_videos_list_response($featured_videos_formatted, $request);

            $data['featured_videos'] = $featured_videos_formatted;

            $live_videos = $base_query = LiveVideo::where('sub_category_id', $sub_category->id)->LiveVideos()->orderBy('id', 'desc');

            $live_videos = $live_videos->skip($this->skip)->take($this->take)->get();

            $live_videos = LiveVideoRepo::live_videos_list_response($live_videos, $request);

            $data['live_videos'] = $live_videos;

            $data['total_live_videos'] = $base_query->count();

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    }

    /**
     * @method explore_categories()
     *
     * @uses to get all the categories with videos except the following categories.
     *
     * @created Karthick
     *
     * @updated 
     *
     * @param user_id
     *
     * @return JSON Response
     */
    public function explore_categories(Request $request) {

        try {

            $following_category_ids  = array_diff(CategoryFollower::where('user_id',$request->id)->distinct('category_id')->pluck('category_id')->toArray(), [0]);

            $explore_category_ids  = Category::whereHas('liveVideos')->whereNotIn('id', $following_category_ids)->pluck('id');

            $explore_categories  = Category::whereIn('id', $explore_category_ids)->get();

            $data['category_live_videos'] = LiveVideoRepo::live_video_category_response($explore_categories, $this->skip, $this->take,$request);

            $data['total_category_live_videos'] = LiveVideo::whereIn('category_id', $explore_category_ids)->count() ?? 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    }

    /**
     * @method recent_categories()
     *
     * @uses to get all the categories with videos recently added.
     *
     * @created Karthick
     *
     * @updated 
     *
     * @param user_id
     *
     * @return JSON Response
     */
    public function recent_categories(Request $request) {

        try {

            $following_category_ids  = array_diff(CategoryFollower::where('user_id',$request->id)->distinct('category_id')->pluck('category_id')->toArray(), [0]);

            $recent_category_ids  = Category::whereHas('liveVideos')->whereNotIn('id', $following_category_ids)->orderBy('created_at', 'desc')->pluck('id')->take($this->take);

            $recent_categories  = Category::whereIn('id', $recent_category_ids)->get();

            $data['recent_categories'] = LiveVideoRepo::live_video_category_response($recent_categories, $this->skip, $this->take,$request);

            $data['total_videos'] = LiveVideo::whereIn('category_id', $recent_category_ids)->count() ?? 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    }

    /**
     * @method ongoing_live_videos()
     *
     * @uses to get all the ongoing videos of an seller based on user id
     *
     * @created Karthick
     *
     * @updated 
     *
     * @param user_id
     *
     * @return JSON Response
     */
    public function ongoing_live_videos(Request $request) {

        try {

            $ongoing_live_videos = $total_query = LiveVideo::where('is_streaming', YES)->where('live_schedule_type', LIVE_SCHEDULE_TYPE_NOW)->orderBy('id', 'desc');

            $data['total_live_videos'] = $total_query->count();

            $live_videos = $ongoing_live_videos->skip($this->skip)->take($this->take)->get();

            $live_videos = LiveVideoRepo::live_videos_list_response($live_videos, $request);

            $data['live_videos'] = $live_videos;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    }

    /**
     * @method become_a_seller()
     *
     * @uses to complete become a seller flow
     *
     * @created Karthick
     *
     * @updated
     *
     * @param
     *
     * @return success / failure with response
     */
    public function become_a_seller(BecomeASellerRequest $request)
    {
        try {

            $user = $this->loginUser;

            throw_if(!$user, new Exception(api_error(310), 310) );

            throw_if(
                $user->is_content_creator == SELLER && 
                $user->content_creator_step == BECOME_A_SELLER_STEP_FIVE,
                new Exception(api_error(309), 309)
            );

            throw_if($user->content_creator_step + 1 < $request->step, new Exception(api_error(315), 315));

            throw_if($request->step == BECOME_A_SELLER_STEP_TWO && $user->is_document_verified == USER_DOCUMENT_APPROVED, new Exception(api_error(316), 316));

            throw_if($request->step != BECOME_A_SELLER_STEP_TWO && $user->content_creator_step > $request->step, new Exception(api_error(311), 311));

            DB::beginTransaction();

            switch ($request->step) {

                case 1:

                    $user->update(['content_creator_step' => BECOME_A_SELLER_STEP_ONE]);

                    $code = 309;

                    break;

                case 2:

                    $user_document = UserDocument::UpdateOrCreate([
                        'user_id' => $request->user_id, 'document_id' => $request->document_id
                    ], $request->validated());

                    throw_if(!$user_document, new Exception(api_error(313, 313)));

                    !$user_document->wasRecentlyCreated && Helper::storage_delete_file($user_document->document_file, USER_DOCUMENTS_PATH);

                    $user_document->document_file = Helper::storage_upload_file($request->document_file, USER_DOCUMENTS_PATH);

                    throw_if(!$user_document->save(), new Exception(api_error(313, 313)));

                    $user->update([
                        'content_creator_step' => BECOME_A_SELLER_STEP_TWO,
                        'is_document_verified' => USER_DOCUMENT_PENDING
                    ]);

                    $code = 310;

                    break;

                case 3:

                    $user_billing_account = UserBillingAccount::Create($request->validated());

                    throw_if(!$user_billing_account, new Exception(api_error(314, 314)));

                    $user->update([
                        'content_creator_step' => BECOME_A_SELLER_STEP_FIVE,
                        'is_content_creator' => SELLER
                    ]);

                    $code = 314;

                    break;

                default:

                    throw new Exception(api_error(312), 312);
            }

            DB::commit();

            $data['user'] = $user->refresh();

            return $this->sendResponse(api_success($code), $code, $data);

        } catch (Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }
    }
}
