<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Helpers\Helper;

use DB, Log, Hash, Validator, Exception, Setting;

use App\{User, UserProductPicture, UserProduct, Order, DeliveryAddress};

use App\{Cart, OrderProduct, ProductReview, UserCard, OrderPayment, UserWallet,LiveVideo};

use App\Repositories\ProductRepository;

use App\Repositories\PaymentRepository as PaymentRepo;

use App\Jobs\SendEmailJob;

class UserProductApiController extends Controller
{
 	protected $loginUser;

    protected $skip, $take;

	public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = User::find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

    }

    /**
     * @method order_payments_list()
     *
     * @uses To display all the order payment list of perticular user
     *
     * @created Subham
     *
     * @updated 
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function order_payments_list(Request $request) {

        try {

            $base_query = $total_query = Order::where('user_id',$request->id)->orderBy('orders.created_at', 'desc');

            if($request->filled('search_key')) {

                  $base_query = $base_query->where('unique_id', 'like', "%".$request->search_key."%")
                                 ->orWhereHas('deliveryAddress', function($query) use($request) {
                                    $query->where('address', "LIKE", "%{$request->search_key}%");
                                })->orWhereHas('orderPayments', function($query) use($request) {
                                    $query->where('payment_id', "LIKE", "%{$request->search_key}%");
                                });
            }

            $data['total'] = $total_query->count() ?? 0;

            $orders = $base_query->skip($this->skip)->take($this->take)->get();

            $orders = ProductRepository::orders_list_response($orders, $request);

            $data['orders'] = $orders ?? [];

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method orders_list_for_others()
     *
     * @uses To display all the order list of perticular user
     *
     * @created Subham
     *
     * @updated 
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function orders_list(Request $request) {

        try {

            $base_query = $total_query = Order::where('user_id',$request->id)->orderBy('orders.created_at', 'desc')->whereHas('orderProducts');

            if($request->filled('search_key')) {

                  $base_query = $base_query->where('unique_id', 'like', "%".$request->search_key."%")
                                 ->orWhereHas('deliveryAddress', function($query) use($request) {
                                    $query->where('address', "LIKE", "%{$request->search_key}%");
                                  })->orWhereHas('orderPayments', function($query) use($request) {
                                    $query->where('payment_id', "LIKE", "%{$request->search_key}%");
                                });
            }

            $data['total'] = $total_query->count() ?? 0;

            $orders = $base_query->skip($this->skip)->take($this->take)->get();

            $orders = ProductRepository::orders_list_response($orders, $request);

            $data['orders'] = $orders ?? [];

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method orders_view()
     *
     * @uses get the selected post details
     *
     * @created Subham
     *
     * @updated 
     *
     * @param
     *
     * @return JSON Response
     */
    public function orders_view(Request $request) {

        try {

            $rules = ['order_unique_id' => 'required|exists:orders,unique_id'];

            Helper::custom_validator($request->all(),$rules);
            
            $order = Order::where('unique_id', $request->order_unique_id)->first();

            if(!$order) {
                throw new Exception(api_error(139), 139);   
            }

            $order = ProductRepository::order_view_single_response($order, $request);

            $data['order'] = $order;

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method ecommerce_home()
     *
     * @uses To display all the product
     *
     * @created Subham
     *
     * @updated 
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function ecommerce_home(Request $request) {

        try {

            $base_query = $total_query = UserProduct::Approved()->where('user_id', $request->user_id)->orderBy('user_products.created_at', 'desc');

            if($request->search_key) {

                $base_query = $base_query->where('user_products.name', 'like', "%".$request->search_key."%");

            }
            

            $user_products = $base_query->skip($this->skip)->take($this->take)->get();

            $user_products = ProductRepository::user_products_list_response($user_products, $request);

            $data['user_products'] = $user_products ?? [];

            $data['total'] = $total_query->count() ?? 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method user_products_view_for_others()
     *
     * @uses get the selected post details
     *
     * @created Subham
     *
     * @updated 
     *
     * @param
     *
     * @return JSON Response
     */
    public function user_products_view_for_others(Request $request) {

        try {

            $rules = ['user_products_unique_id' => 'required|exists:user_products,unique_id'];

            Helper::custom_validator($request->all(),$rules);
            
            $user_product = UserProduct::with('userProductPictures')
                ->with('user')
                ->whereHas('user')
                ->Approved()
                ->where('user_products.unique_id', $request->user_products_unique_id)
                ->first();

            if(!$user_product) {
                throw new Exception(api_error(139), 139);   
            }

            $user_product = ProductRepository::user_product_single_response($user_product, $request);

            $data['user_product'] = $user_product;

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }
	/**
     * @method user_products_index()
     *
     * @uses To list out stardom products details 
     *
     * @created Bhawya
     *
     * @updated Bhawya
     *
     * @param 
     * 
     * @return JSON Response
     *
     */
    public function user_products_index(Request $request) {

    	try {

            $base_query = $total_query = \App\UserProduct::where('user_id', $request->id)->where('is_outofstock',PRODUCT_AVAILABLE);

            $user_products = $base_query->skip($this->skip)->take($this->take)->orderBy('created_at', 'desc')->get();

	        $data['user_products'] = $user_products ?? [];

            $data['total'] = $total_query->count() ?? 0;

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @method user_products_for_owner()
     *
     * @uses To list out stardom products details 
     *
     * @created Bhawya
     *
     * @updated Bhawya
     *
     * @param 
     * 
     * @return JSON Response
     *
     */
    public function user_products_for_owner(Request $request) {

        try {

            $base_query = $total_query = \App\UserProduct::where('user_id', $request->id);

            $data['total'] = $total_query->count() ?? 0;

            $search_key = $request->search_key;

            if($search_key) {

                $base_query = $base_query 

                    ->where(function ($query) use ($search_key, $request) {

                        return $query->where('user_products.user_id', $request->id)->Where('user_products.name','LIKE','%'.$search_key.'%');

                    })->orWhereHas('productCategories', function($q) use ($search_key, $request) {

                        return $q->where('user_products.user_id', $request->id)->Where('categories.name','LIKE','%'.$search_key.'%');

                    })->orWhereHas('productSubCategories', function($q) use ($search_key, $request) {

                        return $q->where('user_products.user_id', $request->id)->Where('sub_categories.name','LIKE','%'.$search_key.'%');

                    });
            }

            $user_products = $base_query->skip($this->skip)->take($this->take)->orderBy('created_at', 'desc')->get();

            $data['user_products'] = $user_products ?? [];

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @method user_products_save()
     *
     * @uses To save the stardom products details of new/existing
     *
     * @created Bhawya
     *
     * @updated 
     *
     * @param object request - Stardom Product Form Data
     *
     * @return JSON Response
     *
     */
    public function user_products_save(Request $request) {
        
        try {
            
            DB::begintransaction();

            $rules = [
                'name' => 'required|max:191',
                'quantity' => 'required|max:100',
                'price' => 'required|max:100',
                'picture' => 'mimes:jpg,png,jpeg',
                'description' => 'max:199',
                'category_id' => 'required|exists:categories,id',
                'sub_category_id' => 'required|exists:sub_categories,id',
            ];

            Helper::custom_validator($request->all(),$rules);

            $user_product = \App\UserProduct::find($request->user_product_id) ?? new \App\UserProduct;

            $success_code = $user_product->id ? 122 : 121;

            $user_product->user_id = $request->id;

            $user_product->name = $request->name ?: $user_product->name;

            $user_product->quantity = $request->quantity ?: $user_product->quantity;

            $user_product->price = $request->price ?: '';

            $user_product->category_id = $request->category_id ?: $user_product->category_id;

            $user_product->sub_category_id = $request->sub_category_id ?: $user_product->sub_category_id;

            $user_product->description = $request->description ?: '';

            // Upload picture
            
            if($request->hasFile('picture')) {

                if($request->user_product_id) {

                    Helper::storage_delete_file($user_product->picture, COMMON_FILE_PATH); 
                    // Delete the old pic
                }

                $user_product->picture = Helper::storage_upload_file($request->file('picture'), COMMON_FILE_PATH);
            }

            if($user_product->save()) {

                DB::commit(); 

                $data = \App\UserProduct::find($user_product->id);

                return $this->sendResponse(api_success($success_code), $success_code, $data);

            } 

            throw new Exception(api_error(130), 130);
            
        } catch(Exception $e){ 

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        } 

    }

    /**
     * @method user_products_view()
     *
     * @uses displays the specified user product details based on user product id
     *
     * @created Bhawya
     *
     * @updated 
     *
     * @param object $request - user product Id
     * 
     * @return JSON Response
     *
     */
    public function user_products_view(Request $request) {
       
        try {
      	
      		$rules = [
                'user_product_id' => 'required|exists:user_products,id,user_id,'.$request->id
            ];

            Helper::custom_validator($request->all(),$rules);

            $user_product = \App\UserProduct::find($request->user_product_id);

            if(!$user_product) { 

                throw new Exception(api_error(133), 133);                
            }

            $data['user_product'] = $user_product;

            return $this->sendResponse($message = "", $success_code = "", $data);
            
        } catch (Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }
    
    }

    /**
     * @method user_products_delete()
     *
     * @uses delete the stardom product details based on stardom id
     *
     * @created Bhawya
     *
     * @updated  
     *
     * @param object $request - Stardom Id
     * 
     * @return response of details
     *
     */
    public function user_products_delete(Request $request) {

        try {

            DB::begintransaction();

            $rules = [
                'user_product_id' => 'required|exists:user_products,id,user_id,'.$request->id
            ];

            Helper::custom_validator($request->all(),$rules,$custom_errors = []);

            $user_product = \App\UserProduct::find($request->user_product_id);

            if(!$user_product) { 

                throw new Exception(api_error(133), 133);                
            }

            $user_product = \App\UserProduct::destroy($request->user_product_id);

            DB::commit();

            $data['user_product_id'] = $request->user_product_id;

            return $this->sendResponse(api_success(123), $success_code = 123, $data);
            
        } catch(Exception $e){

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }       
         
    }

    /**
     * @method user_products_update_availability
     *
     * @uses To update stardom product - Available / Out of Stock
     *
     * @created Bhawya
     *
     * @updated 
     *
     * @param object $request - Stardom Product Id
     * 
     * @return response success/failure message
     *
     **/
    public function user_products_update_availability(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'user_product_id' => 'required|exists:user_products,id,user_id,'.$request->id
            ];

            Helper::custom_validator($request->all(),$rules);

            $user_product = \App\UserProduct::find($request->user_product_id);

            if(!$user_product) { 

                throw new Exception(api_error(133), 133);                
            }

            $user_product->is_outofstock = $user_product->is_outofstock == PRODUCT_AVAILABLE ? PRODUCT_NOT_AVAILABLE : PRODUCT_AVAILABLE;

            if($user_product->save()) {

                DB::commit();

                $success_code = $user_product->is_outofstock == PRODUCT_AVAILABLE ? 126 : 127;

                $data['user_product'] = $user_product;

                return $this->sendResponse(api_success($success_code),$success_code, $data);

            }
            
            throw new Exception(api_error(130), 130);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method user_products_set_visibility
     *
     * @uses To update stardom product status as DECLINED/APPROVED
     *
     * @created Bhawya
     *
     * @updated 
     *
     * @param object $request - Stardom Product Id
     * 
     * @return response success/failure message
     *
     **/
    public function user_products_set_visibility(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'user_product_id' => 'required|exists:user_products,id,user_id,'.$request->id
            ];

            Helper::custom_validator($request->all(),$rules);

            $user_product = \App\UserProduct::find($request->user_product_id);

            if(!$user_product) { 

                throw new Exception(api_error(133), 133);                
            }

            $user_product->is_visible = $user_product->is_visible ? NO : YES;

            if($user_product->save()) {

                DB::commit();

                $success_code = $user_product->is_visible ? 124 : 125;

                $data['user_product'] = $user_product;

                return $this->sendResponse(api_success($success_code),$success_code, $data);

            }
            
            throw new Exception(api_error(130), 130);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method product_categories
     *
     * @uses List Product Categories
     *
     * @created Bhawya
     *
     * @updated 
     *
     * @param object $request - Stardom Product Id
     * 
     * @return response success/failure message
     *
     **/
    public function product_categories(Request $request) {

        try {

            $product_categories = \App\Category::where('status',APPROVED) ->whereHas('subCategories')->skip($this->skip)->take($this->take)->orderBy('created_at', 'desc')->get();

            $product_categories = selected($product_categories, '', 'id');

            if($request->user_product_id) {

                $user_product = \App\UserProduct::find($request->user_product_id);

                $category_id = $user_product->category_id;

                $product_categories = selected($product_categories, $category_id, 'id');
            }

            $data['product_categories'] = $product_categories;

            return $this->sendResponse($message = "", $success_code = "", $data);
            

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method product_sub_categories
     *
     * @uses List Product Sub Categories
     *
     * @created Bhawya
     *
     * @updated 
     *
     * @param object $request - Stardom Product Id
     * 
     * @return response success/failure message
     *
     **/
    public function product_sub_categories(Request $request) {

        try {

            $product_sub_categories = \App\SubCategory::where('status',APPROVED)->skip($this->skip)->take($this->take)->orderBy('created_at', 'desc')->get();

            $product_sub_categories = selected($product_sub_categories, '', 'id');

            if($request->user_product_id) {

                $user_product = \App\UserProduct::find($request->user_product_id);
                
                $sub_category_id = $user_product->sub_category_id;

                $product_sub_categories = selected($product_sub_categories, $sub_category_id, 'id');

            }

            $data['product_sub_categories'] = $product_sub_categories;

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method user_products_search
     *
     * @uses Search Products
     *
     * @created Bhawya
     *
     * @updated 
     *
     * @param object $request - search_key
     * 
     * @return response success/failure message
     *
     **/
    public function user_products_search(Request $request) {

        try {

            $rules = [
                'search_key' => 'required',
            ];

            Helper::custom_validator($request->all(),$rules);

            $base_query = \App\UserProduct::orderBy('updated_at','desc');

            $search_key = $request->search_key;

            if($search_key) {

                $base_query = $base_query 

                    ->where(function ($query) use ($search_key) {

                        return $query->Where('user_products.name','LIKE','%'.$search_key.'%');

                    })->orWhereHas('productCategories', function($q) use ($search_key) {

                        return $q->Where('categories.name','LIKE','%'.$search_key.'%');

                    })->orWhereHas('productSubCategories', function($q) use ($search_key) {

                        return $q->Where('sub_categories.name','LIKE','%'.$search_key.'%');

                    });
            }

            $user_products = $base_query->skip($this->skip)->take($this->take)->get();

            $data['user_products'] = $user_products;

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method user_product_pictures()
     *
     * @uses To load product images
     *
     * @created Bhawya N
     *
     * @updated 
     *
     * @param model image object - $request
     *
     * @return response of succes failure 
     */
    public function user_product_pictures(Request $request) {

        try {

            $rules = [
                'user_product_id' => 'required|exists:user_products,id,user_id,'.$request->id
            ];

            Helper::custom_validator($request->all(),$rules);

            $user_product_pictures = UserProductPicture::where('user_product_id', $request->user_product_id)
                ->skip($this->skip)->take($this->take)->orderBy('created_at', 'desc')->get();
               
            $data['user_product_pictures'] = $user_product_pictures;

            return $this->sendResponse($message = "", $success_code = "", $data);
                
        } catch (Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /**
     * @method user_product_pictures_save()
     *
     * @uses To save gallery product pictures
     *
     * @created Bhawya N
     *
     * @updated 
     *
     * @param object $request - Model Object
     *
     * @return response of success / Failure
     */
    public function user_product_pictures_save(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'user_product_id' => 'required|exists:user_products,id,user_id,'.$request->id,
                'picture.*' => 'required|picture|mimes:jpg,jpeg,png',
            ];

            Helper::custom_validator($request->all(),$rules);
            
            if($request->hasfile('picture')) {

                ProductRepository::user_product_pictures_save($request->file('picture'), $request->user_product_id);

                DB::commit();

                return $this->sendResponse(api_success(133), $success_code = 133, $data = '');
            
            }

            throw new Exception(api_error(130), 130);
            
        } catch (Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method user_product_pictures_delete()
     *
     * @uses To delete Product Image
     *
     * @created  Bhawya N
     *
     * @updated  
     *
     * @param model image object - $request
     *
     * @return response of succes failure 
     */
    public function user_product_pictures_delete(Request $request) {

        try {

            $rules = [
                'user_product_picture_id' => 'required|exists:user_product_pictures,id'
            ];

            Helper::custom_validator($request->all(),$rules);
                
            $user_product_pictures = \App\UserProductPicture::find($request->user_product_picture_id);

            if(!$user_product_pictures) { 

                throw new Exception(api_error(133), 133);                
            }

            $user_product_pictures = \App\UserProductPicture::destroy($request->user_product_picture_id);

            DB::commit();

            $data['user_product_picture_id'] = $request->user_product_picture_id;

            return $this->sendResponse(api_success(132), $success_code = 132, $data);

        } catch (Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }
    
    }

    /**
     * @method carts_list()
     *
     * @uses To list out carts based on user id 
     *
     * @created Jeevan
     *
     * @updated 
     *
     * @param 
     * 
     * @return JSON Response
     *
     */
    public function carts_list(Request $request)
    {
        try {

            $base_query = $total_query = Cart::where('user_id', $request->id)->whereHas('user_product')->with('user_product');

            $carts = $base_query->skip($this->skip)->take($this->take)->orderBy('created_at', 'desc')->get();

            $sub_total = Cart::where('user_id', $request->id)->sum('sub_total');

            $total = Cart::where('user_id', $request->id)->sum('total');

            $data['carts'] = $carts ?? [];

            $data['total'] = $total_query->count() ?? 0;

            $data['sub_total_amount'] = $sub_total ?? 0;

            $data['total_amount'] = $total ?? 0;

            $data['sub_total_formatted'] = formatted_amount($sub_total) ?? 0;

            $data['total_formatted'] = formatted_amount($total) ?? 0;
    
            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @method carts_save()
     *
     * @uses To save the carts
     *
     * @created Jeevan
     *
     * @updated 
     *
     * @param 
     * 
     * @return JSON Response
     *
     */
    public function carts_save(Request $request)
    {
        try {
            
            DB::begintransaction();

            $rules = [
                'user_product_id' => 'required|exists:user_products,id',
                'quantity' => 'required|max:100',
            ];

            Helper::custom_validator($request->all(),$rules);

            $product = UserProduct::find($request->user_product_id);

            if (Setting::get('buy_single_user_products')) {
                
                $cart = Cart::where('user_id', $request->id)->first();

                if ($cart) {
                    
                    $user_product = UserProduct::find($cart->user_product_id);

                    if ($user_product && $product->user_id != $user_product->user_id) {
                        
                        throw new Exception(api_error(251), 251);  
                    }
                }
                
            }

            if ($product->user_id == $request->id) {
                
                throw new Exception(api_error(252), 252);  
            }

            if ($product->quantity < $request->quantity) {
                
                throw new Exception(api_error(253), 253);  
            }

            $product_price = Setting::get('is_only_wallet_payment') ? $product->token : $product->price;

            $cart = Cart::find($request->cart_id) ?? new Cart;

            $success_code = $cart->id ? 248 : 247;

            $cart->user_id = $request->id;

            $cart->order_id = "";

            $cart->user_product_id = $request->user_product_id;

            $cart->quantity = $request->quantity ?: $cart->quantity;

            $cart->per_quantity_price = $product_price;

            $cart->sub_total = ($request->quantity * $product_price) ?? $cart->sub_total;

            $cart->total = ($request->quantity * $product_price) ?? $cart->total;

            if($cart->save()) {

                DB::commit(); 

                $data = Cart::find($cart->id);

                $data->trainer_id = $request->trainer_id;

                return $this->sendResponse(api_success($success_code), $success_code, $data);

            } 

            throw new Exception(api_error(222), 222);
            
        } catch(Exception $e){ 

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        } 
    }

    /**
     * @method carts_remove()
     *
     * @uses Removes the products from the cart
     *
     * @created Jeevan
     *
     * @updated 
     *
     * @param 
     * 
     * @return JSON Response
     *
     */
    public function carts_remove(Request $request) {

        try {

            DB::begintransaction();

            $cart = Cart::where('user_id', $request->id);
            
            if(!$cart) { 

                throw new Exception(api_error(221), 221);                
            }

            if($request->cart_id){

                $cart = Cart::where('id', $request->cart_id)->where('user_id', $request->id);
            }

            $cart->delete();

            DB::commit();

            $data['user_id'] = $request->id;

            $data['cart_id'] = $request->cart_id ?? [];

            return $this->sendResponse(api_success(220), $success_code = 220, $data);
            
        } catch(Exception $e){

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        } 

    } 

     /**
     * @method sold_product_list()
     *
     * @uses To display the all the sold out product list
     *
     * @created Jeevan
     *
     * @updated 
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function sold_product_list(Request $request) {

        try {

            $base_query = $total_query = OrderProduct::where('user_id', $request->id)->where('status',PRODUCT_SOLD)->orderBy('created_at', 'desc');

            $order_products = $base_query->skip($this->skip)->take($this->take)->get();
            
            $order_products = ProductRepository::order_product_list_response($order_products, $request);

            $data['order_products'] = $order_products ?? [];

            $data['total'] = $total_query->count() ?? 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }  

    /**
     * @method product_reviews_save()
     *
     * @uses used to save the Seller reviews and ratings
     *
     * @created Jeevan
     *
     * @updated 
     *
     * @param object $request
     *
     * @return response of details
     */
    public function product_reviews_save(Request $request) {

        try {

            $rules = [
                'user_product_id' => 'required|exists:user_products,id', 
                'ratings' => 'required|min:1',
                'review' => 'required'
            ]; 

            Helper::custom_validator($request->all(), $rules);

            DB::beginTransaction();

            $user = User::find($request->id);

            if(!$user) {
                
                throw new Exception(api_error(135), 135);   
            }

            // Check the user is exists and belongs to the logged in user

            $user_product = UserProduct::find($request->user_product_id);

            if(!$user_product){

                throw new Exception(api_error(133), 133);
            }

            $order_product = OrderProduct::where('user_product_id',$request->user_product_id)->where('status', ORDER_DELIVERD)->first();

            if(!$order_product){

                throw new Exception(api_error(244), 244);
            }

            $product_review = ProductReview::where('user_product_id', $request->user_product_id)->first() ?? new ProductReview;

            $code = $product_review->user_product_id ? 241 : 240;

            $product_review->order_id = $order_product->id;

            $product_review->user_product_id = $request->user_product_id;

            $product_review->seller_id = $user_product->user_id;

            $product_review->user_id = $request->id;

            $product_review->ratings = $request->ratings ?: 0;

            $product_review->review = $request->review ?: "";

            $product_review->status = APPROVED;

            $product_review->save();

            DB::commit();

            $data['review'] = $product_review;

            return $this->sendResponse(api_success($code), $code, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }  
   

    /**
     * @method orders_create_by_paypal()
     *
     * @uses to create the order
     *
     * @created Jeevan
     *
     * @updated 
     *
     * @param object $request
     *
     * @return response of details
     */
    public function orders_create_by_paypal(Request $request) {

        try {

            $rules = [

                'product_id' => 'required',
                'payment_id' => 'required', 
                'total' => 'required|min:1',

            ]; 

            Helper::custom_validator($request->all(), $rules);

            DB::beginTransaction();

            $user = User::find($request->id);

            if(!$user) {
                
                throw new Exception(api_error(135), 135);   
            }

            $user_product = UserProduct::find($request->product_id);
           
             if(!$user_product) {
                
                throw new Exception(api_error(133), 133);   
            }

            $order = Order::where('id', $request->order_id)->first() ?? new Order;

            $code = $request->order_id ? 244 : 243;

            $order->user_id = $request->id;

            $order->delivery_address_id = 0;

            $order->total = $request->total;

            $order->status = ORDER_PLACED;

            if($order->save()) {

                $product_order = ProductRepository::save_product_order($order, $request);

                $request->request->add(['payment_mode' => PAYPAL]);

                $order_payments = ProductRepository::save_order_payment($order, $request);
            }

            DB::commit();

            $data['order'] = $order;

            $data['product_order'] = $product_order;

            $data['order_payments'] = $order_payments;

            return $this->sendResponse(api_success($code), $code, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }  

    /**
     * @method orders_create_by_stripe()
     *
     * @uses to create the order
     *
     * @created Jeevan
     *
     * @updated 
     *
     * @param object $request
     *
     * @return response of details
     */
    public function orders_create_by_stripe(Request $request) {

        try {

            $rules = [
                'user_product_id' => 'required|exists:user_products,id',
                'live_video_id' => 'required|exists:live_videos,id',
            ]; 

            Helper::custom_validator($request->all(), $rules);

            DB::beginTransaction();

            $user = User::find($request->id);

            if(!$user) {
                
                throw new Exception(api_error(135), 135);   
            }

            $delivery_address = DeliveryAddress::where([['user_id',$request->id],['is_default',YES]])->first();

            if(!$delivery_address) {
                
                throw new Exception(api_error(304), 304);   
            }

            $live_video = LiveVideo::find($request->live_video_id);

            $request->request->add([
                'delivery_address_id' => $delivery_address->id, 
            ]);

            $user_product = UserProduct::find($request->user_product_id);
           
             if(!$user_product) {
                
                throw new Exception(api_error(133), 133);   
            }

            $user_card = UserCard::where('user_id', $request->id)->where('is_default', YES)->first();

            if(!$user_card) {

                throw new Exception(api_error(120), 120); 

            }

            $order = Order::where('id', $request->order_id)->first() ?? new Order;

            $code = $request->order_id ? 244 : 243;

            $order->user_id = $request->id;

            $order->delivery_address_id = $request->delivery_address_id;

            $order->total = $user_product->price;

            $order->status = ORDER_PLACED;

            if($order->save()) {

                $product_order = ProductRepository::save_product_order($order, $request);

                $request->request->add([
                    'total' => $order->total, 
                    'customer_id' => $user_card->customer_id,
                    'card_token' => $user_card->card_token,
                    'user_card_id' => $user_card->id,
                    'user_pay_amount' => $order->total,
                    'paid_amount' => $order->total,
                    'payment_mode' => CARD,
                ]);

                $card_payment_response = PaymentRepo::order_payment_by_stripe($request, $order)->getData();

                if($card_payment_response->success) {

                    $order_payments = ProductRepository::save_order_payment($order, $request)->getData();

                    if($order_payments->success) {

                        PaymentRepo::order_product_quantity_update($order)->getData();

                    }
                 
                } else {

                    throw new Exception($card_payment_response->error, $card_payment_response->error_code);
                    
                }
                
            }

            DB::commit();

            $data['order'] = $order;

            $data['product_order'] = $product_order;

            $data['order_payments'] = $order_payments;

            $data['live_video'] = $live_video;

            return $this->sendResponse(api_success($code), $code, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method orders_create_by_wallet()
     *
     * @uses to create the order
     *
     * @created Jeevan
     *
     * @updated 
     *
     * @param object $request
     *
     * @return response of details
     */
    public function orders_create_by_wallet(Request $request) {

        try {

            $rules = [
                'user_product_id' => 'required|exists:user_products,id',
                'live_video_id' => 'required|exists:live_videos,id',
            ]; 

            Helper::custom_validator($request->all(), $rules);

            DB::beginTransaction();

            $user = User::find($request->id);

            if(!$user) {
                
                throw new Exception(api_error(135), 135);   
            }

            $delivery_address = DeliveryAddress::where([['user_id',$request->id],['is_default',YES]])->first();

            if(!$delivery_address) {
                
                throw new Exception(api_error(304), 304);   
            }

            $request->request->add([
                'delivery_address_id' => $delivery_address->id, 
            ]);

            $user_product = UserProduct::find($request->user_product_id);

            if(!$user_product) {
                
                throw new Exception(api_error(133), 133);   
            }

            $live_video = LiveVideo::find($request->live_video_id);

            $user_wallet = UserWallet::where('user_id', $request->id)->first();

            $remaining = $user_wallet->remaining ?? 0;
            
            if($remaining < $user_product->price) {

                throw new Exception(api_error(147), 147);  

            }

            $order = Order::where('id', $request->order_id)->first() ?? new Order;

            $code = $request->order_id ? 244 : 243;

            $order->user_id = $request->id;

            $order->delivery_address_id = $request->delivery_address_id;

            $order->total = $user_product->price;

            $order->status = ORDER_PLACED;

            if($order->save()) {

                $product_order = ProductRepository::save_product_order($order, $request);
                
                $request->request->add([
                    'payment_mode' => PAYMENT_MODE_WALLET,
                    'total' => $order->total, 
                    'user_pay_amount' => $order->total,
                    'paid_amount' => $order->total,
                    'to_user_id' => $user_product->user_id,
                    'payment_type' => WALLET_PAYMENT_TYPE_PAID,
                    'amount_type' => WALLET_AMOUNT_TYPE_MINUS,
                    'payment_id' => 'WPP-'.rand(),
                    'usage_type' => USAGE_TYPE_PRODUCT
                ]);

                $wallet_payment_response = PaymentRepo::user_wallets_payment_save($request)->getData();

                if($wallet_payment_response->success) {

                    $order_payments = ProductRepository::save_order_payment($order, $request)->getData();

                    if($order_payments->success) {

                        PaymentRepo::order_product_quantity_update($order)->getData();

                    }

                } else {

                    throw new Exception($wallet_payment_response->error, $wallet_payment_response->error_code);
                    
                }
            }

            DB::commit();

            $data['order'] = $order;

            $data['product_order'] = $product_order;

            $data['order_payments'] = $order_payments;

            $data['live_video'] = $live_video;

            return $this->sendResponse(api_success($code), $code, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }  

    /**
     * @method orders_list_for_seller()
     *
     * @uses  List Order details for Seller
     *
     * @created Bhawya
     *
     * @updated 
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function orders_list_for_seller(Request $request) {

        try {

            $user_product_ids = UserProduct::where('user_id', $request->id)->pluck('id');

            $order_ids = OrderProduct::whereIn('user_product_id', $user_product_ids)->pluck('order_id');

            $base_query = $total_query = Order::whereIn('id',$order_ids)->orderBy('orders.created_at', 'desc');

            $data['total'] = $total_query->count() ?? 0;

            $orders = $base_query->skip($this->skip)->take($this->take)->get();

            $orders = ProductRepository::orders_list_response($orders, $request);

            $data['orders'] = $orders ?? [];

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method orders_status_update()
     *
     * @uses To change the status of the order placed by the buyer
     *
     * @created Subham Kant
     *
     * @updated 
     *
     * @param - 
     *
     * @return JSON Response
     */
    public function orders_status_update(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'status' => 'required|numeric',
                'order_id' => 'required|exists:orders,id'
            ]; 

            Helper::custom_validator($request->all(), $rules);

            $order = Order::find($request->order_id);

            $order->status = $request->status;

            $order->save();

            $data['order'] = $order;
            
            DB::commit();

            return $this->sendResponse(api_success(303), 303, $data);

        } catch (Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method orders_cancel()
     *
     * @uses To cancel the order via order id
     *
     * @created Subham Kant
     *
     * @updated 
     *
     * @param - 
     *
     * @return JSON Response
     */
    public function orders_cancel(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'order_id' => 'required|exists:orders,id'
            ];

            Helper::custom_validator($request->all(), $rules);

            $order = Order::where('id',$request->order_id)->where('status',ORDER_PLACED)->first();

            if(!$order){
                throw new Exception(api_error(300), 300);
            }

            $order->status = ORDER_CACELLED;

            if($order->save()){

                $request->request->add(['paid_amount' => $order->total, 'payment_id' => 'OPC-'.rand(), 'paid_status' => USER_WALLET_PAYMENT_PAID,'total' => $order->total,'user_pay_amount' => $order->total]);

                $payment_response = PaymentRepo::user_wallets_payment_save($request)->getData();

                if($payment_response->success) {
                    
                    DB::commit();

                    $email_data['subject'] = tr('order_cancel_email' , Setting::get('site_name'));

                    $email_data['email']  = $order->user->email ?? tr('n_a');

                    $email_data['order_unique_id']  = $order->unique_id ?: tr('n_a');

                    $email_data['refunded_amount']  = $order->total ?: 0;

                    $email_data['message'] = tr('order_cancelled_by_user' , $order->user->name);

                    $email_data['page'] = "emails.users.order-cancel";

                    $this->dispatch(new SendEmailJob($email_data));

                    $data['payment_response'] = $payment_response->data;

                    $data['order_product_response'] = $order;

                    return $this->sendResponse(api_success(304), 304, $data);

                } else {

                    throw new Exception($payment_response->error, $payment_response->error_code);
                    
                }

            }

            throw new Exception(api_error(301), 301);

        } catch (Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method order_cancel()
     *
     * @uses To cancel the order via order id
     *
     * @created Subham Kant
     *
     * @updated 
     *
     * @param - 
     *
     * @return JSON Response
     */
    public function order_cancel(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                        'order_id' => 'required|exists:orders,id'
                    ];

            Helper::custom_validator($request->all(), $rules);

            $order = Order::where('user_id',$request->id)->where('id',$request->order_id)->where('status','!=',ORDER_CACELLED)->first();

            if(!$order) {
                throw new Exception(api_error(246), 246);   
            }

            $order_products = OrderProduct::where('order_id',$order->id)->where('status','!=',NO)->count();

            if($order_products > 0){
                throw new Exception(api_error(247), 247);
            }

            $order->status = ORDER_CACELLED;

            OrderProduct::where('order_id',$order->id)->update(['status' => ORDER_CACELLED]);

            if($order->save()){

                $request->request->add(['paid_amount' => $order->total, 'payment_id' => 'OC-'.rand(), 'paid_status' => USER_WALLET_PAYMENT_PAID,'total' => $order->total,'user_pay_amount' => $order->total]);

                $payment_response = PaymentRepo::user_wallets_payment_save($request)->getData();

                if($payment_response->success) {
                    
                    DB::commit();

                    $order_products = OrderProduct::where('order_id',$order->id)->get();

                    foreach($order_products as $order_product){

                        $email_data['subject'] = tr('order_cancel_email' , Setting::get('site_name'));

                        $email_data['email']  = $order_product->userProductDetails->user->email ?? tr('n_a');

                        $email_data['order_unique_id']  = $order->unique_id ?: tr('n_a');

                        $email_data['refunded_amount']  = $order_product->total ?: 0;

                        $email_data['message'] = tr('order_cancelled_by_user' , $order_product->user->name);

                        $email_data['page'] = "emails.users.order-cancel";

                        $this->dispatch(new SendEmailJob($email_data));

                    }

                    $data['payment_response'] = $payment_response->data;

                    $data['order_response'] = $order;

                    return $this->sendResponse(api_success(117), 117, $data);

                } else {

                    throw new Exception($payment_response->error, $payment_response->error_code);
                    
                }

            }

            throw new Exception(api_error(222), 222);

        } catch (Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method orders_list_for_others()
     *
     * @uses To display all the order list of perticular user
     *
     * @created Subham
     *
     * @updated 
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function orders_list_for_others(Request $request) {

        try {

            $base_query = $total_query = Order::where('user_id',$request->id)->orderBy('orders.created_at', 'desc');

            $orders = $base_query->skip($this->skip)->take($this->take)->get();

            $orders = ProductRepository::orders_list_response($orders, $request);

            $data['orders'] = $orders ?? [];

            $data['total'] = $total_query->count() ?? 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method orders_view_for_others()
     *
     * @uses get the selected post details
     *
     * @created Subham
     *
     * @updated 
     *
     * @param
     *
     * @return JSON Response
     */
    public function orders_view_for_others(Request $request) {

        try {

            $rules = ['order_unique_id' => 'required|exists:orders,unique_id'];

            Helper::custom_validator($request->all(),$rules);
            
            $order = Order::where('unique_id', $request->order_unique_id)->first();

            if(!$order) {
                throw new Exception(api_error(139), 139);   
            }

            $order = ProductRepository::order_view_single_response($order, $request);

            $data['order'] = $order;

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

     /**
     * @method user_products_orders_list()
     *
     * @uses To display all the orders list of perticular product
     *
     * @created Arun
     *
     * @updated 
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function user_products_orders_list(Request $request) {

        try {

            $rules = [
                    'user_product_id' => 'required|exists:user_products,id,user_id,'.$request->id,
                ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $order_ids = OrderProduct::where('user_product_id', $request->user_product_id)->pluck('order_id');

            $base_query = $total_query = Order::whereIn('id',$order_ids)->orderBy('orders.created_at', 'desc');

            if($request->filled('search_key')) {

                  $base_query = $base_query->where('unique_id', 'like', "%".$request->search_key."%")
                                 ->orWhereHas('deliveryAddress', function($query) use($request) {
                                    $query->where('address', "LIKE", "%{$request->search_key}%");
                                  })->orWhereHas('orderPayments', function($query) use($request) {
                                    $query->where('payment_id', "LIKE", "%{$request->search_key}%");
                             });
            }

            $data['total'] = $total_query->count() ?? 0;

            $orders = $base_query->skip($this->skip)->take($this->take)->get();

            $orders = ProductRepository::user_products_orders_list_response($orders, $request);

            $data['orders'] = $orders ?? [];

            $data['user_product'] = UserProduct::find($request->user_product_id) ?? [];

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method orders_payment_by_stripe()
     * 
     * @uses Create new order
     *
     * @created Subham Kant
     *
     * @updated
     *
     * @param object $request
     *
     * @return json with boolean output
     */

    public function orders_payment_by_stripe(Request $request) {

        try {
            
            DB::beginTransaction();

            $rules = [
                    'cart_ids' => 'required',
                    'delivery_address_id' => 'nullable|exists:delivery_addresses,id,user_id,'.$request->id,
                    'name' => 'required_without:delivery_address_id',
                    'address' => 'required_without:delivery_address_id',
                    'pincode' => 'required_without:delivery_address_id',
                    'state' => 'required_without:delivery_address_id',
                    'landmark' => 'required_without:delivery_address_id',
                    'contact_number' => 'required_without:delivery_address_id',
                ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $delivery_address = DeliveryAddress::find($request->delivery_address_id) ?? new DeliveryAddress;

            $delivery_address->user_id = $request->id;

            $delivery_address->name = $request->name ?? $delivery_address->name;

            $delivery_address->address = $request->address ?? $delivery_address->address;

            $delivery_address->pincode = $request->pincode ?? $delivery_address->pincode;

            $delivery_address->state = $request->state ?? $delivery_address->state;

            $delivery_address->landmark = $request->landmark ?? $delivery_address->landmark;

            $delivery_address->contact_number = $request->contact_number ?? $delivery_address->contact_number;

            $delivery_address->is_default = $request->is_default ?? ($delivery_address->is_default ?? NO);

            $delivery_address->save();

            if ($request->is_default) {
                
                DeliveryAddress::where('id', '!=', $delivery_address->id)->update(['is_default' => NO]);
            }

            if(!is_array($request->cart_ids)) {

                $request->cart_ids = explode(',', $request->cart_ids);
                
            }

            $sub_total = $total = 0.00;

            $carts = Cart::whereIn('id',$request->cart_ids)->get();

            $order = Order::find($request->order_id) ?? new Order;

            $order->user_id = $request->id;

            $order->delivery_address_id = $delivery_address->id ?? 0;

            $order->total_products = count($request->cart_ids) ?? 0;

            $order->save();

            if ($order) {

                foreach ($carts as $key => $cart) {

                    $order_product_response = PaymentRepo::order_product_save($request, $order, $cart)->getData();

                    if(!$order_product_response->success) {

                        throw new Exception($order_product_response->error, $order_product_response->error_code);
                    }

                    $sub_total = $sub_total + $cart->sub_total;

                    $total = $total + $cart->total;
                }

                $order->sub_total = $sub_total;

                $order->total = $total;

                $order->save();
                
            }

            $request->request->add(['payment_mode' => CARD]);

            $user_pay_amount = $total ?? 0;

            if($user_pay_amount > 0) {

                $user_card = UserCard::where('user_id', $request->id)->firstWhere('is_default', YES);

                if(!$user_card) {

                    throw new Exception(api_error(120), 120); 

                }
                
                $request->request->add([
                    'total' => $total, 
                    'customer_id' => $user_card->customer_id,
                    'card_token' => $user_card->card_token,
                    'user_pay_amount' => $user_pay_amount,
                    'paid_amount' => $user_pay_amount,
                ]);

                $card_payment_response = PaymentRepo::orders_payment_by_stripe($request)->getData();
                
                if($card_payment_response->success == false) {

                    throw new Exception($card_payment_response->error, $card_payment_response->error_code);
                    
                }

                $card_payment_data = $card_payment_response->data;

                $request->request->add(['paid_amount' => $card_payment_data->paid_amount, 'payment_id' => $card_payment_data->payment_id, 'paid_status' => $card_payment_data->paid_status]);
                
            }


            $payment_response = PaymentRepo::order_payments_save($request, $order)->getData();

            if($payment_response->success) {

                PaymentRepo::order_product_quantity_update($order)->getData();
            
                $carts = Cart::whereIn('id',$request->cart_ids)->delete();

                DB::commit();

                return $this->sendResponse(api_success(239), 239, $order);

            } else {
                dd($payment_response);
                
                throw new Exception($payment_response->error, $payment_response->error_code);
                
            }
               
        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method other_model_product_list()
     *
     * @uses To display all the product of a model
     *
     * @created Arun
     *
     * @updated 
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function other_model_product_list(Request $request) {

        try {

            $rules = [
                        'user_unique_id' => 'required|exists:users,unique_id'
                    ];

            Helper::custom_validator($request->all(), $rules);

            $user = User::firstWhere('unique_id', $request->user_unique_id);

            $base_query = $total_query = UserProduct::Approved()->where('user_id', $user->id);

            if ($request->filled('search_key')) {
                
                $user_product_ids = UserProduct::where('name','LIKE','%'.$request->search_key.'%')
                                ->orWhere('description','LIKE','%'.$request->search_key.'%')->pluck('id');

                $base_query = $base_query->whereIn('id', $user_product_ids);

            }

            if ($request->filled('sort_by')) {
                
                $sort_by = $request->sort_by == 'price_hl' ? 'desc' : 'asc';

                $base_query = $base_query->orderBy('price', $sort_by);
            }
            else {

                $base_query = $base_query->orderBy('user_products.created_at', 'desc');
            }

            $user_products = $base_query->skip($this->skip)->take($this->take)->get();

            $user_products = ProductRepository::user_products_list_response($user_products, $request);

            $data['user_products'] = $user_products ?? [];

            $data['total'] = $total_query->count() ?? 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

}