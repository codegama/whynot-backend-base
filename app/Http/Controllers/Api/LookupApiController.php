<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use DB, Log, Hash, Validator, Exception, Setting;

use App\{User,Category,SubCategory};

use App\Helpers\Helper;

use App\Repositories\PaymentRepository as PaymentRepo;

use App\Repositories\WalletRepository as WalletRepo;


class LookupApiController extends Controller
{
    protected $loginUser, $skip, $take;

	public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = User::find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

        $request->request->add(['timezone' => $this->timezone]);

    }    

    /** 
     * @method categories_home()
     *
     * @uses ucategories List (These categories will be displayed in the home page)
     *
     * @created Ganesh
     *
     * @updated Vithya R
     *
     * @param
     * 
     * @return JSON response
     *
     */
    public function categories_home(Request $request) {

        try {


            $base_query = $total_query = Category::CommonResponse()->Approved()->whereHas('userProducts');

            $categories = $base_query->orderBy('categories.name', 'asc')->skip($this->skip)->take(4)->get();

            $data['categories'] = $categories;

            $data['total'] = $total_query->count() ?: 0;

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method categories()
     *
     * @uses categories List
     *
     * @created Ganesh
     *
     * @updated Vithya R
     *
     * @param
     * 
     * @return JSON response
     *
     */
    public function categories(Request $request) {

        try {

            $base_query = $total_query = Category::where('status',APPROVED) ->whereHas('subCategories');

            $categories = $base_query->orderBy('categories.name', 'asc')->get();

            $data['categories'] = $categories;

            $data['total'] = $total_query->count() ?: 0;

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method sub_categories()
     *
     * @uses categories List
     *
     * @created Ganesh
     *
     * @updated Vithya R
     *
     * @param
     * 
     * @return JSON response
     *
     */
    public function sub_categories(Request $request) {

        try {

            $rules = [
                'category_id' => 'required_without:category_unique_id|exists:categories,id',
                'category_unique_id' => 'required_without:category_id|exists:categories,unique_id'
            ];

            Helper::custom_validator($request->all(),$rules);

            if($request->category_unique_id) { 

                $category = Category::where('unique_id',$request->category_unique_id)->first();

            } else {
                
                $category = Category::where('id',$request->category_id)->first();
            }

            $base_query = $total_query = SubCategory::where('status',APPROVED)->where('category_id',$category->category_id);

            $sub_categories = $base_query->orderBy('sub_categories.name', 'asc')->get();

            $data['sub_categories'] = $sub_categories;

            $data['total'] = $total_query->count() ?: 0;

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }   

    /** 
     * @method categories_list()
     *
     * @uses categories List
     *
     * @created Ganesh
     *
     * @updated Vithya R
     *
     * @param
     * 
     * @return JSON response
     *
     */
    public function categories_list(Request $request) {

        try {

            $base_query = $total_query = Category::where('status',APPROVED)
                ->whereHas('subCategories')
                ->with('subCategories');

            $categories = $base_query->orderBy('categories.name', 'asc')->get();

            $data['categories'] = $categories;

            $data['total'] = $total_query->count() ?: 0;

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

}
