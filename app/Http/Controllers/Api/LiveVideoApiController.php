<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Collection;

use Validator, Log, Hash, Setting, DB, Exception, File;

use App\Repositories\LiveVideoRepository as LiveVideoRepo;

use App\Repositories\PaymentRepository as PaymentRepo;

use App\Repositories\CommonRepository as CommonRepo;

use App\Helpers\Helper;

use App\{User, Card, LiveVideo, LiveVideoPayment, Viewer, LiveVideoProduct, LiveVideoCategory, LiveVideoChatMessage,UserProduct,OrderProduct,Order, LiveVideoBookmark, CategoryFollower};

use App\Repositories\ProductRepository;

class LiveVideoApiController extends Controller
{
    protected $loginUser;

    protected $skip, $take, $timezone, $currency, $device_type;

    public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->currency = Setting::get('currency', '$');

        $this->loginUser = User::CommonResponse()->find($request->id);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

        $this->device_type = $this->loginUser->device_type ?? DEVICE_WEB;

    }

    /** 
     * @method live_videos()
     *
     * @uses get the current live streaming videos
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_videos(Request $request) {

        try {

            $rules = [
                'user_unique_id' => 'nullable|exists:users,unique_id',
                'category_id' => 'nullable|exists:categories,id'
            ];
            
            Helper::custom_validator($request->all(), $rules, $custom_errors = []); 

            $user = User::where('unique_id',$request->user_unique_id)->first();

            $public_videos = LiveVideo::when($request->filled('category_id'), function ($query) use ($request) {
                                    $query->where('category_id', $request->category_id);
                                })->when($request->filled('user_unique_id'), function ($query) use ($user) {
                                    $query->where('user_id', $user->id);
                                })->where('type',TYPE_PUBLIC)->orderBy('id', 'desc');

            // this function has all common check conditions for videos
            $public_videos = LiveVideoRepo::live_videos_common_query($request, $public_videos,TYPE_PUBLIC);

            $public_live_videos = $public_videos->skip($this->skip)->take($this->take)->get();


            $public_live_videos = LiveVideoRepo::live_videos_list_response($public_live_videos, $request);

            $private_videos = LiveVideo::CurrentLive()->where('type',TYPE_PRIVATE)->orderBy('live_videos.id', 'desc');

            // this function has all common check conditions for videos
            $private_videos = LiveVideoRepo::live_videos_common_query($request, $private_videos,TYPE_PRIVATE);

            $private_live_videos = $private_videos->skip($this->skip)->take($this->take)->get();

            $private_live_videos = LiveVideoRepo::live_videos_list_response($private_live_videos, $request);
            
            // Create a new collection instance.
            $collection = new Collection($public_live_videos);

            $custom_collection = $collection->merge($private_live_videos);

            $shuffled = $custom_collection->sortByDesc('updated', SORT_REGULAR)->values();

            $feed = $shuffled->values()->all();

            $data['live_videos'] = $feed ?? emptyObject();

            $data['total'] = count($feed);

            return $this->sendResponse($message = '', $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method live_videos_view()
     *
     * @uses get the current live streaming videos
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_videos_view(Request $request) {

        try {

            // Validation start

            $rules = ['live_video_unique_id' => 'required|exists:live_videos,unique_id'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // Validation end

            $live_video = LiveVideo::where('unique_id',$request->live_video_unique_id)->first();

            // this function has all common check conditions for videos
            // $base_query = LiveVideoRepo::live_videos_common_query($request, $base_query);

            // $live_video = $base_query;

            if(!$live_video) {

                throw new Exception(api_error(201), 201);
                
            }
            if($request->id == $live_video->user_id) {

                $live_video->is_user_needs_to_pay =  NO;

                $live_video->is_owner = YES;

            } else {

                $live_video->is_owner = NO;

                $live_video->is_user_needs_to_pay = LiveVideoRepo::live_videos_check_payment($live_video, $request->id); 

            }

            $live_video->start_date = common_date($live_video->created_at , $this->timezone);

            $live_video->is_following = Helper::is_you_following($request->id, $live_video->user_id);

            $request->request->add(['broadcast_type' => $live_video->broadcast_type, 'virtual_id' => $live_video->virtual_id, 'live_video_id' => $live_video->live_video_id]);

            return $this->sendResponse($message = '', $code = '', $live_video);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method live_videos_search()
     *
     * @uses get the current live streaming videos
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_videos_search(Request $request) {

        try {

            $base_query = LiveVideo::CurrentLive();

            // this function has all common check conditions for videos
            $base_query = LiveVideoRepo::live_videos_common_query($request, $base_query);

            // search query

            $base_query = $total_query = $base_query->where('title', 'like', "%".$request->key."%")->orderBy('live_videos.id', 'desc');

            $live_videos = $base_query->skip($this->skip)->take($this->take)->get();

            $live_videos = LiveVideoRepo::live_videos_list_response($live_videos, $request);

            $data['live_videos'] = $live_videos ?? emptyObject();

            $data['total'] = $total_query->count() ?? [];

            return $this->sendResponse($message = '', $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

   

   
    /** 
     * @method live_videos_payment_by_card()
     *
     * @uses get the current live streaming videos
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_videos_payment_by_card(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
                    'live_video_id' => 'required|exists:live_videos,id',
                    'coupon_code' => 'nullable|exists:coupons,coupon_code',
                    ];

            $custom_errors = ['live_video_id' => api_error(150)];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // Validation end

            // Check the live video is streaming

            $live_video = LiveVideo::where('id',  $request->live_video_id)
                                    ->CurrentLive()
                                    ->first();

            if(!$live_video) {

                throw new Exception(api_error(201), 201);
                
            }

            $live_video_payment = LiveVideoPayment::where('live_video_viewer_id', $request->id)->where('live_video_id', $request->live_video_id)->where('status', DEFAULT_TRUE)->count();

            // check the live video payment status || whether user already paid

            if($live_video->payment_status == NO || $live_video_payment) {

                $code = 136;

                goto successReponse;
                
            }

            $request->request->add(['payment_mode' => CARD]);

            $total = $user_pay_amount = $live_video->amount ?? 0.00;

            if($user_pay_amount > 0) {

                $user_card = \App\UserCard::where('user_id', $request->id)->firstWhere('is_default', YES);

                if(!$user_card) {

                    throw new Exception(api_error(120), 120); 

                }

                $request->request->add([
                    'total' => $total, 
                    'customer_id' => $user_card->customer_id,
                    'card_token' => $user_card->card_token,
                    'user_pay_amount' => $user_pay_amount,
                    'paid_amount' => $user_pay_amount,
                ]);
                
                $card_payment_response = PaymentRepo::live_videos_payment_by_stripe($request, $live_video)->getData();

                if($card_payment_response->success == false) {

                    throw new Exception($card_payment_response->error, $card_payment_response->error_code);
                    
                }

                $card_payment_data = $card_payment_response->data;

                $request->request->add(['paid_amount' => $card_payment_data->paid_amount, 'payment_id' => $card_payment_data->payment_id, 'paid_status' => $card_payment_data->paid_status]);

            }

            $payment_response = PaymentRepo::live_videos_payment_save($request, $live_video)->getData();

            if($payment_response->success) {
                
                DB::commit();

                $code = 118;

            } else {

                throw new Exception($payment_response->error, $payment_response->error_code);
            }          

            successReponse:

            $data['live_video_id'] = $request->live_video_id;

            $data['live_video_unique_id'] = $live_video->unique_id;

            $data['payment_mode'] = CARD;

            return $this->sendResponse(api_success($code), $code, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method live_videos_broadcast_start()
     *
     * @uses get the current live streaming videos
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_videos_broadcast_start(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'live_video_id' => 'required|exists:live_videos,id',
            ];

            $custom_errors = ['live_video_id' => api_error(150)];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // Validation end

            // Check the live video is streaming

            $live_video = LiveVideo::where('id',  $request->live_video_id)->first();

            if(!$live_video) {

                throw new Exception(api_error(201), 201);
                
            }

            $user = $this->loginUser;

            if($user->is_content_creator != CONTENT_CREATOR){

                throw new Exception(api_error(218), 218);
            }

            $live_video->status = VIDEO_STREAMING_ONGOING;

            $live_video->is_streaming = IS_STREAMING_YES;

            $live_video->start_time = getUserTime(date('H:i:s'), $this->timezone, "H:i:s");

            $live_video->live_schedule_type = LIVE_SCHEDULE_TYPE_NOW;

            $token = '';

            if(Setting::get('is_agora_configured')) { 

                $agora_app_id = Setting::get('agora_app_id');

                $appCertificate = Setting::get('agora_certificate_id');

                $uid = 0;

                $role = \RtcTokenBuilder::RoleAttendee;

                $expireTimeInSeconds = 3600;

                $currentTimestamp = (new \DateTime("now", new \DateTimeZone('UTC')))->getTimestamp();

                $privilegeExpiredTs = $currentTimestamp + $expireTimeInSeconds;

                $token = \RtcTokenBuilder::buildTokenWithUid($agora_app_id, $appCertificate, $live_video->virtual_id, $uid, $role, $privilegeExpiredTs);

            }

            $live_video->agora_token = $token ?? '';

            $live_video->save();

            DB::commit();

            $this->dispatch(new \App\Jobs\LiveVideoNotificationToFollower($request->id, $live_video->id));

            $data = LiveVideo::where('live_videos.id', $live_video->id)->first();

            return $this->sendResponse(api_success(204), $code = 204, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method live_videos_payment_by_paypal()
     *
     * @uses get the current live streaming videos
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_videos_payment_by_paypal(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
                'live_video_id' => 'required|exists:live_videos,id',
                'payment_id' => 'required',
            ];

            $custom_errors = ['live_video_id' => api_error(150)];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // Validation end

            // Check the live video is streaming

            $live_video = LiveVideo::where('id',  $request->live_video_id)
                                    ->CurrentLive()
                                    ->first();

            if(!$live_video) {

                throw new Exception(api_error(201), 201);
                
            }

            $live_video_payment = LiveVideoPayment::where('live_video_viewer_id', $request->id)->where('live_video_id', $request->live_video_id)->where('status', DEFAULT_TRUE)->count();

            // check the live video payment status || whether user already paid

            if($live_video->payment_status == NO || $live_video_payment) {

                $code = 136;

                goto successReponse;
                
            }

            $total = $user_pay_amount = $live_video->amount ?? 0.00;

            $request->request->add(['payment_mode' => PAYPAL,'paid_amount' => $total]);

            $payment_response = PaymentRepo::live_videos_payment_save($request, $live_video)->getData();

            if($payment_response->success) {
                
                DB::commit();

                $code = 118;

            } else {

                throw new Exception($payment_response->error, $payment_response->error_code);
            }          

            successReponse:

            $data['live_video_id'] = $request->live_video_id;

            $data['live_video_unique_id'] = $live_video->unique_id;

            $data['payment_mode'] = CARD;

            return $this->sendResponse(api_success($code), $code, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method live_videos_payment_by_wallet()
     *
     * @uses get the current live streaming videos
     *
     * @created Subham
     *
     * @updated 
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_videos_payment_by_wallet(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
                'live_video_id' => 'required|exists:live_videos,id',
            ];

            $custom_errors = ['live_video_id' => api_error(150)];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // Validation end

            // Check the live video is streaming

            $live_video = LiveVideo::where('id',  $request->live_video_id)
                                    ->CurrentLive()
                                    ->first();

            if(!$live_video) {

                throw new Exception(api_error(201), 201);
                
            }

            $live_video_payment = LiveVideoPayment::where('live_video_viewer_id', $request->id)->where('live_video_id', $request->live_video_id)->where('status', DEFAULT_TRUE)->count();

            if($live_video_payment) {

                throw new Exception(api_error(239), 239);
                
            }

            $model = User::find($live_video->user_id);

            $total = $user_pay_amount = $live_video->amount ?? 0.00;

            $request->request->add(['payment_mode' => PAYMENT_MODE_WALLET,'paid_amount' => $total]);

            if($user_pay_amount > 0) {
                
                $request->request->add([
                    'total' => $total,
                    'user_pay_amount' => $user_pay_amount,
                    'paid_amount' => $user_pay_amount,
                    'payment_id' => 'LC-'.rand(),
                    'usage_type' => LIVE_VIDEO
                ]);

                $wallet_payment_response = PaymentRepo::user_wallets_payment_save($request)->getData();
                
                if($wallet_payment_response->success) {

                    $payment_response = PaymentRepo::live_videos_payment_save($request,$live_video)->getData();

                    if(!$payment_response->success) {

                        throw new Exception($payment_response->error, $payment_response->error_code);
                    }

                    DB::commit();

                    return $this->sendResponse(api_success(118), 118, $payment_response->data ?? []);

                } else {

                    throw new Exception($wallet_payment_response->error, $wallet_payment_response->error_code);
                    
                }
            
            }


        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method live_videos_payment_history()
     *
     * @uses get the current live streaming videos
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_videos_payment_history(Request $request) {

        try {

            $base_query = $total_query = LiveVideoPayment::where('live_video_viewer_id', $request->id)->orderBy('live_video_payments.id', 'desc');

            $live_video_payments = $base_query->skip($this->skip)->take($this->take)->get();

            foreach ($live_video_payments as $key => $live_video_payment) {

                $live_video = $live_video_payment->getVideo ?? [];

                $live_video_payment->title = $live_video->title ?? "-";

                $live_video_payment->description = $live_video->description ?? "-";

                $live_video_payment->snapshot = $live_video->snapshot ?? asset('images/live-streaming.jpeg');

                $user = $live_video_payment->getUser ?? [];

                $live_video_payment->user_name = $user->name ?? "user-deleted";

                $live_video_payment->user_picture = $user->picture ?? asset('placeholder.jpg');

                unset($live_video_payment->getUser);

                unset($live_video_payment->getVideo);
            }

            $data['live_video_payments'] = $live_video_payments ?? emptyObject();

            $data['total'] = $total_query->count() ?? [];

            return $this->sendResponse($message = '', $code = '', $data);

            return $this->sendResponse($message = '', $code = '', $live_video_payments);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method live_videos_broadcast_create()
     *
     * @uses get the current live streaming videos
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_videos_broadcast_create(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start
            $rules = [
                'title' => 'required',
                'description' => 'required|max:255',
                // 'payment_status'=>'required|numeric',
                // 'amount' => $request->payment_status ? 'required|numeric|min:0.01|max:100000' : '',
                'product_ids' => 'required',
                'category_id' => 'required|exists:categories,id',
                'sub_category_id' => 'required|exists:sub_categories,id',
                'live_schedule_type'=> 'nullable|numeric',
                'schedule_time' => $request->live_schedule_type == LIVE_SCHEDULE_TYPE_LATER ? 'required' : '',
                'preview_file' => 'required',
                'preview_file_type' => 'required',

            ];

            $custom_errors = [
                'product_ids.required' => 'Choose a product and continue',
                'category_id.required' => "Please choose category",
                'sub_category_id.required' => "Please choose sub category",
                'preview_file.required' => "Please upload the preview file",
                'preview_file_type.required' => "Please reupload the preview file and continue",

            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors);
            
            if(Setting::get('is_agora_configured')) {

                if($request->device_type != DEVICE_WEB) {

                    $rules = [
                        'virtual_id' => 'required',
                    ];

                    Helper::custom_validator($request->all(), $rules, $custom_errors = []);

                }

                $agora_app_id = Setting::get('agora_app_id');

                $appCertificate = Setting::get('agora_certificate_id');

                if(!$agora_app_id || !$appCertificate) {

                    throw new Exception(api_error(204), 204);
                    
                }


            }
            
            // Validation end

            $user = $this->loginUser;

            if($user->is_content_creator != CONTENT_CREATOR){

                throw new Exception(api_error(218), 218);
            }

            $schedule_time = $request->schedule_time;

            $current_time = common_date(date('Y-m-d H:i:s'), $this->timezone);

            if($schedule_time){

                if(strtotime($schedule_time) <= strtotime($current_time)) {

                    throw new Exception(api_error(250), 250);
                    
                }
            }

            // Check the user have any ongoing streaming

            $check_ongoing_streaming = LiveVideo::where('user_id', $request->id)->where('status', VIDEO_STREAMING_ONGOING)->count();

            if($check_ongoing_streaming) {
                // throw new Exception(api_error(200), 200);
            }

            $live_video = new LiveVideo;

            $live_video->user_id = $request->id;

            $live_video->title = $request->title;

            $live_video->description = $request->description ?? "";

            $live_video->type = $request->type ?? TYPE_PUBLIC;
            
            $live_video->broadcast_type = $request->broadcast_type ?? BROADCAST_TYPE_BROADCAST;

            $live_video->payment_status = $request->payment_status ?? FREE_VIDEO;

            $live_video->amount = $request->amount ?? 0.00;

            $live_video->status = $request->live_schedule_type == LIVE_SCHEDULE_TYPE_LATER ? VIDEO_STREAMING_CREATED : VIDEO_STREAMING_ONGOING;

            $live_video->is_streaming = $request->live_schedule_type == LIVE_SCHEDULE_TYPE_LATER ? IS_STREAMING_NO : IS_STREAMING_YES;

            $live_video->virtual_id = $request->virtual_id ?? md5(time());

            $live_video->unique_id = $live_video->title ?? "";

            $live_video->browser_name = $request->browser ?? '';

            $live_video->start_time = getUserTime(date('H:i:s'), $this->timezone, "H:i:s");

            $live_video->schedule_time = $request->schedule_time ? common_server_date($request->schedule_time, $this->timezone, 'Y-m-d H:i:s') : ($live_video->schedule_time ?? date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s"))));

            if($request->hasFile('preview_file')) {

                $live_video->preview_file = Helper::storage_upload_file($request->file('preview_file'), COMMON_FILE_PATH);

                compress_image($live_video->preview_file);

            }

            $live_video->preview_file_type = $request->preview_file_type ?? '';

            $live_video->live_schedule_type = $request->live_schedule_type ?? '';

            $live_video->category_id = $request->category_id ?? '';

            $live_video->sub_category_id = $request->sub_category_id ?? '';

            if($request->live_schedule_type == LIVE_SCHEDULE_TYPE_NOW) {

                $token = '';

                if(Setting::get('is_agora_configured')) { 

                    $uid = 0;

                    $role = \RtcTokenBuilder::RoleAttendee;

                    $expireTimeInSeconds = 3600;

                    $currentTimestamp = (new \DateTime("now", new \DateTimeZone('UTC')))->getTimestamp();

                    $privilegeExpiredTs = $currentTimestamp + $expireTimeInSeconds;

                    $token = \RtcTokenBuilder::buildTokenWithUid($agora_app_id, $appCertificate, $live_video->virtual_id, $uid, $role, $privilegeExpiredTs);

                }

                $live_video->agora_token = $token ?? '';

            }
            
            if($live_video->save()){

                if($request->product_ids) {
                    
                    $product_ids = $request->product_ids;
                    
                    if(!is_array($product_ids)) {

                        $product_ids = explode(',', $product_ids);
                        
                    }

                    if($request->live_video_id) {
                    
                        LiveVideoProduct::where('live_video_id', $request->live_video_id)->delete();
                    }                    

                    foreach ($product_ids as $key => $product_id) {

                        $product = new LiveVideoProduct;

                        $product->live_video_id = $live_video->id;
                        
                        $product->user_id = $request->id;

                        $product->user_product_id = $product_id;

                        $product->status = APPROVED;
                        
                        $product->save();

                    } 

                }
            }

            DB::commit();

            $this->dispatch(new \App\Jobs\LiveVideoNotificationToFollower($request->id, $live_video->id, $request->live_group_id));

            $data = LiveVideo::where('live_videos.id', $live_video->id)->first();

            $data->schedule_time_formatted = $data->live_schedule_type == LIVE_SCHEDULE_TYPE_LATER ? formatted_schedule_time($data->schedule_time, $this->timezone) : "";

            return $this->sendResponse(api_success(204), $code = 204, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method live_videos_viewer_update()
     *
     * @uses get the current live streaming videos
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_videos_viewer_update(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['live_video_id' => 'required|exists:live_videos,id'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // Validation end

            $live_video = LiveVideo::where('live_videos.id', $request->live_video_id)->first();

            if(!$live_video) {

                throw new Exception(api_error(201), 201);
                
            }
            
            if($live_video->is_streaming == IS_STREAMING_NO || $live_video->status == VIDEO_STREAMING_STOPPED) {

                throw new Exception(api_error(171), 171);
                
            }

            if ($live_video->user_id == $request->id) {
                
                throw new Exception(api_error(171), 171);

            }

            $viewer = Viewer::where('live_video_id', $request->live_video_id)->where('user_id', $request->id)->first();

            if(!$viewer) {

                $live_video->viewer_cnt += 1;

                $live_video->save();

                $viewer = new Viewer;

                $viewer->user_id = $request->id;

                $viewer->live_video_id = $request->live_video_id;

                $viewer->count += 1;

                $viewer->save();

            }

            DB::commit();

            $data = ['live_video_id' => $request->live_video_id, 'viewer_cnt' => $live_video->viewer_cnt,'virtual_id' => $live_video->virtual_id];

            return $this->sendResponse(api_success(203), $code = 203, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method live_videos_snapshot_save()
     *
     * @uses get the current live streaming videos
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_videos_snapshot_save(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['live_video_id' => 'required|exists:live_videos,id', 'snapshot' => 'required'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // Validation end

            $live_video = LiveVideo::where('live_videos.id', $request->live_video_id)->first();

            if(!$live_video) {

                throw new Exception(api_error(201), 201);
                
            }

            if($live_video->is_streaming == IS_STREAMING_NO || $live_video->status == VIDEO_STREAMING_STOPPED) {

                throw new Exception(api_error(171), 171);
                
            }

            if ($request->device_type == DEVICE_IOS) {

                $picture = $request->file('snapshot');
                
                $ext = $picture->getClientOriginalExtension();

                $picture->move(public_path().'/uploads/rooms/', $request->live_video_id . "." . $ext);

                $live_video->snapshot = url('/').'/uploads/rooms/'.$request->live_video_id . '.png';

            } else {

                $data = explode(',', $request->get('snapshot'));

                file_put_contents(join(DIRECTORY_SEPARATOR, [public_path(), 'uploads', 'rooms', $request->live_video_id . '.png']), base64_decode($data[1]));

                $live_video->snapshot = url('/').'/uploads/rooms/'.$request->live_video_id . '.png';
            }  

            $live_video->save();

            // @todo Wowza stop 

            DB::commit();

            $data = ['live_video_id' => $request->live_video_id];

            return $this->sendResponse(api_success(139), $code = 139, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method live_videos_broadcast_stop()
     *
     * @uses get the current live streaming videos
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_videos_broadcast_stop(Request $request) {

        try {
            
            DB::beginTransaction();

            // Validation start
            $rules = ['live_video_id' => 'required|exists:live_videos,id'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // Validation end

            $live_video = LiveVideo::where('live_videos.id', $request->live_video_id)->first();

            if(!$live_video) {

                throw new Exception(api_error(201), 201);
                
            }
            
            if($live_video->is_streaming == IS_STREAMING_NO || $live_video->status == VIDEO_STREAMING_STOPPED) {

                throw new Exception(api_error(202), 202);
                
            }
            
            $live_video->is_streaming = IS_STREAMING_NO;

            $live_video->status = VIDEO_STREAMING_STOPPED;

            $live_video->save();

            $live_video->end_time = common_date(date('H:i:s'), $this->timezone, 'H:i:s');
            
            $live_video->no_of_minutes = getMinutesBetweenTime($live_video->start_time, $live_video->end_time);

            $live_video->save();

            DB::commit();

            $data = ['live_video_id' => $request->live_video_id];

            return $this->sendResponse(api_success(201), $code = 201, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method live_videos_check_streaming()
     *
     * @uses get the current live streaming videos
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_videos_check_streaming(Request $request) {

        try {

            // Validation start

            $rules = ['live_video_id' => 'required|exists:live_videos,id'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // Validation end

            $live_video = LiveVideo::where('live_videos.id', $request->live_video_id)->first();

            if(!$live_video) {

                throw new Exception(api_error(201), 201);
                
            }

            if($live_video->is_streaming == IS_STREAMING_NO) {

                throw new Exception(api_error(203), 203);
            
            }

            if($live_video->status == VIDEO_STREAMING_STOPPED) {

                throw new Exception(api_error(203), 203);
                
            }

            $data = ['viewer_cnt' => $live_video->viewer_cnt];

            return $this->sendResponse($message = '', $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method live_videos_erase_old_streamings()
     *
     * @uses get the current live streaming videos
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_videos_erase_old_streamings(Request $request) {

        try {

            DB::beginTransaction();

            LiveVideo::where('user_id', $request->id)->where('status', VIDEO_STREAMING_ONGOING)->where('is_streaming', IS_STREAMING_NO)->delete();

            $live_videos = LiveVideo::where('user_id', $request->id)->where('status', VIDEO_STREAMING_ONGOING)->where('is_streaming', IS_STREAMING_YES)->get();

            foreach($live_videos as $key => $live_video) {

                $live_video->status = DEFAULT_TRUE;

                $live_video->end_time = getUserTime(date('H:i:s'), $this->timezone, 'H:i:s');

                $live_video->no_of_minutes = getMinutesBetweenTime($live_video->start_time, $live_video->end_time);

                $live_video->save();

            }

            DB::commit();

            return $this->sendResponse(api_success(205), $code = 205, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method live_videos_owner_list()
     *
     * @uses get the current live streaming videos
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_videos_owner_list(Request $request) {

        try {

            $rules = [
                'id' => 'required_without:user_unique_id|exists:users,id',
                'user_unique_id' => 'required_without:id|exists:users,unique_id'
            ];

            if($request->user_unique_id) { 

                $user = User::where('unique_id',$request->user_unique_id)->first();

            } else {
                
                $user = User::where('id',$request->id)->first();
            }

            $live_videos_upcomings = $live_videos_upcomings_query = LiveVideo::where('live_videos.user_id', $user->id)->where('status', VIDEO_STREAMING_CREATED)->orderBy('created_at', 'desc');

            $data['total_live_video_upcomings'] = $live_videos_upcomings_query->count() ?: 0;

            $live_videos_upcomings = $live_videos_upcomings->skip($this->skip)->take($this->take)->get();

            $live_videos_upcomings = LiveVideoRepo::live_streaming_upcoming_list_response($live_videos_upcomings, $request);

            $data['live_video_upcomings'] = $live_videos_upcomings;

            $live_video_upcoming_ids = $live_videos_upcomings_query = LiveVideo::where('live_videos.user_id', $user->id)->where('status', VIDEO_STREAMING_CREATED)->where('live_schedule_type',LIVE_SCHEDULE_TYPE_LATER)->where('schedule_time', '>=', \DB::raw('NOW()'))->orderBy('created_at','desc')->pluck('id');

            $base_query = $total_query = LiveVideo::where('live_videos.user_id', $user->id)->orderBy('created_at', 'desc')->where('live_schedule_type',LIVE_SCHEDULE_TYPE_NOW);

            $data['total'] = $total_query->count() ?? 0;

            $live_videos = $base_query->skip($this->skip)->take($this->take)->get();
            
            $data['live_videos'] = $live_videos ?? [];

            return $this->sendResponse($message = '', $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method live_videos_scheduled_owner()
     *
     * @uses get the current live streaming videos
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_videos_scheduled_owner(Request $request) {

        try {

            $rules = [
                'id' => 'required_without:user_unique_id|exists:users,id',
                'user_unique_id' => 'required_without:id|exists:users,unique_id'
            ];

            if($request->user_unique_id) { 

                $user = User::where('unique_id',$request->user_unique_id)->first();

            } else {
                
                $user = User::where('id',$request->id)->first();
            }

            $live_videos_upcomings = $live_videos_upcomings_query = LiveVideo::where('live_videos.user_id', $user->id)->where('live_schedule_type', LIVE_SCHEDULE_TYPE_LATER)->where('schedule_time', '>=', \DB::raw('NOW()'))->orderBy('created_at', 'desc');

            $data['total_live_video_upcomings'] = $live_videos_upcomings_query->count() ?: 0;

            $live_videos_upcomings = $live_videos_upcomings->skip($this->skip)->take($this->take)->get();

            $live_videos_upcomings = LiveVideoRepo::live_streaming_upcoming_list_response($live_videos_upcomings, $request);

            $data['live_video_upcomings'] = $live_videos_upcomings;

            return $this->sendResponse($message = '', $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method live_videos_owner_view()
     *
     * @uses get the current live streaming videos
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_videos_owner_view(Request $request) {

        try {

            // Validation start

            $rules = [
                    'live_video_id' => 'required|exists:live_videos,id'
                    ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // Validation end

            $live_video = LiveVideo::where('live_videos.id', $request->live_video_id)->where('live_videos.user_id', $request->id)->first();

            if(!$live_video) {

                throw new Exception(api_error(201), 201);
                
            }

            return $this->sendResponse($message = '', $code = '', $live_video);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method live_video_chat_messages()
     *
     * @uses
     *
     * @created Ganesh
     *
     * @updated Ganesh
     *
     * @param
     * 
     * @return JSON response
     *
     */
    public function live_video_chat_messages(Request $request) {

        try {

            $rules = [
                'search_key' => 'nullable',
                'live_video_id' => 'required_without:live_video_unique_id|exists:live_videos,id',
                'live_video_unique_id' => 'required_without:live_video_id|exists:live_videos,unique_id'
            ];

            Helper::custom_validator($request->all(), $rules);

            if($request->live_video_unique_id) { 

                $live_video = LiveVideo::where('live_videos.unique_id', $request->live_video_unique_id)->first();
            } else {
                $live_video = LiveVideo::where('live_videos.id', $request->live_video_id)->first();
            }

            $search_key = $request->search_key;

            $base_query = $total_query = \App\LiveVideoChatMessage::where('live_video_chat_messages.message', 'like', "%".$search_key."%")
                    ->with('liveVideo')->where('live_video_id',$live_video->id)->orderBy('live_video_chat_messages.updated_at', 'asc');

            $live_video_chat_messages = $base_query->skip($this->skip)->take($this->take)->get();

            foreach ($live_video_chat_messages as $key => $value) {

                $value->created = $value->created_at->diffForHumans() ?? "";
            }
            
            $data['messages'] = $live_video_chat_messages ?? [];

            $data['total'] = $total_query->count() ?: 0;

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

     /** 
     * @method upcoming_live_streamings()
     *
     * @uses get the upcoming live streaming videos
     *
     * @created Jeevan
     *
     * @updated 
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function upcoming_live_streamings(Request $request) {

        try {

            $base_query = $total_query = LiveVideo::where('status', VIDEO_STREAMING_CREATED)->where('schedule_time', '>=', \DB::raw('NOW()'))->orderBy('created_at', 'desc');

            $live_streamings = $base_query->skip($this->skip)->take($this->take)->get();

            $live_streamings = LiveVideoRepo::live_streaming_upcoming_list_response($live_streamings, $request);

            $data['live_streamings'] = $live_streamings;

            $data['total'] = $total_query->count() ?: 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

     /** 
     * @method live_video_chat_message_save_api()
     *
     * @uses
     *
     * @created Jeevan
     *
     * @updated 
     *
     * @param
     * 
     * @return JSON response
     *
     */
    public function live_video_chat_message_save_api(Request $request) {
        try {

            Log::info("message_save".print_r($request->all() , true));

            DB::beginTransaction();
            
            $rules = [
                'from_user_id' => 'required|exists:users,id',
                'live_video_id' => 'required|exists:users,id',
                'message' => 'required',
            ];

            Helper::custom_validator($request->all(),$rules);
            
            CommonRepo::chat_user_update($request->from_user_id,$request->live_video_id);

            if($request->chat_message_id) {

                $chat_message = LiveVideoChatMessage::find($request->chat_message_id) ?? new LiveVideoChatMessage;

            } else {

                $chat_message = new LiveVideoChatMessage;
 
            }

            $chat_message->from_user_id = $request->from_user_id;

            $chat_message->live_video_id = $request->live_video_id;

            $chat_message->message = $request->message ?? '';

            $chat_message->status = APPROVED;

            $chat_message->save();

            DB::commit();

            return $this->sendResponse("", "", $chat_message);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }
    

    /** 
     * @method live_streamings_ongoing()
     *
     * @uses get the upcoming live streaming videos
     *
     * @created Vithya R
     *
     * @updated 
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_streamings_ongoing(Request $request) {

        try {

            $base_query = $total_query = LiveVideo::CurrentLive()->orderBy('created_at', 'desc');

            $live_streamings = $base_query->skip($this->skip)->take($this->take)->get();

            $live_streamings = LiveVideoRepo::live_videos_list_response($live_streamings, $request);

            $data['live_streamings'] = $live_streamings;

            $data['total'] = LiveVideo::CurrentLive()->count() ?: 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /**
     * @method live_video_bookmarks_save()
     *
     * @uses Live Video bookmarks
     *
     * @created vithya
     *
     * @updated vithya
     *
     * @param object $request
     *
     * @return JSON Response
     */
    public function live_video_bookmarks_save(Request $request) {

        try {
            
            DB::begintransaction();

            $rules = ['live_video_id' => 'required|exists:live_videos,id'];

            Helper::custom_validator($request->all(),$rules, []);

            $live_video = LiveVideo::find($request->live_video_id);

            $check_bookmarks = \App\LiveVideoBookmark::where('user_id', $request->id)->where('live_video_id', $request->live_video_id)->first();

            // Check the bookmark already exists 
            if($check_bookmarks) {

                $live_video_bookmark = \App\LiveVideoBookmark::destroy($check_bookmarks->id);

                $code = 154;

            } else {

                $custom_request = new Request();

                $custom_request->request->add(['user_id' => $request->id, 'live_video_id' => $request->live_video_id]);

                $live_video_bookmark = \App\LiveVideoBookmark::updateOrCreate($custom_request->request->all());

                $code = 143;

            }

            DB::commit(); 

            $data['live_video_bookmark'] = $live_video_bookmark;

            $live_video->is_video_bookmarked = $live_video->liveVideoBookmarks->where('user_id', $request->id)->count() ? YES : NO;

            $live_video->schedule_time_formatted = formatted_schedule_time($live_video->schedule_time, $this->timezone);

            $data['live_video'] = $live_video;

            return $this->sendResponse(api_success($code), $code, $data);
            
        } catch(Exception $e){ 

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        } 
    
    }

    /** 
     * @method live_video_products_list()
     *
     * @uses get the current live streaming videos
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function live_video_products_list(Request $request) {

        try {

            // Validation start

            $rules = [
                'live_video_id' => 'required_without:live_video_unique_id|exists:live_videos,id',
                'live_video_unique_id' => 'required_without:live_video_id|exists:live_videos,unique_id'
            ];

            Helper::custom_validator($request->all(), $rules);

            if($request->live_video_unique_id) { 

                $live_video = LiveVideo::where('live_videos.unique_id', $request->live_video_unique_id)->first();
            } else {
                $live_video = LiveVideo::where('live_videos.id', $request->live_video_id)->first();
            }

            $products_ids = LiveVideoProduct::where('live_video_id',$live_video->id)->pluck('user_product_id');

            $base_query = $total_query =  UserProduct::whereIn('user_products.id',$products_ids);

            if($request->filled('search_key')) {

                $base_query = $base_query->where(function ($query) use ($request) {
                                    $query->where('name', "LIKE", "%" . $request->search_key . "%");
                                });
             }

            if($request->type == 'available') {

                $base_query = $base_query->where('is_outofstock',PRODUCT_AVAILABLE);

            } else if($request->type == 'sold'){

                $base_query = $base_query->where('is_outofstock',PRODUCT_NOT_AVAILABLE);

            }

            $data['live_video_products_count'] = $base_query->count();

            $live_video_products = $base_query->skip($this->skip)->take($this->take)->get();

            $data['live_video_products'] = $live_video_products;

            $purchased_product_ids = OrderProduct::whereIn('user_product_id',$products_ids)->where('live_video_id',$live_video->id)->where('user_id',$request->id)->pluck('user_product_id');

            $data['purchased_products'] = UserProduct::whereIn('user_products.id',$purchased_product_ids)->get();

            $data['products_available'] = UserProduct::whereIn('user_products.id',$products_ids)->where('is_outofstock',PRODUCT_AVAILABLE)->count();

            $data['products_sold'] = UserProduct::whereIn('user_products.id',$products_ids)->where('is_outofstock',PRODUCT_NOT_AVAILABLE)->count();

            $data['products_purchased'] = OrderProduct::whereIn('user_product_id',$products_ids)->where('live_video_id',$live_video->id)->where('user_id',$request->id)->count();

            $data['user'] = User::find($live_video->user_id);

            return $this->sendResponse($message = '', $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /**
     * @method live_video_bookmarks()
     *
     * @uses Live Video bookmarks
     *
     * @created vithya
     *
     * @updated vithya
     *
     * @param object $request
     *
     * @return JSON Response
     */
    public function live_video_bookmarks(Request $request) {

        try {

            $live_video_ids = \App\LiveVideoBookmark::where('user_id', $request->id)->pluck('live_video_id')->toArray();

            $base_query = $total_query = LiveVideo::FeaturedLiveVideos()->whereIn('id',$live_video_ids);

            $data['total'] = $total_query->count() ?: 0;

            $live_video_bookmarks = $base_query->skip($this->skip)->take($this->take)->get();
            
            $live_video_bookmarks = LiveVideoRepo::live_videos_list_response($live_video_bookmarks, $request);
            
            $data['live_video_bookmarks'] = $live_video_bookmarks;

            return $this->sendResponse($message = '', $code = '', $data);
            
        } catch(Exception $e){ 

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        } 
    
    }

    /**
     * @method live_videos_orders_list()
     *
     * @uses To display all the orders list of live video
     *
     * @created
     *
     * @updated 
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function live_videos_orders_list(Request $request) {

        try {

            $rules = [
                'live_video_id' => 'required|exists:live_videos,id,user_id,'.$request->id,
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $order_ids = OrderProduct::where('live_video_id', $request->live_video_id)->pluck('order_id');

            $base_query = $total_query = Order::whereIn('id',$order_ids)->orderBy('orders.created_at', 'desc');

            $orders = $base_query->skip($this->skip)->take($this->take)->get();

            $orders = ProductRepository::user_products_orders_list_response($orders, $request);

            $data['orders'] = $orders ?? [];

            $data['total'] = $total_query->count() ?? 0;

            $data['live_video'] = LiveVideo::find($request->live_video_id) ?? [];

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }
}
