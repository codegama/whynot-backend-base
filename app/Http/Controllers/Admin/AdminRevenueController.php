<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Helpers\Helper, App\Helpers\EnvEditorHelper;

use DB, Hash, Setting, Auth, Validator, Exception, Enveditor;

use App\Jobs\SendEmailJob;

use Carbon\Carbon;

use App\{UserProduct, LiveVideo};

use App\Repositories\PaymentRepository as PaymentRepo;


class AdminRevenueController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {

        $this->middleware('auth:admin');

        $this->skip = $request->skip ?: 0;
       
        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

    }

    /**
     * @method main_dashboard()
     *
     * @uses Show the application dashboard.
     *
     * @created vithya
     *
     * @updated vithya
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function main_dashboard() {
        
        $data = new \stdClass;

        $data->total_users = \App\User::count();

        $data->total_premium_users = \App\User::where('user_account_type', USER_PREMIUM_ACCOUNT)->count();

        $data->total_sellers = \App\User::where('is_content_creator', CONTENT_CREATOR)->count();

        $data->total_live_streaming = LiveVideo::where('is_streaming',IS_STREAMING_NO)->count();

        $data->order_payments = \App\OrderPayment::where('status',PAID)->sum('total');

        $data->total_revenue = $data->order_payments;

        $data->recent_users= \App\User::orderBy('id' , 'desc')->skip($this->skip)->take(TAKE_COUNT)->get();

        $data->recent_premium_users = \App\User::where('user_account_type', USER_PREMIUM_ACCOUNT)->orderBy('id' , 'desc')->skip($this->skip)->take(TAKE_COUNT)->get();

        $data->recent_featured_seller = \App\User::where('is_featured_seller', IS_FEATURED_SELLER)->orderBy('id' , 'desc')->skip($this->skip)->take(TAKE_COUNT)->get(); 
        

        $data->analytics = last_x_months_data(12);

        $data->stream_data = last_x_months_stream(11) ?? [];
       
        $data->blocked_users = \App\BlockUser::count();

        return view('admin.dashboard')
                    ->with('page' , 'dashboard')
                    ->with('data', $data);
    
    }
    

    /**
     * @method order_payments()
     *
     * @uses Display the lists of order payments
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     */

    public function order_payments(Request $request) {

        $base_query = \App\OrderPayment::where('status',APPROVED);

        if($request->order_id) {

            $base_query = $base_query->where('order_id',$request->order_id);
        }

        if($request->search_key) {

            $search_key = $request->search_key;

            $base_query = $base_query

                ->whereHas('user',function($query) use($search_key){

                    return $query->where('users.name','LIKE','%'.$search_key.'%');

                })->orWhere('order_payments.unique_id','LIKE','%'.$search_key.'%')
                ->orWhere('order_payments.payment_id','LIKE','%'.$search_key.'%');

        }

        $order_payments = $base_query->paginate(10);
       
        return view('admin.orders.payments')
                ->with('page','payments')
                ->with('sub_page','order-payments')
                ->with('order_payments',$order_payments);
    }


    /**
     * @method order_payments_view()
     *
     * @uses Display the specified order payment details
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     */

    public function order_payments_view(Request $request) {

        try {

            $order_payment = \App\OrderPayment::where('id',$request->order_payment_id)->first();

            if(!$order_payment) {

                throw new Exception(tr('order_payment_not_found'), 1);
                
            }
           
            return view('admin.orders.payments_view')
                    ->with('page','payments')
                    ->with('sub_page','order-payments')
                    ->with('order_payment',$order_payment);

        } catch(Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    }

    /**
     * @method revenue_dashboard()
     *
     * @uses Show the revenue dashboard.
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function revenues_dashboard() {
        
        $data = new \stdClass;

        $start_month = Carbon::now()->startOfMonth();

        $start_week = Carbon::now()->startOfWeek();

        $data->total_order_payments = \App\OrderPayment::where('status',PAID)->sum('total');

        $data->today_order_payments = \App\OrderPayment::where('status',PAID)->whereDate('created_at',today())->sum('total');

        $data->current_month_payments = \App\OrderPayment::where('status',PAID)->whereDate('created_at','>=',$start_month)->sum('total');

        $data->current_week_payments = \App\OrderPayment::where('status',PAID)->whereDate('created_at','>=',$start_week)->sum('total');

        $data->analytics = revenue_graph(6);
        
        return view('admin.revenues.dashboard')
                    ->with('page' , 'revenue-dashboard')
                    ->with('data', $data);
    
    }


    /**
     * @method user_withdrawals
     *
     * @uses Display all stardom withdrawals
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Subscription Id
     * 
     * @return response success/failure message
     *
     **/

    public function user_withdrawals(Request $request) {

        $base_query = \App\UserWithdrawal::orderBy('user_withdrawals.id', 'desc');

        if($request->has('status')) {

            $base_query = $base_query->where('user_withdrawals.status',$request->status);
        }

        if($request->search_key) {

            $search_key = $request->search_key;

            $base_query = $base_query->whereHas('user',function($query) use($search_key){

                return $query->where('users.name','LIKE','%'.$search_key.'%');

            })->orWhere('user_withdrawals.payment_id','LIKE','%'.$search_key.'%');
        }

        if($request->user_id) {

            $base_query = $base_query->where('user_withdrawals.user_id',$request->user_id);
        }


        $user = \App\User::find($request->user_id)??'';

        $user_withdrawals = $base_query->paginate($this->take);
       
        return view('admin.user_withdrawals.index')
                ->with('page', 'content_creator-withdrawals')
                ->with('user', $user)
                ->with('user_withdrawals', $user_withdrawals);

    }


    /**
     * @method user_withdrawals_view
     *
     * @uses Display all stardom specified 
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Subscription Id
     * 
     * @return response success/failure message
     *
     **/

    public function user_withdrawals_view(Request $request) {

          try {

            $user_withdrawal = \App\UserWithdrawal::where('id',$request->user_withdrawal_id)->first();

            if(!$user_withdrawal) { 

                throw new Exception(tr('user_withdrawal_not_found'), 101);                
            }  

            $billing_account = \App\UserBillingAccount::where('user_id', $user_withdrawal->user_id)->first();
       
            return view('admin.user_withdrawals.view')
                ->with('page', 'content_creator-withdrawals')
                ->with('user_withdrawal', $user_withdrawal)
                ->with('billing_account',$billing_account);

        } catch(Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());

        }

    }

     /**
     * @method user_withdrawals_paynow()
     *
     * @uses 
     *
     * @created Akshata
     *
     * @updated
     *
     * @param Integer $request - stardom withdrawal id
     * 
     * @return view page
     *
     **/
    public function user_withdrawals_paynow(Request $request) {

        try {

            DB::begintransaction();

            $user_withdrawal = \App\UserWithdrawal::find($request->user_withdrawal_id);

            if(!$user_withdrawal) {

                throw new Exception(tr('user_withdrawal_not_found'),101);
                
            }

            $user_withdrawal->paid_amount = $user_withdrawal->requested_amount;

            $user_withdrawal->status = WITHDRAW_PAID;
            
            if($user_withdrawal->save()) {

                \App\Repositories\PaymentRepository::user_wallet_update_withdraw_paynow($user_withdrawal->requested_amount, $user_withdrawal->user_id);

                DB::commit();

                $email_data['subject'] = Setting::get('site_name');

                $email_data['page'] = "emails.users.withdrawals-approve";
    
                $email_data['data'] = $user_withdrawal->user;
    
                $email_data['email'] = $user_withdrawal->user->email ?? '';

                $email_data['message'] = tr('user_withdraw_paid_description');

                dispatch(new SendEmailJob($email_data));

                return redirect()->back()->with('flash_success',tr('payment_success'));
            }

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }
    
    }

    /**
     * @method user_withdrawals_reject()
     *
     * @uses 
     *
     * @created Akshata
     *
     * @updated
     *
     * @param Integer $request - stardom withdrawal id
     * 
     * @return view page
     *
     **/
    public function user_withdrawals_reject(Request $request) {

        try {

            DB::begintransaction();

            $user_withdrawal = \App\UserWithdrawal::find($request->user_withdrawal_id);

            if(!$user_withdrawal) {

                throw new Exception(tr('user_withdrawal_not_found'),101);
                
            }
            
            $user_withdrawal->status = WITHDRAW_DECLINED;
            
            if($user_withdrawal->save()) {

                DB::commit();

                PaymentRepo::user_wallet_update_withdraw_cancel($user_withdrawal->requested_amount, $user_withdrawal->user_id);

                $user_wallet_payment = \App\UserWalletPayment::where('id', $user_withdrawal->user_wallet_payment_id)->first();

                if($user_wallet_payment) {

                    $user_wallet_payment->status = USER_WALLET_PAYMENT_CANCELLED;

                    $user_wallet_payment->save();
                }

                
                $email_data['subject'] = Setting::get('site_name');

                $email_data['page'] = "emails.users.withdrawals-decline";
    
                $email_data['data'] = $user_withdrawal->user;
    
                $email_data['email'] = $user_withdrawal->user->email ?? '';

                $email_data['message'] = tr('user_withdraw_decline_description');

                dispatch(new SendEmailJob($email_data));

                return redirect()->back()->with('flash_success',tr('user_withdrawal_rejected'));
            }

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }
    
    }

     /**
     * @method product_inventories_index()
     *
     * @uses Display the total inventory
     *
     * @created Akshata
     *
     * @updated
     *
     * @param -
     *
     * @return view page 
     */
    public function product_inventories_index(Request $request) {

        $title = tr('view_product_inventories');

        $base_query = \App\ProductInventory::orderBy('created_at','DESC');

        if($request->search_key) {

            $search_key = $request->search_key;

            $base_query =  $base_query

                ->whereHas('userProductDetails', function($q) use ($search_key) {

                    return $q->Where('user_products.name','LIKE','%'.$search_key.'%');

                });
                        
        }

        if($request->user_product_id) {

            $base_query = $base_query->where('user_product_id',$request->user_product_id);

            $product = UserProduct::where('user_products.id', $request->user_product_id)->first();
            
            $title = tr('view_product_inventories')." - ".$product->name;
        }

        $product_inventories = $base_query->paginate(10);

        return view('admin.user_products.inventories.index')
                    ->with('page','product-inventories')
                    ->with('title',$title)
                    ->with('product_inventories' , $product_inventories);
    }

    /**
     * @method product_inventories_view()
     *
     * @uses Display the product inventories based on the product inentory id
     *
     * @created Akshata 
     *
     * @updated 
     *
     * @param object $request - product inventory Id
     * 
     * @return View page
     *
     */
    public function product_inventories_view(Request $request) {
       
        try {
      
            $product_inventory = \App\ProductInventory::find($request->product_inventory_id);

            if(!$product_inventory) { 

                throw new Exception(tr('product_inventory_not_found'), 101);                
            }
        
            return view('admin.user_products.inventories.view')
                        ->with('page', 'revenues') 
                        ->with('product_inventory',$product_inventory);
            
        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method user_wallets_index()
     *
     * @uses Display the lists of stardom users
     *
     * @created Akshata
     *
     * @updated
     *
     * @param -
     *
     * @return view page 
     */
    public function user_wallets_index(Request $request) {

        $base_query = \App\UserWallet::orderBy('created_at','DESC');

        if($request->search_key) {

            $search_key = $request->search_key;

            $base_query =  $base_query

                ->whereHas('user', function($q) use ($search_key) {

                    return $q->Where('users.name','LIKE','%'.$search_key.'%');

                })->orWhere('user_wallets.unique_id','LIKE','%'.$search_key.'%');
                        
        }

        if($request->user_id) {

            $base_query = $base_query->where('user_id',$request->user_id);
        }

        $user_wallets = $base_query->has('user')->where('total','>',0)->paginate(10);

        return view('admin.user_wallets.index')
                    ->with('page','user_wallets')
                    ->with('user_wallets' , $user_wallets);
    }

    /**
     * @method user_wallets_view()
     *
     * @uses display the transaction details of the perticulor stardom
     *
     * @created Akshata 
     *
     * @updated 
     *
     * @param object $request - stardom_wallet_id
     * 
     * @return View page
     *
     */
    public function user_wallets_view(Request $request) {
       
        try {
            
            $user_wallet = \App\UserWallet::where('user_id',$request->user_id)->first();
           
            if(!$user_wallet) { 

                $user_wallet = new \App\UserWallet;

                $user_wallet->user_id = $request->user_id;

                $user_wallet->save();

            }

            $user_wallet_payments = \App\UserWalletPayment::where('requested_amount','>',0)->where('user_id',$user_wallet->user_id)->orderBy('created_at','desc')->paginate(10);
                   
            return view('admin.user_wallets.view')
                        ->with('page', 'user_wallets') 
                        ->with('user_wallet', $user_wallet)
                        ->with('user_wallet_payments', $user_wallet_payments);
            
        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method user_wallet_payments_view()
     *
     * @uses display the transaction details of the perticular payment
     *
     * @created Jeevan 
     *
     * @updated 
     *
     * @param object $request - payment_id
     * 
     * @return View page
     *
     */
    public function user_wallet_payments_view(Request $request) {
        
        try {
              
            $user_wallet_payment = \App\UserWalletPayment::where('id',$request->payment_id)->first();
           
            if(!$user_wallet_payment) { 

                throw new Exception(tr('wallet_payment_view_not_found'), 101);
            }

            return view('admin.user_wallets.payment_view')
                        ->with('page', 'user_wallets') 
                        ->with('user_wallet_payment', $user_wallet_payment);
            
        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    
    }
    
}
