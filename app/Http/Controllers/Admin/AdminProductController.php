<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Helpers\Helper, App\Helpers\EnvEditorHelper;

use DB, Hash, Setting, Auth, Validator, Exception, Enveditor;

use App\Jobs\SendEmailJob;

use App\{User,UserProduct,OrderProduct,Category,SubCategory,ProductReview};

class AdminProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {

        $this->middleware('auth:admin');

        $this->skip = $request->skip ?: 0;
       
        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

    }

    /**
     * @method user_products_index()
     *
     * @uses To list out stardom products details 
     *
     * @created Akshata
     *
     * @updated Subham Kant
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function user_products_index(Request $request) {

        $base_query = UserProduct::with(['user'])->orderBy('updated_at','desc');

        if($request->search_key) {

        $user_product_ids = UserProduct::when($request->user_id, function($query) use ($request){

                            $query->where('user_id', $request->user_id);

                        })->when('category_id', function($query) use ($request) {

                            $query->where('category_id', $request->category_id);

                        })->when('status', function($query) use ($request) {

                            $query->where('status', $request->status);

                       })->when('is_outofstock', function($query) use ($request) {

                            $query->where('is_outofstock', $request->is_outofstock);

                        })->whereHas('user', function($query) use ($request) {

                            $query->where('name', "LIKE", "%" . $request->search_key . "%");

                       })->orWhere('name','LIKE','%'.$request->search_key.'%')
                        ->pluck('id');
                        

            $base_query = $base_query->whereIn('id', $user_product_ids);
        
        }

        if($request->status != '') {

            $base_query = $base_query->where('user_products.status',$request->status);

        }

        if($request->product_status != '') {

            $base_query = $base_query->where('user_products.is_outofstock',$request->product_status);

        }

        if($request->category_id){

            $base_query = $base_query->where('category_id',$request->category_id);
        }

        if($request->sub_category_id){

            $base_query = $base_query->where('sub_category_id',$request->sub_category_id);
        }

        if($request->user_id){

            $base_query = $base_query->where('user_id',$request->user_id);
        }

        $category = Category::find($request->category_id)??'';

        $subcategory = SubCategory::find($request->sub_category_id)??'';

        $user = User::find($request->user_id)??'';
       
        $user_products = $base_query->paginate(10);

        return view('admin.user_products.index')
                ->with('page', 'user_products')
                ->with('sub_page' , 'user_products-view')
                ->with('category',$category)
                ->with('subcategory',$subcategory)
                ->with('user',$user)
                ->with('user_products' , $user_products);
    }

    /**
     * @method user_products_create()
     *
     * @uses To create stardom product details
     *
     * @created  Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function user_products_create() {

        $user_product = new UserProduct;

        $users = User::Approved()->where('status', APPROVED)->where('is_content_creator', CONTENT_CREATOR)->get();

        $categories = Category::orderby('name', 'asc')->where('status',APPROVED)->get();

        $sub_categories = [];

        return view('admin.user_products.create')
                ->with('page', 'user_products')
                ->with('sub_page', 'user_products-create')
                ->with('user_product', $user_product)
                ->with('categories', $categories)
                ->with('sub_categories', $sub_categories)
                ->with('users', $users);           
    }

    /**
     * @method user_products_edit()
     *
     * @uses To display and update stardom product details based on the stardom product id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Stardom Product Id
     * 
     * @return redirect view page 
     *
     */
    public function user_products_edit(Request $request) {

        try {

            $user_product = UserProduct::find($request->user_product_id);

            if(!$user_product) { 

                throw new Exception(tr('user_product_not_found'), 101);
            }

            $users = User::Approved()->where('status', APPROVED)->get();

            foreach ($users as $key => $user) {

                $user->is_selected = NO;

                if($user_product->user_id == $user->id){
                    
                    $user->is_selected = YES;
                }

            }

            $categories = selected(Category::orderby('name', 'asc')->where('status',APPROVED)->get(), $user_product->category_id, 'id');

            $sub_categories = selected(SubCategory::where('category_id' , $user_product->category_id)->where('status',APPROVED)->get(), $user_product->sub_category_id, 'id');
            
            return view('admin.user_products.edit')
                        ->with('page' , 'user_products')
                        ->with('sub_page', 'user_products-view')
                        ->with('user_product', $user_product)
                        ->with('users', $users)
                        ->with('categories', $categories)
                        ->with('sub_categories', $sub_categories); 
            
        } catch(Exception $e) {

            return redirect()->route('admin.user_products.index')->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method user_products_save()
     *
     * @uses To save the stardom products details of new/existing stardom product object based on details
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object request - Stardom Product Form Data
     *
     * @return success message
     *
     */
    public function user_products_save(Request $request) {
        
        try {
            
            DB::begintransaction();

            $rules = [
                'name' => 'required|max:191',
                'price' => 'required|max:100',
                'quantity' => 'required|max:100',
                'picture' => 'mimes:jpg,png,jpeg',
                'discription' => 'max:199',
                'user_id' => 'required|exists:users,id',
            ];

            Helper::custom_validator($request->all(),$rules);

            $user_product = UserProduct::find($request->user_product_id) ?? new UserProduct;

            if($user_product->id) {

                $message = tr('user_product_updated_success'); 

            } else {

                $message = tr('user_product_created_success');

            }

            $user_product->user_id = $request->user_id ?: $user_product->user_id;

            $user_product->name = $request->name ?: $user_product->name;

            $user_product->quantity = $request->quantity ?: $user_product->quantity;

            $user_product->price = $request->price ?: '';

            $user_product->category_id = $request->category_id ?: $user_product->category_id;

            $user_product->sub_category_id = $request->sub_category_id ?: $user_product->sub_category_id;

            $user_product->description = $request->description ?: '';

            $user_product->is_outofstock = $request->user_product_id ? $user_product->is_outofstock : NO;

            // Upload picture
            
            if($request->hasFile('picture')) {

                if($request->user_product_id) {

                    Helper::storage_delete_file($user_product->picture, COMMON_FILE_PATH); 
                    // Delete the old pic
                }

                $user_product->picture = Helper::storage_upload_file($request->file('picture'), COMMON_FILE_PATH);
            }

            if($user_product->save()) {

                DB::commit();

                return redirect(route('admin.user_products.view', ['user_product_id' => $user_product->id]))->with('flash_success', $message);

            } 

            throw new Exception(tr('user_product_save_failed'));
            
        } catch(Exception $e){ 

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());

        } 

    }

    /**
     * @method user_products_view()
     *
     * @uses displays the specified user product details based on user product id
     *
     * @created Akshata 
     *
     * @updated 
     *
     * @param object $request - user product Id
     * 
     * @return View page
     *
     */
    public function user_products_view(Request $request) {
       
        try {
      
            $user_product = UserProduct::find($request->user_product_id);

            if(!$user_product) { 

                throw new Exception(tr('user_product_not_found'), 101);                
            }

            return view('admin.user_products.view')
                    ->with('page', 'user_products') 
                    ->with('sub_page', 'user_products-view')
                    ->with('user_product', $user_product);
            
        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method user_products_delete()
     *
     * @uses delete the stardom product details based on stardom id
     *
     * @created Akshata 
     *
     * @updated  
     *
     * @param object $request - Stardom Id
     * 
     * @return response of success/failure details with view page
     *
     */
    public function user_products_delete(Request $request) {

        try {

            DB::begintransaction();

            $user_product = UserProduct::find($request->user_product_id);
            
            if(!$user_product) {

                throw new Exception(tr('user_product_not_found'), 101);                
            }

            if($user_product->delete()) {

                DB::commit();

                return redirect()->route('admin.user_products.index')->with('flash_success',tr('user_product_deleted_success'));   

            } 
            
            throw new Exception(tr('user_product_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }       
         
    }

    /**
     * @method user_products_status
     *
     * @uses To update stardom product status as DECLINED/APPROVED based on stardom product id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Stardom Product Id
     * 
     * @return response success/failure message
     *
     **/
    public function user_products_status(Request $request) {

        try {

            DB::beginTransaction();

            $user_product = UserProduct::find($request->user_product_id);

            if(!$user_product) {

                throw new Exception(tr('user_product_not_found'), 101);
                
            }

            $user_product->status = $user_product->status ? DECLINED : APPROVED ;

            if($user_product->save()) {

                DB::commit();

                if($user_product->status == DECLINED) {

                    $email_data['subject'] = tr('product_decline_email' , Setting::get('site_name'));

                    $email_data['status'] = tr('declined');

                } else {

                    $email_data['subject'] = tr('product_approve_email' , Setting::get('site_name'));

                    $email_data['status'] = tr('approved');
                }

                $email_data['email']  = $user_product->user->email ?? "-";

                $email_data['name']  = $user_product->user->name ?? "-";

                $email_data['product_name']  = $user_product->name;

                $email_data['page'] = "emails.products.status";

                $this->dispatch(new \App\Jobs\SendEmailJob($email_data));

                $message = $user_product->status ? tr('user_product_approve_success') : tr('user_product_decline_success');

                return redirect()->back()->with('flash_success', $message);
            }
            
            throw new Exception(tr('user_product_status_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.user_products.index')->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method user_products_dashboard()
     *
     * @uses 
     *
     * @created Akshata 
     *
     * @updated 
     *
     * @param object $request - stardom_wallet_id
     * 
     * @return View page
     *
     */
    public function user_products_dashboard(Request $request) {

        try {

            $user_product = UserProduct::where('id',$request->user_product_id)->first();

            if(!$user_product) {

                throw new Exception(tr('user_product_not_found'), 101);
            }

            $data = new \stdClass;

            $data->total_orders = OrderProduct::where('user_product_id',$user_product->id)->count();

            $data->today_orders = OrderProduct::where('user_product_id',$user_product->id)->whereDate('created_at',today())->count();

            $order_products_ids = OrderProduct::where('user_product_id',$user_product->id)->pluck('order_id');

            $data->total_revenue = $order_products_ids->count() > 0 ? \App\OrderPayment::whereIn('order_id',[$order_products_ids])->sum('total') : 0;

            $data->today_revenue = count($order_products_ids) > 0 ? \App\OrderPayment::whereIn('order_id',[$order_products_ids])->where('created_at',today())->sum('total') : 0;

            $ids = count($order_products_ids)> 0 ? $order_products_ids : 0 ;
            
            $data->analytics = last_x_days_revenue(6,$ids);

            $user_product = UserProduct::find($request->user_product_id)??'';
           
            return view('admin.user_products.dashboard')
                        ->with('page','user_products')
                        ->with('sub_page' , 'user_products-view')
                        ->with('user_product', $user_product)
                        ->with('data', $data);

        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    
    }


    /**
     * @method order_products
     *
     * @uses Display all orders based the product details
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Stardom Product Id
     * 
     * @return response success/failure message
     *
     **/
    public function order_products(Request $request) {

        try {

            DB::beginTransaction();

            $order_products = OrderProduct::where('user_product_id',$request->user_product_id)->get();


            if(!$order_products) {

                throw new Exception(tr('user_product_not_found'), 101);
                
            }
            $product = UserProduct::where('user_products.id', $request->user_product_id)->first();


            return view('admin.user_products.order_products')
                        ->with('page', 'user_products')
                        ->with('sub_page', 'user_products-view')
                        ->with('product', $product)
                        ->with('order_products', $order_products);

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.user_products.index')->with('flash_error', $e->getMessage());

        }

    }


    /**
     * @method user_products_availability_update
     *
     * @uses To update product is_outofstock as instock/outofstock based on product id
     *
     * @created Subham
     *
     * @updated 
     *
     * @param object $request - Product Id
     * 
     * @return response success/failure message
     *
     **/
    public function user_products_availability_update(Request $request) {

        try {

            DB::beginTransaction();

            $user_product = UserProduct::find($request->user_product_id);

            if(!$user_product) {

                throw new Exception(tr('user_product_not_found'), 101);
                
            }

            $user_product->is_outofstock = $user_product->is_outofstock ? NO : YES ;

            if($user_product->save()) {

                DB::commit();

                $message = $user_product->is_outofstock ? tr('user_product_in_stock_success') : tr('user_product_out_of_stock_success');

                return redirect()->back()->with('flash_success', $message);
            }
            
            throw new Exception(tr('user_product_stock_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.user_products.index')->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method product_reviews_index()
     *
     * @uses To list out stardom products details 
     *
     * @created Subham
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function product_reviews_index(Request $request) {

        $base_query = ProductReview::orderBy('created_at','DESC');

        if($request->search_key) {

            $search_key = $request->search_key;

            $base_query = $base_query->whereHas('fromUser',function($query) use($search_key) {

                return $query->where('users.name','LIKE','%'.$search_key.'%');

            })->orWhereHas('toUser',function($query) use($search_key) {

                return $query->where('users.name','LIKE','%'.$search_key.'%');

            })->orWhereHas('orders',function($query) use($search_key) {

                return $query->where('unique_id','LIKE','%'.$search_key.'%');

            })->orWhere('product_reviews.unique_id','LIKE','%'.$search_key.'%');
        }

        if($request->seller_id){

            $base_query = $base_query->where('seller_id',$request->seller_id);
        }

        if($request->user_id){

            $base_query = $base_query->where('user_id',$request->user_id);
        }
       
        $product_reviews = $base_query->paginate($this->take);

        return view('admin.product_reviews.index')
                ->with('page', 'product_reviews')
                ->with('sub_page' , 'product_reviews-index')
                ->with('product_reviews' , $product_reviews);
    }

    /**
     * @method product_reviews_view()
     *
     * @uses displays the specified user product details based on user product id
     *
     * @created Subham 
     *
     * @updated 
     *
     * @param object $request - user product Id
     * 
     * @return View page
     *
     */
    public function product_reviews_view(Request $request) {
       
        try {
      
            $product_review = ProductReview::find($request->product_review_id);

            if(!$product_review) { 

                throw new Exception(tr('user_product_not_found'), 101);                
            }

            return view('admin.product_reviews.view')
                    ->with('page', 'product_reviews') 
                    ->with('sub_page', 'product_reviews-view')
                    ->with('product_review', $product_review);
            
        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    
    }
}
