<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Helpers\Helper, App\Helpers\EnvEditorHelper;

use DB, Hash, Setting, Auth, Validator, Exception, Enveditor,Log;

use App\Jobs\{SendEmailJob};

use Carbon\Carbon;

use App\{LiveVideoProduct, Category, LiveVideo, User};

class AdminLiveVideoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {

        $this->middleware('auth:admin');

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

    }

    /**
     * @method live_video_products_index()
     *
     * @uses Display the Live Videos product
     *
     * @created Subham
     *
     * @updated
     *
     * @param -
     *
     * @return view page */
     
    public function live_video_products_index(Request $request) {

        $base_query = LiveVideoProduct::orderBy('created_at','desc');

        if($request->live_video_id){

            $base_query = $base_query->where('live_video_id', $request->live_video_id);

        }

        if($request->search_key) {

            $search_key = $request->search_key;

            $base_query->whereHas('user', function($q) use ($search_key) {

                return $q->Where('users.name','LIKE','%'.$search_key.'%');

            })->orWhere('unique_id','LIKE','%'.$search_key.'%');

        }

        $live_video_products = $base_query->paginate($this->take);

        return view('admin.live_videos.products_index')
                ->with('page', 'live-videos')
                ->with('sub_page', 'live-videos-history')
                ->with('live_video_products', $live_video_products);
    
    }

    /**
     * @method live_video_products_view()
     *
     * @uses 
     *
     * @created Subham
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     */

    public function live_video_products_view(Request $request) {

        try {

            $live_video_product = LiveVideoProduct::where('id',$request->live_video_product_id)->first();

            if(!$live_video_product) {

                throw new Exception(tr('live_video_not_found'), 1);
                
            }
           
            return view('admin.live_videos.products_view')
                    ->with('page','live-video')
                    ->with('sub_page','live-videos-history')
                    ->with('live_video_product',$live_video_product);

        } catch(Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    }

     /**
     * @method live_videos_index()
     *
     * @uses Display the Live Videos
     *
     * @created Ganesh
     *
     * @updated
     *
     * @param -
     *
     * @return view page 
     */
    public function live_videos_index(Request $request) {

        $base_query = \App\LiveVideo::with(['user','liveVideoProducts'])->orderBy('created_at','DESC');

        $user = $seller = '';
        
        $category_details = '';

       if($request->search_key) {

                $live_video_ids = LiveVideo::when($request->user_id, function($query) use ($request){

                                $query->where('user_id', $request->user_id);

                            // })->when($request->seller_id, function($query) use ($request) {

                            //     $query->where('seller_id',$request->seller_id);

                          })->whereHas('user', function($query) use ($request) {

                                $query->where('name', "LIKE", "%" . $request->search_key . "%");

                           })->orWhere('title','LIKE','%'.$request->search_key.'%')

                           ->pluck('id');
                            

                $base_query = $base_query->whereIn('id', $live_video_ids);
       }



        if($request->payment_status !='') {

            $base_query->where('payment_status',$request->payment_status);
        }

        if($request->video_type) {

            $base_query->where('type',$request->video_type);
        }


        if($request->seller_id) {

            $seller = User::find($request->seller_id);

            $live_video_ids = LiveVideo::where('user_id', $request->seller_id)->pluck('id');

            $base_query = $base_query->whereIn('id',$live_video_ids);

        }

        $live_videos = $base_query->paginate($this->take);

        $live_videos->title = tr('live_videos_history');

        return view('admin.live_videos.index')
                ->with('page', 'live-videos')
                ->with('sub_page', 'live-videos-history')
                ->with('is_streaming', NO)
                ->with('live_videos', $live_videos)
                ->with('seller', $seller)
                ->with('category_details', $category_details);
    
    }


    /**
     * @method videos_index()
     *
     * @uses To list out LiveVideos
     *
     * @created Anjana H
     *
     * @updated Anjana H
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function live_videos_onlive(Request $request) {
        
        $base_query = \App\LiveVideo::where('live_videos.status',VIDEO_STREAMING_ONGOING)->where('is_streaming', IS_STREAMING_YES)
            ->orderBy('created_at', 'desc');

        if($request->search_key) {

            $search_key = $request->search_key;

            
            $base_query->whereHas('user', function($q) use ($search_key) {

                return $q->Where('users.name','LIKE','%'.$search_key.'%');

            })->orWhere('title','LIKE','%'.$search_key.'%');

        }

        if($request->payment_status !='') {

            $base_query->where('payment_status',$request->payment_status);
        }

        if($request->video_type) {

            $base_query->where('type',$request->video_type);
        }
        
        $live_videos = $base_query->paginate(10);
        
        $live_videos->title = tr('live_videos');

        return view('admin.live_videos.index')
                ->with('page', 'live-videos')
                ->with('sub_page', 'live-videos-live')
                ->with('is_streaming', YES)
                ->with('live_videos', $live_videos);
    }

    /**
     * @method live_videos_delete()
     *
     * To delete a live streaming video which is stopped by the user
     *
     * @created Ganesh
     *
     * @updated by - 
     *
     * @param integer $request - Video id
     *
     * @return repsonse of success/failure message
     */
    public function live_videos_delete(Request $request) {

        try {

            $live_video = \App\LiveVideo::find($request->live_video_id);

            if(!$live_video){

                throw new Exception(tr('live_video_not_found'));

            }
            
            if($live_video->status== VIDEO_STREAMING_ONGOING){

                throw new Exception(tr('broadcast_video_delete_failure'));
            }

            DB::beginTransaction();

            if ($live_video) {                

                $live_video->delete();

                DB::commit();

                return back()->with('flash_success', tr('live_video_delete_success'));

            } 

           throw new Exception(tr('live_video_not_found'));
                

        } catch(Exception $e) {

            DB::rollback();

            return back()->with('flash_error', $e->getMessage());

        }

    } 


     /**
     * @method live_videos_view()
     *
     * @uses displays the specified live video details based on live video id
     *
     * @created Ganesh 
     *
     * @updated 
     *
     * @param object $request - post Id
     * 
     * @return View page
     *
     */
    public function live_videos_view(Request $request) {

        try {
            
            $live_video = \App\LiveVideo::find($request->live_video_id);
            
            if(!$live_video) { 

                throw new Exception(tr('live_video_not_found'), 101);                
            }
            

            $live_video_amount = \App\LiveVideoPayment::where('live_video_id', $request->live_video_id)
                    ->where('amount', '>', 0)
                    ->sum('amount');

            $user_amount = \App\LiveVideoPayment::where('live_video_id', $request->live_video_id)
                    ->where('amount', '>', 0)
                    ->sum('user_amount');

            $admin_amount = \App\LiveVideoPayment::where('live_video_id', $request->live_video_id)
                    ->where('amount', '>', 0)
                    ->sum('admin_amount');

            $live_video->live_video_amount = formatted_amount($live_video_amount ?? 0);

            $live_video->user_amount = formatted_amount($user_amount ?? 0);

            $live_video->admin_amount = formatted_amount($admin_amount ?? 0);

            return view('admin.live_videos.view')
                ->with('page', 'live-videos')
                ->with('sub_page', 'live-videos-history')
                ->with('live_video', $live_video);

        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }

    }

    /**
     * @method live_video_payments()
     *
     * @uses Display the lists of post payments
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     */

    public function live_video_payments(Request $request) {

        $base_query = \App\LiveVideoPayment::orderBy('created_at','DESC');

        $title = tr('live_video_payments');

        if($request->live_video_id) {

            $base_query = $base_query->where('live_video_id',$request->live_video_id);

            $live_video = \App\LiveVideo::find($request->live_video_id);

            $title = tr('live_video_payments')." - ".$live_video->title;
        }

        if($request->search_key) {

            $search_key = $request->search_key;

            $base_query = $base_query
                            ->whereHas('user',function($query) use($search_key){

                                return $query->where('users.name','LIKE','%'.$search_key.'%');
                                
                            })
                            ->orwhereHas('videoDetails',function($query) use($search_key){

                                return $query->where('live_videos.unique_id','LIKE','%'.$search_key.'%');
                            })
                            ->orwhereHas('videoDetails',function($query) use($search_key){

                                return $query->where('live_videos.title','LIKE','%'.$search_key.'%');
                            })
                            ->orWhere('live_video_payments.payment_id','LIKE','%'.$search_key.'%');
        }

        $user = \App\User::find($request->user_id) ?? '';

        if($request->user_id) {

            $base_query  = $base_query->where('user_id',$request->user_id);
        }

        $live_video_payments = $base_query->whereHas('videoDetails')->has('user')->orderBy('created_at','DESC')->paginate(10);
        
        return view('admin.live_videos.payments')
                ->with('page','payments')
                ->with('sub_page','live-video-payments')
                ->with('user',$user)
                ->with('title', $title)
                ->with('live_video_payments',$live_video_payments);
    }


    /**
     * @method live_video_payments_view()
     *
     * @uses 
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     */

    public function live_video_payments_view(Request $request) {

        try {

            $live_video_payment = \App\LiveVideoPayment::where('id',$request->live_video_payment_id)->first();

            if(!$live_video_payment) {

                throw new Exception(tr('live_video_payment_not_found'), 1);
                
            }
           
            return view('admin.live_videos.payments_view')
                    ->with('page','payments')
                    ->with('sub_page','live-video-payments')
                    ->with('live_video_payment',$live_video_payment);

        } catch(Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    }

    /**
     * @method live_videos_scheduled()
     *
     * @uses To list out schedules live videos
     *
     * @created Karthick
     *
     * @updated
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function live_videos_scheduled(Request $request) {
        
        $base_query = LiveVideo::FeaturedVideos()->orderBy('created_at', 'desc');

        if($request->search_key) {

            $search_key = $request->search_key;

            
            $base_query->whereHas('user', function($query) use ($search_key) {

                return $query->Where('users.name','LIKE','%'.$search_key.'%');

            })->orWhere('title','LIKE','%'.$search_key.'%');

        }

        if($request->payment_status !='') {

            $base_query->where('payment_status',$request->payment_status);
        }

        if($request->video_type) {

            $base_query->where('type',$request->video_type);
        }
        
        $live_videos = $base_query->paginate($this->take);
        
        $live_videos->title = tr('live_videos');

        // $live_videos->each(function($live_video) {
        //     dd($live_video->id, $live_video->status_formatted);
        // });

        return view('admin.live_videos.index')
                ->with('page', 'live-videos')
                ->with('sub_page', 'live-videos-scheduled')
                ->with('is_streaming', NO)
                ->with('is_scheduled', YES)
                ->with('live_videos', $live_videos);
    }

}
