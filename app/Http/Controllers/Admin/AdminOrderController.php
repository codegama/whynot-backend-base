<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Helpers\Helper, App\Helpers\EnvEditorHelper;

use DB, Hash, Setting, Auth, Validator, Exception, Enveditor,Log,File;

use App\Jobs\SendEmailJob;

use App\{OrderProduct, UserProduct,User, DeliveryAddress};

use Carbon\Carbon;

use App\Repositories\CommonRepository as CommonRepo;

class AdminOrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {

        $this->middleware('auth:admin');

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

    }


    /**
     * @method orders_index
     *
     * @uses Display list of orders
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Order Id
     * 
     * @return response success/failure message
     *
     **/
    public function orders_index(Request $request) {

        $user = $seller = '';

        $base_query = OrderProduct::with(['user','order'])->withCount(['userProductDetails'])->orderBy('created_at','DESC');

        if($request->status) {

            $base_query = $base_query->where('order_products.status', $request->status);
        }

      if($request->search_key) {

                $order_product_ids = OrderProduct::when($request->user_id, function($query) use ($request){

                                $query->where('user_id', $request->user_id);

                            // })->when($request->seller_id, function($query) use ($request) {

                            //     $query->where('seller_id',$request->seller_id);

                          })->whereHas('user', function($query) use ($request) {

                                $query->where('name', "LIKE", "%" . $request->search_key . "%");

                           })->orWhere('unique_id','LIKE','%'.$request->search_key.'%')

                           ->pluck('id');
                            

                $base_query = $base_query->whereIn('id', $order_product_ids);
       }

        if($request->user_id) {

            $user = User::find($request->user_id);

            $base_query  = $base_query->where('user_id',$request->user_id);
        }

        if($request->seller_id) {

            $seller = User::find($request->seller_id);

            $user_product_ids = UserProduct::where('user_id', $request->seller_id)->pluck('id');

            $base_query = $base_query->whereIn('user_product_id',$user_product_ids);

        }

        $sub_page = 'orders-view';

        if($request->new_orders) {

            $base_query  = $base_query->latest('created_at');

            $sub_page = 'orders-new';
        }

        $order_products = $base_query->paginate($this->take);

        return view('admin.orders.index')
                    ->with('page','orders')
                    ->with('sub_page',$sub_page)
                    ->with('user',$user)
                    ->with('seller',$seller)
                    ->with('order_products',$order_products);
    }

    /**
     * @method orders_view
     *
     * @uses Display the specified order details
     *
     * @created Akshata
     *
     * @updated shakthi
     *
     * @param object $request - Order Id
     * 
     * @return response success/failure message
     *
     **/

    public function orders_view(Request $request) {

        try {

            $order_product = OrderProduct::where('id',$request->order_product_id)->first();

            if(!$order_product) {

                throw new Exception(tr('order_not_found'), 1);

            }  

            $order_payment = \App\OrderPayment::where('order_id',$order_product->id)->first();

            return view('admin.orders.view')
                        ->with('page','orders')
                        ->with('sub_page','orders-view')
                        ->with('order_product', $order_product)
                        ->with('order_payment', $order_payment);

        } catch(Exception $e) {

            return redirect()->back()->with('flash_error',$e->getMessage());
        }
    }

    /**
     * @method delivery_address_index
     *
     * @uses Display list of all the delivery address
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return response success/failure message
     *
     **/
    public function delivery_address_index(Request $request) {

        try {

            $user = '';

            $base_query = DeliveryAddress::with(['user:id,name'])->orderBy('created_at','DESC');

            if($request->filled('search_key')) {

                $billing_address_ids = DeliveryAddress::when($request->user_id, function($query) use ($request){
                                $query->where('user_id', $request->user_id);
                            })->whereHas('user', function($query) use ($request) {
                                $query->where('name', "LIKE", "%" . $request->search_key . "%");
                            })->orWhere(function ($query) use ($request) {
                                $query->where('name', "LIKE", "%" . $request->search_key . "%")
                                ->orWhere('state', "LIKE", "%" . $request->search_key . "%");
                            })->pluck('id');
                            

                $base_query = $base_query->whereIn('id', $billing_address_ids);
            } 

            if($request->user_id){

                $user = User::find($request->user_id);

                $base_query = $base_query->where('user_id',$request->user_id);

            }

            $delivery_addresses = $base_query->paginate($this->take);

            return view('admin.delivery_address.index')
                        ->with('page','users')
                        ->with('sub_page','users-view')
                        ->with('user',$user)
                        ->with('delivery_addresses',$delivery_addresses);

        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    }

    /**
     * @method delivery_address_view
     *
     * @uses Display the specified delivery address details
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Delivery Address Id
     * 
     * @return response success/failure message
     *
     **/

    public function delivery_address_view(Request $request) {

        try {

            $delivery_address = \App\DeliveryAddress::where('id',$request->delivery_address_id)->first();

            if(!$delivery_address) {

                throw new Exception(tr('delvery_address_details_not_found'), 101);

            }

            return view('admin.delivery_address.view')
                    ->with('page','delivery-address')
                    ->with('delivery_address_details',$delivery_address);

        } catch(Exception $e) {

            return redirect()->back()->with('flash_error',$e->getMessage());
        }
    }


    /**
     * @method delivery_address_delete
     *
     * @uses Display list of all the delivery address
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param $object delivery_address_id
     * 
     * @return response success/failure message
     *
     **/

    public function delivery_address_delete(Request $request) {

        try {
            
            DB::begintransaction();

            $delivery_address = \App\DeliveryAddress::find($request->delivery_address_id);

            if(!$delivery_address) {

                throw new Exception(tr('delivery_address_details_not_found'), 101);                
            }

            if($delivery_address->delete()) {

                DB::commit();

                return redirect()->route('admin.delivery_address.index',['user_id'=>$delivery_address->user_id,'page'=>$request->page])->with('flash_success',tr('delivery_address_deleted_success'));   

            } 

            throw new Exception(tr('delivery_address_delete_failed'));

        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }       

    }
}
