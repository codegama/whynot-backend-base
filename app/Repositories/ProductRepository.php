<?php

namespace App\Repositories;

use App\Helpers\Helper;

use Log, Validator, Setting, Exception, DB;

use App\{UserProductPicture, DeliveryAddress, OrderProduct, OrderPayment, UserProduct, User, Order, Cart};

use App\Repositories\PaymentRepository as PaymentRepo;


class ProductRepository {

    /**
     * @method orders_list_response()
     *
     * @uses Format the product response
     *
     * @created Subham
     * 
     * @updated 
     *
     * @param object $request
     *
     * @return object $payment_details
     */

    public static function orders_list_response($orders, $request) {
        
        $orders = $orders->map(function ($order, $key) use ($request) {

            $order->delivery_address = DeliveryAddress::where('id', $order->delivery_address_id)->first() ?? '';

            $order_product = OrderProduct::where('order_id', $order->id)->with('userProductDetails');

            $order->order_product = $order_product->get();

            $user_product_ids = $order_product->pluck('user_product_id');

            $order_payment = OrderPayment::where('order_id', $order->id)->first();

            $order_payment->paid_date_formatted = common_date($order_payment->paid_date,'','d M Y');

            $order->order_payment = $order_payment;

            $order->product_details = UserProduct::whereIn('id', $user_product_ids)->get();

            return $order;
        });


        return $orders;

    }

    /**
     * @method order_view_single_response()
     *
     * @uses Format the post response
     *
     * @created Subham
     * 
     * @updated 
     *
     * @param object $request
     *
     * @return object $payment_details
     */

    public static function order_view_single_response($order, $request) {

        $order->delivery_address = DeliveryAddress::where('id', $order->delivery_address_id)->first() ?? '';

        $base_query = OrderProduct::where('order_id', $order->id)->whereHas('userProductDetails')->with('userProductDetails');

        $order->order_product = $base_query->get();

        $user_product_id = $base_query->pluck('user_product_id');

        $order->product_details = UserProduct::whereIn('id',$user_product_id)->get(); 
        
        $order_payment = OrderPayment::where('order_id', $order->id)->first();

        $order_payment->paid_date_formatted = common_date($order_payment->paid_date,$request->timezone,'d M Y');

        $order->order_payment = $order_payment;

        $order->publish_time_formatted = common_date($order->created_at, $request->timezone, 'M d');

        return $order;
    
    }

    /**
     * @method user_products_list_response()
     *
     * @uses Format the product response
     *
     * @created Subham
     * 
     * @updated 
     *
     * @param object $request
     *
     * @return object $payment_details
     */

    public static function user_products_list_response($user_products, $request) {
        
        $user_products = $user_products->map(function ($user_product, $key) use ($request) {

                        $user_product->userProductFiles = UserProductPicture::where('user_product_id', $user_product->id)->get();

                        $user_product->publish_time_formatted = common_date($user_product->created_at, $request->timezone, 'M d');

                        $cart = Cart::where('user_product_id', $user_product->id)->where('user_id', $request->id)->first();

                        $user_product->add_to_cart = $cart ? NO : YES ;

                        return $user_product;
                    });


        return $user_products;

    }

    /**
     * @method user_product_single_response()
     *
     * @uses Format the post response
     *
     * @created Subham
     * 
     * @updated 
     *
     * @param object $request
     *
     * @return object $payment_details
     */

    public static function user_product_single_response($user_product, $request) {

        $user_product->userProductFiles = UserProductPicture::where('user_product_id', $user_product->id)->get();

        $user_product->publish_time_formatted = common_date($user_product->created_at, $request->timezone, 'M d');

        $cart = Cart::where('user_product_id', $user_product->id)->where('user_id', $request->id)->first();

        $user_product->add_to_cart = $cart ? NO : YES ;

        return $user_product;    
    }

	/**
     *
     * @method user_product_pictures_save()
     *
     * @uses To Upoad Product Pictures
     *
     * @created Bhawya
     *
     * @updated Bhawya
     *
     * @param 
     *
     * @return
     */
    public static function user_product_pictures_save($files, $user_product_id) {

        $allowedfileExtension=['jpeg','jpg','png'];

        // Single file upload

        if(!is_array($files)) {
            
            $file = $files;

            $user_product_pictures = new \App\UserProductPicture;

            $user_product_pictures->user_product_id = $user_product_id;

            $user_product_pictures->picture = Helper::storage_upload_file($file, COMMON_FILE_PATH);

            $user_product_pictures->save();

            return true;
       
        }

        // Multiple files upload
        foreach($files as $file) {

            $filename = $file->getClientOriginalName();

            $extension = $file->getClientOriginalExtension();

            $check_picture = in_array($extension, $allowedfileExtension);
            
            if($check_picture) {

                $user_product_pictures = new \App\UserProductPicture;

	            $user_product_pictures->user_product_id = $user_product_id;

	            $user_product_pictures->picture = Helper::storage_upload_file($file, COMMON_FILE_PATH);

	            $user_product_pictures->save();

           }
        
        }

        return true;
    
    }


      /**
     * @method order_product_list_response()
     *
     * @uses Format the product response
     *
     * @created Jeevan
     * 
     * @updated 
     *
     * @param object $request
     *
     * @return object $payment_details
     */

    public static function order_product_list_response($order_products, $request) {
       
        $order_products = $order_products->map(function ($order_product, $key) use ($request) {

                        $order_product->product = UserProduct::find($order_product->user_product_id);

                        $order_product->user = User::find($order_product->user_id);

                        $order_product->order = Order::find($order_product->id);

                        $order_product->order_payment = OrderPayment::where('order_id', $order_product->id)->first();

                        return $order_product;
                    });


        return $order_products;

    }

    /**
     * @method save_product_order()
     *
     * @uses used to save order product details
     *
     * @created 
     * 
     * @updated 
     * 
     * @param object $request
     *
     * @return object 
     */

    public static function save_product_order($order, $request) {

        try {

            $order_product = OrderProduct::where('order_id', $request->order_id)->first() ?? new OrderProduct;
            
            $order_product->user_id = $request->id;

            $order_product->order_id = $order->id;

            $order_product->user_product_id = $request->user_product_id ?? 0;

            $order_product->quantity = 1;

            $order_product->sub_total = $order->total ?? 0;

            $order_product->total = $order->total ?? 0;
 
            $order_product->status = ORDER_PLACED;
                
            $order_product->live_video_id = $request->live_video_id;

            $order_product->save();

            return  $order_product;

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }
    
    }

    /**
     * @method save_order_payment()
     *
     * @uses used to save order payment
     *
     * @created 
     * 
     * @updated 
     * 
     * @param object $request
     *
     * @return object 
     */

    public static function save_order_payment($order, $request) {

        try {

            $order_payments = OrderPayment::where('order_id', $request->order_id)->first() ?? new OrderPayment;
            
            $order_payments->user_id = $request->id;

            $order_payments->order_id = $order->id;

            $order_payments->payment_id = $request->payment_id ?? "NO-".rand();

            $order_payments->payment_mode = $request->payment_mode;

            $order_payments->currency = Setting::get('currency') ?? "$";

            $order_payments->sub_total = $request->paid_amount ?? 0;

            $order_payments->total = $total = $request->paid_amount ?? 0;

            $order_payments->paid_date = date("Y-m-d H:i:s");

            $order_payments->status = PAID;
                
            $admin_commission_in_per = Setting::get('order_admin_commission', 1)/100;

            $admin_amount = $total * $admin_commission_in_per;

            $user_amount = $total - $admin_amount;

            $order_payments->admin_amount = $admin_amount ?? 0.00;

            $order_payments->user_amount = $user_amount ?? 0.00;

            $order_payments->save();

             // Add to order payment to user wallet
            if($order_payments->status == PAID) {

                self::order_payment_wallet_update($request, $order, $order_payments);

            }

            $response = ['success' => true, 'message' => 'paid', 'data' => [ 'payment_id' => $request->payment_id]];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }
    
    }

    /**
     * @method order_payment_wallet_update
     *
     * @uses Order payment amount will update to the model wallet
     *
     * @created Arun
     *
     * @updated 
     *
     * @param
     *
     * @return
     */

    public static function order_payment_wallet_update($request, $order, $order_payments) {

        try {
            
            $to_user_inputs = [
                'id' => $order->user_id,
                'received_from_user_id' => $request->id,
                'total' => $order->total, 
                'user_pay_amount' => $order_payments->user_amount,
                'paid_amount' => $order_payments->user_amount,
                'payment_type' => WALLET_PAYMENT_TYPE_CREDIT,
                'amount_type' => WALLET_AMOUNT_TYPE_ADD,
                'payment_id' => $order_payments->payment_id,
                'user_amount' => $order_payments->user_amount,
                'admin_amount' => $order_payments->admin_amount,
                'message' => $request->message,
            ];

            $to_user_request = new \Illuminate\Http\Request();

            $to_user_request->replace($to_user_inputs);

            $to_user_payment_response = PaymentRepo::user_wallets_payment_save($to_user_request)->getData();

            if($to_user_payment_response->success) {

                DB::commit();

                return $to_user_payment_response;

            } else {

                throw new Exception($to_user_payment_response->error, $to_user_payment_response->error_code);
            }
        
        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

    /**
     * @method user_products_orders_list_response()
     *
     * @uses Format the product response
     *
     * @created Arun
     * 
     * @updated 
     *
     * @param object $request
     *
     * @return object $payment_details
     */

    public static function user_products_orders_list_response($orders, $request) {
        
        $orders = $orders->map(function ($order, $key) use ($request) {

            $order->delivery_address = DeliveryAddress::where('id', $order->delivery_address_id)->first();

            $order_product = OrderProduct::where('order_id', $order->id);

            if($request->live_video_id) {
                
                $order_product = $order_product->where('live_video_id', $request->live_video_id);

            } else if($request->user_product_id) {

                $order_product = $order_product->where('user_product_id', $request->user_product_id);
            }
                                
            $order_product = $order_product->with('userProductDetails')->first();

            $order->order_product = $order_product;

            $order->order_payment = OrderPayment::where('order_id', $order->id)->first();

            return $order;
        });

        return $orders;

    }
}