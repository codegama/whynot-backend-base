<?php

namespace App\Repositories;

use App\Helpers\Helper;

use Log, Validator, Setting, Exception, DB;

use App\{User, LiveVideo, LiveGroup, UserProduct, LiveVideoProduct, LiveVideoBookmark};

use App\{ Category };

class LiveVideoRepository {

    /**
     * @method live_videos_common_query()
     *
     * @uses used to check the conditions to fetch the live videos
     *
     * @created Bhawya N
     * 
     * @updated Bhawya N
     *
     * @param object $base_query, object $request
     *
     * @return object $base_query
     */

    public static function live_videos_common_query($request, $base_query,$type = '') {

        if($request->id) {

            // omit the live videos - blocked by you, who blocked you & your live videos

            $block_user_ids = blocked_users($request->id);

            // groups based videos

            if($block_user_ids) {

                $base_query = $base_query->whereNotIn('live_videos.user_id', $block_user_ids);
            }

        }

        if($type) {

            if($type == TYPE_PRIVATE) {

                $follower_ids = get_follower_ids($request->id);

                $base_query = $base_query->whereIn('live_videos.user_id', $follower_ids);
            }

            $base_query = $base_query->where('live_videos.type', $type);
        }

        return $base_query;

    }

    /**
     * @method live_videos_list_response()
     *
     * @uses used to format the live videos response
     *
     * @created Bhawya N
     * 
     * @updated Bhawya N
     *
     * @param object $live_videos, object $request
     *
     * @return object $live_videos
     */

    public static function live_videos_list_response($live_videos, $request) {

        $live_videos = $live_videos->map(function ($live_video, $key) use ($request) {

            $live_video->is_video_bookmarked = $live_video->liveVideoBookmarks->where('user_id', $request->id)->count() ? YES : NO;
            
            $live_video->schedule_time_formatted = formatted_schedule_time($live_video->schedule_time, $request->timezone);

            $live_video->unsetRelation('liveVideoBookmarks');

            return $live_video;
        });

        return $live_videos;
    
    }

    public static function live_videos_check_payment($live_video, $user_id) {

        $is_user_needs_to_pay = NO;

        if($live_video->payment_status == YES && $live_video->amount > 0) {

            $is_user_needs_to_pay = \App\LiveVideoPayment::where('live_video_viewer_id', $user_id)->where('status', PAID_STATUS)->where('live_video_id', $live_video->live_video_id)->count() ? NO : YES;

        }

        return $is_user_needs_to_pay;
    }

    
     /**
     * @method live_streaming_upcoming_list_response()
     *
     * @uses used to format the live streaming upcoming list
     *
     * @created Jeevan
     * 
     * @updated 
     *
     * @param object $live_videos, object $request
     *
     * @return object $live_videos
     */

    public static function live_streaming_upcoming_list_response($live_streamings, $request) {

        $live_streamings = $live_streamings->map(function ($live_streaming, $key) use ($request) {
            
            $live_streaming->schedule_time_formatted = formatted_schedule_time($live_streaming->schedule_time, $request->timezone);

            $live_streaming->unsetRelation('liveVideoBookmarks');

            $live_streaming->user = User::find($live_streaming->user_id);
            
            $products_ids = LiveVideoProduct::where('live_video_id',$live_streaming->id)->pluck('user_product_id');
           
            $live_streaming->products = UserProduct::whereIn('user_products.id',$products_ids)->get();

            $live_streaming->is_video_bookmarked = LiveVideoBookmark::firstWhere('user_id', $request->id) ? YES : NO;

            return $live_streaming;
        });

        return $live_streamings;
    
    }

    /**
     * @method live_video_category_response()
     *
     * @uses used to format the live videos with category
     *
     * @created Karthick
     * 
     * @updated 
     *
     * @param object $categories
     *
     * @return object $categories
     */

    public static function live_video_category_response($categories, $skip, $take,$request) {

        $categories_videos = $categories->map(function ($category) use ($skip, $take,$request) {

            $live_videos = LiveVideo::where('category_id', $category->category_id)->orderBy('schedule_time', 'asc')->where('is_streaming', YES)->where('live_schedule_type', LIVE_SCHEDULE_TYPE_NOW)->skip($skip)->take($take)->get();

            $live_videos = self::live_videos_list_response($live_videos, $request);

            $category->live_videos = $live_videos;

            return $category;
        });


        return $categories_videos;
    
    }

    /**
     * @method live_video_sub_category_response()
     *
     * @uses used to format the live videos with sub category
     *
     * @created Karthick
     * 
     * @updated 
     *
     * @param object $sub_categories
     *
     * @return object $sub_categories
     */

    public static function live_video_sub_category_response($sub_categories, $skip, $take, $request) {

        $sub_categories_videos = $sub_categories->map(function ($sub_category) use ($skip, $take) {

            $live_video = LiveVideo::where('sub_category_id', $sub_category->sub_category_id)->where('is_streaming', YES)->where('live_schedule_type', LIVE_SCHEDULE_TYPE_NOW)->skip($skip)->take($take)->get();

            $live_videos = self::live_videos_list_response($live_videos, $request);

            $sub_category->live_videos = $live_videos;

            return $sub_category;
        });


        return $sub_categories_videos;
    
    }


}