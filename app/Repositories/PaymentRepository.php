<?php

namespace App\Repositories;

use App\Helpers\Helper;

use Log, Validator, Setting, Exception, DB;

use App\User, App\PromoCode;

use App\{LiveVideoPayment,OrderProduct,OrderPayment,Cart,UserProduct};

class PaymentRepository {

    /**
     * @method user_wallets_payment_save()
     *
     * @uses used to save user wallet payment details
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $request
     *
     * @return object $user_wallet_payment
     */
    public static function user_wallets_payment_save($request) {

        try {

            $user_wallet_payment = new \App\UserWalletPayment;
            
            $user_wallet_payment->user_id = $request->id;

            $user_wallet_payment->to_user_id = $request->to_user_id ?? 0;

            $user_wallet_payment->received_from_user_id = $request->received_from_user_id ?? 0;

            $user_wallet_payment->user_billing_account_id = $request->user_billing_account_id ?: 0;
            
            $user_wallet_payment->payment_id = $request->payment_id ?:generate_payment_id();

            $user_wallet_payment->paid_amount = $user_wallet_payment->requested_amount = $request->paid_amount ?? 0.00;

            $user_wallet_payment->admin_amount = $request->admin_amount ?? 0.00;

            $user_wallet_payment->user_amount = $request->user_pay_amount ?? 0.00;

            $user_wallet_payment->payment_type = $request->payment_type ?: WALLET_PAYMENT_TYPE_ADD;

            $user_wallet_payment->amount_type = $request->amount_type ?: WALLET_AMOUNT_TYPE_ADD;

            $user_wallet_payment->usage_type = $request->usage_type ?: "";

            $user_wallet_payment->currency = Setting::get('currency') ?? "$";

            $user_wallet_payment->payment_mode = $request->payment_mode ?? PAYMENT_MODE_WALLET;

            $user_wallet_payment->paid_date = date('Y-m-d H:i:s');

            $user_wallet_payment->status = $request->paid_status ?: USER_WALLET_PAYMENT_PAID;

            if($request->file('bank_statement_picture')) {

                $user_wallet_payment->bank_statement_picture = Helper::storage_upload_file($request->file('bank_statement_picture'));
            }

            $user_wallet_payment->message = "";

            $user_wallet_payment->save();

            $message = strtoupper($request->usage_type)." - " ?: "";

            $message .= get_wallet_message($user_wallet_payment);

            $message .= $request->message ? " - ".$request->message : "";

            $user_wallet_payment->message = $message;

            $user_wallet_payment->save();

            if($user_wallet_payment->payment_type != WALLET_PAYMENT_TYPE_WITHDRAWAL && $user_wallet_payment->status == USER_WALLET_PAYMENT_PAID) {

                self::user_wallet_update($user_wallet_payment);
            }

            $response = ['success' => true, 'message' => 'paid', 'data' => $user_wallet_payment];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }
    
    }

    /**
     * @method user_wallets_payment_by_stripe()
     *
     * @uses pay for live videos using stripe
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $request
     *
     * @return object $payment_details
     */

    public static function user_wallets_payment_by_stripe($request) {

        try {

            // Check stripe configuration
        
            $stripe_secret_key = Setting::get('stripe_secret_key');

            if(!$stripe_secret_key) {

                throw new Exception(api_error(107), 107);

            } 

            \Stripe\Stripe::setApiKey($stripe_secret_key);
           
            $currency_code = Setting::get('currency_code', 'USD') ?: 'USD';

            $total = intval(round($request->user_pay_amount * 100));

            // $charge_array = [
            //                     'amount' => $total,
            //                     'currency' => $currency_code,
            //                     'customer' => $request->customer_id,
            //                 ];


            // $stripe_payment_response =  \Stripe\Charge::create($charge_array);

            $charge_array = [
                'amount' => $total,
                'currency' => $currency_code,
                'customer' => $request->customer_id,
                "payment_method" => $request->card_token,
                'off_session' => true,
                'confirm' => true,
            ];

            $stripe_payment_response = \Stripe\PaymentIntent::create($charge_array);

            $payment_data = [
                                'payment_id' => $stripe_payment_response->id ?? 'CARD-'.rand(),
                                'paid_amount' => $stripe_payment_response->amount/100 ?? $total,

                                'paid_status' => $stripe_payment_response->paid ?? true
                            ];

            $response = ['success' => true, 'message' => 'done', 'data' => $payment_data];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

    /**
     * @method user_wallet_update()
     *
     * @uses pay for live videos using stripe
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $request
     *
     * @return object $payment_details
     */

    public static function user_wallet_update($user_wallet_payment) {

        try {

            $is_referral_applied = NO; $referral_message = '';

            $user_wallet = \App\UserWallet::where('user_id', $user_wallet_payment->user_id)->first() ?: new \App\UserWallet;

            $user_wallet->user_id = $user_wallet_payment->user_id;

            if($user_wallet_payment->amount_type == WALLET_AMOUNT_TYPE_ADD) {

                $user_wallet->total += $user_wallet_payment->user_amount;

                if(Setting::get('is_referral_enabled') && $user_wallet_payment->usage_type == USAGE_TYPE_REFERRAL) {
                    
                    $user_wallet->referral_amount += $user_wallet_payment->user_amount;

                } else {

                    $user_wallet->remaining += $user_wallet_payment->user_amount;
                }

            } else {

                if($user_wallet_payment->payment_mode == PAYMENT_MODE_WALLET && Setting::get('is_referral_enabled')) {

                    if($user_wallet->referral_amount >= $user_wallet_payment->user_amount) {

                        $user_wallet->referral_amount -= $user_wallet_payment->user_amount;

                        $referral_message = tr('REFERRAL_PAYMENT_TYPE_PAID_TEXT');

                        $is_referral_applied = YES;

                    } else {

                        $referral_amount = $user_wallet->referral_amount;
                        
                        $user_wallet->referral_amount = 0;

                        $remaining_to_deduct = ($user_wallet_payment->user_amount - $referral_amount);

                        $user_wallet->remaining -= $remaining_to_deduct;

                        $referral_message = tr('REFERRAL_PAYMENT_TYPE_PARTIAL_TEXT');

                        $is_referral_applied = YES;
                    }

                } else{

                    $user_wallet->used += $user_wallet_payment->user_amount;

                    $user_wallet->remaining -= $user_wallet_payment->user_amount;

                }

            }

            $user_wallet->save();

            if(Setting::get('is_referral_enabled') && $is_referral_applied) {

                $message = strtoupper($user_wallet_payment->usage_type)." - " ?: "";
                $message .= $referral_message;

                $user_wallet_payment->message = $message;

                $user_wallet_payment->save();
                
            }
                

            $response = ['success' => true, 'message' => 'done', 'data' => $user_wallet];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

    /**
     * @method user_wallet_update_withdraw_send()
     *
     * @uses pay for live videos using stripe
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $request
     *
     * @return object $payment_details
     */

    public static function user_wallet_update_withdraw_send($amount, $user_id) {
        
        try {

            $user_wallet = \App\UserWallet::where('user_id', $user_id)->first() ?: new \App\UserWallet;

            $user_wallet->user_id = $user_id;

            $user_wallet->remaining -= $amount;

            $user_wallet->onhold += $amount;

            $user_wallet->save();

            $response = ['success' => true, 'message' => 'done', 'data' => $user_wallet];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

    /**
     * @method user_wallet_update_withdraw_cancel()
     *
     * @uses pay for live videos using stripe
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $request
     *
     * @return object $payment_details
     */

    public static function user_wallet_update_withdraw_cancel($amount, $user_id) {

        try {

            $user_wallet = \App\UserWallet::where('user_id', $user_id)->first() ?: new \App\UserWallet;

            $user_wallet->user_id = $user_id;

            $user_wallet->remaining += $amount;

            $user_wallet->onhold -= $amount;

            $user_wallet->save();

            $response = ['success' => true, 'message' => 'done', 'data' => $user_wallet];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

    /**
     * @method user_wallet_update_withdraw_paynow()
     *
     * @uses pay for live videos using stripe
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $request
     *
     * @return object $payment_details
     */

    public static function user_wallet_update_withdraw_paynow($amount, $user_id) {

        try {

            $user_wallet = \App\UserWallet::where('user_id', $user_id)->first() ?: new \App\UserWallet;

            $user_wallet->user_id = $user_id;

            $user_wallet->onhold -= $amount;

            $user_wallet->used += $amount;

            $user_wallet->save();

            $response = ['success' => true, 'message' => 'done', 'data' => $user_wallet];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

    /**
     * @method user_wallet_update_dispute_send()
     *
     * @uses pay for live videos using stripe
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $request
     *
     * @return object $payment_details
     */

    public static function user_wallet_update_dispute_send($amount, $user_id) {

        try {

            $user_wallet = \App\UserWallet::where('user_id', $user_id)->first() ?: new \App\UserWallet;

            $user_wallet->user_id = $user_id;

            $user_wallet->remaining -= $amount;

            $user_wallet->onhold += $amount;

            $user_wallet->save();

            $response = ['success' => true, 'message' => 'done', 'data' => $user_wallet];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

    /**
     * @method user_wallet_update_dispute_cancel()
     *
     * @uses pay for live videos using stripe
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $request
     *
     * @return object $payment_details
     */

    public static function user_wallet_update_dispute_cancel($amount, $user_id) {

        try {

            $user_wallet = \App\UserWallet::where('user_id', $user_id)->first() ?: new \App\UserWallet;

            $user_wallet->user_id = $user_id;

            $user_wallet->remaining += $amount;

            $user_wallet->onhold -= $amount;

            $user_wallet->save();

            $response = ['success' => true, 'message' => 'done', 'data' => $user_wallet];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

    /**
     * @method user_wallet_update_dispute_approve()
     *
     * @uses pay for live videos using stripe
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $request
     *
     * @return object $payment_details
     */

    public static function user_wallet_update_dispute_approve($amount, $user_id, $receiver_user_id) {

        try {

            // Winner wallet update

            $user_wallet = \App\UserWallet::where('user_id', $user_id)->first() ?: new \App\UserWallet;

            $user_wallet->user_id = $user_id;

            $user_wallet->remaining += $amount;

            $user_wallet->used -= $amount;

            $user_wallet->save();

            // Loser wallet update
            $receiver_user_wallet = \App\UserWallet::where('user_id', $receiver_user_id)->first() ?: new \App\UserWallet;

            $receiver_user_wallet->user_id = $receiver_user_id;

            $receiver_user_wallet->total -= $amount;

            $receiver_user_wallet->onhold -= $amount;

            $receiver_user_wallet->save();

            $response = ['success' => true, 'message' => 'done', 'data' => $user_wallet];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

    /**
     * @method user_wallet_update_dispute_reject()
     *
     * @uses pay for live videos using stripe
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $request
     *
     * @return object $payment_details
     */

    public static function user_wallet_update_dispute_reject($amount, $receiver_user_id) {

        try {

            // Opposite party wallet update
            $receiver_user_wallet = \App\UserWallet::where('user_id', $receiver_user_id)->first() ?: new \App\UserWallet;

            $receiver_user_wallet->user_id = $receiver_user_id;

            $receiver_user_wallet->total += $remaining;

            $receiver_user_wallet->onhold -= $amount;

            $receiver_user_wallet->save();

            $response = ['success' => true, 'message' => 'done', 'data' => $user_wallet];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

    /**
     * @method user_wallet_update_invoice_payment()
     *
     * @uses pay for live videos using stripe
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $request
     *
     * @return object $payment_details
     */

    public static function user_wallet_update_invoice_payment($amount, $sender_id, $to_user_id) {

        try {

            // Receiver wallet update

            $sender_wallet = \App\UserWallet::where('user_id', $sender_id)->first() ?: new \App\UserWallet;

            Log::info("sender_wallet".print_r($sender_wallet->toArray(), true));

            $sender_wallet->user_id = $sender_id;

            $sender_wallet->total += $amount;

            $sender_wallet->remaining += $amount;

            $sender_wallet->save();

            // Payer wallet update
            $to_user_wallet = \App\UserWallet::where('user_id', $to_user_id)->first() ?: new \App\UserWallet;

            Log::info("to_user_wallet".print_r($to_user_wallet->toArray(), true));

            $to_user_wallet->user_id = $to_user_id;

            $to_user_wallet->remaining -= $amount;

            $to_user_wallet->used += $amount;

            $to_user_wallet->save();

            $response = ['success' => true, 'message' => 'done', 'data' => $sender_wallet];

            return response()->json($response, 200);

        } catch(Exception $e) {

            Log::info("error".print_r($e->getMessage(), true));

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

   
    /**
     * @method users_account_upgrade()
     *
     * @uses add amount to user
     *
     * @created Bhawya
     *
     * @updated Bhawya
     *
     * @param integer $user_id, float $admin_amount, $user_amount
     *
     * @return - 
     */
    
    public static function users_account_upgrade($user_id, $paid_amount = 0.00, $subscription_amount, $expiry_date) {

        if($user = User::find($user_id)) {

            $user->user_type = SUBSCRIBED_USER;

            $user->one_time_subscription = $subscription_amount <= 0 ? YES : NO;

            $user->amount_paid += $paid_amount ?? 0.00;

            $user->expiry_date = $expiry_date;

            $user->no_of_days = total_days($expiry_date);

            $user->save();
        
        }
    
    }

    
    /**
     * @method chat_assets_payment_by_stripe()
     *
     * @uses 
     *
     * @created Arun
     * 
     * @updated Arun
     *
     * @param object $chat_message, object $request
     *
     * @return object $chat_message
     */

    public static function chat_assets_payment_by_stripe($request, $chat_message) {

        try {

            // Check stripe configuration

            $stripe_secret_key = Setting::get('stripe_secret_key');

            if(!$stripe_secret_key) {

                throw new Exception(api_error(107), 107);

            } 

            \Stripe\Stripe::setApiKey($stripe_secret_key);
           
            $currency_code = Setting::get('currency_code', 'USD') ?: "USD";

            $total = intval(round($request->user_pay_amount * 100));

            $charge_array = [
                'amount' => $total,
                'currency' => $currency_code,
                'customer' => $request->customer_id,
                "payment_method" => $request->card_token,
                'off_session' => true,
                'confirm' => true,
            ];

            $stripe_payment_response = \Stripe\PaymentIntent::create($charge_array);

            $payment_data = [
                'payment_id' => $stripe_payment_response->id ?? 'CARD-'.rand(),
                'paid_amount' => $stripe_payment_response->amount/100 ?? $total,
                'paid_status' => $stripe_payment_response->paid ?? true
            ];

            $response = ['success' => true, 'message' => 'done', 'data' => $payment_data];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

    /**
     * @method chat_assets_payment_save()
     *
     * @uses used to save chat_assets payment details
     *
     * @created Arun
     * 
     * @updated Arun
     *
     * @param object $request
     *
     * @return object $chat_asset_payment
     */

    public static function chat_assets_payment_save($request, $chat_message) {

        try {

            $chat_asset_payment = new \App\ChatAssetPayment;
            
            $chat_asset_payment->from_user_id = $chat_message->from_user_id;

            $chat_asset_payment->to_user_id = $chat_message->to_user_id;

            $chat_asset_payment->chat_message_id = $chat_message->chat_message_id;

            $chat_asset_payment->user_card_id = $request->user_card_id ?? 0;
            
            $chat_asset_payment->payment_id = $request->payment_id ?:generate_payment_id();

            $chat_asset_payment->paid_amount = $request->paid_amount ?? 0.00;

            $chat_asset_payment->currency = Setting::get('currency') ?? "$";

            $chat_asset_payment->payment_mode = $request->payment_mode ?? CARD;

            $chat_asset_payment->paid_date = date('Y-m-d H:i:s');

            $chat_asset_payment->status = $request->paid_status ?: PAID;

            $commission = admin_commission_spilit($request->paid_amount);

            $chat_asset_payment->admin_amount = $commission->admin_amount ?? 0.00;

            $chat_asset_payment->user_amount = $commission->user_amount ?? 0.00;

            $chat_asset_payment->save();

            self::chat_payment_wallet_update($request, $chat_asset_payment);

            $response = ['success' => true, 'message' => 'paid', 'data' => $chat_asset_payment];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }
    
    }
    
      /**
     * @method order_payment_by_stripe()
     *
     * @uses pay for order product using stripe
     *
     * @created Jeevan
     * 
     * @updated 
     *
     * @param object $request
     *
     * @return object $payment_details
     */

    public static function order_payment_by_stripe($request) {

        try {

            // Check stripe configuration
        
            $stripe_secret_key = Setting::get('stripe_secret_key');

            if(!$stripe_secret_key) {

                throw new Exception(api_error(107), 107);

            } 

            \Stripe\Stripe::setApiKey($stripe_secret_key);
           
            $currency_code = Setting::get('currency_code', 'USD') ?: 'USD';

            $total = intval(round($request->user_pay_amount * 100));

            $charge_array = [
                'amount' => $total,
                'currency' => $currency_code,
                'customer' => $request->customer_id,
                "payment_method" => $request->card_token,
                'off_session' => true,
                'confirm' => true,
            ];

            $stripe_payment_response = \Stripe\PaymentIntent::create($charge_array);

            $payment_data = [
                                'payment_id' => $stripe_payment_response->id ?? 'CARD-'.rand(),
                                'paid_amount' => $stripe_payment_response->amount/100 ?? $total,

                                'paid_status' => $stripe_payment_response->paid ?? true
                            ];

            $response = ['success' => true, 'message' => 'done', 'data' => $payment_data];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

    /**
     * @method order_product_save()
     *
     * @uses used to save Order Product details
     *
     * @created Arun
     * 
     * @updated 
     *
     * @param object $post, object $request
     *
     * @return object $order_product
     */

    public static function order_product_save($request, $order, $cart) {

        try {

            $order_product = OrderProduct::find($request->order_id) ?? new OrderProduct;

            $order_product->user_id = $request->id;

            $order_product->order_id = $order->id ?? 0;

            $order_product->user_product_id = $cart->user_product_id ?? 0;

            $order_product->quantity = $cart->quantity ?? 0;

            $order_product->per_quantity_price = $cart->per_quantity_price ?? 0.00;

            $order_product->sub_total = $cart->sub_total ?? 0.00;

            $order_product->tax_price = $cart->tax_price ?? 0.00;

            $order_product->delivery_price = $cart->delivery_price ?? 0.00;

            $order_product->total = $cart->total ?? 0.00;

            $order_product->save();
            
            $response = ['success' => true, 'message' => 'paid', 'data' => [ 'order_product' => $order_product]];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }
    
    }

    /**
     * @method orders_payment_by_stripe()
     *
     * @uses order payment - card
     *
     * @created Subham Kant
     * 
     * @updated 
     *
     * @param object $order, object $request
     *
     * @return object $order_paym
     */

    public static function orders_payment_by_stripe($request) {

        try {

            // Check stripe configuration
        
            $stripe_secret_key = Setting::get('stripe_secret_key');

            if(!$stripe_secret_key) {

                throw new Exception(api_error(107), 107);

            } 

            \Stripe\Stripe::setApiKey($stripe_secret_key);
           
            $currency_code = Setting::get('currency_code', 'USD') ?: "USD";

            $total = intval(round($request->user_pay_amount * 100));

            $charge_array = [
                'amount' => $total,
                'currency' => $currency_code,
                'customer' => $request->customer_id,
                "payment_method" => $request->card_token,
                'off_session' => true,
                'confirm' => true,
            ];

            $stripe_payment_response = \Stripe\PaymentIntent::create($charge_array);

            $payment_data = [
                'payment_id' => $stripe_payment_response->id ?? 'CARD-'.rand(),
                'paid_amount' => $stripe_payment_response->amount/100 ?? $total,
                'paid_status' => $stripe_payment_response->paid ?? true
            ];

            $response = ['success' => true, 'message' => 'done', 'data' => $payment_data];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

    /**
     * @method order_payments_save()
     *
     * @uses used to save Order payment details
     *
     * @created Arun
     * 
     * @updated 
     *
     * @param object $post, object $request
     *
     * @return object $order_payment
     */

    public static function order_payments_save($request, $order) {

        try {

            $order_payment = new OrderPayment;

            $order_payment->user_id = $request->id;

            $order_payment->order_id = $order->id ?: 0;

            $order_payment->payment_id = $request->payment_id ?? "NO-".rand();

            $order_payment->payment_mode = $request->payment_mode ?? CARD;

            $order_payment->delivery_price = $order->delivery_price ?? 0.00;

            $order_payment->sub_total = $order->sub_total ?? 0.00;

            $order_payment->tax_price = $order->tax_price ?? 0.00;

            $order_payment->total = $total = $order->total ?? 0.00;

            $order_payment->paid_date = date('Y-m-d H:i:s');

            $order_payment->status = $request->payment_status ?? PAID;

            $order_payment->save();

            // Add to Order model wallet

            if($order_payment->status == PAID) {

                $carts = Cart::whereIn('carts.id',$request->cart_ids)->BaseResponse()->get();

                foreach ($carts as $key => $cart) {
                    
                    self::order_payment_wallet_update($request, $order_payment, $cart);
                }
                
            }
            
            $response = ['success' => true, 'message' => 'paid', 'data' => [ 'payment_id' => $request->payment_id]];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }
    
    }

     /**
     * @method order_payment_wallet_update
     *
     * @uses Order payment amount will update to the model wallet
     *
     * @created Arun
     *
     * @updated 
     *
     * @param
     *
     * @return
     */

    public static function order_payment_wallet_update($request, $order_payment, $cart) {

        try {

            // Commission calculation

            $total = $cart->actual_total ?? 0.00;

            $admin_token = $user_token = 0.00;

            if(Setting::get('is_only_wallet_payment')) {

                $admin_commission_in_per = Setting::get('admin_commission', 1)/100;

                $admin_token = $total * $admin_commission_in_per;

                $user_token = $total - $admin_token;

                $admin_amount = $admin_token * Setting::get('token_amount');

                $user_amount = $user_token * Setting::get('token_amount');

            } else {

                $admin_commission_in_per = Setting::get('admin_commission', 1)/100;
                
                $admin_amount = $total * $admin_commission_in_per;

                $user_amount = $total - $admin_amount;

            }

            $to_user_inputs = [
                'id' => $cart->model_id,
                'received_from_user_id' => $request->id,
                'total' => $cart->actual_total, 
                'user_pay_amount' => $user_amount,
                'paid_amount' => $user_amount,
                'payment_type' => WALLET_PAYMENT_TYPE_CREDIT,
                'amount_type' => WALLET_AMOUNT_TYPE_ADD,
                'payment_id' => $order_payment->payment_id,
                'user_amount' => $user_amount,
                'admin_amount' => $admin_amount,
                'message' => $request->message,
                'user_token' => $user_token,
                'admin_token' => $admin_token,
                'tokens' => $user_token,
                'usage_type' => USAGE_TYPE_ORDER,
            ];

            $to_user_request = new \Illuminate\Http\Request();

            $to_user_request->replace($to_user_inputs);

            $to_user_payment_response = self::user_wallets_payment_save($to_user_request)->getData();

            if($to_user_payment_response->success) {

                DB::commit();

                return $to_user_payment_response;

            } else {

                throw new Exception($to_user_payment_response->error, $to_user_payment_response->error_code);
            }
        
        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

    /**
     * @method product_quantity_update
     *
     * @uses Update user product quantity after order
     *
     * @created Arun
     *
     * @updated 
     *
     * @param
     *
     * @return
     */

    public static function order_product_quantity_update($order) {

        try {

            $order_products = OrderProduct::where('order_id', $order->id)->get();

            foreach($order_products as $order_product){

                $user_product = UserProduct::find($order_product->user_product_id);

                if($user_product) { 

                    $quantity = $user_product->quantity - $order_product->quantity;

                    $user_product->quantity = $quantity;
                    
                    $user_product->is_outofstock = intval($quantity) > 0 ? IN_STOCK : OUT_OF_STOCK;

                    $user_product->save();               
                }
                
            }

            $response = ['success' => true, 'message' => tr('product_quantity_updated'), 'data' => [ 'order_products' => $order_products]];

            return response()->json($response, 200);
        
        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

}