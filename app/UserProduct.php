<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProduct extends Model
{
    protected $appends = ['user_product_id','price_formatted','category_name','sub_category_name','user_product_price_formatted'];

    public function getUserProductIdAttribute() {

        return $this->id;
    }

    public function getPriceFormattedAttribute() {

        return formatted_amount($this->price);
    }

    public function getUserProductPriceFormattedAttribute() {

        return formatted_amount($this->price);
    }

    public function getCategoryNameAttribute() {

        $name = $this->productCategories->name ?? "";

        // unset($this->user);

        return $name ?? "";
    }

    public function getSubCategoryNameAttribute() {

        $name = $this->productSubCategories->name ?? "";

        unset($this->productSubCategories);

        return $name ?? "";
    }


    public function user(){

    	return $this->belongsTo(User::class,'user_id');
    }

    public function userProductPictures() {

        return $this->hasMany(UserProductPicture::class,'user_product_id');
    }

    public function orderProducts() {

        return $this->hasMany(OrderProduct::class,'user_product_id');
    }

    public function productCategories() {

        return $this->belongsTo(Category::class,'category_id');
    }

    public function productSubCategories() {

        return $this->belongsTo(SubCategory::class,'sub_category_id');
    }

    public function cart() {

        return $this->hasMany(Cart::class,'user_product_id');
    }
    
    /**
     * Scope a query to only include active product.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query) {

        $query->where('user_products.status', YES);

        return $query;

    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {
            $model->attributes['unique_id'] = "UP"."-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "UP"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

        static::deleting(function ($model) {

            \Helper::storage_delete_file($model->picture, PRODUCT_FILE_PATH);

            $model->userProductPictures()->delete();

            $model->orderProducts()->delete();

            $model->cart()->delete();

        });

    }
}
