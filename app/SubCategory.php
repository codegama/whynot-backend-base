<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{    
	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id'];


    protected $appends = ['sub_category_id'];

    public function getSubCategoryIdAttribute() {

        return $this->id;
    }

    public function category() {

        return $this->belongsTo(Category::class,'category_id');

    }

    public function userProducts() {

        return $this->hasMany(UserProduct::class,'sub_category_id');
    }

    public function liveVideos() {

        return $this->hasMany(LiveVideo::class,'sub_category_id');
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = routefreestring(strtolower($model->attributes['name']));

        });

        static::created(function($model) {

            $model->attributes['unique_id'] = routefreestring(strtolower($model->attributes['name']));

            $model->save();
        
        });

        static::updating(function ($model) {

            $model->attributes['unique_id'] = routefreestring(strtolower($model->attributes['name']));
            
        });

        static::deleting(function ($model){

            $model->userProducts()->delete();
        });

    }

}
