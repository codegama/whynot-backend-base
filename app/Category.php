<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id'];

    protected $appends = ['category_id'];

    public function getCategoryIdAttribute() {

        return $this->id;
    }

    public function subCategories() {

        return $this->hasMany(SubCategory::class,'category_id');
    }

    public function userProducts() {

        return $this->hasMany(UserProduct::class,'category_id');
    }

    public function liveVideos() {

        return $this->hasMany(LiveVideo::class,'category_id');
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommonResponse($query) {

        return $query;
    
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query) {

        return $query->where('categories.status', APPROVED);

    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = routefreestring(strtolower($model->attributes['name']));

        });

        static::created(function($model) {

            $model->attributes['unique_id'] = routefreestring(strtolower($model->attributes['name']));

            $model->save();
        
        });

        static::updating(function ($model) {

            $model->attributes['unique_id'] = routefreestring(strtolower($model->attributes['name']));
            
        });

        static::deleting(function ($model){

            $model->subCategories()->delete();

            $model->userProducts()->delete();
        });

    }
}
