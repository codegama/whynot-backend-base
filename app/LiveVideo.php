<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveVideo extends Model
{	
    protected $fillable = ['is_streaming'];

	protected $hidden = ['id','unique_id'];

    protected $appends = ['live_video_id', 'live_video_unique_id','amount_formatted', 'payment_type_text','created_at_formatted','status_formatted','username', 'user_displayname','user_picture', 'user_unique_id', 'admin_amount_formatted','user_amount_formatted','is_verified_badge','formatted_schedule_time','total_bookmarks','category_unique_id','category_name'];

    public function getLiveVideoIdAttribute() {

        return $this->id;
    }

    public function getLiveVideoUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function getAmountFormattedAttribute() {

        return formatted_amount($this->amount);
    }

    public function getAdminAmountFormattedAttribute() {

        return formatted_amount($this->admin_amount);
    }

    public function getUserAmountFormattedAttribute() {

        $total = $this->orderProducts->sum('total') ?? 0;

        unset($this->orderProducts);
        
        return formatted_amount($total);

    }

    public function getPaymentTypeTextAttribute() {

        return formatted_live_payment_text($this->payment_status);
    }

    public function getStatusFormattedAttribute() {

        return live_video_status($this->status);
    }

    public function getCreatedAtFormattedAttribute() {
        return $this->asDateTime($this->created_at)->format('Y-m-d h:i A');
    }

    public function getFormattedScheduleTimeAttribute() {

        return $this->asDateTime($this->schedule_time)->format('Y-m-d h:i A');
    }

    public function getTotalBookmarksAttribute() {

        $total_bookmarks = $this->liveVideoBookmarks->count() ?? 0;

        unset($this->liveVideoBookmarks);
        
        return $total_bookmarks;

    }

    public function getCategoryUniqueIdAttribute() {

        $unique_id = $this->category->unique_id ?? '';

        unset($this->category);
        
        return $unique_id;

    }

    public function getCategoryNameAttribute() {

        $name = $this->category->name ?? '';

        unset($this->category);
        
        return $name;

    }

    public function getIsVerifiedBadgeAttribute() {

        $is_verified_badge = $this->user->is_verified_badge ?? "";

        unset($this->user);

        return $is_verified_badge ?? "";
    }

    public function getUserUniqueIdAttribute() {

        $user_unique_id = $this->user->unique_id ?? "";

        unset($this->user);

        return $user_unique_id ?? "";
    }

    public function getUsernameAttribute() {

        $username = $this->user->username ?? "";

        unset($this->user);

        return $username ?? "";
    }

    public function getUserDisplaynameAttribute() {

        $name = $this->user->name ?? "";

        unset($this->user);

        return $name ?? "";
    }

    public function getUserPictureAttribute() {

        $picture = $this->user->picture ?? "";

        unset($this->user);

        return $picture ?? "";
    }

    public function setUniqueIdAttribute($value){

		$this->attributes['unique_id'] = uniqid(str_replace(' ', '-', $value));

	}

	public function payments() {

		return $this->hasMany('App\LiveVideoPayment');
		
	}  

    public function orderProducts() {

       return $this->hasMany(OrderProduct::class, 'live_video_id');
    }

    public function liveVideoBookmarks() {

       return $this->hasMany(LiveVideoBookmark::class, 'live_video_id');
    }

	public function user() {

       return $this->belongsTo(User::class, 'user_id');
    }

	/**
     * Load viewers using relation model
     */
    public function getViewers()
    {
        return $this->hasMany('App\Viewer', 'video_id', 'id');
    }

    /**
     * Load viewers using relation model
     */
    public function getVideosPayments()
    {
        return $this->hasMany('App\LiveVideoPayment', 'live_video_id', 'id');
    }

    public function liveVideoProducts() {

        return $this->hasMany(LiveVideoProduct::class, 'live_video_id', 'id');
    }

    public function scopeFeaturedVideos($query) {

        $query->where([ ['live_schedule_type', LIVE_SCHEDULE_TYPE_LATER], ['schedule_time' , '>' , now()] ]);

        return $query;
    }

    public function scopeLiveVideos($query) {

        $query->where([ ['live_schedule_type', LIVE_SCHEDULE_TYPE_NOW], ['is_streaming', YES] ]);

        return $query;
    }

    public function category() {

       return $this->belongsTo(Category::class, 'category_id');
    }

    public function subCategory() {

       return $this->belongsTo(SubCategory::class, 'sub_category_id');
    }

    /**
     * Boot function for using with User Events
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = $model->id.'-'.routefreestring(strtolower($model->attributes['title']));

        });

        static::created(function($model) {

            $model->attributes['unique_id'] = $model->id.'-'.routefreestring(strtolower($model->attributes['title']));

            $model->save();
        
        });

        static::updating(function ($model) {

            $model->attributes['unique_id'] = $model->id.'-'.routefreestring(strtolower($model->attributes['title']));
            
        });
        
         //delete your related models here, for example
        static::deleting(function($model)
        {

            if ($model->getVideosPayments->count() > 0) {

                foreach($model->getVideosPayments as $videoPayments)
                {
                    $videoPayments->delete();
                } 

            }

            $model->orderProducts()->delete();

        });
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCurrentLive($query) {

        return $query->where('live_videos.is_streaming', YES)
                ->where('live_videos.status', VIDEO_STREAMING_ONGOING);

    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFeaturedLiveVideos($query) {

        $query->where('schedule_time' , '>' , now())
            ->orWhere('is_streaming', YES);

        return $query;
    }

}
