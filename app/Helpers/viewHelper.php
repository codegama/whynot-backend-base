<?php

use Carbon\Carbon;

// Helper, Setting, Log;

use App\User, App\PageCounter, App\Settings;

use App\PromoCode, App\UserPromoCode, App\OrderPayment;

use App\Repositories\CommonRepository as CommonRepo;

function amount_convertion($percentage, $amt) {

    $converted_amt = $amt * ($percentage/100);

    return $converted_amt;
}

/**
 * @method promo_calculation()
 *
 * @uses Calculate price based on total days
 *
 * @created Subham
 *
 * @updated
 *
 * @param date $start_date
 *
 * @return string $amount
 */
function promo_calculation($amount,$request) {

    if ($request->promo_code) {

        $promo_code = PromoCode::where('promo_code', $request->promo_code)->first();

        $user_details = User::where('id', $request->id)->first();

        $check_promo_code = CommonRepo::check_promo_code_applicable_to_user($user_details, $promo_code)->getData();

        if($check_promo_code->success == true){

            $amount_convertion = $promo_code->amount;

            if ($promo_code->amount_type == PERCENTAGE) {

                $amount_convertion = amount_convertion($promo_code->amount, $amount);

            }

            if ($amount_convertion < $amount) {

                $total_amount = $amount - $amount_convertion;

                $coupon_amount = $amount_convertion;

            } else {

                // If the coupon amount greater than subscription amount, then assign to zero.

                $total_amount = 0;

                $coupon_amount = $amount_convertion;
                
            }

            $amount = $coupon_amount;

        }

        $user_promo_code = UserPromoCode::where('user_id', $request->id)->where('promo_code', $request->promo_code)->first() ?? new UserPromoCode;

        $user_promo_code->user_id = $request->id;

        $user_promo_code->promo_code = $request->promo_code;

        $user_promo_code->no_of_times_used += 1;

        $user_promo_code->save();

    }
    
    return $coupon_amount;

}

/**
 * @method tr()
 *
 * Description: used to convert the string to language based string
 *
 * @created Vidhya R
 *
 * @updated
 *
 * @param string $key
 *
 * @return string value
 */
function tr($key , $additional_key = "" , $lang_path = "messages.") {

    // if(Auth::guard('admin')->check()) {

    //     $locale = config('app.locale');

    // } else {

        if (!\Session::has('locale')) {

            $locale = \Session::put('locale', config('app.locale'));

        }else {

            $locale = \Session::get('locale');

        }
    // }
    return \Lang::choice('messages.'.$key, 0, Array('other_key' => $additional_key), $locale);

}

function api_success($key , $other_key = "" , $lang_path = "messages.") {

    if (!\Session::has('locale')) {

        $locale = \Session::put('locale', config('app.locale'));

    } else {

        $locale = \Session::get('locale');

    }
    return \Lang::choice('api-success.'.$key, 0, Array('other_key' => $other_key), $locale);

}

function api_error($key , $other_key = "" , $lang_path = "messages.") {

    if (!\Session::has('locale')) {

        $locale = \Session::put('locale', config('app.locale'));

    } else {

        $locale = \Session::get('locale');

    }
    return \Lang::choice('api-error.'.$key, 0, Array('other_key' => $other_key), $locale);

}

/**
 * @method envfile()
 *
 * Description: get the configuration value from .env file 
 *
 * @created Vidhya R
 *
 * @updated
 *
 * @param string $key
 *
 * @return string value
 */

function envfile($key) {

    $data = getEnvValues();

    if($data) {
        return $data[$key];
    }

    return "";

}

function getEnvValues() {

    $data =  [];

    $path = base_path('.env');

    if(file_exists($path)) {

        $values = file_get_contents($path);

        $values = explode("\n", $values);

        foreach ($values as $key => $value) {

            $var = explode('=',$value);

            if(count($var) == 2 ) {
                if($var[0] != "")
                    $data[$var[0]] = $var[1] ? $var[1] : null;
            } else if(count($var) > 2 ) {
                $keyvalue = "";
                foreach ($var as $i => $imp) {
                    if ($i != 0) {
                        $keyvalue = ($keyvalue) ? $keyvalue.'='.$imp : $imp;
                    }
                }
                $data[$var[0]] = $var[1] ? $keyvalue : null;
            }else {
                if($var[0] != "")
                    $data[$var[0]] = null;
            }
        }

        array_filter($data);
    
    }

    return $data;

}

/**
 * @method register_mobile()
 *
 * Description: Update the user register device details 
 *
 * @created Vidhya R
 *
 * @updated
 *
 * @param string $device_type
 *
 * @return - 
 */

function register_mobile($device_type) {

    // if($reg = MobileRegister::where('type' , $device_type)->first()) {

    //     $reg->count = $reg->count + 1;

    //     $reg->save();
    // }
    
}

/**
 * Function Name : subtract_count()
 *
 * Description: While Delete user, subtract the count from mobile register table based on the device type
 *
 * @created vithya R
 *
 * @updated vithya R
 *
 * @param string $device_ype : Device Type (Andriod,web or IOS)
 * 
 * @return boolean
 */

function subtract_count($device_type) {

    if($reg = MobileRegister::where('type' , $device_type)->first()) {

        $reg->count = $reg->count - 1;
        
        $reg->save();
    }

}

/**
 * @method get_register_count()
 *
 * Description: Get no of register counts based on the devices (web, android and iOS)
 *
 * @created Vidhya R
 *
 * @updated
 *
 * @param - 
 *
 * @return array value
 */

function get_register_count() {

    $ios_count = MobileRegister::where('type' , 'ios')->get()->count();

    $android_count = MobileRegister::where('type' , 'android')->get()->count();

    $web_count = MobileRegister::where('type' , 'web')->get()->count();

    $total = $ios_count + $android_count + $web_count;

    return array('total' => $total , 'ios' => $ios_count , 'android' => $android_count , 'web' => $web_count);

}

/**
 * @method: last_x_days_page_view()
 *
 * @uses: to get last x days page visitors analytics
 *
 * @created Anjana H
 *
 * @updated Anjana H
 *
 * @param - 
 *
 * @return array value
 */
function last_x_days_page_view($days){

    $views = PageCounter::orderBy('created_at','asc')->where('created_at', '>', Carbon::now()->subDays($days))->where('page','home');
 
    $arr = array();
 
    $arr['count'] = $views->count();

    $arr['get'] = $views->get();

      return $arr;
}

function counter($page = 'home'){

    $count_home = PageCounter::wherePage($page)->where('created_at', '>=', new DateTime('today'));

        if($count_home->count() > 0) {
            $update_count = $count_home->first();
            $update_count->count = $update_count->count + 1;
            $update_count->save();
        } else {
            $create_count = new PageCounter;
            $create_count->page = $page;
            $create_count->count = 1;
            $create_count->save();
        }

}

//this function convert string to UTC time zone

function convertTimeToUTCzone($str, $userTimezone, $format = 'Y-m-d H:i:s') {

    $new_str = new DateTime($str, new DateTimeZone($userTimezone));

    $new_str->setTimeZone(new DateTimeZone('UTC'));

    return $new_str->format( $format);
}

//this function converts string from UTC time zone to current user timezone

function convertTimeToUSERzone($str, $userTimezone, $format = 'Y-m-d H:i:s') {

    if(empty($str)){
        return '';
    }
    
    try {
        
        $new_str = new DateTime($str, new DateTimeZone('UTC') );
        
        $new_str->setTimeZone(new DateTimeZone( $userTimezone ));
    }
    catch(\Exception $e) {
        // Do Nothing
    }
    
    return $new_str->format( $format);
}

function number_format_short( $n, $precision = 1 ) {

    if ($n < 900) {
        // 0 - 900
        $n_format = number_format($n, $precision);
        $suffix = '';
    } else if ($n < 900000) {
        // 0.9k-850k
        $n_format = number_format($n / 1000, $precision);
        $suffix = 'K';
    } else if ($n < 900000000) {
        // 0.9m-850m
        $n_format = number_format($n / 1000000, $precision);
        $suffix = 'M';
    } else if ($n < 900000000000) {
        // 0.9b-850b
        $n_format = number_format($n / 1000000000, $precision);
        $suffix = 'B';
    } else {
        // 0.9t+
        $n_format = number_format($n / 1000000000000, $precision);
        $suffix = 'T';
    }
  // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
  // Intentionally does not affect partials, eg "1.50" -> "1.50"
    if ( $precision > 0 ) {
        $dotzero = '.' . str_repeat( '0', $precision );
        $n_format = str_replace( $dotzero, '', $n_format );
    }
    return $n_format . $suffix;

}

function common_date($date , $timezone , $format = "d M Y h:i A") {

    if(!$date) {

        return "";

    }
    
    if($timezone) {

        $date = convertTimeToUSERzone($date , $timezone , $format);

    }   
   
    return date($format , strtotime($date));
}


/**
 * function delete_value_prefix()
 * 
 * @uses used for concat string, while deleting the records from the table
 *
 * @created vidhya R
 *
 * @updated vidhya R
 *
 * @param $prefix - from settings table (Setting::get('prefix_user_delete'))
 *
 * @param $primary_id - Primary ID of the delete record
 *
 * @param $is_email 
 *
 * @return concat string based on the input values
 */

function delete_value_prefix($prefix , $primary_id , $is_email = 0) {

    if($is_email) {

        $site_name = str_replace(' ', '_', Setting::get('site_name'));

        return $prefix.$primary_id."@".$site_name.".com";
        
    } else {
        return $prefix.$primary_id;

    }

}

/**
 * function routefreestring()
 * 
 * @uses used for remove the route parameters from the string
 *
 * @created vidhya R
 *
 * @updated vidhya R
 *
 * @param string $string
 *
 * @return Route parameters free string
 */

function routefreestring($string) {

    $string = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $string));
    
    $search = [' ', '&', '%', "?",'=','{','}','$'];

    $replace = ['-', '-', '-' , '-', '-', '-' , '-','-'];

    $string = str_replace($search, $replace, $string);

    return $string;
    
}

/**
 * @method selected()
 *
 * @uses set selected item 
 *
 * @created Anjana H
 *
 * @updated Anjana H
 *
 * @param $array, $id, $check_key_name
 *
 * @return response of array 
 */
function selected($array, $id, $check_key_name) {
    
    $is_key_array = is_array($id);
    
    foreach ($array as $key => $value) {

        $value->is_selected = ($value->$check_key_name == $id) ? YES : NO;
    }  

    return $array;
}


function nFormatter($num, $currency = "") {

    $currency = Setting::get('currency', "$");

    if($num>1000) {

        $x = round($num);

        $x_number_format = number_format($x);

        $x_array = explode(',', $x_number_format);

        $x_parts = ['k', 'm', 'b', 't'];

        $x_count_parts = count($x_array) - 1;

        $x_display = $x;

        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');

        $x_display .= $x_parts[$x_count_parts - 1];

        return $currency." ".$x_display;

    }

    return $currency." ".$num;

}

/**
 * @method formatted_plan()
 *
 * @uses used to format the number
 *
 * @created Bhawya
 *
 * @updated Akshata
 *
 * @param integer $num
 * 
 * @param string $currency
 *
 * @return string $formatted_plan
 */

function formatted_plan($plan = 0, $type = "month") {

    switch ($type) {

        case 'weeks':

            $text = $plan <= 1 ? tr('week') : tr('weeks');

            break;

        case 'days':

            $text = $plan <= 1 ? tr('day') : tr('days');

            break;

        case 'years':

            $text = $plan <= 1 ? tr('year') : tr('years');

            break;
        
        default:
        
            $text = $plan <= 1 ? tr('month') : tr('months');
            
            break;
    }
    
    return $plan." ".$text;
}

/**
 * @method formatted_amount()
 *
 * @uses used to format the number
 *
 * @created vidhya R
 *
 * @updated vidhya R
 *
 * @param integer $num
 * 
 * @param string $currency
 *
 * @return string $formatted_amount
 */

function formatted_amount($amount = 0.00, $currency = "") {
   
    $currency = $currency ?: Setting::get('currency', '$');

    $amount = number_format((float)$amount, 2, '.', '');

    $formatted_amount = $currency."".$amount ?: "0.00";

    return $formatted_amount;
}

function readFileLength($file)  {

    $variableLength = 0;
    if (($handle = fopen($file, "r")) !== FALSE) {
         $row = 1;
         while (($data = fgetcsv($handle, 1000, "\n")) !== FALSE) {
            $num = count($data);
            $row++;
            for ($c=0; $c < $num; $c++) {
                $exp = explode("=>", $data[$c]);
                if (count($exp) == 2) {
                    $variableLength += 1; 
                }
            }
        }
        fclose($handle);
    }

    return $variableLength;
}


function getExpiryDate($plan,$plan_type) {
   
    $expiry_date = date('Y-m-d h:i:s',strtotime("+{$plan}{$plan_type}"));

    return $expiry_date;
}

function last_6_months_data() {

    $months = 6;

    $data = new \stdClass;

    $data->currency = $currency = Setting::get('currency', '$');

    $last_x_days_revenues = [];

    $start  = new \DateTime('-6 month', new \DateTimeZone('UTC'));
    
    $period = new \DatePeriod($start, new \DateInterval('P1M'), $months);
    
    $dates = $last_x_days_revenues = [];

    foreach ($period as $date) {

        $current_month = $date->format('M');

        $last_x_days_data = new \stdClass;

        $last_x_days_data->month= $current_month;

        $month = $date->format('m');
      
        $last_x_days_total_earnings = 0;
        
        $last_x_days_data->total_earnings = $last_x_days_total_earnings ?: 0.00;

        array_push($last_x_days_revenues, $last_x_days_data);

    }
    
    $data->last_x_days_revenues = $last_x_days_revenues;
    
    return $data;  
}

function static_page_footers($section_type = 0, $is_list = NO) {

    $lists = [
                STATIC_PAGE_SECTION_1 => tr('STATIC_PAGE_SECTION_1')."(".Setting::get('site_name').")",
                STATIC_PAGE_SECTION_2 => tr('STATIC_PAGE_SECTION_2')."(
                Discover)",
                //STATIC_PAGE_SECTION_3 => tr('STATIC_PAGE_SECTION_3')."(Hosting)",
                //STATIC_PAGE_SECTION_4 => tr('STATIC_PAGE_SECTION_4')."(Social)",
            ];

    if($is_list == YES) {
        return $lists;
    }

    return isset($lists[$section_type]) ? $lists[$section_type] : "Common";

}


function last_x_months_data($months) {

    $data = new \stdClass;

    $data->currency = $currency = Setting::get('currency', '$');

    $last_x_days_revenues = [];

    $start  = new \DateTime('-6 month', new \DateTimeZone('UTC'));
    
    $period = new \DatePeriod($start, new \DateInterval('P1M'), $months);
   
    $dates = $last_x_days_revenues = [];

    foreach ($period as $date) {

        $current_month = $date->format('M');

        $formatted_month = $date->format('Y-m');

        $last_x_days_data =  new \stdClass;

        $last_x_days_data->month= $current_month;

        $last_x_days_data->formatted_month = $formatted_month;

        $month = $date->format('m');
      
        $last_x_days_order_earnings = \App\OrderPayment::whereMonth('paid_date', '=', $month)->where('status' , PAID)->sum('total');
        

        $last_x_days_data->order_earnings = $last_x_days_order_earnings ?: 0.00;

        array_push($last_x_days_revenues, $last_x_days_data);

    }
    
    $data->last_x_days_revenues = $last_x_days_revenues;
    
    return $data;  
}

function last_x_days_revenue($days,$order_products_ids) {
            
    $data = new \stdClass;

    $data->currency = $currency = Setting::get('currency', '$');

    // Last 10 days revenues

    $last_x_days_revenues = [];

    $start  = new \DateTime('-7 day', new \DateTimeZone('UTC'));
    
    $period = new \DatePeriod($start, new \DateInterval('P1D'), $days);
   
    $dates = $last_x_days_revenues = [];

    foreach ($period as $date) {

        $current_date = $date->format('Y-m-d');

        $last_x_days_data = new \stdClass;

        $last_x_days_data->date = $current_date;
      
        $last_x_days_total_earnings = \App\OrderPayment::whereIn('order_id',[$order_products_ids])->whereDate('paid_date', '=', $current_date)->sum('total');
      
        $last_x_days_data->total_earnings = $last_x_days_total_earnings ?: 0.00;

        array_push($last_x_days_revenues, $last_x_days_data);

    }
    
    $data->last_x_days_revenues = $last_x_days_revenues;
    
    return $data;   

}

/**
 * @method revenue_graph()
 *
 * @uses to get revenue analytics 
 *
 * @created Akshata
 * 
 * @updated Akshata
 * 
 * @param  integer $days
 * 
 * @return array of revenue totals
 */
function revenue_graph($days) {
            
    $data = new \stdClass;

    $data->currency = $currency = Setting::get('currency', '$');

    // Last 10 days revenues

    $last_x_days_revenues = [];

    $start  = new \DateTime('-7 day', new \DateTimeZone('UTC'));
    
    $period = new \DatePeriod($start, new \DateInterval('P1D'), $days);
   
    $dates = $last_x_days_revenues = [];

    foreach ($period as $date) {

        $current_date = $date->format('Y-m-d');

        $last_x_days_data = new \stdClass;

        $last_x_days_data->date = $current_date;
      
        $last_x_days_order_total_earnings = OrderPayment::where('status',PAID)->whereDate('paid_date', '=', $current_date)->sum('total');
      
        $last_x_days_data->total_order_earnings = $last_x_days_order_total_earnings ?: 0.00;

        array_push($last_x_days_revenues, $last_x_days_data);

    }
    
    $data->last_x_days_revenues = $last_x_days_revenues;
    
    return $data;   

}
/**
 * @method get_wallet_message()
 *
 * @uses used to get the wallet message based on the types
 * 
 * @created vidhya R
 *
 * @updated vidhya R
 * 
 */

function get_wallet_message($user_wallet_payment) {

    $amount_type = $user_wallet_payment->payment_type;

    $status_text = [
        WALLET_PAYMENT_TYPE_ADD => tr('WALLET_PAYMENT_TYPE_ADD_TEXT'),WALLET_PAYMENT_TYPE_PAID => tr('WALLET_PAYMENT_TYPE_PAID_TEXT'), WALLET_PAYMENT_TYPE_CREDIT => tr('WALLET_PAYMENT_TYPE_CREDIT_TEXT'), WALLET_PAYMENT_TYPE_WITHDRAWAL => tr('WALLET_PAYMENT_TYPE_WITHDRAWAL_TEXT')];

    return isset($status_text[$amount_type]) ? $status_text[$amount_type] : tr('WALLET_PAYMENT_TYPE_ADD_TEXT');

}

function wallet_formatted_amount($amount = 0.00, $amount_type = WALLET_AMOUNT_TYPE_ADD) {

    $amount_symbol = $amount_type == WALLET_AMOUNT_TYPE_ADD ? "+" : "-";

    return $amount_symbol." ".formatted_amount($amount);

}

function paid_status_formatted($status) {

    $status_list = [
        USER_WALLET_PAYMENT_INITIALIZE => tr('USER_WALLET_PAYMENT_INITIALIZE'), 
        USER_WALLET_PAYMENT_PAID => tr('USER_WALLET_PAYMENT_PAID'), 
        USER_WALLET_PAYMENT_UNPAID => tr('USER_WALLET_PAYMENT_UNPAID'), 
        USER_WALLET_PAYMENT_CANCELLED => tr('USER_WALLET_PAYMENT_CANCELLED'),
        USER_WALLET_PAYMENT_DISPUTED => tr('USER_WALLET_PAYMENT_DISPUTED'),
        USER_WALLET_PAYMENT_WAITING => tr('USER_WALLET_PAYMENT_WAITING')
    ];

    return isset($status_list[$status]) ? $status_list[$status] : tr('paid');
}

function wallet_picture($amount_type = WALLET_AMOUNT_TYPE_ADD) {

    $wallet_picture = $amount_type == WALLET_AMOUNT_TYPE_ADD ? asset('images/wallet_plus.svg') : asset('images/wallet_minus.svg');

    return $wallet_picture;

}

function total_days($end_date, $start_date = "") {

    $start_date = $start_date ?? date('Y-m-d H:i:s');

    $start_date = strtotime($start_date);

    $end_date = strtotime($end_date);

    $datediff = $start_date - $end_date;

    return round($datediff / (60 * 60 * 24));
}


/**
 * @param $image_path
 * @return bool|mixed
 */
function get_image_mime_type($image_path)
{
    $mimes  = array(
        IMAGETYPE_GIF => "image/gif",
        IMAGETYPE_JPEG => "image/jpg",
        IMAGETYPE_PNG => "image/png",
        IMAGETYPE_SWF => "image/swf",
        IMAGETYPE_PSD => "image/psd",
        IMAGETYPE_BMP => "image/bmp",
        IMAGETYPE_TIFF_II => "image/tiff",
        IMAGETYPE_TIFF_MM => "image/tiff",
        IMAGETYPE_JPC => "image/jpc",
        IMAGETYPE_JP2 => "image/jp2",
        IMAGETYPE_JPX => "image/jpx",
        IMAGETYPE_JB2 => "image/jb2",
        IMAGETYPE_SWC => "image/swc",
        IMAGETYPE_IFF => "image/iff",
        IMAGETYPE_WBMP => "image/wbmp",
        IMAGETYPE_XBM => "image/xbm",
        IMAGETYPE_ICO => "image/ico");

    if (($image_type = exif_imagetype($image_path))
        && (array_key_exists($image_type ,$mimes)))
    {
        return $mimes[$image_type];
    }
    else
    {
        return FALSE;
    }
}


function user_document_status_formatted($status) {

    $status_list = [
                USER_DOCUMENT_NONE => tr('USER_DOCUMENT_NONE'),
                USER_DOCUMENT_PENDING => tr('USER_DOCUMENT_PENDING'),
                USER_DOCUMENT_APPROVED => tr('USER_DOCUMENT_APPROVED'),
                USER_DOCUMENT_DECLINED => tr('USER_DOCUMENT_DECLINED')
                ];

    return isset($status_list[$status]) ? $status_list[$status] : tr('USER_DOCUMENT_NONE');
}

function get_follower_ids($user_id) {

    $follower_ids = \App\Follower::where('follower_id', $user_id)->where('status', YES)->pluck('user_id');

    $follower_ids = $follower_ids ? $follower_ids->toArray() : [];

    return $follower_ids;
}


function push_messages($key , $other_key = "" , $lang_path = "messages.") {


    if (!\Session::has('locale')) {

        $locale = \Session::put('locale', config('app.locale'));

    }else {

        $locale = \Session::get('locale');

    }

  return \Lang::choice('push-messages.'.$key, 0, Array('other_key' => $other_key), $locale);

}


function user_account_type_formatted($type) {

    $list = [USER_FREE_ACCOUNT => tr('USER_FREE_ACCOUNT'), USER_PREMIUM_ACCOUNT => tr('USER_PREMIUM_ACCOUNT')];

    return $list[$type] ?? tr('USER_FREE_ACCOUNT');

}
function is_content_creator_formatted($type) {

    $list = [DEFAULT_USER => tr('DEFAULT_USER'), CONTENT_CREATOR => tr('CONTENT_CREATOR')];

    return $list[$type] ?? tr('DEFAULT_USER');

}

function withdraw_picture($amount_type = WALLET_AMOUNT_TYPE_ADD) {

    $withdraw_picture = asset('images/withdraw_sent.svg');

    return $withdraw_picture;

}


function withdrawal_status_formatted($status) {

    $status_list = [WITHDRAW_INITIATED => tr('WITHDRAW_INITIATED'), WITHDRAW_PAID => tr('WITHDRAW_PAID'), WITHDRAW_ONHOLD => tr('WITHDRAW_ONHOLD'), WITHDRAW_DECLINED => tr('WITHDRAW_DECLINED'), WITHDRAW_CANCELLED => tr('WITHDRAW_CANCELLED')];

    return isset($status_list[$status]) ? $status_list[$status] : tr('WITHDRAW_INITIATED');
}

function document_status_formatted($status) {

    $status_list = [USER_DOCUMENT_NONE => tr('USER_DOCUMENT_NONE'), USER_DOCUMENT_PENDING => tr('USER_DOCUMENT_PENDING'), USER_DOCUMENT_APPROVED => tr('USER_DOCUMENT_APPROVED'), USER_DOCUMENT_DECLINED => tr('USER_DOCUMENT_DECLINED')];

    return isset($status_list[$status]) ? $status_list[$status] : tr('USER_KYC_DOCUMENT_NONE');
}

function document_status_text_formatted($status) {

    $status_list = [
        USER_DOCUMENT_NONE => tr('user_document_none'), 
        USER_DOCUMENT_PENDING => tr('user_document_veification_pending'),
        USER_DOCUMENT_APPROVED => tr('user_document_approved_text'), 
        USER_DOCUMENT_DECLINED => tr('user_document_declined')
    ];

    return isset($status_list[$status]) ? $status_list[$status] : tr('USER_KYC_DOCUMENT_NONE');
}

/**
 * @method last_x_months_stream()
 *
 * @uses used to get no.of.stream for the month
 * 
 * @created Ganesh
 *
 * @updated Subham
 * 
 */


function last_x_months_stream($months,$user_id='') {

    $data = new \stdClass;

    $start  = new \DateTime('-8 month', new \DateTimeZone('UTC'));
    
    $period = new \DatePeriod($start, new \DateInterval('P1M'), $months);
   
    $dates = $last_x_months_stream = [];

    foreach ($period as $date) {

        $formatted_month = $date->format('Y-m');

        $last_x_months_streams_data =  new \stdClass;

        $base_query = \App\LiveVideo::whereYear('created_at',$date->format('Y'))->whereMonth('created_at', '=', $date->format('m'));

        if($user_id){

            $base_query = $base_query->where('user_id',$user_id);
        }

        $last_x_months_streams_data->no_of_streams = $base_query->count();

        $last_x_months_streams_data->month = $formatted_month;

        array_push($last_x_months_stream, $last_x_months_streams_data);

    }
    

    return $last_x_months_stream;   

}


function plan_text($plan, $plan_type = PLAN_TYPE_MONTH) {
    
    $plan_type_text = $plan <= 1 ? tr($plan_type) : tr($plan_type)."s";
    
   return  $plan_text = $plan." ".$plan_type_text;

}

function generate_payment_id() {

    $payment_id = time();

    $payment_id .= rand();

    $payment_id = sha1($payment_id);

    return strtoupper($payment_id);

}

/**
 * @method blocked_users()
 *
 * @uses used to get the blocked users 
 * 
 * @created Ganesh
 *
 * @updated Ganesh
 * 
 */
function blocked_users($user_id){

    $block_user_ids = \App\BlockUser::where('block_by',$user_id)->pluck('blocked_to')->toArray() ?? [];

    return $block_user_ids;
}

/**
 * @method blocked_user_status()
 *
 * @uses used to get the blocked users 
 * 
 * @created Ganesh
 *
 * @updated Ganesh
 * 
 */
function blocked_user_status($loggedin_user_id, $user_id){

    $block_user = \App\BlockUser::where('block_by', $loggedin_user_id)->where('blocked_to', $user_id)->count() ?? 0;

    return $block_user;
}


function admin_commission_spilit($total) {

    $admin_commission = Setting::get('admin_commission', 1)/100;

    $admin_amount = $total * $admin_commission;

    $user_amount = $total - $admin_amount;

    return  (object) ['admin_amount' => $admin_amount, 'user_amount' => $user_amount];

}

function emptyObject() {
    return (Object)[];
}


function get_file_type($file) {

    $imagemimes = ['image/png', 'image/jpeg', 'image/jpg']; //Add more mimes that you want to support
    
    $videomimes = ['video/mp4', 'video/mov', 'video/webm', 'video/flv', 'video/avi', 'video/mkv']; //Add more mimes that you want to support
    
    $audiomimes = ['audio/mpeg', 'audio/mp3']; //Add more mimes that you want to support

    if(in_array($file->getMimeType() ,$imagemimes)) {
        return FILE_TYPE_IMAGE;
    }
    //Validate video
    if (in_array($file->getMimeType() ,$videomimes)) {
        return FILE_TYPE_VIDEO;
    }
    //validate audio
    if (in_array($file->getMimeType() ,$audiomimes)) {
        return FILE_TYPE_AUDIO;
    }

    return FILE_TYPE_IMAGE;   
}

function formatUrl($url) {

    $parsed = parse_url($url);

    if(empty($parsed['scheme'])) {

        if(false === strpos($url, '://')) {

            $url = 'https://' . ltrim($url, '/');

        } else {

            $url = 'https' . ltrim($url, '/');
        }
    }

    return $url;

    Log::info("url".$url);
}

function get_video_end($video_url) {
    $url = explode('/',$video_url);
    $result = end($url);
    return $result;
}

function getUserTime($time, $timezone = "Asia/Kolkata", $format = "H:i:s") {

    if ($timezone) {

        $new_str = new DateTime($time, new DateTimeZone('UTC') );

        $new_str->setTimeZone(new DateTimeZone( $timezone ));

        return $new_str->format($format);

    }
}
function get_extension_from_path($file) {

    $path_explode = explode('/',$file);

    $file_parameters = explode('.', end($path_explode));

    return end($file_parameters);

}


function getMinutesBetweenTime($startTime, $endTime) {

    $to_time = strtotime($endTime);

    $from_time = strtotime($startTime);

    $diff = abs($to_time - $from_time);

    if ($diff <= 0) {

        $diff = 0;

    } else {

        $diff = round($diff/60);

    }

    return $diff;

}

function formatted_live_payment_text($type = FREE_VIDEO) {

    return $type == FREE_VIDEO ? tr('free_video') : tr('paid_video');
}

function live_video_status($status = VIDEO_STREAMING_ONGOING) {

    $status_lists = [VIDEO_STREAMING_ONGOING => tr('streaming_live'), 
                    VIDEO_STREAMING_STOPPED => tr('streaming_stopped'), 
                    VIDEO_STREAMING_CREATED => tr('streaming_scheduled')];

    return count($status_lists) >= $status ? $status_lists[$status] : tr('streaming_live');
}



function common_server_date($date , $timezone = "" , $format = "d M Y h:i A") {

    if($date == "0000-00-00 00:00:00" || $date == "0000-00-00" || !$date) {

        return $date = '';
    }

    if($timezone) {

        $date = convertTimeToUTCzone($date, $timezone, $format);

    }

    return $timezone ? $date : date($format, strtotime($date));

}


function height_formatted($height) {

    $height = $height ?? 0;

    return $height.' CM';

}

function weight_formatted($weight) {

    $weight = $weight ?? 0;

    return $weight.' '.tr('pounds');

}


function following_users($user_id){

    $following_user_ids = \App\Follower::where('follower_id',$user_id)->where('status', YES)->pluck('user_id')->toArray() ?? [];

    return $following_user_ids;
}

 function placeholder_path_formate($key) {

    $formate = 'jpg';

    $filepath = 'jpg';

    $filepath = PUBLIC_COMMON_FILE_PATH;

    if ($key == 'profile_placeholder') {
        
        $formate = 'jpeg';
    }

    if ($key == 'profile_placeholder' || $key == 'cover_placeholder') {
        
        $filepath = PUBLIC_HOME;
    }
    
    $status_list = [
        'profile_placeholder' => 'placeholder',
        'cover_placeholder' => 'cover',
        'live_streaming_placeholder_img' => 'live-streaming',
    ];

    $data['file_name'] = isset($status_list[$key]) ? $status_list[$key] : tr('placeholder');

    $data['formate'] = $formate;

    $data['filepath'] = $filepath;

    return $data;   
}

function last_x_months_conent_creator_data($months, $user_id) {

    $data = new \stdClass;

    $data->currency = $currency = Setting::get('currency', '$');

    $last_x_days_revenues = [];

    $start  = new \DateTime('-6 month', new \DateTimeZone('UTC'));
    
    $period = new \DatePeriod($start, new \DateInterval('P1M'), $months);
   
    $dates = $last_x_days_revenues = $last_x_days_month = $last_x_days_earning = [];

    $order_ids = \App\Order::where('user_id', $user_id)->pluck('id');

    foreach ($period as $date) {

        $current_month = $date->format('M');

        $formatted_month = $date->format('Y-m');

        $last_x_days_data =  new \stdClass;

        $last_x_days_data->month= $current_month;

        $last_x_days_data->formatted_month = $formatted_month;

        $month = $date->format('m');
      
        $last_x_days_order_earnings = \App\OrderPayment::whereIn('order_id', $order_ids)->whereMonth('paid_date', '=', $month)->where('status' , PAID)->sum('total');
        
        $last_x_days_data->order_earnings = $last_x_days_order_earnings ?: 0.00;

        $total_earning =  0.00;

        array_push($last_x_days_revenues, $last_x_days_data);

        array_push($last_x_days_month, $formatted_month);

        array_push($last_x_days_earning, $total_earning);

    }
    
    $data->last_x_days_revenues = $last_x_days_revenues;

    $data->last_x_days_month = $last_x_days_month;

    $data->last_x_days_earning = $last_x_days_earning;
    
    return $data;  
}

function order_btn_status($order_status) {
 
    $buttons = new \stdClass;

    $buttons->cancel_btn_status = $buttons->review_btn_status = $buttons->shipped_btn_status = $buttons->delivered_btn_status = NO;

    if(in_array($order_status, [ORDER_PLACED])) {

        $buttons->cancel_btn_status = YES;
        
    }

    if(in_array($order_status, [ORDER_DELIVERD])) {      
    
        $buttons->review_btn_status = YES;      
    
    }   

    if(in_array($order_status, [ORDER_PLACED])) {

        $buttons->shipped_btn_status = YES;
    }

    if(in_array($order_status, [ORDER_SHIPPED])) {

        $buttons->delivered_btn_status = YES;

    }
    
    return $buttons;

}

function formatted_schedule_time($date, $userTimezone) {

    $user_time_zone = convertTimeToUSERzone($date, $userTimezone);

    $formatted_date = date('Y-m-d', strtotime($user_time_zone));

    switch($formatted_date) {

        case (Carbon::now()->format('Y-m-d')) :
            $formatted_time = "Today - ".date("g:i A" , strtotime($user_time_zone));
            break;
        case (Carbon::tomorrow()->format('Y-m-d')) :
            $formatted_time = "Tomorrow - ".date("g:i A" , strtotime($user_time_zone));
            break;
        case ($formatted_date > Carbon::now()->addDays(6)->format('Y-m-d')) :
            $formatted_time = date("M jS - g:i A" , strtotime($user_time_zone));
            break;
        default:
            $formatted_time = date("l - g:i A" , strtotime($user_time_zone));

    }
   
    return $formatted_time;
}


function order_status_formatted($status) {

    $status_list = [
        ORDER_PLACED => tr('order_apporve_cancel'), 
        ORDER_SHIPPED => tr('order_apporved'), 
        ORDER_DELIVERD => tr('order_apporved'),
        ORDER_CACELLED => tr('order_cancelled')
    ];

    return isset($status_list[$status]) ? $status_list[$status] : tr('paid');
}

function order_status($status) {

    $status_list = [
        ORDER_PLACED => tr('placed'), 
        ORDER_SHIPPED => tr('shipped'), 
        ORDER_DELIVERD => tr('delivered'),
        ORDER_CACELLED => tr('cancelled')
    ];

    return isset($status_list[$status]) ? $status_list[$status] : tr('paid');
}

function compress_image($preview_file){
    
    $output_file_path = 'public/'.COMMON_FILE_PATH.basename($preview_file);
             
    $uploadTo = $output_file_path; 
    $allowImageExt = array('jpg','png','jpeg','gif');

    $tempPath= $_FILES["preview_file"]["tmp_name"];
    
    $imageQuality= 20;
    $basename = basename($preview_file);

    $originalPath = $uploadTo; 
    $imageExt = pathinfo($originalPath, PATHINFO_EXTENSION);

    if(in_array($imageExt, $allowImageExt)){ 
       
        // Get image info 
        $imgInfo = getimagesize($tempPath); 
        $mime = $imgInfo['mime']; 
        
        // Create a new image from file 
        switch($mime){ 
            case 'image/jpeg': 
                $image = imagecreatefromjpeg($tempPath); 
                break; 
            case 'image/png': 
                $image = imagecreatefrompng($tempPath); 
                break; 
            case 'image/gif': 
                $image = imagecreatefromgif($tempPath); 
                break; 
            default: 
                $image = imagecreatefromjpeg($tempPath); 
        } 
        // Save image 
        imagejpeg($image, \Storage::path($originalPath), $imageQuality); 
    }

    // Return compressed image 
    return $originalPath; 
}