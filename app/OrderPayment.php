<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
    protected $appends = ['delivery_price_formatted','sub_total_formatted','tax_price_formatted','total_formatted','user_amount_formatted','admin_amount_formatted'];


    public function getTotalFormattedAttribute() {

    	return formatted_amount($this->total);
    }

    public function getTaxPriceFormattedAttribute() {

    	return formatted_amount($this->tax_price);
    }

    public function getSubTotalFormattedAttribute() {

    	return formatted_amount($this->sub_total);
    }

    public function getDeliveryPriceFormattedAttribute() {

    	return formatted_amount($this->delivery_price);
    }

    public function getAdminAmountFormattedAttribute() {

        return formatted_amount($this->admin_amount);
    }

    public function getUserAmountFormattedAttribute() {

        return formatted_amount($this->user_amount);
    }


    public function user() {

        return $this->belongsTo(User::class,'user_id');
    }
    public function order() {

        return $this->belongsTo(Order::class,'order_id');
    }
}
