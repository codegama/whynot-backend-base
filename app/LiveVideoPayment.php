<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveVideoPayment extends Model
{
    protected $appends = ['live_video_payment_id', 'admin_amount_formatted', 'user_amount_formatted', 'live_video_amount_formatted'];

    protected $hidden = ['id'];

    public function getLiveVideoPaymentIdAttribute() {

        return $this->id;
    }

    public function getAdminAmountFormattedAttribute() {

        return formatted_amount($this->admin_amount);
    }

    public function getUserAmountFormattedAttribute() {

        return formatted_amount($this->user_amount);
    }

    public function getLiveVideoAmountFormattedAttribute() {

        return formatted_amount($this->live_video_amount);
    }

    public function user() {

        return $this->belongsTo(User::class,'user_id');
    }

    public function videoDetails() {

        return $this->belongsTo(LiveVideo::class, 'live_video_id');
    }
}
