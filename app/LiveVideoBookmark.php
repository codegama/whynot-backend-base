<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveVideoBookmark extends Model
{
    protected $fillable = ['live_video_id', 'user_id'];

    protected $hidden = ['id', 'unique_id'];

	protected $appends = ['live_video_bookmark_id', 'live_video_bookmark_unique_id', 'username', 'user_picture'];
	
	public function getLiveVideoBookmarkIdAttribute() {

		return $this->id;
	}

	public function getLiveVideoBookmarkUniqueIdAttribute() {

		return $this->unique_id;
	}

	public function getUsernameAttribute() {

		$username = $this->user->name ?? "";

		unset($this->user);

		return $username;
	}

	public function getUserPictureAttribute() {

		$user_picture = $this->user->picture ?? "";

		unset($this->user);

		return $user_picture;
	}

	public function user() {

	   return $this->belongsTo(User::class, 'user_id');
	}

	public function liveVideo() {

	   return $this->belongsTo(LiveVideo::class, 'live_video_id');
	}

	/**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query) {

        $query->where('live_video_bookmarks.status', APPROVED);

        return $query;

    }

	public static function boot() {

        parent::boot();

        static::creating(function ($model) {
            $model->attributes['unique_id'] = "LVBM-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "LVBM-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });
    }
}
