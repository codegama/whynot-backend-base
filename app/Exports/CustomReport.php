<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;

use Maatwebsite\Excel\Concerns\FromView;

use Illuminate\Http\Request;

use App\{User,Follower};

use Carbon\Carbon;
  
class CustomReport implements FromView 
{



    public function __construct(Request $request)
    {
        
        $this->user_id = $request->user_id;
        $this->start_date = $request->from_date;
        $this->end_date = $request->to_date;
       
    }


    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View {

        $user_id = $this->user_id;

        $now = Carbon::now();

        $start_date = $this->start_date;

        $end_date = $this->end_date;

        $data=[];

        $data['followers'] = Follower::where('user_id',$user_id)->whereBetween('created_at', [$start_date, $end_date])->count();

        $data['followings'] = Follower::where('follower_id',$user_id)->whereBetween('created_at', [$start_date, $end_date])->count();

        return view('exports.report', [
            'data' => $data
        ]);


    }

    public function send_week_report(Request $request) {

        try {

            $now = Carbon::now();

            $start_date = $now->startOfWeek()->format('Y-m-d H:i:s');

            $end_date = $now->endOfWeek()->format('Y-m-d H:i:s');

            $response = CommonRepo::send_report($start_date,$end_date,$request->user_id,WEELKY_REPORT);

            return redirect()->back()->with('flash_success',tr('report_mail_sent_success'));

        } catch(Exception $e) {

            return redirect()->route('admin.users.index')->with('flash_error', $e->getMessage());

        }

    }

}