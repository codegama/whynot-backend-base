<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{

    protected $hidden = ['id', 'unique_id'];

    protected $appends = ['product_review_id', 'product_review_unique_id', 'from_username','from_user_picture','to_user_picture','to_username','product_name'];
    
    public function getProductReviewIdAttribute() {

        return $this->id;
    }

    public function getProductReviewUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function getFromUsernameAttribute() {

        return $this->fromUser->name ?? 'N/A';
    }

    public function getFromUserPictureAttribute() {

        return $this->fromUser->picture ?? asset('placeholder.jpeg');
    }

    public function getToUsernameAttribute() {

        return $this->toUser->name ?? 'N/A';
    }

    public function getToUserPictureAttribute() {

        return $this->toUser->picture ?? asset('placeholder.jpeg');
    }

    public function getProductNameAttribute() {

        return $this->userProductDetails->name ?? 'N/A';
    }


    public function userProductDetails() {

        return $this->belongsTo(UserProduct::class,'user_product_id');
    }

    
    public function fromUser() {

       return $this->belongsTo(User::class, 'user_id');
    }

    public function toUser() {

       return $this->belongsTo(User::class, 'seller_id');
    }

    public function orders() {

       return $this->belongsTo(OrderProduct::class, 'order_id');
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {
            $model->attributes['unique_id'] = "PR-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "PR-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });
    }
}
