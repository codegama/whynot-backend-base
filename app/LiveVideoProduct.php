<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveVideoProduct extends Model
{
    protected $hidden = ['id', 'unique_id'];

    protected $appends = ['live_video_product_id', 'live_video_product_unique_id'];
    
    public function getLiveVideoProductIdAttribute() {

        return $this->id;
    }

    public function getLiveVideoProductUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function user() {

        return $this->belongsTo(User::class,'user_id');
    }

    public function userProduct() {

        return $this->belongsTo(UserProduct::class,'user_product_id');
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {
            $model->attributes['unique_id'] = "LVP-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "LVP-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });
    }
}
