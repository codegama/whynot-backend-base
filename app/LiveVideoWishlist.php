<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveVideoWishlist extends Model
{
    protected $fillable = ['user_id', 'live_video_id'];

    public function liveVideo() {

        return $this->belongsTo(LiveVideo::class,'live_video_id');
    }

    public function user() {

        return $this->belongsTo(User::class,'user_id');
    }

	/**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query) {

        $query->where('live_video_wishlists.status', APPROVED);

        return $query;

    }

	public static function boot() {

        parent::boot();

        static::creating(function ($model) {
            $model->attributes['unique_id'] = "LVW-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "LVW-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

    }
}
