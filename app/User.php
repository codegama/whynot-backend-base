<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Setting, DB, Cache;

use App\Helpers\Helper;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'content_creator_step', 'is_content_creator', 'is_document_verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['user_id', 'user_unique_id', 'is_notification', 'is_document_verified_formatted', 'total_followers', 'total_followings', 'user_account_type_formatted', 'share_link','orders_count', 'is_user_online', 'is_welcome_steps', 'verified_badge_file','about_formatted','eyes_color_formatted','height_formatted','weight_formatted','total_user_products','total_blocked_users'];

    public function getIsWelcomeStepsAttribute() {

        return $this->id;
    }

    public function getUserIdAttribute() {

        return $this->id;
    }

    public function getUserUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function getIsNotificationAttribute() {

        return $this->is_email_notification ? YES : NO;
    }

    public function getIsUserOnlineAttribute() {

        return Cache::has($this->id) ? YES : NO;
    }


    public function getIsDocumentVerifiedFormattedAttribute() {

        return user_document_status_formatted($this->is_document_verified);
    }

    public function getTotalFollowersAttribute() {

        $count = $this->followers->where('status',FOLLOWER_ACTIVE)->count();

        unset($this->followers);
        
        return $count;

    }

    public function getShareLinkAttribute() {

        $share_link = \Setting::get('frontend_url').$this->unique_id;
        
        return $share_link;

    }

    public function getTotalFollowingsAttribute() {

        $count = $this->followings->where('status', YES)->count();

        unset($this->followings);
        
        return $count;

    }

    public function getVerifiedBadgeFileAttribute() {

        $verified_badge_file = $this->is_verified_badge ? \Setting::get('verified_badge_file') : '';

        // unset($this->user);

        return $verified_badge_file ?? "";
    }

    public function getOrdersCountAttribute() {

        $count = $this->orders->count();

        unset($this->orders);
        
        return $count;

    }

    public function getUserAccountTypeFormattedAttribute() {
        
        return user_account_type_formatted($this->user_account_type);

    }

    public function getAboutFormattedAttribute() {
        
        return $this->about == "null" ? "" : $this->about;

    }

    public function getEyesColorFormattedAttribute() {
        
        return $this->eyes_color ?? "";

    }

    public function getHeightFormattedAttribute() {
        
        return height_formatted($this->height);

    }

    public function getWeightFormattedAttribute() {
        
        return weight_formatted($this->weight);

    }

    public function userBillingAccounts() {

        return $this->hasMany(UserBillingAccount::class, 'user_id');
    }

    public function userDocuments() {

        return $this->hasMany(UserDocument::class, 'user_id');
    }

    public function deliveryAddresses() {

        return $this->hasMany(DeliveryAddress::class,'user_id');
    }

    public function orderPayments() {

        return $this->hasMany(OrderPayment::class,'user_id');
    }
    public function orders() {

        return $this->hasMany(Order::class,'user_id');
    }

    public function userWallets() {

        return $this->hasOne(UserWallet::class, 'user_id');
    }

    public function userWithdrawals() {

        return $this->hasMany(UserWithdrawal::class,'user_id');
    }

    public function referralCode() {

        return $this->hasOne(ReferralCode::class, 'user_id');
    }

    /**
     * Get the UserCard record associated with the user.
     */
    public function userCards() {
        
        return $this->hasMany(UserCard::class, 'user_id');
    }

    public function followers() {
        
        return $this->hasMany(Follower::class, 'user_id')->whereHas('follower');
    }

    public function followings() {
        
        return $this->hasMany(Follower::class, 'follower_id')->whereHas('user');
    }


     public function userReferralCode()
    {
        return $this->hasOne(ReferralCode::class, 'user_id');
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query) {

        $query->where('users.status', USER_APPROVED)->where('is_email_verified', USER_EMAIL_VERIFIED);

        return $query;

    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDocumentVerified($query) {

        $query->where('users.is_document_verified', USER_DOCUMENT_APPROVED);

        return $query;

    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFeaturedSeller($query) {

        $query->where('users.is_featured_seller', YES);

        return $query;

    }


    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommonResponse($query) {

        return $query->select(
            'users.id as user_id',
            'users.unique_id as user_unique_id',
            'users.*'
            );
    
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOtherResponse($query) {

        return $query->select(
            'users.id as user_id',
            'users.unique_id as user_unique_id',
            'users.*'
            );
    }
    
    public function liveVideos() {

        return $this->hasMany(LiveVideo::class, 'user_id');
    }

    public function userProducts() {

        return $this->hasMany(UserProduct::class, 'user_id');
    }

    public function getTotalUserProductsAttribute() {
        
        $count = $this->userProducts->count();

        unset($this->userProducts);
        
        return $count;

    }

    public function blockedUsers() {

        return $this->hasMany(BlockUser::class, 'block_by');
    }

    public function getTotalBlockedUsersAttribute() {
        
        $count = $this->blockedUsers->count();

        unset($this->blockedUsers);
        
        return $count;

    }

    
    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            // $model->attributes['name'] = "";

            if($model->attributes['first_name'] && $model->attributes['last_name']) {

                $model->attributes['name'] = $model->attributes['first_name']." ".$model->attributes['last_name'];
            }

            $model->attributes['unique_id'] = routefreestring(strtolower($model->attributes['name'] ?: rand(1,10000).rand(1,10000)));

            if($model->attributes['username'] == '') {

                $model->attributes['username'] = $model->attributes['unique_id'];

            }

            if($model->attributes['username']) {

                $model->attributes['unique_id'] = $model->attributes['username'];
            }

            $model->attributes['is_email_verified'] = USER_EMAIL_VERIFIED;

            if (Setting::get('is_account_email_verification') == YES && envfile('MAIL_USERNAME') && envfile('MAIL_PASSWORD')) { 

                if($model->attributes['login_by'] == 'manual') {

                    $model->generateEmailCode();

                }

            }

            $model->attributes['payment_mode'] = COD;

            $model->attributes['token'] = Helper::generate_token();

            $model->attributes['token_expiry'] = Helper::generate_token_expiry();

            $model->attributes['status'] = USER_APPROVED;

            if(in_array($model->attributes['login_by'], ['facebook', 'google', 'apple', 'linkedin', 'instagram'] )) {
                
                $model->attributes['password'] = \Hash::make($model->attributes['social_unique_id']);
            }

        });

        static::created(function($model) {

            $model->attributes['user_account_type'] = USER_FREE_ACCOUNT;
            
            $model->attributes['user_account_type'] = USER_FREE_ACCOUNT;

            $model->attributes['is_email_notification'] = $model->attributes['is_push_notification'] = YES;

            $model->attributes['unique_id'] = routefreestring(strtolower($model->attributes['name'] ?: rand(1,10000).rand(1,10000)));

            if($model->attributes['username'] == '') {

                $model->attributes['username'] = $model->attributes['unique_id'];

            }
            
            $model->save();
        
        });

        static::updating(function($model) {

            if($model->attributes['first_name'] && $model->attributes['last_name']) {

                $model->attributes['name'] = $model->attributes['first_name']." ".$model->attributes['last_name'];
            }

            // $model->attributes['first_name'] = $model->attributes['last_name'] = $model->attributes['name'];

            $model->attributes['website'] = isset($model->attributes['website']) && $model->attributes['website'] ? formatUrl($model->attributes['website']) : "";

            $model->attributes['amazon_wishlist'] =  isset($model->attributes['amazon_wishlist']) &&  $model->attributes['amazon_wishlist'] ? formatUrl($model->attributes['amazon_wishlist']) : "";

            $model->attributes['facebook_link'] =  isset($model->attributes['facebook_link']) &&  $model->attributes['facebook_link'] ? formatUrl($model->attributes['facebook_link']) : "";

            $model->attributes['instagram_link'] =  isset($model->attributes['instagram_link']) &&  $model->attributes['instagram_link'] ? formatUrl($model->attributes['instagram_link']) : "";

            $model->attributes['twitter_link'] =  isset($model->attributes['twitter_link']) &&  $model->attributes['twitter_link'] ? formatUrl($model->attributes['twitter_link']) : "";

            $model->attributes['linkedin_link'] =  isset($model->attributes['linkedin_link']) &&  $model->attributes['linkedin_link'] ? formatUrl($model->attributes['linkedin_link']) : "";

            $model->attributes['pinterest_link'] =  isset($model->attributes['pinterest_link']) &&  $model->attributes['pinterest_link'] ? formatUrl($model->attributes['pinterest_link']) : "";

            $model->attributes['youtube_link'] =  isset($model->attributes['youtube_link']) &&  $model->attributes['youtube_link'] ? formatUrl($model->attributes['youtube_link']) : "";

            $model->attributes['twitch_link'] =  isset($model->attributes['twitch_link']) &&  $model->attributes['twitch_link'] ? formatUrl($model->attributes['twitch_link']) : "";
            
            $model->attributes['snapchat_link'] =  isset($model->attributes['snapchat_link']) &&  $model->attributes['snapchat_link'] ? formatUrl($model->attributes['snapchat_link']) : "";

        });

        static::deleting(function ($model){

            Helper::storage_delete_file($model->picture, PROFILE_PATH_USER);

            $model->userCards()->delete();

            $model->userDocuments()->delete();
            
            $model->userBillingAccounts()->delete();

            foreach ($model->orders as $key => $order) {
                $order->delete();
            }

            $model->deliveryAddresses()->delete();

            $model->userWallets()->delete();
            
            $model->referralCode()->delete();
            
            $model->userWithdrawals()->delete();

            $model->followers()->delete();

            $model->followings()->delete();
                                    
            $model->liveVideos()->delete();

            \App\ChatUser::where('from_user_id', $model->id)->orWhere('to_user_id', $model->id)->delete();

            \App\ChatUser::where('to_user_id', $model->id)->delete();

            \App\ChatMessage::where('from_user_id', $model->id)->orWhere('to_user_id', $model->id)->delete();
            
            \App\ChatMessage::where('to_user_id', $model->id)->delete();

        });

    }

    /**
     * Generates Token and Token Expiry
     * 
     * @return bool returns true if successful. false on failure.
     */

    protected function generateEmailCode() {

        $this->attributes['verification_code'] = Helper::generate_email_code();

        $this->attributes['verification_code_expiry'] = Helper::generate_email_expiry();

        // Check Email verification controls and email configurations

        if(Setting::get('is_account_email_verification') == YES && Setting::get('is_email_notification') == YES && Setting::get('is_email_configured') == YES) {

            if($this->attributes['login_by'] != 'manual') {

                $this->attributes['is_email_verified'] = USER_EMAIL_VERIFIED;

            } else {

                $this->attributes['is_email_verified'] = USER_EMAIL_NOT_VERIFIED;
            }

        } else { 

            $this->attributes['is_email_verified'] = USER_EMAIL_VERIFIED;
        }

        return true;
    
    }


     /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAuthCommonResponse($query) {

        return $query->select(
            'users.id',
            'users.unique_id',
            'users.username',
            'users.email',
            'users.mobile',
            'users.name',
            'users.picture',
            'users.is_email_verified',
            'users.is_content_creator',
            'users.is_push_notification',
            'users.is_document_verified',
            'users.is_email_notification',
            'users.is_two_step_auth_enabled',
            );
    }

}
